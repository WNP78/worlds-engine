#include "Input.hpp"
#include <Input/Input.hpp>

namespace worlds
{
	InputChannel::InputChannel(InputManager* inputManager)
		: inputManager(inputManager)
	{ }

	bool InputChannel::mouseButtonHeld(MouseButton button, bool ignoreImGui) const
	{
		if (!active) return false;

		return inputManager->mouseButtonHeld(button, ignoreImGui);
	}

	bool InputChannel::mouseButtonPressed(MouseButton button, bool ignoreImGui) const
	{
		if (!active) return false;

		return inputManager->mouseButtonPressed(button, ignoreImGui);
	}

	bool InputChannel::mouseButtonReleased(MouseButton button, bool ignoreImGui) const
	{
		if (!active) return false;

		return inputManager->mouseButtonReleased(button, ignoreImGui);
	}

	bool InputChannel::keyHeld(SDL_Scancode scancode, bool ignoreImGui) const
	{
		if (!active) return false;

		return inputManager->keyHeld(scancode, ignoreImGui);
	}

	bool InputChannel::keyPressed(SDL_Scancode scancode, bool ignoreImGui) const
	{
		if (!active) return false;

		return inputManager->keyPressed(scancode, ignoreImGui);
	}

	bool InputChannel::keyReleased(SDL_Scancode scancode, bool ignoreImGui) const
	{
		if (!active) return false;

		return inputManager->keyReleased(scancode, ignoreImGui);
	}

	glm::ivec2 InputChannel::getMouseDelta() const
	{
		if (!active) return glm::ivec2{ 0, 0 };

		return inputManager->getMouseDelta();
	}

	bool InputChannel::ctrlHeld() const
	{
		if (!active) return false;

		return inputManager->ctrlHeld();
	}

	bool InputChannel::shiftHeld() const
	{
		if (!active) return false;

		return inputManager->shiftHeld();
	}

	void InputChannel::addKeydownHandler(std::function<void(SDL_Scancode)> handler)
	{
		keydownHandlers.add(handler);
	}
}
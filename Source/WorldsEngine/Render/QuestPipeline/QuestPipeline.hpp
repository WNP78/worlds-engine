#pragma once
#include <Render/IRenderPipeline.hpp>

namespace R2::VK
{
    class DescriptorSetLayout;
    class DescriptorSet;
    class Pipeline;
    class PipelineLayout;
    class Buffer;
    class Texture;
    class Core;
    class FrameSeparatedBuffer;
}

namespace worlds
{
    class VKRenderer;
    class QuestPipeline : public IRenderPipeline
    {
        R2::VK::DescriptorSetLayout* descriptorSetLayout;
        R2::VK::DescriptorSet* descriptorSet[2];
        R2::VK::Pipeline* pipeline;
        R2::VK::PipelineLayout* pipelineLayout;
        R2::VK::Buffer* multiVPBuffer;
        R2::VK::FrameSeparatedBuffer* modelMatrixBuffer;
        R2::VK::Texture* depthBuffer;

        VKRenderer* renderer;
        VKRTTPass* rttPass;
        glm::mat4 overrideViews[2];
        glm::mat4 overrideProjs[2];
    public:
        QuestPipeline(VKRenderer* renderer);
        ~QuestPipeline();
        
        void setup(VKRTTPass* rttPass) override;
        void onResize(int width, int height) override;
        void draw(entt::registry& reg, R2::VK::CommandBuffer& cb) override;
        void setView(int viewIndex, glm::mat4 viewMatrix, glm::mat4 projectionMatrix) override;
        bool supportsExternalTarget() override;
    };
}

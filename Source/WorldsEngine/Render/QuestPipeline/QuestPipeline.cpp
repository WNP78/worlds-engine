#include <Render/QuestPipeline/QuestPipeline.hpp>
#include <Core/AssetDB.hpp>
#include <R2/VK.hpp>
#include <R2/BindlessTextureManager.hpp>
#include <Render/RenderInternal.hpp>
#include <Render/ShaderCache.hpp>
#include <Render/ShaderReflector.hpp>
#include <entt/entity/registry.hpp>
#include <Render/RenderMaterialManager.hpp>
#include <Render/Frustum.hpp>
#include <Render/CullMesh.hpp>

using namespace R2;

namespace worlds
{
    struct StandardPushConstants
    {
        uint32_t modelMatrixID;
        uint32_t materialOffset;
        uint32_t cubemapId;
        uint32_t flags;
        glm::vec3 cubemapProjectPos;
        uint32_t pad;
        glm::vec3 cubemapProjectExtent;
    };

    inline uint32_t getViewMask(int viewCount)
    {
        if (viewCount == 1)
            return 0;

        uint32_t mask = 0;
        for (int i = 0; i < viewCount; i++)
        {
            mask |= 1 << i;
        }

        return mask;
    }

    struct QuestVPs
    {
        glm::mat4 vp[2];
        glm::vec4 viewPos[2];
    };

    QuestPipeline::QuestPipeline(VKRenderer* renderer) : renderer(renderer)
    {
        VK::Core* core = renderer->getCore();

        VK::BufferCreateInfo vpBci{VK::BufferUsage::Uniform, sizeof(QuestVPs), true};
        multiVPBuffer = core->CreateBuffer(vpBci);

        VK::BufferCreateInfo modelMatrixBci{VK::BufferUsage::Storage, sizeof(glm::mat4) * 4096, true};
        modelMatrixBuffer = new VK::FrameSeparatedBuffer(core, modelMatrixBci);

        VK::DescriptorSetLayoutBuilder dslb{core};
        dslb.Binding(0, VK::DescriptorType::UniformBuffer, 1, VK::ShaderStage::Vertex | VK::ShaderStage::Fragment);
        dslb.Binding(1, VK::DescriptorType::StorageBuffer, 1, VK::ShaderStage::Vertex);
        dslb.Binding(2, VK::DescriptorType::StorageBuffer, 1, VK::ShaderStage::Fragment);

        descriptorSetLayout = dslb.Build();
        for (int i = 0; i < 2; i++)
        {
            descriptorSet[i] = core->CreateDescriptorSet(descriptorSetLayout);

            VK::DescriptorSetUpdater dsu{core, descriptorSet[i]};
            dsu.AddBuffer(0, 0, VK::DescriptorType::UniformBuffer, multiVPBuffer);
            dsu.AddBuffer(1, 0, VK::DescriptorType::StorageBuffer, modelMatrixBuffer->GetBuffer(i));
            dsu.AddBuffer(2, 0, VK::DescriptorType::StorageBuffer,
                          RenderMaterialManager::GetBuffer());
            dsu.Update();
        }

        VK::PipelineLayoutBuilder plb{core->GetHandles()};
        plb.PushConstants(VK::ShaderStage::Vertex | VK::ShaderStage::Fragment, 0, sizeof(StandardPushConstants))
                .DescriptorSet(descriptorSetLayout)
                .DescriptorSet(&renderer->getBindlessTextureManager()->GetTextureDescriptorSetLayout());
        pipelineLayout = plb.Build();
    }

    QuestPipeline::~QuestPipeline()
    {
        VK::Core* core = renderer->getCore();
        core->DestroyBuffer(multiVPBuffer);
        core->DestroyTexture(depthBuffer);
        delete descriptorSetLayout;
        for (auto& ds : descriptorSet)
        {
            delete ds;
        }
        delete modelMatrixBuffer;
        delete pipeline;
        delete pipelineLayout;
    }

    void QuestPipeline::setup(VKRTTPass* rttPass)
    {
        VK::Core* core = renderer->getCore();
        this->rttPass = rttPass;
        VK::TextureCreateInfo depthBufferCI =
                VK::TextureCreateInfo::RenderTarget2D(VK::TextureFormat::D32_SFLOAT, rttPass->width, rttPass->height);
        //depthBufferCI.IsTransient = true;
        depthBufferCI.Layers = rttPass->getSettings().numViews;
        if (depthBufferCI.Layers > 1)
            depthBufferCI.Dimension = VK::TextureDimension::Array2D;
        depthBuffer = core->CreateTexture(depthBufferCI);

        VK::VertexBinding vb;
        vb.Size = sizeof(Vertex);
        vb.Binding = 0;

        vb.Attributes.emplace_back(0, VK::TextureFormat::R32G32B32_SFLOAT, (uint32_t)offsetof(Vertex, position));
        vb.Attributes.emplace_back(1, VK::TextureFormat::R32G32B32_SFLOAT, (uint32_t)offsetof(Vertex, normal));
        vb.Attributes.emplace_back(2, VK::TextureFormat::R32G32B32A32_SFLOAT, (uint32_t)offsetof(Vertex, tangent));
        vb.Attributes.emplace_back(3, VK::TextureFormat::R32G32_SFLOAT, (uint32_t)offsetof(Vertex, uv));

        AssetID vs = AssetDB::pathToId("Shaders/quest_standard.vert.spv");
        AssetID fs = AssetDB::pathToId("Shaders/quest_standard.frag.spv");
        VK::ShaderModule& stdVert = ShaderCache::getModule(vs);
        VK::ShaderModule& stdFrag = ShaderCache::getModule(fs);

        VK::PipelineBuilder pb{core};
        pb.PrimitiveTopology(VK::Topology::TriangleList)
                .CullMode(VK::CullMode::Back)
                .Layout(pipelineLayout)
                .ColorAttachmentFormat(VK::TextureFormat::R8G8B8A8_SRGB)
                .AddVertexBinding(vb)
                .AddShader(VK::ShaderStage::Vertex, stdVert)
                .AddShader(VK::ShaderStage::Fragment, stdFrag)
                .DepthTest(true)
                .DepthWrite(true)
                .DepthCompareOp(VK::CompareOp::Greater)
                .DepthAttachmentFormat(VK::TextureFormat::D32_SFLOAT);
        pb.ViewMask(getViewMask(rttPass->getSettings().numViews));

        pipeline = pb.Build();
    }

    void QuestPipeline::onResize(int width, int height)
    {
        renderer->getCore()->DestroyTexture(depthBuffer);
        VK::TextureCreateInfo depthBufferCI =
                VK::TextureCreateInfo::RenderTarget2D(VK::TextureFormat::D32_SFLOAT, rttPass->width, rttPass->height);
        depthBufferCI.IsTransient = true;
        depthBufferCI.Layers = rttPass->getSettings().numViews;
        depthBuffer = renderer->getCore()->CreateTexture(depthBufferCI);
    }

    void QuestPipeline::draw(entt::registry& reg, R2::VK::CommandBuffer& cb)
    {
        VK::Core* core = renderer->getCore();
        RenderMeshManager* meshManager = renderer->getMeshManager();

        VK::RenderPass r;
        r.ColorAttachment(rttPass->getFinalTarget(), VK::LoadOp::DontCare, VK::StoreOp::Store)
                .ViewMask(getViewMask(rttPass->getSettings().numViews))
                .ColorAttachmentClearValue(VK::ClearValue::FloatColorClear(1.f, 0.f, 1.f, 1.f))
                .DepthAttachment(depthBuffer, VK::LoadOp::Clear, VK::StoreOp::DontCare)
                .DepthAttachmentClearValue(VK::ClearValue::DepthClear(0.0f))
                .RenderArea(rttPass->width, rttPass->height)
                .Begin(cb);

        Camera* camera = rttPass->getCamera();
        cb.SetScissor(VK::ScissorRect::Simple(rttPass->width, rttPass->height));

        QuestVPs multiVPs{};
        int numViews = rttPass->getSettings().numViews;
        Frustum* frustums = (Frustum*)alloca(sizeof(Frustum) * rttPass->getSettings().numViews);
        if (numViews == 1)
        {
            glm::mat4 view = camera->getViewMatrix();
            glm::mat4 proj = camera->getProjectionMatrix(
                    (float) rttPass->width / rttPass->height);
            glm::mat4 vp = proj * view;
            multiVPs.vp[0] = vp;
            multiVPs.viewPos[0] = glm::vec4(camera->position, 0.0);
            new(&frustums[0]) Frustum();
            frustums[0].fromVPMatrix(vp);
        }
        else
        {
            for (int i = 0; i < numViews; i++)
            {
                glm::mat4 proj = overrideProjs[i];
                glm::mat4 view = overrideViews[i] * camera->getViewMatrix();
                glm::mat4 vp = proj * view;
                multiVPs.vp[i] = vp;
                new(&frustums[i]) Frustum();
                frustums[i].fromVPMatrix(vp);
                multiVPs.viewPos[i] = glm::vec4((glm::vec3)glm::inverse(view)[3], 0.0f);
            }
        }

        core->QueueBufferUpload(multiVPBuffer, &multiVPs, sizeof(multiVPs), 0);

        cb.BindPipeline(pipeline);
        cb.BindGraphicsDescriptorSet(pipelineLayout, descriptorSet[core->GetFrameIndex()], 0);
        cb.BindGraphicsDescriptorSet(pipelineLayout, &renderer->getBindlessTextureManager()->GetTextureDescriptorSet(), 1);
        cb.SetViewport(VK::Viewport::Simple(rttPass->width, rttPass->height));
        cb.BindVertexBuffer(0, meshManager->getVertexBuffer(), 0);
        cb.BindIndexBuffer(meshManager->getIndexBuffer(), 0, VK::IndexType::Uint32);

        glm::mat4* modelMatricesMapped = (glm::mat4*)modelMatrixBuffer->MapCurrent();
        uint32_t modelMatrixIndex = 0;

        reg.view<WorldObject, Transform>().each([&](WorldObject& wo, Transform& t) {
            RenderMeshInfo& rmi = meshManager->loadOrGet(wo.mesh);

            if (!isMeshVisible(rmi, t, frustums, numViews)) return;

            StandardPushConstants spc{};
            spc.cubemapId = ~0u;
            int highestPriority = -1;
            float highestPenetration = -1000.f;
            AABB aabb = AABB{rmi.aabbMin, rmi.aabbMax}.transform(t);
            reg.view<WorldCubemap, Transform>().each([&](WorldCubemap& wc, Transform& cubeT) {
                glm::vec3 ma = wc.extent + cubeT.position;
                glm::vec3 mi = cubeT.position - wc.extent;

                AABB cubemapBB{ mi, ma };
                if (cubemapBB.intersects(aabb))
                {
                    float penetration = cubemapBB.getPenetration(aabb);
                    VKTextureManager* texMan = renderer->getTextureManager();
                    if (texMan->isLoaded(wc.cubemapId))
                    {
                        bool penned = penetration > highestPenetration && wc.priority >= highestPriority;
                        if (penned || wc.priority > highestPriority)
                        {
                            highestPriority = wc.priority;
                            highestPenetration = penetration;
                            spc.cubemapId = texMan->get(wc.cubemapId);
                            spc.flags = wc.cubeParallax;
                            spc.cubemapProjectPos = cubeT.position;
                            spc.cubemapProjectExtent = wc.extent;
                        }
                    }
                }
            });

            spc.modelMatrixID = modelMatrixIndex;

            modelMatricesMapped[modelMatrixIndex++] = t.getMatrix();

            uint32_t vertexOffset = rmi.vertsOffset / sizeof(Vertex);
            for (int i = 0; i < rmi.numSubmeshes; i++)
            {
                RenderSubmeshInfo& rsi = rmi.submeshInfo[i];
                if (!wo.drawSubmeshes[i]) return;

                uint8_t materialIndex = rsi.materialIndex;
                if (!wo.presentMaterials[materialIndex])
                    materialIndex = 0;

                spc.materialOffset = RenderMaterialManager::GetMaterial(wo.materials[materialIndex]);

                cb.PushConstants(spc, VK::ShaderStage::AllRaster, pipelineLayout);
                uint32_t indexOffset = (rmi.indexOffset / sizeof(uint32_t)) + rsi.indexOffset;
                cb.DrawIndexed(rsi.indexCount, 1, indexOffset, vertexOffset, 0);
            }
        });
        r.End(cb);

        modelMatrixBuffer->UnmapCurrent();
    }

    void QuestPipeline::setView(int viewIndex, glm::mat4 viewMatrix, glm::mat4 projectionMatrix)
    {
        overrideViews[viewIndex] = viewMatrix;
        overrideProjs[viewIndex] = projectionMatrix;
    }

    bool QuestPipeline::supportsExternalTarget()
    {
        return true;
    }
}

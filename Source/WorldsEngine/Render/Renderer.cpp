#include <Core/AssetDB.hpp>
#include <Core/ConVar.hpp>
#include <Core/Engine.hpp>
#include <Core/Fatal.hpp>
#include <Core/Log.hpp>
#include <Core/MaterialManager.hpp>
#include <R2/BindlessTextureManager.hpp>
#include <R2/VK.hpp>
#include <R2/VKSwapchain.hpp>
#include <R2/VKSyncPrims.hpp>
#include <R2/VKTimestampPool.hpp>
#include <Render/CubemapConvoluter.hpp>
#include <Render/FakeLitPipeline.hpp>
#include <Render/QuestPipeline/QuestPipeline.hpp>
#include <Render/ObjectPickPass.hpp>
#include <Render/ParticleDataManager.hpp>
#include <Render/ParticleSimulator.hpp>
#include <Render/R2ImGui.hpp>
#include <Render/RenderInternal.hpp>
#include <Render/ShaderCache.hpp>
#include <Render/StandardPipeline/StandardPipeline.hpp>
#include <Render/RenderMaterialManager.hpp>
#include <UI/UISystem.hpp>
#include <SDL_vulkan.h>
#include <tracy/Tracy.hpp>
#include <VR/OpenXRInterface.hpp>
#include <Util/TimingUtil.hpp>
#include <TaskScheduler.h>
#include <Core/TaskScheduler.hpp>
#include <readerwriterqueue.h>

#include "R2/VKUtil.hpp"

namespace R2::VK
{
    extern int allocatedDescriptorSets;
}

using namespace R2;

namespace worlds
{
    std::mutex convoluteQueueMutex;
    moodycamel::ReaderWriterQueue<AssetID> convoluteQueue;
    
    class LogDebugOutputReceiver : public VK::IDebugOutputReceiver
    {
    public:
        void DebugMessage(const char* msg) override
        {
            logErr("VK: %s", msg);
        }
    };

    VKRenderer::VKRenderer(const RendererInitInfo& initInfo, bool* success)
        : timeAccumulator(0.0)
    {
        ZoneScoped;
        bool enableValidation = true;

#ifdef NDEBUG
        enableValidation = false;
        enableValidation |= EngineArguments::hasArgument("validation-layers");
#else
        enableValidation = !EngineArguments::hasArgument("no-validation-layers");
#endif

        std::vector<const char*> instanceExts;

        for (const std::string& s : initInfo.additionalInstanceExtensions)
        {
            instanceExts.push_back(s.c_str());
        }
        instanceExts.push_back(nullptr);

        std::vector<const char*> deviceExts;

        for (const std::string& s : initInfo.additionalDeviceExtensions)
        {
            deviceExts.push_back(s.c_str());
        }
        deviceExts.push_back(nullptr);

        core = new VK::Core(new LogDebugOutputReceiver, enableValidation, instanceExts.data(), deviceExts.data());
        VK::SwapchainCreateInfo sci{};

        SDL_Vulkan_CreateSurface(initInfo.window, core->GetHandles()->Instance, &sci.surface);

        swapchain = new VK::Swapchain(core, sci);
        frameFence = new VK::Fence(core->GetHandles(), VK::FenceFlags::CreateSignaled);

        debugStats = RenderDebugStats{};

        bindlessTextureManager = new R2::BindlessTextureManager(core);
        textureManager = new VKTextureManager(core, bindlessTextureManager);
        uiTextureManager = new VKUITextureManager(textureManager);
        renderMeshManager = new RenderMeshManager(core);
        timestampPool = new R2::VK::TimestampPool(core->GetHandles(), 4);

        if (!ImGui_ImplR2_Init(core, bindlessTextureManager))
            return;

        R2::VK::GraphicsDeviceInfo deviceInfo = core->GetDeviceInfo();
        logVrb(WELogCategoryRender, "Device name: %s", deviceInfo.Name);
        timestampPeriod = deviceInfo.TimestampPeriod;
        auto supportedFeatures = core->GetSupportedFeatures();
        logVrb(WELogCategoryRender, "Ray tracing: %i, Variable rate shading: %i",
               (int)supportedFeatures.RayTracing, (int)supportedFeatures.VariableRateShading);

        ShaderCache::setDevice(core);

        if (initInfo.enableVR)
        {
            // 0 means the size is set automatically
            xrPresentManager = new XRPresentManager(this, 0, 0);
        }

        shadowmapManager = new ShadowmapManager(this);
        objectPickPass = new ObjectPickPass(this);

        particleDataManager = new ParticleDataManager(this);
        particleSimulator = new ParticleSimulator(this);
        
        RenderMaterialManager::Initialize(this);

        *success = true;
    }

    VKRenderer::~VKRenderer()
    {
        core->WaitIdle();
        RenderMaterialManager::Shutdown();
        delete frameFence;
        delete swapchain;

        for (VKRTTPass* pass : rttPasses)
        {
            delete pass;
        }

        delete renderMeshManager;
        delete uiTextureManager;
        delete textureManager;
        delete bindlessTextureManager;
        xrPresentManager.Reset();
        shadowmapManager.Reset();
        objectPickPass.Reset();
        particleSimulator.Reset();
        particleDataManager.Reset();
        ImGui_ImplR2_Shutdown();

        delete core;
    }

    template <typename Object>
    struct ObjectMaterialLoadTask : public enki::ITaskSet
    {
        VKRenderer* renderer;
        entt::registry& reg;

        ObjectMaterialLoadTask(VKRenderer* renderer, entt::registry& reg) : renderer(renderer), reg(reg)
        {
        }

        void ExecuteRange(enki::TaskSetPartition range, uint32_t threadnum) override
        {
            auto view = reg.view<Object>();

            auto begin = view.begin();
            auto end = view.begin();
            std::advance(begin, range.start);
            std::advance(end, range.end);

            for (auto it = begin; it != end; it++)
            {
                Object& wo = reg.get<Object>(*it);
                RenderMeshInfo* rmi;
                if (!renderer->getMeshManager()->get(wo.mesh, &rmi))
                    continue;

                for (int i = 0; i < rmi->numSubmeshes; i++)
                {
                    const RenderSubmeshInfo& rsi = rmi->submeshInfo[i];
                    uint8_t materialIndex = rsi.materialIndex;

                    if (!wo.presentMaterials[materialIndex])
                        materialIndex = 0;

                    if (RenderMaterialManager::IsMaterialLoaded(wo.materials[materialIndex]))
                        continue;

                    RenderMaterialManager::LoadOrGetMaterial(wo.materials[materialIndex]);
                }
            }
        }
    };

    struct LoadSingleCubemapTask : public enki::ITaskSet
    {
        VKTextureManager* textureManager;
        AssetID id;

        LoadSingleCubemapTask(VKTextureManager* textureManager, AssetID id)
            : textureManager(textureManager)
            , id(id)
        {
        }

        void ExecuteRange(enki::TaskSetPartition range, uint32_t threadnum) override
        {
            textureManager->loadAndGetAsync(id);
        }
    };

    struct LoadCubemapsTask : public enki::ITaskSet
    {
        entt::registry& registry;
        VKTextureManager* textureManager;

        LoadCubemapsTask(entt::registry& registry, VKTextureManager* textureManager)
            : registry(registry), textureManager(textureManager)
        {
        }

        void ExecuteRange(enki::TaskSetPartition range, uint32_t threadnum) override
        {
            ZoneScoped;

            auto cubemapView = registry.view<WorldCubemap>();

            auto begin = cubemapView.begin();
            auto end = cubemapView.begin();
            std::advance(begin, range.start);
            std::advance(end, range.end);

            for (auto it = begin; it != end; it++)
            {
                WorldCubemap& wc = registry.get<WorldCubemap>(*it);
                if (!textureManager->isLoaded(wc.cubemapId))
                {
                    textureManager->loadAndGetAsync(wc.cubemapId);
                }
                wc.loadedId = wc.cubemapId;
            }
        }
    };

    void VKRenderer::frame(World& world, float deltaTime)
    {
        ZoneScoped;

        timeAccumulator += (double)deltaTime;

        // Reset debug stats
        debugStats.numDrawCalls = 0;
        debugStats.numRTTPasses = (int)rttPasses.size();
        debugStats.numActiveRTTPasses = 0;
        debugStats.numTriangles = 0;

        if (g_vrInterface)
            g_vrInterface->waitFrame();

#ifdef __ANDROID__
        if (!this->xrPresentManager)
#endif
        {
            ZoneScopedN("Wait for GPU");
            frameFence->WaitFor();
            frameFence->Reset();
        }

		VK::Texture* swapchainImage = nullptr;
#ifdef __ANDROID__
        if (!this->xrPresentManager)
#endif
        {
            ZoneScopedN("Acquire swapchain image");
            swapchainImage = swapchain->Acquire(frameFence);
        }

        PerfTimer cmdBufWrite{};

        currentDebugLines = swapDebugLineBuffer(currentDebugLineCount);

        int width;
        int height;

        swapchain->GetSize(width, height);

        {
            ZoneScopedN("Core::BeginFrame");
            core->BeginFrame();
        }

        VK::CommandBuffer cb = core->GetFrameCommandBuffer();
        textureManager->uploadTextures(cb);
        bindlessTextureManager->UpdateDescriptorsIfNecessary();

        if (width > 0 && height > 0)
        {
            // Read back previous frame's timestamps for GPU time
            uint64_t lastTimestamps[2];

            if (timestampPool->GetTimestamps(core->GetFrameIndex() * 2, 2, lastTimestamps))
            {
                lastGPUTime = (float)(lastTimestamps[1] - lastTimestamps[0]) * timestampPeriod;
            }

            timestampPool->Reset(cb, core->GetFrameIndex() * 2, 2);
            timestampPool->WriteTimestamp(cb, core->GetFrameIndex() * 2);

            {
                ZoneScopedN("Mesh loading");
				world.registry.view<WorldObject>().each([&](WorldObject& wo)
					{
						renderMeshManager->loadOrGet(wo.mesh);
					});

                world.registry.view<SkinnedWorldObject>().each([&](WorldObject& wo)
                    {
                        renderMeshManager->loadOrGet(wo.mesh);
                    });
            }
            RenderMaterialManager::UnloadUnusedMaterials(world.registry);

            glm::mat4 shadowViewMatrix = g_mainCamera->getViewMatrix();
            for (VKRTTPass* pass : rttPasses)
            {
                if (pass->active && pass->settings.enableShadows)
                {
                    if (pass->settings.setViewsFromXR)
                    {
#ifdef ENABLE_OPENXR
                        const UnscaledTransform& leftEye = g_vrInterface->getEyeTransform(Eye::LeftEye);
                        shadowViewMatrix = glm::inverse(leftEye.getMatrix()) * pass->cam->getViewMatrix();
#endif
                    }
                    else
                    {
                        shadowViewMatrix = pass->cam->getViewMatrix();
                    }
                }

                if (pass->settings.registryOverride != nullptr)
                {
                    entt::registry* regOverride = pass->settings.registryOverride;
                    regOverride->view<WorldObject>().each([&](WorldObject& wo)
                        {
                            renderMeshManager->loadOrGet(wo.mesh);
                        });

                    regOverride->view<SkinnedWorldObject>().each([&](WorldObject& wo)
                        {
                            renderMeshManager->loadOrGet(wo.mesh);
                        });

                    // Load materials and stuff
                    ObjectMaterialLoadTask<WorldObject> woLoadTask{ this, *regOverride };
                    woLoadTask.m_SetSize = regOverride->view<WorldObject>().size();
                    ObjectMaterialLoadTask<SkinnedWorldObject> swoLoadTask{ this, *regOverride };
                    swoLoadTask.m_SetSize = regOverride->view<SkinnedWorldObject>().size();
                    LoadCubemapsTask cubemapsTask{ *regOverride, textureManager };
                    cubemapsTask.m_SetSize = regOverride->view<WorldCubemap>().size();

                    TasksFinished finisher;
                    finisher.SetDependenciesVec<std::vector<enki::Dependency>, enki::ITaskSet>(
                        finisher.dependencies, { &woLoadTask, &swoLoadTask, &cubemapsTask });
                    g_taskSched.AddTaskSetToPipe(&woLoadTask);

                    g_taskSched.AddTaskSetToPipe(&swoLoadTask);
                    g_taskSched.AddTaskSetToPipe(&cubemapsTask);
                    g_taskSched.WaitforTask(&finisher, enki::TASK_PRIORITY_HIGH);

                    auto& sceneSettings = regOverride->ctx<SkySettings>();
                    if (!textureManager->isLoaded(sceneSettings.skybox))
                    {
                        LoadSingleCubemapTask lsct{ textureManager, sceneSettings.skybox };
                        g_taskSched.AddTaskSetToPipe(&lsct);
                        g_taskSched.WaitforTask(&lsct, enki::TASK_PRIORITY_HIGH);
                    }
                }
            }

            auto& sceneSettings = world.registry.ctx<SkySettings>();
            if (!textureManager->isLoaded(sceneSettings.skybox))
            {
                LoadSingleCubemapTask lsct{ textureManager, sceneSettings.skybox };
                g_taskSched.AddTaskSetToPipe(&lsct);
                g_taskSched.WaitforTask(&lsct, enki::TASK_PRIORITY_HIGH);
            }

            // Load materials and stuff
            ObjectMaterialLoadTask<WorldObject> woLoadTask{ this, world.registry };
            woLoadTask.m_SetSize = world.registry.view<WorldObject>().size();
            ObjectMaterialLoadTask<SkinnedWorldObject> swoLoadTask{ this, world.registry };
            swoLoadTask.m_SetSize = world.registry.view<SkinnedWorldObject>().size();
            LoadCubemapsTask cubemapsTask{ world.registry, textureManager };
            cubemapsTask.m_SetSize = world.registry.view<WorldCubemap>().size();

            TasksFinished finisher;
            finisher.SetDependenciesVec<std::vector<enki::Dependency>, enki::ITaskSet>(
                finisher.dependencies, { &woLoadTask, &swoLoadTask, &cubemapsTask });


            g_taskSched.AddTaskSetToPipe(&woLoadTask);
            g_taskSched.AddTaskSetToPipe(&swoLoadTask);
            g_taskSched.AddTaskSetToPipe(&cubemapsTask);
            {
                ZoneScopedN("Wait for load tasks");
                // Only wait for high priority tasks
                // If we waited for medium and lower, we'd start running the texture loading on the
                // main thread, block it and defeating the point of doing it in tasks
                g_taskSched.WaitforTask(&finisher, enki::TASK_PRIORITY_HIGH);
            }

#ifdef ENABLE_RMLUI
            g_ui->renderPanels();
#endif

            
            shadowmapManager->allocateShadowmaps(world.registry, glm::inverse(shadowViewMatrix)[3]);
            shadowmapManager->renderShadowmaps(cb, world.registry, shadowViewMatrix);

            particleSimulator->execute(world.registry, cb, deltaTime);

            for (VKRTTPass* pass : rttPasses)
            {
                if (!pass->active && !pass->hdrDataRequested)
                    continue;

                debugStats.numActiveRTTPasses++;

                // I don't like having to do this in the renderer, but doing it elsewhere introduces unnecessary latency
                // and frame-pacing issues :(
                if (pass->settings.setViewsFromXR)
                {
#ifdef ENABLE_OPENXR
                    const UnscaledTransform& leftEye = g_vrInterface->getEyeTransform(Eye::LeftEye);
                    const UnscaledTransform& rightEye = g_vrInterface->getEyeTransform(Eye::RightEye);
                    pass->setView(
                        0, glm::inverse(leftEye.getMatrix()),
                        g_vrInterface->getEyeProjectionMatrix(Eye::LeftEye));

                    pass->setView(
                        1, glm::inverse(rightEye.getMatrix()),
                        g_vrInterface->getEyeProjectionMatrix(Eye::RightEye));
#endif
                }

                if (pass->pipeline->supportsExternalTarget() && pass->settings.outputToXR)
                {
                    VK::Texture* target = g_vrInterface->acquireSwapchainImage(cb);
                    pass->setExternalFinalTarget(target);
                }

                cb.BeginDebugLabel("RTT Pass", 0.0f, 0.0f, 0.0f);
                pass->pipeline->draw(pass->settings.registryOverride ? *pass->settings.registryOverride : world.registry, cb);

                if (!pass->settings.outputToXR)
                {
                    pass->getFinalTarget()->Acquire(
                        cb,
                        VK::ImageLayout::ShaderReadOnlyOptimal,
                        VK::AccessFlags::ShaderSampledRead,
                        VK::PipelineStageFlags::FragmentShader);
                }

                if (pass->hdrDataRequested)
                {
                    pass->hdrDataRequested = false;
                    pass->downloadHDROutput(cb);
                }

                if (pass->settings.outputToXR)
                {
                    if (pass->pipeline->supportsExternalTarget())
                    {
                        g_vrInterface->releaseSwapchainImage();
                    }
                    else
                    {
                        g_vrInterface->submitLayered(cb, pass->getFinalTarget());
                    }
                }

                cb.EndDebugLabel();
            }

            // Draw ImGui directly to the swapchain
            if (swapchainImage != nullptr)
            {
                VK::RenderPass rp;
                rp.ColorAttachment(swapchainImage, VK::LoadOp::Clear, VK::StoreOp::Store);
                rp.ColorAttachmentClearValue(VK::ClearValue::FloatColorClear(0.f, 0.f, 0.f, 1.0f));
                rp.RenderArea(width, height);

                cb.BeginDebugLabel("Dear ImGui", 0.0f, 0.0f, 0.0f);

                rp.Begin(cb);
                ImGui_ImplR2_RenderDrawData(imguiDrawData, cb);
                rp.End(core->GetFrameCommandBuffer());

                cb.EndDebugLabel();

                swapchainImage->Acquire(
                    cb, VK::ImageLayout::PresentSrc, VK::AccessFlags::MemoryRead, VK::PipelineStageFlags::AllCommands);
            }

#ifndef __ANDROID__
            objectPickPass->execute(cb, world.registry);
#endif

            timestampPool->WriteTimestamp(cb, core->GetFrameIndex() * 2 + 1);
        }

        debugStats.cmdBufWriteTime = cmdBufWrite.stopGetMs();

#ifdef ENABLE_OPENXR
        if (g_vrInterface)
        {
            g_vrInterface->beginFrame();
        }
#endif
        core->EndFrame();
        TracyMessageL("Submitted");
        if (swapchainImage)
        {
            swapchain->Present();
            TracyMessageL("Presented");
        }

    }

    float VKRenderer::getLastGPUTime() const
    {
        return lastGPUTime;
    }

    void VKRenderer::setVRUsedPose(glm::mat4 usedPose)
    {
        vrUsedPose = usedPose;
    }

    void VKRenderer::setVsync(bool vsync)
    {
        swapchain->SetVsync(vsync);
    }

    bool VKRenderer::getVsync() const
    {
        return swapchain->GetVsync();
    }

    RenderDebugStats& VKRenderer::getDebugStats()
    {
        return debugStats;
    }

    IUITextureManager* VKRenderer::getUITextureManager()
    {
        return uiTextureManager;
    }

    void VKRenderer::setImGuiDrawData(void* drawData)
    {
        imguiDrawData = (ImDrawData*)drawData;
    }

    ConVar useQuestPipeline { "r_useQuestPipeline", "0" };

    RTTPass* VKRenderer::createRTTPass(RTTPassSettings& ci)
    {
        ZoneScoped;
        if (ci.msaaLevel == 0)
        {
            ci.msaaLevel = 1;
        }

        IRenderPipeline* renderPipeline;
        if (useQuestPipeline)
        {
            renderPipeline = new QuestPipeline(this);
        }
        else
        {
            renderPipeline = new StandardPipeline();
        }
        
        bool useExtTarget = ci.outputToXR && renderPipeline->supportsExternalTarget();
        VKRTTPass* pass = new VKRTTPass(this, ci, renderPipeline, useExtTarget);

        renderPipeline->setup(pass);

        rttPasses.push_back(pass);
        return pass;
    }

    void VKRenderer::destroyRTTPass(RTTPass* pass)
    {
        pass->active = false;
        delete static_cast<VKRTTPass*>(pass);
        rttPasses.erase(std::remove(rttPasses.begin(), rttPasses.end(), pass), rttPasses.end());
    }

    void VKRenderer::requestPick(PickParams params)
    {
        objectPickPass->requestPick(params);
    }

    bool VKRenderer::getPickResult(uint32_t& entityId)
    {
        return objectPickPass->getResult(entityId);
    }

    void VKRenderer::reloadShaders()
    {
        ShaderCache::clear();

        for (VKRTTPass* pass : rttPasses)
        {
            delete pass->pipeline;
            if (useQuestPipeline)
            {
                pass->pipeline = new QuestPipeline(this);
            }
            else
            {
                pass->pipeline = new StandardPipeline();
            }
            pass->pipeline->setup(pass);
        }

        particleSimulator.Reset();
        particleSimulator = new ParticleSimulator(this);
    }

    static ConVar showRenderDebugMenus{"r_showExtraDebug", "0"};

    void VKRenderer::drawDebugMenus()
    {
        if (!showRenderDebugMenus) return;

        bool shown = showRenderDebugMenus;
        if (ImGui::Begin("Renderer Debug", &shown))
        {
            ImGui::Text("%i descriptor sets allocated", R2::VK::allocatedDescriptorSets);

            if (ImGui::BeginTabBar("RDebugTabs"))
            {
                if (ImGui::BeginTabItem("Texture Manager"))
                {
                    textureManager->showDebugMenu();
                    ImGui::EndTabItem();
                }

                if (ImGui::BeginTabItem("Material Manager"))
                {
                    RenderMaterialManager::ShowDebugMenu();
                    ImGui::EndTabItem();
                }

                if (ImGui::BeginTabItem("RTT Passes"))
                {
                    for (VKRTTPass* pass : rttPasses)
                    {
                        float maxWidth = 300.0f;
                        float scaleFactor = maxWidth / pass->width;
                        if (scaleFactor > 1.0f) scaleFactor = 1.0f;

                        float scaledWidth = pass->width * scaleFactor;
                        float scaledHeight = pass->height * scaleFactor;

                        if (ImGui::TreeNode(pass, "%ix%i, %ix MSAA, %s", pass->width, pass->height, pass->settings.msaaLevel, pass->active ? "active" : "inactive"))
                        {
                            if (ImGui::BeginChild((ImGuiID)pass, ImVec2(0.0f, scaledHeight + ImGui::GetStyle().ItemSpacing.y)))
                            {
                                if (!pass->useExternalFinalTarget)
                                {
                                    ImGui::Columns(2);
                                    ImGui::SetColumnWidth(0, maxWidth + 50.0f);
                                    ImGui::Image((ImTextureID)pass->finalTargetBindlessID, ImVec2(scaledWidth, scaledHeight));
                                    ImGui::NextColumn();
                                }

                                ImGui::Text("Camera:");
                                Camera* cam = pass->cam;
                                if (pass->cam == nullptr)
                                {
                                    ImGui::Text("- Main Camera");
                                    cam = g_mainCamera;
                                }
                                ImGui::Text("- Vertical FOV: %.3g degrees", glm::degrees(cam->verticalFOV));
                                ImGui::Text("- Near plane: %.3g", cam->near);
                                ImGui::Text("- Position: %.3g, %.3g, %.3g", cam->position.x, cam->position.y, cam->position.z);
                                ImGui::Text("Settings:");
                                ImGui::Text("- %i view%s", pass->settings.numViews, pass->settings.numViews > 1 ? "s" : "");
                                ImGui::Text("- Enable shadows: %i", (int)pass->settings.enableShadows);
                                ImGui::Text("- Render debug shapes: %i", (int)pass->settings.renderDebugShapes);

                                if (pass->useExternalFinalTarget)
                                {
                                    ImGui::NextColumn();
                                    ImGui::Columns(1);
                                }
                            }
                            ImGui::EndChild();
                            ImGui::TreePop();
                        }
                    }
                    ImGui::EndTabItem();
                }

                if (ImGui::BeginTabItem("Shadowmaps"))
                {
                    shadowmapManager->drawDebugMenu();
                    ImGui::EndTabItem();
                }
				ImGui::EndTabBar();
            }
        }

        ImGui::End();

        if (!shown)
        {
            showRenderDebugMenus.setValue("0");
        }
    }

    R2::VK::Core* VKRenderer::getCore()
    {
        return core;
    }

    RenderMeshManager* VKRenderer::getMeshManager()
    {
        return renderMeshManager;
    }

    R2::BindlessTextureManager* VKRenderer::getBindlessTextureManager()
    {
        return bindlessTextureManager;
    }

    VKTextureManager* VKRenderer::getTextureManager()
    {
        return textureManager;
    }

    ShadowmapManager* VKRenderer::getShadowmapManager()
    {
        return shadowmapManager.Get();
    }

    ParticleDataManager* VKRenderer::getParticleDataManager()
    {
        return particleDataManager.Get();
    }

    const DebugLine* VKRenderer::getCurrentDebugLines(size_t* count)
    {
        *count = currentDebugLineCount;
        return currentDebugLines;
    }

    double VKRenderer::getTime()
    {
        return timeAccumulator;
    }
}

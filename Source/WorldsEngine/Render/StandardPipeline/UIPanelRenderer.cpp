﻿#include <entt/entity/registry.hpp>
#include <R2/BindlessTextureManager.hpp>
#include <Render/StandardPipeline/UIPanelRenderer.hpp>
#include <R2/VK.hpp>
#include <Render/RenderInternal.hpp>
#include <Render/ShaderCache.hpp>
#include <UI/UIPanel.hpp>
#include <UI/WorldSpaceUIPanel.hpp>

using namespace R2;

namespace worlds
{
    struct WorldSpacePanelPushConstants
    {
        glm::mat4 transform;
        uint32_t texId;
        float brightness;
    };
    
    UIPanelRenderer::UIPanelRenderer(VKRenderer* renderer, int msaaSamples, uint32_t viewMask, R2::VK::Buffer* vpBuf)
        : renderer(renderer)
    {
        VK::Core* core = renderer->getCore();
        VK::DescriptorSetLayoutBuilder dslb{core};
        dslb.Binding(0, VK::DescriptorType::UniformBuffer, 1, VK::ShaderStage::AllRaster);
        dsl = dslb.Build();
        ds = core->CreateDescriptorSet(dsl);
        VK::DescriptorSetUpdater dsu{core, ds};
        dsu.AddBuffer(0, 0, VK::DescriptorType::UniformBuffer, vpBuf);
        dsu.Update();

        VK::PipelineLayoutBuilder plb{core};
        plb.DescriptorSet(dsl);
        plb.DescriptorSet(&renderer->getBindlessTextureManager()->GetTextureDescriptorSetLayout());
        plb.PushConstants(VK::ShaderStage::AllRaster, 0, sizeof(WorldSpacePanelPushConstants));
        pipelineLayout = plb.Build();
        
        VK::ShaderModule& vs = ShaderCache::getModule("Shaders/world_space_ui.vert.spv");
        VK::ShaderModule& fs = ShaderCache::getModule("Shaders/world_space_ui.frag.spv");

        VK::PipelineBuilder pb{core};
        pb.Layout(pipelineLayout)
          .PrimitiveTopology(VK::Topology::TriangleList)
          .DepthTest(true)
          .DepthCompareOp(VK::CompareOp::Greater)
          .DepthWrite(false)
          .DepthAttachmentFormat(VK::TextureFormat::D32_SFLOAT)
          .ColorAttachmentFormat(VK::TextureFormat::B10G11R11_UFLOAT_PACK32)
          .AddShader(VK::ShaderStage::Vertex, vs)
          .AddShader(VK::ShaderStage::Fragment, fs)
          .CullMode(VK::CullMode::None)
          .MSAASamples(msaaSamples)
          .ViewMask(viewMask)
          .AlphaBlend(true);
        
        pipeline = pb.Build();
    }

    UIPanelRenderer::~UIPanelRenderer()
    {
        delete pipeline;
        delete pipelineLayout;
        delete ds;
        delete dsl;
    }

    struct PanelRenderInfo
    {
        Transform transform;
        uint32_t texId;
        float brightness;
    };

    void UIPanelRenderer::execute(R2::VK::CommandBuffer& cb, entt::registry& registry, Camera* camera)
    {
        cb.BindPipeline(pipeline);
        cb.BindGraphicsDescriptorSet(pipelineLayout, ds, 0);
        VK::DescriptorSet* texDs = &renderer->getBindlessTextureManager()->GetTextureDescriptorSet();
        cb.BindGraphicsDescriptorSet(pipelineLayout, texDs, 1);

        auto view = registry.view<WorldSpaceUIPanel, Transform>();
        PanelRenderInfo renderInfos[32];
        int numPanels = 0;

        view.each(
            [&](entt::entity entity, WorldSpaceUIPanel& panel, Transform& t)
            {
                if (numPanels >= 32) return;
                Transform realTransform = t;
                realTransform.scale *= glm::vec3(panel.pixelSize * panel.width, panel.pixelSize * panel.height, 1.0f);
                renderInfos[numPanels].transform = realTransform;
                renderInfos[numPanels].texId = panel.panel->getBindlessTextureId();
                renderInfos[numPanels].brightness = panel.brightness;
                
                numPanels++;
            }
        );

        std::sort(renderInfos, renderInfos + numPanels, [&camera](PanelRenderInfo& a, PanelRenderInfo& b)
        {
            float distA = glm::distance2(a.transform.position, camera->position);
            float distB = glm::distance2(b.transform.position, camera->position);

            return distA > distB;
        });

        for (int i = 0; i < numPanels; i++)
        {
            const PanelRenderInfo& panelInfo = renderInfos[i];
            WorldSpacePanelPushConstants wsppc
            {
                .transform = panelInfo.transform.getMatrix(),
                .texId = panelInfo.texId,
                .brightness = panelInfo.brightness
            };
            cb.PushConstants(wsppc, VK::ShaderStage::AllRaster, pipelineLayout);
            cb.Draw(6, 1, 0, 0);
        }
            
    }
}

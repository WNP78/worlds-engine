﻿#pragma once
#include <entt/entity/lw_fwd.hpp>

namespace R2::VK
{
    class Buffer;
    class CommandBuffer;
    class Pipeline;
    class PipelineLayout;
    class DescriptorSet;
    class DescriptorSetLayout;
}

namespace worlds
{
    class VKRenderer;
    struct Camera;
    
    class UIPanelRenderer
    {
    public:
        explicit UIPanelRenderer(VKRenderer* renderer, int msaaSamples, uint32_t viewMask, R2::VK::Buffer* vpBuf);
        ~UIPanelRenderer();
        void execute(R2::VK::CommandBuffer& cb, entt::registry& registry, Camera* camera);
    private:
        VKRenderer* renderer;
        R2::VK::PipelineLayout* pipelineLayout;
        R2::VK::Pipeline* pipeline;
        R2::VK::DescriptorSetLayout* dsl;
        R2::VK::DescriptorSet* ds;
    };
}
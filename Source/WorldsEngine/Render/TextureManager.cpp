#include <deque>
#include <tracy/Tracy.hpp>
#include <Core/Fatal.hpp>
#include <Core/TaskScheduler.hpp>
#include <Editor/GuiUtil.hpp>
#include <R2/BindlessTextureManager.hpp>
#include <R2/VKCore.hpp>
#include <Render/Loaders/TextureLoader.hpp>
#include <Render/RenderInternal.hpp>
#include <ImGui/imgui.h>
#include <Core/Log.hpp>
#include <R2/VKBuffer.hpp>
#include <R2/VKCommandBuffer.hpp>
#include <R2/VKUtil.hpp>
#include <Util/TimingUtil.hpp>
#include <Util/StringBuilder.hpp>

namespace worlds
{
    struct TextureUploadInfo
    {
        AssetID assetId;
        size_t dataSize;
        R2::VK::Texture* tex;
        R2::VK::Buffer* stagingBuffer;
        bool isCubemap;
        uint32_t uploadMips;
        uint32_t handle;
        uint32_t writtenMipLevel = 0;
        uint64_t dataCursor = 0;
        R2::VK::TextureView* currentView = nullptr;
    };

    std::mutex texUploadQueueMutex;
    std::deque<TextureUploadInfo> texUploadInfos;
    
    struct VKTextureManager::TextureLoadTask : enki::ITaskSet
    {
        AssetID id;
        uint32_t handle;
        VKTextureManager* vkTextureManager;

        TextureLoadTask(AssetID id, uint32_t handle, VKTextureManager* vkTextureManager)
            : id(id)
              , handle(handle)
              , vkTextureManager(vkTextureManager)
        {
        }

        void ExecuteRange(enki::TaskSetPartition range, uint32_t threadnum) override
        {
            R2::BindlessTextureManager* textureManager = vkTextureManager->textureManager;
            TextureData td = loadTexData(id);

            if (td.data == nullptr)
            {
                textureManager->SetTextureAt(handle, vkTextureManager->missingTexture);
                TexInfo& ti = vkTextureManager->textureIds[id];
                ti.tex = vkTextureManager->missingTexture;
                return;
            }

            R2::VK::TextureCreateInfo tci = R2::VK::TextureCreateInfo::Texture2D(td.format, td.width, td.height);
            tci.CanSample = true;

            tci.NumMips = td.numMips;

            if (td.isCubemap)
            {
                tci.Dimension = R2::VK::TextureDimension::Cube;
                tci.Layers = 1;
                if (td.numMips == 1)
                {
                    tci.SetFullMipChain();
                    tci.NumMips = std::min(6, tci.NumMips); // Give cubemaps 6 mips for convolution
                }
            }

            R2::VK::Texture* t = vkTextureManager->core->CreateTexture(tci);
            t->SetDebugName(td.name.c_str());
            
            TexInfo& ti = vkTextureManager->textureIds[id];
            ti.tex = t;
            ti.isCubemap = td.isCubemap;
            ti.refCount++;

            R2::VK::BufferCreateInfo bci{ R2::VK::BufferUsage::Storage, td.totalDataSize, true };
            R2::VK::Buffer* stagingBuffer = vkTextureManager->core->CreateBuffer(bci);
            
            void* dataDest = stagingBuffer->Map();
            memcpy(dataDest, td.data, td.totalDataSize);
            stagingBuffer->Unmap();
            free(td.data);
            
            std::unique_lock lock{texUploadQueueMutex};
            texUploadInfos.push_back({ id, td.totalDataSize, t, stagingBuffer, td.isCubemap, td.numMips, handle });
        }
    };

    VKUITextureManager::VKUITextureManager(VKTextureManager* texMan) : texMan(texMan)
    {
    }

    ImTextureID VKUITextureManager::loadOrGet(AssetID id)
    {
        return (ImTextureID)(uint64_t)texMan->loadAndGetAsync(id);
    }

    void VKUITextureManager::unload(AssetID id)
    {
        texMan->unload(id);
    }

    VKTextureManager::VKTextureManager(R2::VK::Core* core, R2::BindlessTextureManager* textureManager)
        : core(core), textureManager(textureManager)
    {
        missingTextureID = loadSynchronous(AssetDB::pathToId("Textures/missing.png"));
        R2::VK::CommandBuffer cb = R2::VK::Utils::AcquireImmediateCommandBuffer();
        uploadTextures(cb);
        R2::VK::Utils::ExecuteImmediateCommandBuffer();
        missingTexture = textureManager->GetTextureAt(missingTextureID);
        AssetDB::registerAssetChangeCallback(
            [&](AssetID asset)
            {
                if (!textureIds.contains(asset)) return;
                logMsg("Reloading %s", AssetDB::idToPath(asset).c_str());
                auto& texInfo = textureIds.at(asset);
                if (texInfo.isCubemap)
                {
                    // Fully unload cubemaps so convolution can take care of them
                    unload(asset);
                    return;
                }
                if (texInfo.tex != missingTexture)
                {
                    delete texInfo.tex;
                }
                load(asset, texInfo.bindlessId);
            }
        );
    }

    VKTextureManager::~VKTextureManager()
    {
        for (auto& pair : textureIds)
        {
            if (pair.second.tex != missingTexture)
                delete pair.second.tex;
            textureManager->FreeTextureHandle(pair.second.bindlessId);
        }
        delete missingTexture;
    }

    uint32_t VKTextureManager::loadAndGetAsync(AssetID id)
    {
        std::unique_lock lock{idMutex};
        if (textureIds.contains(id))
        {
            TexInfo& info = textureIds[id];
            info.refCount++;
            return info.bindlessId;
        }

        // Allocate a handle while we still have the lock but without actual
        // texture data. This stops other threads from trying to load the same
        // texture
        uint32_t handle = textureManager->AllocateTextureHandle(missingTexture);
        textureIds.insert({id, TexInfo{nullptr, handle}});
        lock.unlock();

        auto tlt = new TextureLoadTask{id, handle, this};
        tlt->m_Priority = enki::TASK_PRIORITY_MED;
        TaskDeleter* td = new TaskDeleter();
        td->SetDependency(td->dependency, tlt);
        td->deleteSelf = true;
        g_taskSched.AddTaskSetToPipe(tlt);
        return handle;
    }

    uint32_t VKTextureManager::get(AssetID id)
    {
        return textureIds.at(id).bindlessId;
    }

    bool VKTextureManager::isLoaded(AssetID id)
    {
        return textureIds.contains(id);
    }

    void VKTextureManager::unload(AssetID id)
    {
        std::lock_guard lock{idMutex};
        TexInfo info = textureIds[id];
        if (info.bindlessId == missingTextureID)
        {
            // Unloading the missing texture will break a lot of things so... let's just not
            return;
        }

        if (info.tex != missingTexture)
        {
            delete info.tex;
        }

        textureIds.erase(id);
        textureManager->FreeTextureHandle(info.bindlessId);
    }

    void VKTextureManager::release(AssetID id)
    {
        TexInfo& info = textureIds.at(id);
        info.refCount--;

        if (info.refCount <= 0)
            unload(id);
    }

    bool VKTextureManager::isTextureCubemap(AssetID id)
    {
        return textureIds.at(id).isCubemap;
    }

    uint32_t VKTextureManager::load(AssetID id, uint32_t handle)
    {
        TextureLoadTask tlt{id, handle, this};
        tlt.m_Priority = enki::TASK_PRIORITY_MED;
        g_taskSched.AddTaskSetToPipe(&tlt);
        g_taskSched.WaitforTask(&tlt);
        return handle;
    }

    enki::ITaskSet* VKTextureManager::loadAsync(AssetID id)
    {
        std::unique_lock lock{idMutex};
        releaseAssert(!textureIds.contains(id));
        uint32_t handle = textureManager->AllocateTextureHandle(missingTexture);
        textureIds.insert({id, TexInfo{nullptr, handle}});
        lock.unlock();
        auto tlt = new TextureLoadTask{id, handle, this};
        tlt->m_Priority = enki::TASK_PRIORITY_MED;
        return tlt;
    }

    uint32_t VKTextureManager::loadSynchronous(worlds::AssetID id)
    {
        std::unique_lock lock{idMutex};
        if (textureIds.contains(id))
        {
            textureIds.at(id).refCount++;
            return textureIds.at(id).bindlessId;
        }

        // Allocate a handle while we still have the lock but without actual
        // texture data. This stops other threads from trying to load the same
        // texture
        uint32_t handle = textureManager->AllocateTextureHandle(missingTexture);
        textureIds.insert({id, TexInfo{nullptr, handle}});
        lock.unlock();

        TextureLoadTask tlt{id, handle, this};
        tlt.m_Priority = enki::TASK_PRIORITY_MED;
        g_taskSched.AddTaskSetToPipe(&tlt);
        g_taskSched.WaitforTask(&tlt);
        return handle;
    }

    void VKTextureManager::showDebugMenu()
    {
		ImGui::Text("%zu textures loaded", textureIds.size());

		ImGui::BeginChild("Textures", ImVec2(0, 0), true);
		float spaceUsed = 0.0f;
		for (auto& p : textureIds)
		{
			float width = ImGui::GetWindowContentRegionWidth();
			float remaining = width - spaceUsed;

			if (remaining > 128.0f && spaceUsed != 0.0f)
			{
				ImGui::SameLine();
			}
			else
			{
				spaceUsed = 0.0f;
			}

			if (ImGui::ImageButton((ImTextureID)p.second.bindlessId, ImVec2(128, 128)))
            {
                unload(p.first);
                break;
            }
			StackStringBuilder<256> sb;
			sb.appendFmt("%s refcount %i", AssetDB::idToPath(p.first).c_str(), p.second.refCount);
			spaceUsed += 128.0f + ImGui::GetStyle().FramePadding.x * 2.0f;
			tooltipHover(sb.get());
		}
		ImGui::EndChild();
    }

    void VKTextureManager::uploadTextures(R2::VK::CommandBuffer cb)
    {
        ZoneScoped;
        std::unique_lock lock{texUploadQueueMutex};
        int uploaded = 0;
        // TODO: We could theoretically upload more than one texture a frame,
        // but currently that causes stutters, so we'll limit to one for now
        while (!texUploadInfos.empty() && uploaded < 1)
        {
            auto& op = texUploadInfos.front();

            // Full buffer->texture copy
            for (int i = 0; i < op.uploadMips; i++)
            {
                R2::VK::BufferTextureCopy btc{};
                btc.textureRange.LayerCount = op.tex->GetLayerCount();
                btc.textureExtent.Z = 1;
                btc.bufferOffset = op.dataCursor;

                int currWidth = R2::VK::mipScale(op.tex->GetWidth(), i);
                int currHeight = R2::VK::mipScale(op.tex->GetHeight(), i);
                btc.textureExtent.X = currWidth;
                btc.textureExtent.Y = currHeight;
                btc.textureRange.MipLevel = i;

                cb.CopyBufferToTexture(op.stagingBuffer, op.tex, btc);
                op.dataCursor += CalculateTextureByteSize(op.tex->GetFormat(), currWidth, currHeight) * op.tex->GetLayerCount();
            }

            core->DestroyBuffer(op.stagingBuffer);

            textureManager->SetTextureAt(op.handle, op.tex);
            for (auto& callback : texUploadCallbacks)
            {
                callback(op.assetId, op.handle);
            }
            textureIds[op.assetId].refCount--;
            texUploadInfos.pop_front();
            uploaded++;
        }
    }

    void VKTextureManager::registerUploadCompletionCallback(TextureUploadCallback callback)
    {
        texUploadCallbacks.push_back(callback);
    }
}

#include <Render/RenderInternal.hpp>
#include <Core/Engine.hpp>
#include <R2/VK.hpp>
#include <VR/OpenXRInterface.hpp>

using namespace R2;

namespace worlds
{
    XRPresentManager::XRPresentManager(VKRenderer* renderer, int width, int height)
        : width(width)
        , height(height)
        , core(renderer->getCore())
    {
    }

    void XRPresentManager::copyFromLayered(VK::CommandBuffer cb, VK::Texture* layeredTexture)
    {
#ifdef ENABLE_OPENXR
        g_vrInterface->submitLayered(cb, layeredTexture);
#endif
    }

    void XRPresentManager::beginFrame()
    {
#ifdef ENABLE_OPENXR
        g_vrInterface->beginFrame();
#endif
    }

    void XRPresentManager::waitFrame()
    {
#ifdef ENABLE_OPENXR
        g_vrInterface->waitFrame();
#endif
    }

    void XRPresentManager::endFrame()
    {
#ifdef ENABLE_OPENXR
        g_vrInterface->endFrame();
#endif
    }
}
#include <Render/RenderInternal.hpp>
#include <Core/AssetDB.hpp>
#include <Core/ConVar.hpp>
#include <entt/entity/registry.hpp>
#include <ImGui/imgui.h>
#include <Render/CullMesh.hpp>
#include <Render/Frustum.hpp>
#include <Render/ShaderCache.hpp>
#include <R2/BindlessTextureManager.hpp>
#include <R2/VK.hpp>
#include <tracy/Tracy.hpp>

using namespace R2;

namespace worlds
{
    struct ForceShadowRerenderTag
    {
    };
    
    ConVar r_skipShadows {"r_skipShadows", "0"};
    ConVar r_shadowmapRes {"r_shadowmapRes", "1024"};
    ConVar r_maxShadowmapRes {"r_maxShadowmapRes", "2048"};
    ConVar r_shadowmapTexelDensity {"r_shadowmapTexelDensity", "64"};

    ShadowmapManager::ShadowmapManager(VKRenderer* renderer) : renderer(renderer)
    {
        VK::PipelineLayoutBuilder plb{renderer->getCore()->GetHandles()};
        plb.PushConstants(VK::ShaderStage::Vertex, 0, sizeof(glm::mat4));
        pipelineLayout = plb.Build();

        VK::VertexBinding vertBinding{};
        vertBinding.Size = sizeof(Vertex);
        vertBinding.Attributes.emplace_back(0, VK::TextureFormat::R32G32B32_SFLOAT, 0);

        VK::PipelineBuilder pb{renderer->getCore()};
        pb.AddShader(VK::ShaderStage::Vertex, ShaderCache::getModule(AssetDB::pathToId("Shaders/shadowmap.vert.spv")))
            .AddShader(VK::ShaderStage::Fragment, ShaderCache::getModule(AssetDB::pathToId("Shaders/blank.frag.spv")))
            .AddVertexBinding(vertBinding)
            .Layout(pipelineLayout.Get())
            .DepthTest(true)
            .DepthWrite(true)
            .DepthCompareOp(VK::CompareOp::Greater)
            .DepthAttachmentFormat(VK::TextureFormat::D16_UNORM)
            .CullMode(VK::CullMode::Back)
            .PrimitiveTopology(VK::Topology::TriangleList)
            .DepthBias(true)
            .SlopeDepthBias(-1.0f);

        pipeline = pb.Build();

        for (int i = 0; i < NUM_SHADOW_LIGHTS; i++)
        {
            ShadowmapInfo si{};
            si.isAllocated = false;
            shadowmapInfo.push_back(std::move(si));
        }
        shadowmapMatrices.resize(NUM_SHADOW_LIGHTS);
    }

    int calcTargetResolution(const WorldLight& worldLight, const Transform& t, glm::vec3 camPos)
    {
        if (worldLight.type == LightType::Directional) return r_shadowmapRes.getInt();
        
        Camera shadowCam{};
        shadowCam.position = t.position;
        shadowCam.rotation = t.rotation;
        shadowCam.verticalFOV = worldLight.spotOuterCutoff * 2.0f;
        shadowCam.near = worldLight.shadowNear;
        shadowCam.far = worldLight.shadowFar;

        glm::mat4 invProj = glm::inverse(shadowCam.getProjectMatrixNonInfinite(1.0f));
        glm::vec4 furthestPoint = glm::vec4(1.0f, 1.0f, 0.0f, 1.0f);
        glm::vec4 projPos = invProj * furthestPoint;
        projPos /= projPos.w;
        
        glm::mat4 vp = shadowCam.getProjectMatrixNonInfinite(1.0f) * shadowCam.getViewMatrix();
        glm::mat4 invVp = glm::inverse(vp);
        
        glm::vec4 shadowClipCamPos = vp * glm::vec4(camPos, 1.0f);
        
        const glm::vec4 frustumMin(glm::vec3(-1.0f), FLT_MIN);
        const glm::vec4 frustumMax(glm::vec3(1.0f), FLT_MAX);
        shadowClipCamPos = glm::clamp(shadowClipCamPos, frustumMin, frustumMax);
        
        glm::vec3 projectedCamPos = invVp * shadowClipCamPos;
        // rough heuristic to scale resolution with camera distance
        float camScale = 1.0f / glm::sqrt(glm::max(glm::distance(camPos, projectedCamPos) * 0.5f, 1.0f));

        return glm::clamp((int)(glm::ceil((projPos.x * 2.0f * r_shadowmapTexelDensity.getFloat() * camScale) / 128.0f) * 128.0f), 16, r_maxShadowmapRes.getInt());
    }

    void ShadowmapManager::allocateShadowmaps(entt::registry& registry, glm::vec3 eyePos)
    {
        ZoneScoped;
        uint32_t count = 0;

        bool hasUpscaled = false;
        
        registry.view<WorldLight, Transform>().each([&](entt::entity ent, WorldLight& worldLight, const Transform& t) {
            bool isShadowable = worldLight.type == LightType::Spot || worldLight.type == LightType::Directional;
            if (!worldLight.enabled || !worldLight.enableShadows || !isShadowable || count == NUM_SHADOW_LIGHTS)
            {
                worldLight.shadowmapIdx = ~0u;
                return;
            }
            uint32_t lastIdx = worldLight.shadowmapIdx;
            worldLight.shadowmapIdx = count++;

            if (lastIdx != worldLight.shadowmapIdx)
            {
                registry.emplace_or_replace<ForceShadowRerenderTag>(ent);
            }

            ShadowmapInfo& si = shadowmapInfo[worldLight.shadowmapIdx];
            int targetRes = calcTargetResolution(worldLight, t, eyePos);

            if (targetRes > worldLight.currentResolution)
            {
                if (hasUpscaled)
                {
                    targetRes = worldLight.currentResolution;
                }
                else
                {
                    hasUpscaled = true;
                }
            }
            worldLight.currentResolution = targetRes;
            
            VK::TextureCreateInfo tci = VK::TextureCreateInfo::RenderTarget2D(
                VK::TextureFormat::D16_UNORM,
                targetRes, targetRes 
            );
            tci.CanSample = true;
            
            if (!si.isAllocated)
            {
                si.texture = renderer->getCore()->CreateTexture(tci);
                si.bindlessID = renderer->getBindlessTextureManager()->AllocateTextureHandle(si.texture.Get());
                si.isAllocated = true;
                registry.emplace_or_replace<ForceShadowRerenderTag>(ent);
            }
            
            if (si.texture->GetWidth() != targetRes)
            {
                si.texture = renderer->getCore()->CreateTexture(tci);
                renderer->getBindlessTextureManager()->SetTextureAt(si.bindlessID, si.texture.Get());
                registry.emplace_or_replace<ForceShadowRerenderTag>(ent);
            }
            
          
        });
    }

    glm::mat4 getCascadeMatrix(glm::mat4 camVP, glm::vec3 lightDir, float& texelsPerUnit)
    {
        glm::mat4 vpInv = glm::inverse(camVP);

        glm::vec3 frustumCorners[8] = {
                glm::vec3(-1.0f,  1.0f, -1.0f),
                glm::vec3(1.0f,  1.0f, -1.0f),
                glm::vec3(1.0f, -1.0f, -1.0f),
                glm::vec3(-1.0f, -1.0f, -1.0f),
                glm::vec3(-1.0f,  1.0f,  1.0f),
                glm::vec3(1.0f,  1.0f,  1.0f),
                glm::vec3(1.0f, -1.0f,  1.0f),
                glm::vec3(-1.0f, -1.0f,  1.0f),
        };

        for (int i = 0; i < 8; i++)
        {
            glm::vec4 transformed = vpInv * glm::vec4{ frustumCorners[i], 1.0f };
            transformed /= transformed.w;
            frustumCorners[i] = transformed;
        }

        glm::vec3 center{ 0.0f };

        for (int i = 0; i < 8; i++)
        {
            center += frustumCorners[i];
        }

        center /= 8.0f;

        float diameter = 0.0f;
        for (int i = 0; i < 8; i++)
        {
            float dist = glm::length(frustumCorners[i] - center);
            diameter = glm::max(diameter, dist);
        }
        float radius = diameter * 0.5f;

        texelsPerUnit = r_shadowmapRes.getFloat() / diameter;

        glm::mat4 scaleMatrix = glm::scale(glm::mat4{ 1.0f }, glm::vec3{ texelsPerUnit });

        glm::mat4 lookAt = glm::lookAt(glm::vec3{ 0.0f }, lightDir, glm::vec3{ 0.0f, 1.0f, 0.0f });
        lookAt *= scaleMatrix;

        glm::mat4 lookAtInv = glm::inverse(lookAt);

        center = lookAt * glm::vec4{ center, 1.0f };
        center = glm::floor(center);
        center = lookAtInv * glm::vec4{ center, 1.0f };

        glm::vec3 eye = center + (lightDir * diameter);

        glm::mat4 viewMat = glm::lookAt(eye, center, glm::vec3{ 0.0f, 1.0f, 0.0f });
        glm::mat4 projMat = glm::orthoZO(-radius, radius, -radius, radius, radius * 20.0f, -radius * 20.0f);

        return projMat * viewMat;
    }

    void ShadowmapManager::renderShadowmaps(R2::VK::CommandBuffer& cb, entt::registry& registry, glm::mat4& viewMatrix)
    {
        ZoneScoped;
        RenderMeshManager* meshManager = renderer->getMeshManager();
        BindlessTextureManager* btm = renderer->getBindlessTextureManager();
        renderer->getDebugStats().numActiveShadowmaps = 0;

        if (r_skipShadows) return;

        cb.BeginDebugLabel("Shadows", 0.1f, 0.1f, 0.1f);

        cb.BindPipeline(pipeline.Get());
        cb.BindIndexBuffer(meshManager->getIndexBuffer(), 0, VK::IndexType::Uint32);
        cb.BindVertexBuffer(0, meshManager->getVertexBuffer(), 0);

        registry.view<WorldLight, Transform>().each([&](entt::entity lightEntity, WorldLight& worldLight, Transform& t) {
            bool isShadowable = worldLight.type == LightType::Spot || worldLight.type == LightType::Directional;
            if (!worldLight.enableShadows || !isShadowable || worldLight.shadowmapIdx == ~0u)
                return;

            glm::mat4 vp;

            int shadowRes = shadowmapInfo[worldLight.shadowmapIdx].texture->GetWidth();
            cb.SetScissor(VK::ScissorRect::Simple(shadowRes, shadowRes));
            cb.SetViewport(VK::Viewport::Simple(shadowRes, shadowRes));

            if (worldLight.type == LightType::Spot)
            {
                Camera shadowCam{};
                shadowCam.position = t.position;
                shadowCam.rotation = t.rotation;
                shadowCam.verticalFOV = worldLight.spotOuterCutoff * 2.0f;
                shadowCam.near = worldLight.shadowNear;
                shadowCam.far = worldLight.shadowFar;

                vp = shadowCam.getProjectMatrixNonInfinite(1.0f) * shadowCam.getViewMatrix();
            }
            else
            {
                Camera viewerCopy{};
                viewerCopy.near = 0.1f;
                viewerCopy.far = 60.0f;
                viewerCopy.verticalFOV = 1.8f;
                glm::mat4 cascadeVp = viewerCopy.getProjectionMatrixZONonInfinite(1.0f) * viewMatrix;
                float whatever;
                vp = getCascadeMatrix(cascadeVp, t.transformDirection(glm::vec3(0.0f, 0.0f, -1.0f)),
                                      whatever);
            }

            shadowmapMatrices[worldLight.shadowmapIdx] = vp;

            Frustum f{};
            f.fromVPMatrix(vp);

            // force directional lights to always update
            bool hasMovingObject = worldLight.type == LightType::Directional
                || registry.has<ForceShadowRerenderTag>(lightEntity)
                || registry.has<LocalTransformDirty>(lightEntity);

            registry.view<LocalTransformDirty, WorldObject, Transform>().each([&](WorldObject& wo, Transform& woT) {
                RenderMeshInfo* rmi;
                if (!meshManager->get(wo.mesh, &rmi))
                    return;

                if (!isMeshVisible(*rmi, woT, &f, 1))
                    return;
                
                hasMovingObject = true;
            });

            /* TODO: This incorrectly assumes 2 things:
             * 1. Skinned objects are always moving
             * 2. Skinned objects will not significantly exceed their original pre-computed bounds
             */
            registry.view<SkinnedWorldObject, Transform>().each([&](SkinnedWorldObject& wo, Transform& woT) {
                RenderMeshInfo* rmi;
                if (!meshManager->get(wo.mesh, &rmi))
                    return;

                if (!isMeshVisible(*rmi, woT, &f, 1))
                    return;
                
                hasMovingObject = true;
            });

            if (!hasMovingObject) return;
            renderer->getDebugStats().numActiveShadowmaps++;

            VK::RenderPass rp{};
            rp.RenderArea(shadowRes, shadowRes);
            rp.DepthAttachment(
                shadowmapInfo[worldLight.shadowmapIdx].texture.Get(), VK::LoadOp::Clear, VK::StoreOp::Store);
            rp.DepthAttachmentClearValue(VK::ClearValue::DepthClear(0.0f));

            rp.Begin(cb);

            registry.view<WorldObject, Transform>().each([&](WorldObject& wo, Transform& woT) {
                RenderMeshInfo* rmi;
                if (!meshManager->get(wo.mesh, &rmi))
                    return;

                if (!isMeshVisible(*rmi, woT, &f, 1))
                    return;

                glm::mat4 mvp = vp * woT.getMatrix();
                cb.PushConstants(mvp, VK::ShaderStage::Vertex, pipelineLayout.Get());

                for (int i = 0; i < rmi->numSubmeshes; i++)
                {
                    if (!wo.drawSubmeshes[i]) continue;
                    const RenderSubmeshInfo& rsi = rmi->submeshInfo[i];
                    cb.DrawIndexed(
                        rsi.indexCount,
                        1,
                        rsi.indexOffset + (rmi->indexOffset / sizeof(uint32_t)),
                        rmi->vertsOffset / sizeof(Vertex),
                        0);
                }
            });

            registry.view<SkinnedWorldObject, Transform>().each([&](SkinnedWorldObject& wo, Transform& woT) {
                RenderMeshInfo* rmi;
                if (!meshManager->get(wo.mesh, &rmi))
                    return;

                glm::mat4 mvp = vp * woT.getMatrix();
                cb.PushConstants(mvp, VK::ShaderStage::Vertex, pipelineLayout.Get());

                for (int i = 0; i < rmi->numSubmeshes; i++)
                {
                    if (!wo.drawSubmeshes[i]) continue;
                    const RenderSubmeshInfo& rsi = rmi->submeshInfo[i];
                    cb.DrawIndexed(
                        rsi.indexCount,
                        1,
                        rsi.indexOffset + (rmi->indexOffset / sizeof(uint32_t)),
                        wo.skinnedVertexOffset + (meshManager->getSkinnedVertsOffset() / sizeof(Vertex)),
                        0);
                }
            });

            rp.End(cb);
            shadowmapInfo[worldLight.shadowmapIdx].texture->Acquire(
                cb,
                VK::ImageLayout::ShaderReadOnlyOptimal,
                VK::AccessFlags::ShaderSampledRead,
                VK::PipelineStageFlags::FragmentShader);
        });

        registry.clear<ForceShadowRerenderTag>();

        cb.EndDebugLabel();
    }

    void ShadowmapManager::drawDebugMenu()
    {
        for (int i = 0; i < NUM_SHADOW_LIGHTS; i++)
        {
            ShadowmapInfo& si = shadowmapInfo[i];
            if (!si.isAllocated)
            {
                ImGui::Text("Shadow slot %i unallocated", i);
                continue;
            }
            
            ImGui::Text("Shadow slot %i: %ix%i", i, si.texture->GetWidth(), si.texture->GetHeight());
            ImGui::Image((ImTextureID)si.bindlessID, ImVec2(256, 256));
        }
    }

    uint32_t ShadowmapManager::getShadowmapId(uint32_t idx)
    {
        return shadowmapInfo[idx].bindlessID;
    }

    glm::mat4& ShadowmapManager::getShadowVpMatrix(uint32_t idx)
    {
        return shadowmapMatrices[idx];
    }
}

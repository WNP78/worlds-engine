#include "OpenXRInterface.hpp"
#include <tracy/Tracy.hpp>
// EW EW EW
#include "../../R2/PrivateInclude/volk.h"
#include <Core/Log.hpp>
#include <Core/Fatal.hpp>
#include <string.h>
#include <Core/Fatal.hpp>
#include <Render/RenderInternal.hpp>
#include <Core/Engine.hpp>

#define XR_USE_GRAPHICS_API_VULKAN
#ifdef _WIN32
#define XR_USE_PLATFORM_WIN32
#endif
#ifdef __ANDROID__
#define XR_USE_PLATFORM_ANDROID
#include <SDL.h>
#include <jni.h>
#endif
#include <vulkan/vulkan.h>
#include <R2/VK.hpp>
#include <openxr/openxr.h>
#include <openxr/openxr_platform.h>
#include <glm/gtc/type_ptr.hpp>
#include <Util/EnumUtil.hpp>
#include <IO/IOUtil.hpp>
#include <nlohmann/json.hpp>
#include <robin_hood.h>
#include <Core/Console.hpp>
#undef near
#undef far

using namespace R2;

namespace worlds
{
#define XRCHECK(expr) checkXrResult(expr, __FILE__, __LINE__)

    XrInstance resultToStringInstance = nullptr;
    void checkXrResult(XrResult result, const char* file, int line)
    {
        if (!XR_SUCCEEDED(result))
        {
            char buffer[XR_MAX_RESULT_STRING_SIZE];
            if (resultToStringInstance != nullptr)
            {
                xrResultToString(resultToStringInstance, result, buffer);
				logErr("OpenXR result: %s (%i) (file %s, line %i)", buffer, result, file, line);
            }
            else
            {
                const char* msg = result == XR_ERROR_RUNTIME_UNAVAILABLE ? "XR_ERROR_RUNTIME_UNAVAILABLE" : "idk man";
				logErr("OpenXR result: %s (%i) (file %s, line %i)", msg, result, file, line);
            }
        }
    }

    class OpenXRInterface::OpenXRSwapchain
    {
    public:
        std::vector<XrSwapchainImageVulkanKHR> images;
        std::vector<VK::Texture*> textures;
        XrSwapchain swapchain;
        uint32_t width;
        uint32_t height;

        OpenXRSwapchain(XrSession session, XrSwapchainCreateInfo ci, R2::VK::Core* core)
            : width(ci.width)
            , height(ci.height)
        {
            XRCHECK(xrCreateSwapchain(session, &ci, &swapchain));

            // Enumerate swapchain images
            uint32_t imageCount;
            XRCHECK(xrEnumerateSwapchainImages(swapchain, 0, &imageCount, nullptr));

            images.resize(imageCount, { XR_TYPE_SWAPCHAIN_IMAGE_VULKAN_KHR });

            XRCHECK(xrEnumerateSwapchainImages(swapchain, imageCount, &imageCount,
                    (XrSwapchainImageBaseHeader*)(images.data())));

            for (uint32_t i = 0; i < imageCount; i++)
            {
                VK::TextureCreateInfo tci = VK::TextureCreateInfo::RenderTarget2D(VK::TextureFormat::R8G8B8A8_SRGB, ci.width, ci.height);
                tci.Layers = 2;
                tci.Dimension = VK::TextureDimension::Array2D;
                textures.push_back(new VK::Texture(core, images[i].image, VK::ImageLayout::Undefined, tci, VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT));
            }
        }

        ~OpenXRSwapchain()
        {
            XRCHECK(xrDestroySwapchain(swapchain));

            for (VK::Texture* tex : textures)
            {
                // Since the images are owned by the OpenXR swapchain, they've already
                // been destroyed by the time we get here
                tex->ReleaseHandle();
                delete tex;
            }
        }
    };


    struct ActionInternal
    {
        XrAction action;
        bool isPoseAction;
        XrSpace poseSpace;
        robin_hood::unordered_flat_map<XrPath, XrSpace> poseSubactionSpaces;
    };

	robin_hood::unordered_flat_map<std::string, uint64_t> actionNameToHandle;
    robin_hood::unordered_flat_map<uint64_t, ActionInternal> actionsInternal;
    uint64_t actionIdCounter = 1000;

    class ActionSet
    {
    public:
        XrActionSet actionSet;
        std::string name;

        ActionSet(XrInstance instance, XrSession session, std::string name, nlohmann::json& value)
             : name(name)
             , actionSet(XR_NULL_HANDLE)
        {
            XrActionSetCreateInfo setCreateInfo{ XR_TYPE_ACTION_SET_CREATE_INFO };

            if (name.size() >= XR_MAX_ACTION_SET_NAME_SIZE)
            {
                logErr("Action set name %s is too long", name.c_str());
                return;
            }

            strcpy(setCreateInfo.actionSetName, name.c_str());
            // At some point, finish this so the action set name is localized
            strcpy(setCreateInfo.localizedActionSetName, name.c_str());
            setCreateInfo.priority = 0;
            XRCHECK(xrCreateActionSet(instance, &setCreateInfo, &actionSet));
            assert(actionSet != XR_NULL_HANDLE);

            // Iterate over the actions and add them to the set
            for (auto& actionPair : value["actions"].items())
            {
                std::string actionName = actionPair.key();
                if (actionName.size() >= XR_MAX_ACTION_NAME_SIZE)
                {
                    logErr("Action name %s is too long!", actionName.c_str());
                    continue;
                }

                ActionInternal actionInternal{};
                XrActionCreateInfo actionCreateInfo{ XR_TYPE_ACTION_CREATE_INFO };
                strcpy(actionCreateInfo.actionName, actionName.c_str());
                strcpy(actionCreateInfo.localizedActionName, actionName.c_str());

                auto& actionTypeVal = actionPair.value()["type"];

                if (actionTypeVal == "boolean")
                {
                    actionCreateInfo.actionType = XR_ACTION_TYPE_BOOLEAN_INPUT;
                }
                else if (actionTypeVal == "float")
                {
                    actionCreateInfo.actionType = XR_ACTION_TYPE_FLOAT_INPUT;
                }
                else if (actionTypeVal == "vector2f")
                {
                    actionCreateInfo.actionType = XR_ACTION_TYPE_VECTOR2F_INPUT;
                }
                else if (actionTypeVal == "pose")
                {
                    actionCreateInfo.actionType = XR_ACTION_TYPE_POSE_INPUT;
                    actionInternal.isPoseAction = true;
                }
                else if (actionTypeVal == "vibration_output")
                {
                    actionCreateInfo.actionType = XR_ACTION_TYPE_VIBRATION_OUTPUT;
                }
                else
                {
                    logErr("Invalid action type %s", actionTypeVal.get<std::string>().c_str());
                    continue;
                }

                // There are a maximum of 4 possible actions, so just use an array for that
                XrPath subactions[4];
                if (actionPair.value().contains("subactions"))
                {
                    auto& subactionsVal = actionPair.value()["subactions"];
                    actionCreateInfo.countSubactionPaths = subactionsVal.size();
                    actionCreateInfo.subactionPaths = subactions;

                    for (int i = 0; i < subactionsVal.size(); i++)
                    {
                        XRCHECK(xrStringToPath(instance,
                                               subactionsVal[i].get<std::string>().c_str(),
                                               &subactions[i]));
                    }
                }

                XRCHECK(xrCreateAction(actionSet, &actionCreateInfo, &actionInternal.action));

                if (actionInternal.isPoseAction)
                {
                    if (actionCreateInfo.countSubactionPaths > 0)
                    {
                        for (int i = 0; i < actionCreateInfo.countSubactionPaths; i++)
                        {
                            XrActionSpaceCreateInfo asci{XR_TYPE_ACTION_SPACE_CREATE_INFO};
                            asci.poseInActionSpace.orientation = XrQuaternionf{0.0f, 0.0f, 0.0f, 1.0f};
                            asci.subactionPath = subactions[i];
                            asci.action = actionInternal.action;

                            XrSpace poseSpace;
                            XRCHECK(xrCreateActionSpace(session, &asci, &poseSpace));
                            actionInternal.poseSubactionSpaces.insert({subactions[i], poseSpace});
                        }
                    }
                    else
                    {
                        XrActionSpaceCreateInfo asci{XR_TYPE_ACTION_SPACE_CREATE_INFO};
                        asci.poseInActionSpace.orientation = XrQuaternionf{0.0f, 0.0f, 0.0f, 1.0f};
                        asci.action = actionInternal.action;
                        XRCHECK(xrCreateActionSpace(session, &asci, &actionInternal.poseSpace));
                    }
                }

                uint64_t publicId;
                if (!actionNameToHandle.contains(actionName))
                {
                    publicId = actionIdCounter++;
					actionNameToHandle.insert({ actionName, publicId });
                }
                else
                {
                    publicId = actionNameToHandle[actionName];
                }

				actionsInternal.insert({ publicId, actionInternal });
            }
        }

        ~ActionSet()
        {
            for (auto& pair : actionNameToHandle)
            {
                ActionInternal& ai = actionsInternal[pair.second];
                XRCHECK(xrDestroyAction(ai.action));
                if (ai.isPoseAction && ai.poseSubactionSpaces.size() == 0)
                {
                    XRCHECK(xrDestroySpace(ai.poseSpace));
                }
                else
                {
                    for (auto& spacePair : ai.poseSubactionSpaces)
                    {
                        XRCHECK(xrDestroySpace(spacePair.second));
                    }
                }
            }

            XRCHECK(xrDestroyActionSet(actionSet));
        }
    };

    PFN_xrGetVulkanGraphicsRequirementsKHR xrGetVulkanGraphicsRequirementsKHR;
    PFN_xrGetVulkanGraphicsDeviceKHR xrGetVulkanGraphicsDeviceKHR;
    PFN_xrGetVulkanInstanceExtensionsKHR xrGetVulkanInstanceExtensionsKHR;
    PFN_xrGetVulkanDeviceExtensionsKHR xrGetVulkanDeviceExtensionsKHR;
    PFN_xrGetVisibilityMaskKHR xrGetVisibilityMaskKHR;
    PFN_xrPerfSettingsSetPerformanceLevelEXT xrPerfSettingsSetPerformanceLevelEXT;
    PFN_xrRequestDisplayRefreshRateFB xrRequestDisplayRefreshRateFB;
    PFN_xrEnumerateDisplayRefreshRatesFB xrEnumerateDisplayRefreshRatesFB;
    PFN_xrCreateDebugUtilsMessengerEXT xrCreateDebugUtilsMessengerEXT;

    robin_hood::unordered_flat_map<std::string, ActionSet*> actionSets;
    std::string loadedActionJson;

    OpenXRInterface::OpenXRInterface()
        : systemId(XR_NULL_SYSTEM_ID)
        , colorSwapchain(nullptr)
        , depthSwapchain(nullptr)
    {
        g_console->registerCommand([this](const char*)
            {
                if (!initialized)
                {
                    init();
                }
                else
                {
                    deinit();
                }
            }, "vr_toggle", "Toggle VR on and off. Requires that the engine was started with the headset connected");
    }

    std::vector<std::string> parseExtensionString(std::string extString)
    {
        std::vector<std::string> extList;

        auto it = extString.begin();
        while (it < extString.end())
        {
            auto extStart = it;

            while (*it != ' ')
            {
                it++;
                if (it == extString.end()) break;
            }

            std::string ext;
            ext.assign(extStart, it);

            // Hacky thing: SteamVR always requests VK_EXT_debug_marker which is only present when
            // using debug layers, so we just ignore its request
            if (ext.find("debug_marker") == std::string::npos)
            {
                extList.push_back(ext);
            }

            if (it != extString.end())
				it++;
        }

        return extList;
    }

    std::vector<std::string> defaultInstanceExtensions
    {
        "VK_KHR_external_memory_capabilities",
		"VK_KHR_get_physical_device_properties2",
		"VK_KHR_surface",
		"VK_KHR_win32_surface",
		"VK_KHR_external_fence_capabilities",
		"VK_KHR_external_semaphore_capabilities"
    };

    std::vector<std::string> defaultDeviceExtensions
    {
        "VK_KHR_external_memory",
		"VK_KHR_external_memory_win32",
		"VK_KHR_external_semaphore",
		"VK_KHR_external_semaphore_win32",
		"VK_KHR_win32_keyed_mutex",
		"VK_KHR_swapchain",
		"VK_KHR_external_fence",
		"VK_KHR_external_fence_win32",
		"VK_KHR_get_memory_requirements2",
		"VK_KHR_dedicated_allocation"
    };

    std::vector<std::string> OpenXRInterface::getRequiredInstanceExtensions()
    {
        if (systemId == XR_NULL_SYSTEM_ID)
            return defaultInstanceExtensions;

        uint32_t bufferSize;
        XRCHECK(xrGetVulkanInstanceExtensionsKHR(instance, systemId, 0, &bufferSize, nullptr));

        std::string result;
        result.resize(bufferSize, ' ');
        XRCHECK(xrGetVulkanInstanceExtensionsKHR(instance, systemId, bufferSize, &bufferSize, result.data()));

        return parseExtensionString(result);
    }

    std::vector<std::string> OpenXRInterface::getRequiredDeviceExtensions()
    {
        if (systemId == XR_NULL_SYSTEM_ID)
            return defaultDeviceExtensions;

        uint32_t bufferSize;
        XRCHECK(xrGetVulkanDeviceExtensionsKHR(instance, systemId, 0, &bufferSize, nullptr));

        std::string result;
        result.resize(bufferSize, ' ');
        XRCHECK(xrGetVulkanDeviceExtensionsKHR(instance, systemId, bufferSize, &bufferSize, result.data()));

        return parseExtensionString(result);
    }

    void OpenXRInterface::initForRenderExtensions()
    {
        if (!hasInstance) return;
        XrSystemGetInfo systemGetInfo{ XR_TYPE_SYSTEM_GET_INFO };
        systemGetInfo.formFactor = XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY;

        XrResult getSystemResult = xrGetSystem(instance, &systemGetInfo, &systemId);

        if (getSystemResult != XR_ERROR_FORM_FACTOR_UNAVAILABLE)
        {
            XRCHECK(getSystemResult);
        }
    }

    XrBool32 oxrDebugCallback(XrDebugUtilsMessageSeverityFlagsEXT severity, XrDebugUtilsMessageTypeFlagsEXT type, const XrDebugUtilsMessengerCallbackDataEXT* data, void* udata)
    {
        logErr(WELogCategoryVR, "OpenXR val: %s", data->message);
        return XR_FALSE;
    }

    XrDebugUtilsMessengerEXT messenger;

    bool OpenXRInterface::createInstance()
    {
        ZoneScoped;
#ifdef __ANDROID__
        // On Android we have to do some quick setup before creating the instance
        PFN_xrInitializeLoaderKHR xrInitLoader;
        XRCHECK(xrGetInstanceProcAddr(nullptr, "xrInitializeLoaderKHR", (PFN_xrVoidFunction*)&xrInitLoader));
        JNIEnv* env = (JNIEnv*)SDL_AndroidGetJNIEnv();
        JavaVM* vm;
        env->GetJavaVM(&vm);
        XrLoaderInitInfoAndroidKHR androidInitInfo
        {
            .type = XR_TYPE_LOADER_INIT_INFO_ANDROID_KHR,
            .applicationVM = vm,
            .applicationContext = SDL_AndroidGetActivity()
        };
        xrInitLoader((XrLoaderInitInfoBaseHeaderKHR*)&androidInitInfo);
#endif
        XrInstanceCreateInfo instanceCreateInfo{ XR_TYPE_INSTANCE_CREATE_INFO };

        // Create the instance
        XrApplicationInfo appInfo{};
        appInfo.apiVersion = XR_CURRENT_API_VERSION;
        memcpy(appInfo.applicationName, "WorldsEngine", sizeof("WorldsEngine"));
        memcpy(appInfo.engineName, "WorldsEngine", sizeof("WorldsEngine"));
        appInfo.engineVersion = 1;
        appInfo.applicationVersion = 1;

        instanceCreateInfo.applicationInfo = appInfo;

        uint32_t numExtensionProps;
        XRCHECK(xrEnumerateInstanceExtensionProperties(nullptr, 0, &numExtensionProps, nullptr));

        std::vector<XrExtensionProperties> extensionProps{ numExtensionProps };
        for (uint32_t i = 0; i < numExtensionProps; i++)
        {
            extensionProps[i] = XrExtensionProperties{ XR_TYPE_EXTENSION_PROPERTIES };
        }

        XRCHECK(xrEnumerateInstanceExtensionProperties(nullptr, numExtensionProps, &numExtensionProps, extensionProps.data()));

        for (const auto& extProps : extensionProps)
        {
            if (strcmp(extProps.extensionName, XR_KHR_COMPOSITION_LAYER_DEPTH_EXTENSION_NAME) == 0)
            {
                canSubmitDepth = true;
            }
        }

        std::vector<const char*> extensions;
        extensions.push_back(XR_KHR_VULKAN_ENABLE_EXTENSION_NAME);
#ifndef __ANDROID__
        extensions.push_back(XR_KHR_VISIBILITY_MASK_EXTENSION_NAME);
#else
        extensions.push_back(XR_EXT_PERFORMANCE_SETTINGS_EXTENSION_NAME);
        extensions.push_back(XR_FB_DISPLAY_REFRESH_RATE_EXTENSION_NAME);
#endif
#ifdef _WIN32
        extensions.push_back("XR_KHR_win32_convert_performance_counter_time");
#endif

        if (canSubmitDepth)
        {
            extensions.push_back(XR_KHR_COMPOSITION_LAYER_DEPTH_EXTENSION_NAME);
        }

        extensions.push_back(XR_EXT_DEBUG_UTILS_EXTENSION_NAME);

        instanceCreateInfo.enabledExtensionCount = extensions.size();
        instanceCreateInfo.enabledExtensionNames = extensions.data();

        std::vector<const char*> layers;
        if (EngineArguments::hasArgument("xr-validation-layers"))
        {
            layers.push_back("XR_APILAYER_LUNARG_core_validation");
        }

        instanceCreateInfo.enabledApiLayerCount = layers.size();
        instanceCreateInfo.enabledApiLayerNames = layers.data();

        {
            ZoneScopedN("xrCreateInstance");
			XrResult res = xrCreateInstance(&instanceCreateInfo, &instance);
            XRCHECK(res);

            if (XR_FAILED(res))
            {
                return false;
            }

            resultToStringInstance = instance;
        }

        // Set up the function pointers to things not included by the default OpenXR loader
#define LOAD_XR_FUNC(name) XRCHECK(xrGetInstanceProcAddr(instance, #name, (PFN_xrVoidFunction*)&name));
        LOAD_XR_FUNC(xrGetVulkanGraphicsRequirementsKHR);
        LOAD_XR_FUNC(xrGetVulkanGraphicsDeviceKHR);
        LOAD_XR_FUNC(xrGetVulkanInstanceExtensionsKHR);
        LOAD_XR_FUNC(xrGetVulkanDeviceExtensionsKHR);
        LOAD_XR_FUNC(xrCreateDebugUtilsMessengerEXT);
#ifndef __ANDROID__
        LOAD_XR_FUNC(xrGetVisibilityMaskKHR);
#else
        xrGetVisibilityMaskKHR = nullptr;
        LOAD_XR_FUNC(xrPerfSettingsSetPerformanceLevelEXT);
        LOAD_XR_FUNC(xrEnumerateDisplayRefreshRatesFB);
        LOAD_XR_FUNC(xrRequestDisplayRefreshRateFB);
#endif
#undef LOAD_XR_FUNC

        XrDebugUtilsMessengerCreateInfoEXT mci{ XR_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT };
        mci.messageSeverities = XR_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT | XR_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT;
        mci.messageTypes = XR_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | XR_DEBUG_UTILS_MESSAGE_TYPE_CONFORMANCE_BIT_EXT | XR_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT | XR_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT;
        mci.userCallback = oxrDebugCallback;
        XRCHECK(xrCreateDebugUtilsMessengerEXT(instance, &mci, &messenger));
        return true;
    }

    void OpenXRInterface::startSession()
    {
		XrSessionBeginInfo beginInfo{ XR_TYPE_SESSION_BEGIN_INFO };
		beginInfo.primaryViewConfigurationType = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;
		XRCHECK(xrBeginSession(session, &beginInfo));

		if (xrPerfSettingsSetPerformanceLevelEXT)
		{
			XRCHECK(xrPerfSettingsSetPerformanceLevelEXT(session, XR_PERF_SETTINGS_DOMAIN_CPU_EXT, XR_PERF_SETTINGS_LEVEL_SUSTAINED_LOW_EXT));
			XRCHECK(xrPerfSettingsSetPerformanceLevelEXT(session, XR_PERF_SETTINGS_DOMAIN_GPU_EXT, XR_PERF_SETTINGS_LEVEL_SUSTAINED_HIGH_EXT));
		}

		if (xrRequestDisplayRefreshRateFB)
		{
			XRCHECK(xrRequestDisplayRefreshRateFB(session, 120.0f));
		}

		// When re-initialising VR we want to carry over the previously loaded action json
		if (!loadedActionJson.empty() && actionSets.empty())
		{
			loadActionJson(loadedActionJson.c_str());
		}

		sessionRunning = true;
    }

    bool OpenXRInterface::init()
    {
        ZoneScoped;
        // if we don't have an instance, try creating it
		if (!hasInstance)
			hasInstance = createInstance();

        // if that still fails give up
        if (!hasInstance)
            return false;

        if (initialized)
        {
            logWarn(WELogCategoryVR, "VR already initialized!");
            return false;
        }

        VKRenderer* renderer = (VKRenderer*)g_renderer;
        const R2::VK::Handles* handles = renderer->getCore()->GetHandles();

        // Get the XR system
        XrSystemGetInfo systemGetInfo{ XR_TYPE_SYSTEM_GET_INFO };
        systemGetInfo.formFactor = XR_FORM_FACTOR_HEAD_MOUNTED_DISPLAY;

        XrResult getSystemResult = xrGetSystem(instance, &systemGetInfo, &systemId);

        if (getSystemResult == XR_ERROR_FORM_FACTOR_UNAVAILABLE)
        {
            logWarn(WELogCategoryVR, "Couldn't find HMD!");
            return false;
        }
        else
        {
            XRCHECK(getSystemResult);
        }

        getRequiredDeviceExtensions();
        getRequiredInstanceExtensions();

        XrSystemProperties systemProperties{ XR_TYPE_SYSTEM_PROPERTIES };
        XRCHECK(xrGetSystemProperties(instance, systemId, &systemProperties));

        logMsg(WELogCategoryVR, "XR System name: %s (vendor ID %u)", systemProperties.systemName, systemProperties.vendorId);

        XrGraphicsRequirementsVulkanKHR requirements{ XR_TYPE_GRAPHICS_REQUIREMENTS_VULKAN_KHR };
        XRCHECK(xrGetVulkanGraphicsRequirementsKHR(instance, systemId, &requirements));

        VkPhysicalDevice device;
        XRCHECK(xrGetVulkanGraphicsDeviceKHR(instance, systemId, handles->Instance, &device));

        if (device != handles->PhysicalDevice)
        {
            fatalErr("OpenXR's desired graphics device didn't match the graphics device R2 picked!");
        }

        // Set up the Vulkan graphics binding
        XrGraphicsBindingVulkanKHR graphicsBinding{ XR_TYPE_GRAPHICS_BINDING_VULKAN_KHR };
        graphicsBinding.instance = handles->Instance;
        graphicsBinding.device = handles->Device;
        graphicsBinding.physicalDevice = handles->PhysicalDevice;
        graphicsBinding.queueFamilyIndex = handles->Queues.GraphicsFamilyIndex;
        graphicsBinding.queueIndex = 0;

        // Create the XR session
        XrSessionCreateInfo sessionCreateInfo{ XR_TYPE_SESSION_CREATE_INFO };
        sessionCreateInfo.systemId = systemId;
        sessionCreateInfo.next = &graphicsBinding;

        XRCHECK(xrCreateSession(instance, &sessionCreateInfo, &session));

        // Create the Stage and View reference spaces that we need.
        XrReferenceSpaceCreateInfo referenceSpaceCreateInfo { XR_TYPE_REFERENCE_SPACE_CREATE_INFO };
        referenceSpaceCreateInfo.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_STAGE;
        referenceSpaceCreateInfo.poseInReferenceSpace.orientation = { 0.0f, 0.0f, 0.0f, 1.0f };

        XRCHECK(xrCreateReferenceSpace(session, &referenceSpaceCreateInfo, &stageReferenceSpace));

        referenceSpaceCreateInfo.referenceSpaceType = XR_REFERENCE_SPACE_TYPE_VIEW;

        XRCHECK(xrCreateReferenceSpace(session, &referenceSpaceCreateInfo, &viewReferenceSpace));

        XrViewConfigurationProperties viewConfigProps{ XR_TYPE_VIEW_CONFIGURATION_PROPERTIES };
        XRCHECK(xrGetViewConfigurationProperties(instance, systemId, XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO, &viewConfigProps));

        // Enumerate the view configurations
        uint32_t viewCount;
        XRCHECK(xrEnumerateViewConfigurationViews(
                instance, systemId, XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO,
                0, &viewCount, nullptr));

        if (viewCount == 0)
        {
            fatalErr("Nooo views?! <:(");
        }

        viewConfigViews.resize(viewCount, { XR_TYPE_VIEW_CONFIGURATION_VIEW });
        views.resize(viewCount, { XR_TYPE_VIEW });

        XRCHECK(xrEnumerateViewConfigurationViews(
                instance, systemId, XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO,
                (uint32_t)viewConfigViews.size(), &viewCount, viewConfigViews.data()));

        // Make sure the right swapchain format is supported
        std::vector<int64_t> swapchainFormats;
        uint32_t numSwapchainFormats;
        XRCHECK(xrEnumerateSwapchainFormats(session, 0, &numSwapchainFormats, nullptr));
        swapchainFormats.resize(numSwapchainFormats);
        XRCHECK(xrEnumerateSwapchainFormats(session, numSwapchainFormats, &numSwapchainFormats,
                                            swapchainFormats.data()));

        // For now, we're only going to support R8G8B8A8_SRGB. I think this'll be fine since this is
        // the format that's most likely to be supported, but in case this is required somewhere,
        // it shouldn't be too difficult to fix up.
        bool foundFormat = false;
        for (int64_t format : swapchainFormats)
        {
            logVrb(WELogCategoryVR, "Found OpenXR swapchain format %zu", format);
            if (format == VK_FORMAT_R8G8B8A8_SRGB)
            {
                foundFormat = true;
            }
        }

        if (!foundFormat)
        {
            fatalErr("Couldn't find correct OpenXR swapchain format");
        }

        // Create the OpenXR swapchains
		const XrViewConfigurationView& viewConfigView = viewConfigViews[0];

		XrSwapchainCreateInfo swapchainCreateInfo{ XR_TYPE_SWAPCHAIN_CREATE_INFO };
		swapchainCreateInfo.arraySize = viewCount;
		swapchainCreateInfo.format = VK_FORMAT_R8G8B8A8_SRGB;
		swapchainCreateInfo.width = viewConfigView.recommendedImageRectWidth;
		swapchainCreateInfo.height = viewConfigView.recommendedImageRectHeight;
		swapchainCreateInfo.mipCount = 1;
		swapchainCreateInfo.faceCount = 1;
		swapchainCreateInfo.sampleCount = 1;
		// We always write directly to the swapchain on Android
#ifndef __ANDROID__
		swapchainCreateInfo.usageFlags = XR_SWAPCHAIN_USAGE_TRANSFER_DST_BIT | XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT;
#else
		swapchainCreateInfo.usageFlags = XR_SWAPCHAIN_USAGE_COLOR_ATTACHMENT_BIT;
#endif

		colorSwapchain = new OpenXRSwapchain(session, swapchainCreateInfo, renderer->getCore());

        if (canSubmitDepth)
        {
			XrSwapchainCreateInfo depthSwapchainCreateInfo{ XR_TYPE_SWAPCHAIN_CREATE_INFO };
			depthSwapchainCreateInfo.arraySize = viewCount;
			depthSwapchainCreateInfo.format = VK_FORMAT_D32_SFLOAT;
			depthSwapchainCreateInfo.width = viewConfigView.recommendedImageRectWidth;
			depthSwapchainCreateInfo.height = viewConfigView.recommendedImageRectHeight;
			depthSwapchainCreateInfo.mipCount = 1;
			depthSwapchainCreateInfo.faceCount = 1;
			depthSwapchainCreateInfo.sampleCount = 1;
			depthSwapchainCreateInfo.usageFlags = XR_SWAPCHAIN_USAGE_TRANSFER_DST_BIT | XR_SWAPCHAIN_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
            depthSwapchain = new OpenXRSwapchain(session, depthSwapchainCreateInfo, renderer->getCore());
        }

        initialized = true;
		logVrb(WELogCategoryVR, "Swapchain res %ix%i", swapchainCreateInfo.width, swapchainCreateInfo.height);

        return true;
    }

    void OpenXRInterface::deinit()
    {
        if (!initialized) return;
        sessionRunning = false;

        delete colorSwapchain;
        colorSwapchain = nullptr;

        if (depthSwapchain)
        {
            delete depthSwapchain;
            depthSwapchain = nullptr;
        }

        XRCHECK(xrDestroySpace(stageReferenceSpace));
        XRCHECK(xrDestroySession(session));
        initialized = false;

        for (auto& pair : actionSets)
        {
            delete pair.second;
        }

        actionsInternal.clear();
        actionSets.clear();
        activeActionSet.clear();
    }

    bool OpenXRInterface::isInitialized() const
    {
        return initialized;
    }

    bool OpenXRInterface::hasFocus() const
    {
        return isFocused;
    }

    std::string OpenXRInterface::getSystemName()
    {
        XrSystemProperties systemProperties{ XR_TYPE_SYSTEM_PROPERTIES };
        XRCHECK(xrGetSystemProperties(instance, systemId, &systemProperties));

        return { systemProperties.systemName };
    }

    OpenXRInterface::~OpenXRInterface()
    {
        deinit();
        if (hasInstance)
			XRCHECK(xrDestroyInstance(instance));
    }

    const char* sessionStateNames[] = {
        "Unknown",
        "Idle",
        "Ready",
        "Synchronized",
        "Visible",
        "Focused",
        "Stopping",
        "Loss Pending",
        "Exiting"
    };

    void OpenXRInterface::updateEvents()
    {
        if (!initialized) return;

        XrEventDataBuffer event{};
        while (true)
        {
            event = XrEventDataBuffer{ XR_TYPE_EVENT_DATA_BUFFER };
            XrResult pollResult = xrPollEvent(instance, &event);

            if (pollResult == XR_SUCCESS)
            {
                if (event.type == XR_TYPE_EVENT_DATA_SESSION_STATE_CHANGED)
                {
                    auto* evt = (XrEventDataSessionStateChanged*)&event;
                    logMsg(WELogCategoryVR, "Session state changed: %s", sessionStateNames[(int)evt->state]);

                    switch (evt->state)
                    {
                    case XR_SESSION_STATE_READY:
                        startSession();
                        break;
                    case XR_SESSION_STATE_STOPPING:
                        XRCHECK(xrEndSession(session));
                        sessionRunning = false;
                        isFocused = false;
                        break;
                    case XR_SESSION_STATE_FOCUSED:
                        isFocused = true;
                        break;
                    }
                }
            }
            else if (pollResult == XR_EVENT_UNAVAILABLE)
            {
                break;
            }
            else
            {
                fatalErr("xrPollEvent failed!");
            }
        }

        if (actionSets.size() > 0)
        {
            ActionSet* activeSet = actionSets[activeActionSet];
            XrActiveActionSet activeActionSet{ activeSet->actionSet, (XrPath)XR_NULL_HANDLE };
            XrActionsSyncInfo actionSyncInfo{ XR_TYPE_ACTIONS_SYNC_INFO };
            actionSyncInfo.countActiveActionSets = 1;
            actionSyncInfo.activeActionSets = &activeActionSet;
            XRCHECK(xrSyncActions(session, &actionSyncInfo));
        }

        uint32_t viewCount;
        XRCHECK(xrEnumerateViewConfigurationViews(
                instance, systemId, XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO,
                (uint32_t)viewConfigViews.size(), &viewCount, viewConfigViews.data()));

        if (viewConfigViews[0].recommendedImageRectWidth != colorSwapchain->width ||
            viewConfigViews[0].recommendedImageRectHeight != colorSwapchain->height)
        {
            logWarn(WELogCategoryVR, "OpenXR swapchain out of date");
        }
    }

    void OpenXRInterface::getRenderResolution(uint32_t* x, uint32_t* y)
    {
        *x = viewConfigViews[0].recommendedImageRectWidth;
        *y = viewConfigViews[0].recommendedImageRectHeight;
    }

    const UnscaledTransform& OpenXRInterface::getEyeTransform(Eye eye)
    {
        switch (eye)
        {
            default:
            case Eye::LeftEye:
                return leftEyeTransform;
            case Eye::RightEye:
                return rightEyeTransform;
        }
    }

    const UnscaledTransform& OpenXRInterface::getHmdTransform()
    {
        return hmdTransform;
    }

    const glm::mat4& OpenXRInterface::getEyeProjectionMatrix(Eye eye)
    {
        switch (eye)
        {
            default:
            case Eye::LeftEye:
                return leftEyeProjectionMatrix;
            case Eye::RightEye:
                return rightEyeProjectionMatrix;
        }
    }

    bool OpenXRInterface::getHiddenAreaMesh(Eye eye, HiddenAreaMesh& mesh)
    {
        if (xrGetVisibilityMaskKHR == nullptr)
        {
            return false;
        }

        XrVisibilityMaskKHR mask { XR_TYPE_VISIBILITY_MASK_KHR };
        XRCHECK(xrGetVisibilityMaskKHR(session,
                                       XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO, (int)eye,
                                       XR_VISIBILITY_MASK_TYPE_HIDDEN_TRIANGLE_MESH_KHR, &mask));

        if (mask.vertexCountOutput == 0 || mask.indexCountOutput == 0)
        {
            return false;
        }

        mesh.indices.resize(mask.indexCountOutput);
        mesh.verts.resize(mask.vertexCountOutput);
        mask.vertexCapacityInput = mesh.verts.size();
        mask.indexCapacityInput = mesh.indices.size();
        mask.indices = mesh.indices.data();
        mask.vertices = (XrVector2f*)mesh.verts.data();

        XRCHECK(xrGetVisibilityMaskKHR(session,
                                       XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO, (int)eye,
                                       XR_VISIBILITY_MASK_TYPE_HIDDEN_TRIANGLE_MESH_KHR, &mask));

        return true;
    }

    void OpenXRInterface::loadActionJson(const char* path)
    {
        if (!initialized)
        {
			loadedActionJson = path;
            return;
        }

        auto loadResult = LoadFileToString(path);
        if (loadResult.error != IOError::None)
        {
            logWarn(WELogCategoryVR, "Failed to load action json %s", path);
            return;
        }

        // Action JSON files are a combination of a number of action sets and suggested
        // bindings for various kinds of OpenXR controller.
        // They're structured as so:
        // - action_sets
        //     Object containing action set JSON objects, with a name key (e.g. "gameplay")
        //     An action set is structured as an object of actions with name keys and 
        //     object values with the following properties:
        //       - type: one of "vector2f", "boolean", "float", "pose", "vibration_output"
        //       - subactions: array of paths to OpenXR subactions (e.g. /user/hand/left)
        // - suggested_bindings
        //     Object containing objects for bindings, keyed by interaction profile
        //     (e.g. /interaction_profiles/oculus/touch_controller)
        //     A binding object has arrays of bindings points keyed 
        //     by action path (e.g. "/gameplay/movement")
        XrActionSet activeSet = XR_NULL_HANDLE;
        nlohmann::json j = nlohmann::json::parse(loadResult.value);
        for (auto& actionSetPair : j["action_sets"].items())
        {
            ActionSet* set = new ActionSet(instance, session, actionSetPair.key(), actionSetPair.value());
            actionSets.insert({ actionSetPair.key(), set });
            activeActionSet = actionSetPair.key();
            activeSet = set->actionSet;
        }

        if (activeSet == XR_NULL_HANDLE)
        {
            logErr(WELogCategoryVR, "Action json file didn't have an action set!");
            return;
        }

        for (auto& suggestedBindingPair : j["suggested_bindings"].items())
        {
            std::string interactionProfile = suggestedBindingPair.key();
            XrInteractionProfileSuggestedBinding suggestedBinding
                { XR_TYPE_INTERACTION_PROFILE_SUGGESTED_BINDING };

            std::vector<XrActionSuggestedBinding> suggestedActionBindings;

            for (auto& actionBinding : suggestedBindingPair.value().items())
            {
                // Parse the full action path by splitting on the / and finding the set
                // name and the action name
                std::string actionPath = actionBinding.key();
                size_t secondSlashPos = actionPath.find('/', 1);
                std::string actionSetName = actionPath.substr(1, secondSlashPos - 1);
                std::string actionName = actionPath.substr(secondSlashPos + 1);

                ActionSet* set = actionSets[actionSetName];
                XrAction action = actionsInternal[actionNameToHandle[actionName]].action;

                for (auto& inputSource : actionBinding.value())
                {
                    XrActionSuggestedBinding asb{ action };
                    asb.binding = getXrPath(inputSource.get<std::string>().c_str());
                    suggestedActionBindings.push_back(asb);
                }
            }

            suggestedBinding.interactionProfile = getXrPath(interactionProfile.c_str());
            suggestedBinding.countSuggestedBindings = suggestedActionBindings.size();
            suggestedBinding.suggestedBindings = suggestedActionBindings.data();

            XRCHECK(xrSuggestInteractionProfileBindings(instance, &suggestedBinding));
        }

        XrSessionActionSetsAttachInfo attachInfo{ XR_TYPE_SESSION_ACTION_SETS_ATTACH_INFO };
        attachInfo.actionSets = &activeSet;
        attachInfo.countActionSets = 1;
        XRCHECK(xrAttachSessionActionSets(session, &attachInfo));
        loadedActionJson = path;
    }

    uint64_t OpenXRInterface::getActionHandle(const char* actionSet, const char* action)
    {
        auto setIterator = actionSets.find(actionSet);
        if (setIterator == actionSets.end())
        {
            logErr(WELogCategoryVR, "Couldn't find action set %s", actionSet);
            return 0;
        }

        auto actionIterator = actionNameToHandle.find(action);
        if (actionIterator == actionNameToHandle.end())
        {
            logErr(WELogCategoryVR, "Couldn't find action %s", action);
            return 0;
        }

        return actionIterator->second;
    }

    uint64_t OpenXRInterface::getSubactionHandle(const char* subaction)
    {
        if (!initialized)
            return XR_NULL_PATH;

        return (uint64_t)getXrPath(subaction);
    }

    BooleanActionState OpenXRInterface::getBooleanActionState(uint64_t actionHandle, uint64_t subactionHandle)
    {
        auto actionIt = actionsInternal.find(actionHandle);
        if (actionIt == actionsInternal.end())
        {
            logErr(WELogCategoryVR, "getBooleanActionState was passed an invalid actionHandle!");
            return BooleanActionState{};
        }

        auto action = actionIt->second.action;
        XrActionStateGetInfo stateGetInfo { XR_TYPE_ACTION_STATE_GET_INFO };
        stateGetInfo.action = action;
        if (subactionHandle != 0)
        {
            stateGetInfo.subactionPath = (XrPath)subactionHandle;
        }

        XrActionStateBoolean xrState{ XR_TYPE_ACTION_STATE_BOOLEAN };
        XRCHECK(xrGetActionStateBoolean(session, &stateGetInfo, &xrState));

        BooleanActionState state{};
        state.changedSinceLastFrame = xrState.changedSinceLastSync;
        state.currentState = xrState.currentState;

        return state;
    }

    FloatActionState OpenXRInterface::getFloatActionState(uint64_t actionHandle, uint64_t subactionHandle)
    {
        auto actionIt = actionsInternal.find(actionHandle);
        if (actionIt == actionsInternal.end())
        {
            logErr(WELogCategoryVR, "getFloatActionState was passed an invalid actionHandle!");
            return FloatActionState{};
        }

        auto action = actionIt->second.action;
        XrActionStateGetInfo stateGetInfo { XR_TYPE_ACTION_STATE_GET_INFO };
        stateGetInfo.action = action;
        if (subactionHandle != 0)
        {
            stateGetInfo.subactionPath = (XrPath)subactionHandle;
        }

        XrActionStateFloat xrState{ XR_TYPE_ACTION_STATE_FLOAT };
        XRCHECK(xrGetActionStateFloat(session, &stateGetInfo, &xrState));

        FloatActionState state{};
        state.changedSinceLastFrame = xrState.changedSinceLastSync;
        state.currentState = xrState.currentState;

        return state;
    }

    Vector2fActionState OpenXRInterface::getVector2fActionState(uint64_t actionHandle, uint64_t subactionHandle)
    {
        auto actionIt = actionsInternal.find(actionHandle);
        if (actionIt == actionsInternal.end())
        {
            logErr(WELogCategoryVR, "getVector2fActionState was passed an invalid actionHandle!");
            return Vector2fActionState{};
        }

        auto action = actionIt->second.action;
        XrActionStateGetInfo stateGetInfo { XR_TYPE_ACTION_STATE_GET_INFO };
        stateGetInfo.action = action;
        if (subactionHandle != 0)
        {
            stateGetInfo.subactionPath = (XrPath)subactionHandle;
        }

        XrActionStateVector2f xrState{ XR_TYPE_ACTION_STATE_VECTOR2F };
        XRCHECK(xrGetActionStateVector2f(session, &stateGetInfo, &xrState));

        Vector2fActionState state{};
        state.changedSinceLastFrame = xrState.changedSinceLastSync;
        state.currentState = glm::vec2(xrState.currentState.x, xrState.currentState.y);

        return state;
    }

    UnscaledTransform OpenXRInterface::getPoseActionState(uint64_t actionHandle, uint64_t subactionHandle)
    {
        if (!actionsInternal.contains(actionHandle))
        {
            logErr(WELogCategoryVR, "getPoseActionState was passed an invalid actionHandle!");
            return UnscaledTransform{};
        }

        const ActionInternal& actionInternal = actionsInternal[actionHandle];

        XrSpaceLocation spaceLocation{ XR_TYPE_SPACE_LOCATION };
        XrSpace poseSpace;

        if (subactionHandle == 0)
        {
            poseSpace = actionInternal.poseSpace;
        }
        else
        {
            if (!actionInternal.poseSubactionSpaces.contains((XrPath)subactionHandle))
            {
                logErr(WELogCategoryVR, "getPoseActionState was passed an invalid subactionHandle!");
                return UnscaledTransform{};
            }
            poseSpace = actionInternal.poseSubactionSpaces.at((XrPath)subactionHandle);
        }

        XRCHECK(xrLocateSpace(poseSpace, stageReferenceSpace, nextDisplayTime, &spaceLocation));

        UnscaledTransform transform{};
        XrVector3f position = spaceLocation.pose.position;
        transform.position = glm::vec3(-position.x, position.y, -position.z);

        XrQuaternionf rot = spaceLocation.pose.orientation;
        transform.rotation = glm::quat{rot.w, -rot.x, rot.y, -rot.z};

        return transform;
    }

    void OpenXRInterface::applyHapticFeedback(float duration, float frequency, float amplitude, uint64_t actionHandle, uint64_t subactionHandle)
    {       
        auto actionIt = actionsInternal.find(actionHandle);
        if (actionIt == actionsInternal.end())
        {
            logErr(WELogCategoryVR, "applyHapticFeedback was passed an invalid actionHandle!");
            return;
        }

        auto action = actionIt->second.action;

        XrHapticVibration vibration{ XR_TYPE_HAPTIC_VIBRATION };
        // Convert duration from float seconds to int nanoseconds
        vibration.duration = (int64_t)(duration * 1000.0f) * 1000 * 1000;
        vibration.frequency = frequency;
        vibration.amplitude = amplitude;

        XrHapticActionInfo actionInfo{ XR_TYPE_HAPTIC_ACTION_INFO };
        actionInfo.action = action;
        if (actionHandle != 0)
            actionInfo.subactionPath = subactionHandle;

        XRCHECK(xrApplyHapticFeedback(session, &actionInfo, (XrHapticBaseHeader*)&vibration));
    }

    void OpenXRInterface::beginFrame()
    {
        ZoneScoped;
        if (!sessionRunning) return;
        XrFrameBeginInfo frameBeginInfo{ XR_TYPE_FRAME_BEGIN_INFO };
        XRCHECK(xrBeginFrame(session, &frameBeginInfo));
    }

    // Build a reverse-Z infinite projection matrix from XrFovf
    void composeProjection(XrFovf fov, float zNear, glm::mat4& p)
    {
        float left = tanf(fov.angleLeft);
        float right = tanf(fov.angleRight);
        float up = tanf(fov.angleUp);
        float down = tanf(fov.angleDown);

        float idx = 1.0f / (right - left);
        float idy = 1.0f / (up - down);
        float sx = right + left;
        float sy = down + up;

        p[0][0] = 2 * idx; p[1][0] = 0;       p[2][0] = sx * idx;    p[3][0] = 0;
        p[0][1] = 0;       p[1][1] = 2 * idy; p[2][1] = sy * idy;    p[3][1] = 0;
        p[0][2] = 0;       p[1][2] = 0;       p[2][2] = 0.0f; p[3][2] = zNear;
        p[0][3] = 0;       p[1][3] = 0;       p[2][3] = -1.0f;       p[3][3] = 0;
    }

    void OpenXRInterface::waitFrame()
    {
        ZoneScoped;
        if (!sessionRunning) return;

        XrFrameWaitInfo waitInfo{ XR_TYPE_FRAME_WAIT_INFO };
        XrFrameState frameState{ XR_TYPE_FRAME_STATE };
        XRCHECK(xrWaitFrame(session, &waitInfo, &frameState));
        nextDisplayTime = frameState.predictedDisplayTime;

        // Get the pose of each eye
        XrViewLocateInfo viewLocateInfo{ XR_TYPE_VIEW_LOCATE_INFO };
        viewLocateInfo.viewConfigurationType = XR_VIEW_CONFIGURATION_TYPE_PRIMARY_STEREO;
        viewLocateInfo.displayTime = nextDisplayTime;
        viewLocateInfo.space = stageReferenceSpace;

        XrViewState viewState{ XR_TYPE_VIEW_STATE };
        uint32_t numViews;
        XRCHECK(xrLocateViews(session, &viewLocateInfo, &viewState, views.size(), &numViews, views.data()));

        // Ensure that we only set valid orientations and positions
        if (enumHasFlag(viewState.viewStateFlags, XR_VIEW_STATE_ORIENTATION_VALID_BIT))
        {
            XrQuaternionf lO = views[0].pose.orientation;
            leftEyeTransform.rotation = glm::quat{lO.w, lO.x, lO.y, lO.z};

            XrQuaternionf rO = views[1].pose.orientation;
            rightEyeTransform.rotation = glm::quat{rO.w, rO.x, rO.y, rO.z};
        }

        if (enumHasFlag(viewState.viewStateFlags, XR_VIEW_STATE_POSITION_VALID_BIT))
        {
            leftEyeTransform.position = glm::make_vec3(&views[0].pose.position.x);
            rightEyeTransform.position = glm::make_vec3(&views[1].pose.position.x);
        }

        // Get the pose of the HMD
        XrSpaceLocation spaceLocation{ XR_TYPE_SPACE_LOCATION };
        XRCHECK(xrLocateSpace(viewReferenceSpace, stageReferenceSpace, nextDisplayTime, &spaceLocation));
        if (enumHasFlag(spaceLocation.locationFlags, XR_SPACE_LOCATION_ORIENTATION_VALID_BIT))
        {
            XrQuaternionf o = spaceLocation.pose.orientation;
            hmdTransform.rotation = glm::quat{o.w, -o.x, o.y, -o.z};
        }

        if (enumHasFlag(spaceLocation.locationFlags, XR_SPACE_LOCATION_POSITION_VALID_BIT))
        {
            XrVector3f position = spaceLocation.pose.position;
            hmdTransform.position = glm::vec3(-position.x, position.y, -position.z);
        }

        // Set projection matrices
        leftEyeProjectionMatrix = glm::mat4{ 1.0f };
        rightEyeProjectionMatrix = glm::mat4{ 1.0f };
        composeProjection(views[0].fov, 0.01f, leftEyeProjectionMatrix);
        composeProjection(views[1].fov, 0.01f, rightEyeProjectionMatrix);
    }

    void OpenXRInterface::submitLayered(R2::VK::CommandBuffer& cb, R2::VK::Texture* texture)
    {
        ZoneScoped;
        if (!sessionRunning) return;

        releaseAssert(texture->GetWidth() == colorSwapchain->width);
        releaseAssert(texture->GetHeight() == colorSwapchain->height);

        texture->Acquire(cb,
			 VK::ImageLayout::TransferSrcOptimal,
			 VK::AccessFlags::TransferRead, VK::PipelineStageFlags::Transfer);

        XrSwapchainImageAcquireInfo acquireInfo{ XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO };

        uint32_t imageIndex;
        XRCHECK(xrAcquireSwapchainImage(colorSwapchain->swapchain, &acquireInfo, &imageIndex));

        XrSwapchainImageWaitInfo waitInfo{ XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO };
        waitInfo.timeout = XR_INFINITE_DURATION;
        XRCHECK(xrWaitSwapchainImage(colorSwapchain->swapchain, &waitInfo));

        VK::Texture* destTexture = colorSwapchain->textures[imageIndex];

        VK::TextureCopy textureCopy
        {
            .Source = {
                .MipLevel = 0,
                .LayerStart = 0,
                .LayerCount = 2
			},
            .SourceOffset = { 0, 0, 0 },
            .Destination = {
                .MipLevel = 0,
                .LayerStart = 0,
                .LayerCount = 2
			},
            .DestinationOffset = { 0, 0, 0 },
            .Extent = { (uint32_t)texture->GetWidth(), (uint32_t)texture->GetHeight(), 1 }
        };

        cb.TextureCopy(texture, destTexture, textureCopy);

        destTexture->Acquire(cb, 
            VK::ImageLayout::ColorAttachmentOptimal,
            VK::AccessFlags::ColorAttachmentReadWrite, VK::PipelineStageFlags::AllGraphics);

        XrSwapchainImageReleaseInfo releaseInfo{ XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO };
        XRCHECK(xrReleaseSwapchainImage(colorSwapchain->swapchain, &releaseInfo));
    }

    void OpenXRInterface::submitLayeredDepth(R2::VK::CommandBuffer& cb, R2::VK::Texture* texture)
    {
        /*ZoneScoped;
        if (!sessionRunning) return;

        releaseAssert(texture->GetWidth() == depthSwapchain->width);
        releaseAssert(texture->GetHeight() == depthSwapchain->height);

        texture->Acquire(cb,
			 VK::ImageLayout::TransferSrcOptimal,
			 VK::AccessFlags::TransferRead, VK::PipelineStageFlags::Transfer);

        XrSwapchainImageAcquireInfo acquireInfo{ XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO };

        uint32_t imageIndex;
        XRCHECK(xrAcquireSwapchainImage(depthSwapchain->swapchain, &acquireInfo, &imageIndex));

        XrSwapchainImageWaitInfo waitInfo{ XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO };
        waitInfo.timeout = XR_INFINITE_DURATION;
        XRCHECK(xrWaitSwapchainImage(depthSwapchain->swapchain, &waitInfo));

        VK::Texture* destTexture = depthSwapchain->textures[imageIndex];

        VK::TextureCopy textureCopy
        {
            .Source = {
                .MipLevel = 0,
                .LayerStart = 0,
                .LayerCount = 2
			},
            .SourceOffset = { 0, 0, 0 },
            .Destination = {
                .MipLevel = 0,
                .LayerStart = 0,
                .LayerCount = 2
			},
            .DestinationOffset = { 0, 0, 0 },
            .Extent = { (uint32_t)texture->GetWidth(), (uint32_t)texture->GetHeight(), 1 }
        };

        cb.TextureCopy(texture, destTexture, textureCopy);

        destTexture->Acquire(cb, 
            VK::ImageLayout::DepthStencilAttachmentOptimal,
            VK::AccessFlags::DepthStencilAttachmentReadWrite, VK::PipelineStageFlags::AllGraphics);

        XrSwapchainImageReleaseInfo releaseInfo{ XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO };
        XRCHECK(xrReleaseSwapchainImage(depthSwapchain->swapchain, &releaseInfo));*/
    }

    VK::Texture* OpenXRInterface::acquireSwapchainImage(R2::VK::CommandBuffer& cb)
    {
        XrSwapchainImageAcquireInfo acquireInfo{ XR_TYPE_SWAPCHAIN_IMAGE_ACQUIRE_INFO };

        uint32_t imageIndex;
        XRCHECK(xrAcquireSwapchainImage(colorSwapchain->swapchain, &acquireInfo, &imageIndex));

        XrSwapchainImageWaitInfo waitInfo{ XR_TYPE_SWAPCHAIN_IMAGE_WAIT_INFO };
        waitInfo.timeout = XR_INFINITE_DURATION;
        XRCHECK(xrWaitSwapchainImage(colorSwapchain->swapchain, &waitInfo));

        VK::Texture* tex = colorSwapchain->textures[imageIndex];
        tex->Acquire(cb, VK::ImageLayout::ColorAttachmentOptimal,
                     VK::AccessFlags::ColorAttachmentWrite, VK::PipelineStageFlags::ColorAttachmentOutput);

        return tex;
    }

    void OpenXRInterface::releaseSwapchainImage()
    {
        XrSwapchainImageReleaseInfo releaseInfo{ XR_TYPE_SWAPCHAIN_IMAGE_RELEASE_INFO };
        XRCHECK(xrReleaseSwapchainImage(colorSwapchain->swapchain, &releaseInfo));
    }

    void OpenXRInterface::endFrame()
    {
        ZoneScoped;
        if (!sessionRunning) return;
        XrCompositionLayerProjectionView layerViews[2]{ XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW };
        XrCompositionLayerDepthInfoKHR depthInfos[2]{ XR_TYPE_COMPOSITION_LAYER_DEPTH_INFO_KHR };

        for (int i = 0; i < 2; i++)
        {
            layerViews[i].type = XR_TYPE_COMPOSITION_LAYER_PROJECTION_VIEW;
            layerViews[i].fov = views[i].fov;
            layerViews[i].pose = views[i].pose;
            layerViews[i].subImage.swapchain = colorSwapchain->swapchain;
            layerViews[i].subImage.imageArrayIndex = i;
            layerViews[i].subImage.imageRect.extent = {
				(int)viewConfigViews[i].recommendedImageRectWidth,
				(int)viewConfigViews[i].recommendedImageRectHeight
            };

            // This *should* work. It doesn't cause validation errors, and I can't find anything
            // in the spec suggesting that it's wrong. But Oculus's XR runtime reports layers using
            // this as invalid
            /*if (canSubmitDepth)
            {
                layerViews[i].next = &depthInfos[i];

                depthInfos[i].type = XR_TYPE_COMPOSITION_LAYER_DEPTH_INFO_KHR;
                depthInfos[i].farZ = interfaces.mainCamera->near;
                depthInfos[i].nearZ = std::numeric_limits<float>::infinity();
                depthInfos[i].minDepth = 0.0f;
                depthInfos[i].maxDepth = 1.0f;
                depthInfos[i].subImage.swapchain = depthSwapchain->swapchain;
                depthInfos[i].subImage.imageArrayIndex = i;
                depthInfos[i].subImage.imageRect.extent = {
                    (int)viewConfigViews[i].recommendedImageRectWidth,
                    (int)viewConfigViews[i].recommendedImageRectHeight
                };
            }*/
        }

        XrCompositionLayerProjection layer{ XR_TYPE_COMPOSITION_LAYER_PROJECTION };
        layer.space = stageReferenceSpace;
        layer.viewCount = 2;
        layer.views = layerViews;

        XrCompositionLayerProjection* layerPtr = &layer;

        XrFrameEndInfo frameEndInfo{ XR_TYPE_FRAME_END_INFO };
        frameEndInfo.displayTime = nextDisplayTime;
        frameEndInfo.environmentBlendMode = XR_ENVIRONMENT_BLEND_MODE_OPAQUE;
        frameEndInfo.layers = (XrCompositionLayerBaseHeader**)&layerPtr;
        frameEndInfo.layerCount = 1;
        XRCHECK(xrEndFrame(session, &frameEndInfo));
    }

    XrPath OpenXRInterface::getXrPath(const char* path)
    {
        XrPath ret;
        XRCHECK(xrStringToPath(instance, path, &ret));
        return ret;
    }
}

#pragma once
#include "AssetCompilers.hpp"
#include <nlohmann/json_fwd.hpp>

namespace worlds
{
    struct CrunchTextureSettings
    {
        bool isNormalMap;
        int qualityLevel;
        bool isSrgb;
        bool hasAlpha;
        AssetID sourceTexture;
    };

    struct RGBATextureSettings
    {
        bool isSrgb;
        AssetID sourceTexture;
    };

    struct PBRTextureSettings
    {
        AssetID roughnessSource;
        AssetID metallicSource;
        AssetID occlusionSource;
        AssetID normalMap;
        float normalRoughnessMipStrength;
        float defaultRoughness;
        float defaultMetallic;
        int qualityLevel;
    };

    struct CubemapTextureSettings
    {
        // Array of faces
        // Layout is all 6 mips of face 0, then face 1 etc
        AssetID faces[36];
    };

    enum class TextureAssetType
    {
        Crunch,
        RGBA,
        PBR,
        Cubemap
    };

    struct TextureAssetSettings
    {
        TextureAssetType type;
        union
        {
            CrunchTextureSettings crunch;
            RGBATextureSettings rgba;
            PBRTextureSettings pbr;
            CubemapTextureSettings cubemap;
        };

        void initialiseForType(TextureAssetType t);
        static TextureAssetSettings fromJson(nlohmann::json& j);
        void toJson(nlohmann::json& j);
    };

    class TextureCompiler : public IAssetCompiler
    {
    public:
        TextureCompiler();
        AssetCompileOperation* compile(std::string_view projectRoot, AssetID src) override;
        void getFileDependencies(AssetID src, std::vector<std::string>& out) override;
        const char* getSourceExtension() override;
        const char* getCompiledExtension() override;

    private:
        struct TexCompileThreadInfo;
        void compileCrunch(TexCompileThreadInfo*);
        void compileRGBA(TexCompileThreadInfo*);
        void compilePBR(TexCompileThreadInfo*);
        void compileASTC(TexCompileThreadInfo*);
        void compileRGBACube(TexCompileThreadInfo*);
        void compileInternal(TexCompileThreadInfo*);
    };
}

#define CRND_HEADER_FILE_ONLY
#include "TextureCompiler.hpp"
#include "AssetCompilerUtil.hpp"
#include "RawTextureLoader.hpp"
#include <Core/Log.hpp>
#include <IO/IOUtil.hpp>
#include <Libs/crnlib/crn_texture_conversion.h>
#include <SDL_cpuinfo.h>
#include <WTex.hpp>
#include <filesystem>
#include <nlohmann/json.hpp>
#include <thread>
#include <astcenc.h>
#define STB_IMAGE_RESIZE_IMPLEMENTATION
#include <stb_image_resize.h>

#include <Core/ConVar.hpp>
#include <Core/TaskScheduler.hpp>

#include "Core/Fatal.hpp"
using namespace crnlib;

namespace worlds
{
    struct TextureCompressSettings
    {
        bool isSrgb;
        bool isNormalMap;
        bool hasAlpha;
        int quality = 127;
        AssetCompileOperation* compileOp;
    };
    
    TextureCompiler::TextureCompiler()
    {
        AssetCompilers::registerCompiler(this);
    }

    texture_type getTextureType(std::string typeStr)
    {
        if (typeStr == "regular")
        {
            return cTextureTypeRegularMap;
        }
        else if (typeStr == "normal")
        {
            return cTextureTypeNormalMap;
        }
        else
        {
            logErr("Invalid texture type: %s", typeStr.c_str());
            return cTextureTypeRegularMap;
        }
    }

    struct TextureCompiler::TexCompileThreadInfo
    {
        TextureAssetSettings assetSettings;
        std::string inputPath;
        std::string outputPath;
        std::string_view projectRoot;
        AssetCompileOperation* compileOp;
        bool astcOverride;
    };

    TextureAssetType getAssetType(std::string typeStr)
    {
        if (typeStr == "crunch")
        {
            return TextureAssetType::Crunch;
        }
        else if (typeStr == "rgba")
        {
            return TextureAssetType::RGBA;
        }
        else if (typeStr == "pbr")
        {
            return TextureAssetType::PBR;
        }
        else if (typeStr == "cubemap")
        {
            return TextureAssetType::Cubemap;
        }
        else
        {
            logWarn("Invalid texture asset type: %s", typeStr.c_str());
            return TextureAssetType::RGBA;
        }
    }

    std::string getAssetTypeString(TextureAssetType t)
    {
        switch (t)
        {
        case TextureAssetType::Crunch:
            return "crunch";
        case TextureAssetType::RGBA:
            return "rgba";
        case TextureAssetType::PBR:
            return "pbr";
        case TextureAssetType::Cubemap:
            return "cubemap";
        default:
            return "unknown";
        }
    }

    void TextureAssetSettings::initialiseForType(TextureAssetType t)
    {
        type = t;
        switch (t)
        {
        case TextureAssetType::Crunch:
            crunch.sourceTexture = INVALID_ASSET;
            crunch.isNormalMap = false;
            crunch.isSrgb = true;
            crunch.qualityLevel = 127;
            crunch.hasAlpha = false;
            break;
        case TextureAssetType::RGBA:
            rgba.sourceTexture = INVALID_ASSET;
            rgba.isSrgb = true;
            break;
        case TextureAssetType::PBR:
            pbr.roughnessSource = INVALID_ASSET;
            pbr.metallicSource = INVALID_ASSET;
            pbr.occlusionSource = INVALID_ASSET;
            pbr.normalMap = INVALID_ASSET;
            pbr.normalRoughnessMipStrength = 0.0f;
            pbr.defaultRoughness = 0.5f;
            pbr.defaultMetallic = 0.0f;
            pbr.qualityLevel = 127;
            break;
        }
    }

    void loadCrunchSettings(TextureAssetSettings& settings, nlohmann::json& j)
    {
        settings.crunch.isNormalMap = j.value("isNormalMap", false);
        settings.crunch.qualityLevel = j.value("qualityLevel", 127);
        settings.crunch.isSrgb = j.value("isSrgb", true);
        if (j.contains("sourceTexture"))
            settings.crunch.sourceTexture = AssetDB::pathToId(j["sourceTexture"].get<std::string>());
        else
            settings.crunch.sourceTexture = INVALID_ASSET;
        settings.crunch.hasAlpha = j.value("hasAlpha", false);
    }

    void loadRGBASettings(TextureAssetSettings& settings, nlohmann::json& j)
    {
        settings.rgba.sourceTexture = AssetDB::pathToId(j["sourceTexture"].get<std::string>());
        settings.rgba.isSrgb = j.value("isSrgb", true);
    }

    void loadPBRSettings(TextureAssetSettings& settings, nlohmann::json& j)
    {
        settings.pbr.defaultMetallic = j.value("defaultMetallic", 0.0f);
        settings.pbr.defaultRoughness = j.value("defaultRoughness", 0.0f);

        if (j.contains("metallicSource"))
            settings.pbr.metallicSource = AssetDB::pathToId(j["metallicSource"].get<std::string>());
        else
            settings.pbr.metallicSource = INVALID_ASSET;

        if (j.contains("roughnessSource"))
            settings.pbr.roughnessSource = AssetDB::pathToId(j["roughnessSource"].get<std::string>());
        else
            settings.pbr.roughnessSource = INVALID_ASSET;

        if (j.contains("occlusionSource"))
            settings.pbr.occlusionSource = AssetDB::pathToId(j["occlusionSource"].get<std::string>());
        else
            settings.pbr.occlusionSource = INVALID_ASSET;

        if (j.contains("normalMap"))
            settings.pbr.normalMap = AssetDB::pathToId(j["normalMap"].get<std::string>());
        else
            settings.pbr.normalMap = INVALID_ASSET;

        settings.pbr.normalRoughnessMipStrength = j.value("normalRoughnessMipStrength", 0.0f);
        settings.pbr.qualityLevel = j.value("qualityLevel", 127);
    }

    void loadCubemapSettings(TextureAssetSettings& settings, nlohmann::json& j)
    {
        for (int i = 0; i < 36; i++)
        {
            settings.cubemap.faces[i] = AssetDB::pathToId(j["faces"][i].get<std::string>());
        }
    }

    TextureAssetSettings TextureAssetSettings::fromJson(nlohmann::json& j)
    {
        TextureAssetSettings s{};

        s.type = getAssetType(j.value("type", "crunch"));

        switch (s.type)
        {
        case TextureAssetType::Crunch:
            loadCrunchSettings(s, j);
            break;
        case TextureAssetType::RGBA:
            loadRGBASettings(s, j);
            break;
        case TextureAssetType::PBR:
            loadPBRSettings(s, j);
            break;
        case TextureAssetType::Cubemap:
            loadCubemapSettings(s, j);
            break;
        }

        return s;
    }

    void saveCrunchSettings(TextureAssetSettings& settings, nlohmann::json& j)
    {
        CrunchTextureSettings& cts = settings.crunch;
        j["isNormalMap"] = cts.isNormalMap;
        j["qualityLevel"] = cts.qualityLevel;
        j["isSrgb"] = cts.isSrgb;
        j["hasAlpha"] = cts.hasAlpha;
        j["sourceTexture"] = AssetDB::idToPath(cts.sourceTexture);
    }

    void savePBRSettings(TextureAssetSettings& settings, nlohmann::json& j)
    {
        PBRTextureSettings& pbs = settings.pbr;
        j["defaultMetallic"] = pbs.defaultMetallic;
        j["defaultRoughness"] = pbs.defaultRoughness;

        if (pbs.metallicSource != INVALID_ASSET)
        {
            j["metallicSource"] = AssetDB::idToPath(pbs.metallicSource);
        }

        if (pbs.roughnessSource != INVALID_ASSET)
        {
            j["roughnessSource"] = AssetDB::idToPath(pbs.roughnessSource);
        }

        if (pbs.occlusionSource != INVALID_ASSET)
        {
            j["occlusionSource"] = AssetDB::idToPath(pbs.occlusionSource);
        }

        j["normalRoughnessMipStrength"] = pbs.normalRoughnessMipStrength;
        j["qualityLevel"] = pbs.qualityLevel;
    }

    void TextureAssetSettings::toJson(nlohmann::json& j)
    {
        j["type"] = getAssetTypeString(type);

        switch (type)
        {
        case TextureAssetType::Crunch:
            saveCrunchSettings(*this, j);
            break;
        case TextureAssetType::RGBA:
            break;
        case TextureAssetType::PBR:
            savePBRSettings(*this, j);
            break;
        }
    }

    ConVar astc{"tc_astc", "0"};

    AssetCompileOperation* TextureCompiler::compile(std::string_view projectRoot, AssetID src)
    {
        std::string inputPath = AssetDB::idToPath(src);
        std::string outputPath = getOutputPath(inputPath);
        if (astc)
        {
            outputPath = "Android" + outputPath;
        }

        auto jsonContents = LoadFileToString(inputPath);
        AssetCompileOperation* compileOp = new AssetCompileOperation;

        if (jsonContents.error != IOError::None)
        {
            logErr("Error opening asset file");
            compileOp->complete = true;
            compileOp->result = CompilationResult::Error;
            return compileOp;
        }

        nlohmann::json j = nlohmann::json::parse(jsonContents.value);
        compileOp->outputId = getOutputAsset(inputPath);
        TexCompileThreadInfo* tcti = new TexCompileThreadInfo;
        tcti->assetSettings = TextureAssetSettings::fromJson(j);
        tcti->inputPath = inputPath;
        tcti->outputPath = outputPath;
        tcti->compileOp = compileOp;
        tcti->projectRoot = projectRoot;
        tcti->astcOverride = astc;
        
        std::filesystem::path fullPath = tcti->projectRoot;
        fullPath /= tcti->outputPath;
        fullPath = fullPath.parent_path();
        fullPath = fullPath.lexically_normal();

        std::filesystem::create_directories(fullPath);

        std::thread([tcti, this]() {
            compileInternal(tcti);
            delete tcti;
        }).detach();

        logMsg("Compiling %s to %s", AssetDB::idToPath(src).c_str(), outputPath.c_str());
        return compileOp;
    }

    void TextureCompiler::getFileDependencies(AssetID src, std::vector<std::string>& out)
    {
        std::string inputPath = AssetDB::idToPath(src);
        auto jsonContents = LoadFileToString(inputPath);

        if (jsonContents.error != IOError::None)
        {
            logErr("Error opening asset file");
            return;
        }

        nlohmann::json j = nlohmann::json::parse(jsonContents.value);

        TextureAssetSettings tas = TextureAssetSettings::fromJson(j);

        switch (tas.type)
        {
        case TextureAssetType::Crunch:
            out.push_back(j["sourceTexture"]);
            break;
        case TextureAssetType::PBR:
            if (j.contains("metallicSource"))
                out.push_back(j["metallicSource"]);

            if (j.contains("roughnessSource"))
                out.push_back(j["roughnessSource"]);

            if (j.contains("occlusionSource"))
                out.push_back(j["occlusionSource"]);
            break;
        case TextureAssetType::Cubemap:
            for (auto& face : j["faces"])
            {
                out.push_back(face);
            }
            break;
        }
    }

    const char* TextureCompiler::getSourceExtension()
    {
        return ".wtexj";
    }

    const char* TextureCompiler::getCompiledExtension()
    {
        return ".wtex";
    }

    RawTextureData generateMipLevel(RawTextureData rawData, int mipLevel, bool isSrgb)
    {
        assert(mipLevel > 0);

        int desiredWidth = std::max(rawData.width >> mipLevel, 1);
        int desiredHeight = std::max(rawData.height >> mipLevel, 1);
        int pixelSize = rawData.format == RawTextureFormat::RGBA8 ? 4 : 16;

        uint8_t* outputData = (uint8_t*)malloc(desiredWidth * desiredHeight * pixelSize);
        int result;
        if (rawData.format == RawTextureFormat::RGBA8)
        {
            if (isSrgb)
            {
                result = stbir_resize_uint8_srgb(
                    (uint8_t*)rawData.data, rawData.width, rawData.height, 0,
                    outputData, desiredWidth, desiredHeight, 0,
                    4, 3, 0);
            }
            else
            {
                result = stbir_resize_uint8(
                    (uint8_t*)rawData.data, rawData.width, rawData.height, 0,
                    outputData, desiredWidth, desiredHeight, 0,
                    4);
            }
        }
        else
        {
           result = stbir_resize_float(
               (float*)rawData.data, rawData.width, rawData.height, 0,
               (float*)outputData, desiredWidth, desiredHeight, 0, 4);
        }

        releaseAssert(result != 0);

        return RawTextureData
        {
            .format = rawData.format,
            .width = desiredWidth,
            .height = desiredHeight,
            .data = outputData,
            .totalDataSize = (size_t)(desiredWidth * desiredHeight * 4)
        };
    }

    crn_bool progressCallback(crn_uint32 phaseIndex, crn_uint32 totalPhases, crn_uint32 subphaseIndex,
                              crn_uint32 totalSubphases, void* data)
    {
        AssetCompileOperation* compileOp = (AssetCompileOperation*)data;
        compileOp->progress = ((float)phaseIndex + ((float)subphaseIndex / totalSubphases)) / totalPhases;
        return true;
    }
    
    void writeCrunchedWtex(std::string outPath, bool isSrgb, int width, int height, int nMips,
                                            void* data, size_t dataSize)
    {
        wtex::Header header{};
        header.containedFormat = wtex::ContainedFormat::Crunch;
        header.dataOffset = sizeof(header);
        header.dataSize = dataSize;
        header.width = width;
        header.height = height;
        header.numMipLevels = nMips;
        header.isSrgb = isSrgb;

        PHYSFS_File* outFile = PHYSFS_openWrite(outPath.c_str());
        PHYSFS_writeBytes(outFile, &header, sizeof(header));
        PHYSFS_writeBytes(outFile, data, dataSize);
        PHYSFS_close(outFile);
    }
    
    bool compressCrunchToFile(RawTextureData rawData, TextureCompressSettings compressSettings, std::string outputPath)
    {
        if (rawData.format == RawTextureFormat::RGBA32F)
            return false;

        crn_comp_params compParams;
        compParams.m_width = rawData.width;
        compParams.m_height = rawData.height;
        compParams.set_flag(cCRNCompFlagPerceptual, compressSettings.isSrgb);
        compParams.m_file_type = cCRNFileTypeCRN;
        compParams.m_format = compressSettings.isNormalMap ? cCRNFmtDXN_XY : (compressSettings.hasAlpha ? cCRNFmtDXT5 : cCRNFmtDXT1);
        compParams.m_pImages[0][0] = (crn_uint32*)rawData.data;
        compParams.m_quality_level = compressSettings.quality;
        compParams.m_num_helper_threads = SDL_GetCPUCount() - 2;

        if (compressSettings.compileOp)
        {
            compParams.m_pProgress_func = progressCallback;
            compParams.m_pProgress_func_data = compressSettings.compileOp;
        }

        compParams.m_userdata0 = compressSettings.isSrgb;

        int nMips = std::floor(std::log2(std::max(rawData.width, rawData.height))) + 1;
        RawTextureData* mipDatas = new RawTextureData[nMips];
        mipDatas[0] = rawData;

        for (int i = 1; i < nMips; i++)
        {
            mipDatas[i] = generateMipLevel(rawData, i, compressSettings.isSrgb);
        }

        crn_mipmap_params mipParams;
        mipParams.m_gamma_filtering = compressSettings.isSrgb;
        mipParams.m_mode = cCRNMipModeUseSourceMips;
        compParams.m_levels = nMips;

        for (int i = 0; i < nMips; i++)
        {
            compParams.m_pImages[0][i] = (crn_uint32*)mipDatas[i].data;
        }

        crn_uint32 outputSize;
        float actualBitrate;
        crn_uint32 actualQualityLevel;

        void* outData = crn_compress(compParams, mipParams, outputSize, &actualQualityLevel, &actualBitrate);
        writeCrunchedWtex(outputPath, compressSettings.isSrgb, rawData.width, rawData.height, nMips, outData, outputSize);

        crn_free_block(outData);

        for (int i = 1; i < nMips; i++)
        {
            free(mipDatas[i].data);
        }

        delete[] mipDatas;
        
        return true;
    }

    void TextureCompiler::compileCrunch(TexCompileThreadInfo* tcti)
    {
        const CrunchTextureSettings& cts = tcti->assetSettings.crunch;
        AssetID sourceId = cts.sourceTexture;
        bool isSrgb = cts.isSrgb;
        RawTextureData inTexData;

        if (!RawTextureLoader::loadRawTexture(sourceId, inTexData))
        {
            logErr("Failed to compile %s", AssetDB::idToPath(tcti->compileOp->outputId).c_str());
            return;
        }

        TextureCompressSettings compressSettings
        {
            .isSrgb = cts.isSrgb,
            .isNormalMap = cts.isNormalMap,
            .hasAlpha = cts.hasAlpha,
            .quality = cts.qualityLevel,
            .compileOp = tcti->compileOp
        };
        
        if (compressCrunchToFile(inTexData, compressSettings, tcti->outputPath))
        {
            tcti->compileOp->completeAsSuccess();
        }
        else
        {
            tcti->compileOp->completeAsFail();
        }

        free(inTexData.data);
    }
    
    void TextureCompiler::compileRGBA(TexCompileThreadInfo* tcti)
    {
        const RGBATextureSettings& rts = tcti->assetSettings.rgba;
        AssetID sourceId = rts.sourceTexture;
        bool isSrgb = rts.isSrgb;
        RawTextureData inTexData{};

        if (!RawTextureLoader::loadRawTexture(sourceId, inTexData))
        {
            logErr("Failed to compile %s", AssetDB::idToPath(tcti->compileOp->outputId).c_str());
            tcti->compileOp->completeAsFail();
            return;
        }

        if (inTexData.format != RawTextureFormat::RGBA8)
        {
            logErr("Invalid input format for RGBA texture");
            tcti->compileOp->completeAsFail();
            return;
        }

        wtex::Header header{};
        header.containedFormat = wtex::ContainedFormat::RGBA;
        header.dataOffset = sizeof(header);
        header.dataSize = inTexData.totalDataSize;
        header.width = inTexData.width;
        header.height = inTexData.height;
        header.numMipLevels = 1;
        header.isSrgb = isSrgb;
        
        PHYSFS_File* outFile = PHYSFS_openWrite(tcti->outputPath.c_str());
        PHYSFS_writeBytes(outFile, &header, sizeof(header));
        PHYSFS_writeBytes(outFile, inTexData.data, inTexData.totalDataSize);
        PHYSFS_close(outFile);
        
        tcti->compileOp->completeAsSuccess();
    }

    struct AstcParams
    {
        astcenc_context* context;
        astcenc_image* image;
        const astcenc_swizzle* swizzle;
        uint8_t* data_out;
        size_t data_len;
    };

    struct AstcEncodeTask : public enki::ITaskSet
    {
        AstcParams params;
        astcenc_error err = ASTCENC_SUCCESS;
        
        void ExecuteRange(enki::TaskSetPartition range, uint32_t threadNum) override
        {
            for (int i = range.start; i < range.end; i++)
            {
                astcenc_error localErr = astcenc_compress_image(
                    params.context, params.image, params.swizzle, params.data_out, params.data_len, i);

                if (localErr != ASTCENC_SUCCESS)
                {
                    err = localErr;
                }
            }
        }
    };

    struct ASTCCompressResult
    {
        size_t dataSize;
        uint8_t* data;
    };
    
    bool compressASTCWithMipsInternal(RawTextureData* mipData, int numMips, TextureCompressSettings settings, ASTCCompressResult* result)
    {
        bool isFloat = mipData[0].format == RawTextureFormat::RGBA32F;
        astcenc_profile profile = settings.isSrgb ? ASTCENC_PRF_LDR_SRGB : ASTCENC_PRF_LDR;

        if (isFloat) profile = ASTCENC_PRF_HDR;
        
        uint32_t flags = 0;
        
        if (settings.isSrgb)
        {
            flags |= ASTCENC_FLG_USE_PERCEPTUAL;
        }

        if (settings.isNormalMap)
        {
            flags |= ASTCENC_FLG_MAP_NORMAL;
        }
        
        astcenc_config config{};
        astcenc_error err;
        int blockX = 6;
        int blockY = 6;

        err = astcenc_config_init(
            profile, blockX, blockY, 1, ASTCENC_PRE_MEDIUM, flags, &config);

        if (err != ASTCENC_SUCCESS)
        {
            logErr("Failed to initialize astcenc: %s", astcenc_get_error_string(err));
            return false;
        }

        int numThreads = 8;
        astcenc_context* context;
        err = astcenc_context_alloc(&config, numThreads, &context);
        
        if (err != ASTCENC_SUCCESS)
        {
            logErr("Failed to initialize astcenc: %s", astcenc_get_error_string(err));
            return false;
        }

        astcenc_swizzle swizzle
        {
            .r = ASTCENC_SWZ_R,
            .g = ASTCENC_SWZ_G,
            .b = ASTCENC_SWZ_B,
            .a = ASTCENC_SWZ_A
        };

        if (settings.isNormalMap)
        {
            swizzle = astcenc_swizzle
            {
                .r = ASTCENC_SWZ_G,
                .g = ASTCENC_SWZ_G,
                .b = ASTCENC_SWZ_G,
                .a = ASTCENC_SWZ_R
            };
        }

        size_t totalDataLen = 0;

        for (int mipIndex = 0; mipIndex < numMips; mipIndex++)
        {
            int blockCountX = (mipData[mipIndex].width + blockX - 1) / blockX;
            int blockCountY = (mipData[mipIndex].height + blockY - 1) / blockY;
            totalDataLen += blockCountX * blockCountY * 16;
        }
        
        uint8_t* outData = new uint8_t[totalDataLen];
        size_t dataOffset = 0;

        for (int mipIndex = 0; mipIndex < numMips; mipIndex++)
        {
            RawTextureData source = mipData[mipIndex];
            
            int blockCountX = (source.width + blockX - 1) / blockX;
            int blockCountY = (source.height + blockY - 1) / blockY;
            size_t mipDataLen = blockCountX * blockCountY * 16;
            
            astcenc_image sourceImage
            {
                .dim_x = (uint32_t)source.width,
                .dim_y = (uint32_t)source.height,
                .dim_z = 1u,
                .data_type = isFloat ? ASTCENC_TYPE_F32 : ASTCENC_TYPE_U8,
                .data = &source.data
            };

            AstcParams params
            {
                .context = context,
                .image = &sourceImage,
                .swizzle = &swizzle,
                .data_out = outData + dataOffset,
                .data_len = mipDataLen
            };
            
            AstcEncodeTask encodeTask{};
            encodeTask.params = params;
            encodeTask.m_SetSize = numThreads;
            encodeTask.m_MinRange = 1;

            g_taskSched.AddTaskSetToPipe(&encodeTask);
            g_taskSched.WaitforTask(&encodeTask);
            
            if (encodeTask.err != ASTCENC_SUCCESS)
            {
                logErr("Failed to compress image: %s", astcenc_get_error_string(encodeTask.err));
                delete[] outData;
                astcenc_context_free(context);
                return false;
            }

            astcenc_compress_reset(context);

            dataOffset += mipDataLen;
        }

        result->dataSize = totalDataLen;
        result->data = outData;
        
        astcenc_context_free(context);

        return true;
    }

    bool compressASTCWithMipsToFile(RawTextureData* mipData, int numMips, TextureCompressSettings settings, std::string outputPath)
    {
        ASTCCompressResult compressResult{};
        
        if (!compressASTCWithMipsInternal(mipData, numMips, settings, &compressResult))
        {
            return false;
        }
        
        wtex::Header header{};
        header.containedFormat = wtex::ContainedFormat::ASTC6x6;
        header.dataOffset = sizeof(header);
        header.dataSize = compressResult.dataSize;
        header.width = mipData[0].width;
        header.height = mipData[0].height;
        header.numMipLevels = numMips;
        header.isSrgb = settings.isSrgb;

        PHYSFS_File* outFile = PHYSFS_openWrite(outputPath.c_str());
        PHYSFS_writeBytes(outFile, &header, sizeof(header));
        PHYSFS_writeBytes(outFile, compressResult.data, compressResult.dataSize);
        PHYSFS_close(outFile);

        delete[] compressResult.data;
        
        return true;
    }

    // returns true on success
    bool compressASTCToFile(RawTextureData rawData, TextureCompressSettings settings, std::string outputPath)
    {
        int numMips = (int)floor(log2(std::max(rawData.width, rawData.height))) + 1;
        RawTextureData* mipDatas = new RawTextureData[numMips];
        mipDatas[0] = rawData;

        for (int i = 1; i < numMips; i++)
        {
            mipDatas[i] = generateMipLevel(rawData, i, settings.isSrgb);
        }

        if (!compressASTCWithMipsToFile(mipDatas, numMips, settings, outputPath))
            return false;
 
        for (int i = 1; i < numMips; i++)
        {
            free(mipDatas[i].data);
        }

        delete[] mipDatas;

        return true;
    }

    
    void TextureCompiler::compilePBR(TexCompileThreadInfo* tcti)
    {
        assert(tcti->assetSettings.type == TextureAssetType::PBR);
        const PBRTextureSettings& pts = tcti->assetSettings.pbr;

        RawTextureData roughnessData;
        RawTextureData metallicData;
        RawTextureData occlusionData;
        bool useRoughness = false;
        bool useMetallic = false;
        bool useOcclusion = false;

        if (pts.roughnessSource != INVALID_ASSET)
        {
            if (RawTextureLoader::loadRawTexture(pts.roughnessSource, roughnessData))
            {
                useRoughness = true;
            }
            else
            {
                logErr("Failed to load roughness layer");
            }
        }

        if (pts.metallicSource != INVALID_ASSET)
        {
            if (RawTextureLoader::loadRawTexture(pts.metallicSource, metallicData))
            {
                useMetallic = true;
            }
            else
            {
                logErr("Failed to load metallic layer");
            }
        }

        if (pts.occlusionSource != INVALID_ASSET)
        {
            if (RawTextureLoader::loadRawTexture(pts.occlusionSource, occlusionData))
            {
                useOcclusion = true;
            }
            else
            {
                logErr("Failed to load occlusion layer");
            }
        }

        int outWidth;
        int outHeight;
        
        if (useRoughness)
        {
            outWidth = roughnessData.width;
            outHeight = roughnessData.height;
        }
        else if (useMetallic)
        {
            outWidth = metallicData.width;
            outHeight = metallicData.height;
        }
        else if (useOcclusion)
        {
            outWidth = occlusionData.width;
            outHeight = occlusionData.height;
        }
        else
        {
            logErr("No layers in PBR texture?!?!?!");
            tcti->compileOp->completeAsFail();
            return;
        }

        if (useMetallic)
        {
            if (metallicData.width != outWidth || metallicData.height != outHeight)
                logErr("Metallic texture size doesn't match!");
        }
        
        if (useRoughness)
        {
            if (roughnessData.width != outWidth || roughnessData.height != outHeight)
                logErr("Roughness texture size doesn't match!");
        }
        
        if (useOcclusion)
        {
            if (occlusionData.width != outWidth || occlusionData.height != outHeight)
                logErr("Occlusion texture size doesn't match!");
        }

        uint8_t* layeredData = new uint8_t[outWidth * outHeight * 4];

        for (int x = 0; x < outWidth; x++)
        {
            for (int y = 0; y < outHeight; y++)
            {
                int baseIdx = 4 * (x + y * outWidth);

                // Red channel - Metallic
                if (useMetallic)
                    layeredData[baseIdx + 0] = static_cast<uint8_t*>(metallicData.data)[baseIdx + 0];
                else
                    layeredData[baseIdx + 0] = 255 * pts.defaultMetallic;

                // Green channel - Roughness
                if (useRoughness)
                    layeredData[baseIdx + 1] = static_cast<uint8_t*>(roughnessData.data)[baseIdx + 1];
                else
                    layeredData[baseIdx + 1] = 255 * pts.defaultRoughness;

                // Red channel - Occlusion
                if (useOcclusion)
                    layeredData[baseIdx + 2] = static_cast<uint8_t*>(occlusionData.data)[baseIdx + 2];
                else
                    layeredData[baseIdx + 2] = 255;

                // Alpha channel is currently unused, set it to 255
                layeredData[baseIdx + 3] = 255;
            }
        }

        if (useMetallic) free(metallicData.data);
        if (useRoughness) free(roughnessData.data);
        if (useOcclusion) free(occlusionData.data);

        RawTextureData layeredRtd
        {
            .format = RawTextureFormat::RGBA8,
            .width = outWidth,
            .height = outHeight,
            .data = layeredData,
            .totalDataSize = (size_t)(outWidth * outHeight * 4)
        };

        TextureCompressSettings compressSettings
        {
            .isSrgb = false,
            .isNormalMap = false,
            .quality = tcti->assetSettings.pbr.qualityLevel,
            .compileOp = tcti->compileOp
        };

        bool result;

        if (tcti->astcOverride)
        {
            result = compressASTCToFile(layeredRtd, compressSettings, tcti->outputPath);
        }
        else
        {
            result = compressCrunchToFile(layeredRtd, compressSettings, tcti->outputPath);
        }
        
        if (result)
        {
            tcti->compileOp->completeAsSuccess();
        }
        else
        {
            tcti->compileOp->completeAsFail();
        }
        
        delete[] layeredData;
    }

    void TextureCompiler::compileASTC(TexCompileThreadInfo* tcti)
    {
        if (tcti->assetSettings.type == TextureAssetType::Crunch)
        {
            RawTextureData inTexData{};

            if (!RawTextureLoader::loadRawTexture(tcti->assetSettings.crunch.sourceTexture, inTexData))
            {
                logErr("Failed to compile %s", AssetDB::idToPath(tcti->compileOp->outputId).c_str());
                tcti->compileOp->completeAsFail();
                return;
            }

            if (inTexData.format != RawTextureFormat::RGBA8)
            {
                logErr("Invalid input for ASTC texture");
                tcti->compileOp->completeAsFail();
                return;
            }
        

            TextureCompressSettings settings
            {
                .isSrgb = tcti->assetSettings.crunch.isSrgb,
                .isNormalMap = tcti->assetSettings.crunch.isNormalMap
            };
        
            if (compressASTCToFile(inTexData, settings, tcti->outputPath))
            {
                tcti->compileOp->completeAsSuccess();
            }
            else
            {
                tcti->compileOp->completeAsFail();
            }
            free(inTexData.data);
        }
        else if (tcti->assetSettings.type == TextureAssetType::Cubemap)
        {
            size_t totalDataSize = 0;
            ASTCCompressResult compressedFaces[6];
            int resolution = 0;
            for (int face = 0; face < 6; face++)
            {
                RawTextureData mips[6];

                for (int mip = 0; mip < 6; mip++)
                {
                    AssetID id = tcti->assetSettings.cubemap.faces[face * 6 + mip];
                    if (!RawTextureLoader::loadRawTexture(id, mips[mip]))
                    {
                        logErr("Failed to compile %s", AssetDB::idToPath(tcti->compileOp->outputId).c_str());
                        tcti->compileOp->completeAsFail();
                        return;
                    }
                }
                
                if (resolution == 0)
                {
                    resolution = mips[0].width;
                    if (mips[0].height != resolution)
                    {
                        logErr("Cubemap face %i width of %i didn't match height of %i", face, mips[0].width, mips[0].height);
                        tcti->compileOp->completeAsFail();
                        return;
                    }
                }
                else
                {
                    if (mips[0].width != resolution || mips[0].height != resolution)
                    {
                        logErr("Resolution of cubemap face %i didn't match previous resolution of %i", face, resolution);
                        tcti->compileOp->completeAsFail();
                        return;
                    }
                }

                TextureCompressSettings settings { false, false };

                if (!compressASTCWithMipsInternal(mips, 6, settings, &compressedFaces[face]))
                {
                    logErr("Failed to compress cubemap face %i", face);
                    tcti->compileOp->completeAsFail();
                    return;
                }
                
                for (int mip = 0; mip < 6; mip++)
                {
                    free(mips[mip].data);
                }

                totalDataSize += compressedFaces[face].dataSize;
            }

            // Since we have a big array of individual compressed faces of the cubemaps with mip chains, we have to
            // do some rearranging so we get the ordering of mip0 for each face, mip1 for each face etc instead
            // of the mip0 for first face, mip1 for first face, etc ordering just combining them would give
            
            uint8_t* finalData = new uint8_t[totalDataSize];

            // offset to write into in finalData
            size_t writeCursor = 0;
            // offset of the current mip within each face
            size_t mipOffset = 0;

            for (int mip = 0; mip < 6; mip++)
            {
                int currentMipRes = std::max(resolution >> mip, 1);
                int blockCount = (currentMipRes + 5) / 6;
                size_t mipDataLen = blockCount * blockCount * 16;
                
                for (int face = 0; face < 6; face++)
                {
                    memcpy(finalData + writeCursor, compressedFaces[face].data + mipOffset, mipDataLen);
                    writeCursor += mipDataLen;
                }

                mipOffset += mipDataLen;
            }

            wtex::Header header
            {
                .containedFormat = wtex::ContainedFormat::ASTC6x6Cube,
                .width = resolution,
                .height = resolution,
                .numMipLevels = 6,
                .isSrgb = false,
                .dataSize = totalDataSize,
                .dataOffset = sizeof(wtex::Header)
            };
            
            PHYSFS_File* outFile = PHYSFS_openWrite(tcti->outputPath.c_str());
            PHYSFS_writeBytes(outFile, &header, sizeof(header));
            PHYSFS_writeBytes(outFile, finalData, totalDataSize);
            PHYSFS_close(outFile);

            delete finalData;
            tcti->compileOp->completeAsSuccess();
        }
    }

    void TextureCompiler::compileRGBACube(TexCompileThreadInfo* tcti)
    {
        int resolution = 0;
        RawTextureData faceMips[36];
        size_t totalDataSize = 0;
        
        for (int face = 0; face < 6; face++)
        {
            for (int mip = 0; mip < 6; mip++)
            {
                AssetID id = tcti->assetSettings.cubemap.faces[face * 6 + mip];
                if (!RawTextureLoader::loadRawTexture(id, faceMips[face * 6 + mip]))
                {
                    logErr("Failed to compile %s", AssetDB::idToPath(tcti->compileOp->outputId).c_str());
                    tcti->compileOp->completeAsFail();
                    return;
                }
                
                if (resolution == 0)
                {
                    resolution = faceMips[0].width;
                    if (faceMips[0].height != resolution)
                    {
                        logErr("Cubemap face %i width of %i didn't match height of %i", face, faceMips[0].width, faceMips[0].height);
                        tcti->compileOp->completeAsFail();
                        return;
                    }
                }
                else
                {
                    if (faceMips[0].width != resolution || faceMips[0].height != resolution)
                    {
                        logErr("Resolution of cubemap face %i didn't match previous resolution of %i", face, resolution);
                        tcti->compileOp->completeAsFail();
                        return;
                    }
                }

                totalDataSize += faceMips[face * 6 + mip].totalDataSize;
            }
        }
        // Since we have a big array of individual compressed faces of the cubemaps with mip chains, we have to
        // do some rearranging so we get the ordering of mip0 for each face, mip1 for each face etc instead
        // of the mip0 for first face, mip1 for first face, etc ordering just combining them would give

        uint8_t* finalData = new uint8_t[totalDataSize];

        // offset to write into in finalData
        size_t writeCursor = 0;
        // offset of the current mip within each face
        size_t mipOffset = 0;

        for (int mip = 0; mip < 6; mip++)
        {
            int currentMipRes = std::max(resolution >> mip, 1);
            size_t mipDataLen = currentMipRes * currentMipRes * 4 * sizeof(float);

            for (int face = 0; face < 6; face++)
            {
                memcpy(finalData + writeCursor, faceMips[face * 6 + mip].data, mipDataLen);
                writeCursor += mipDataLen;
            }

            mipOffset += mipDataLen;
        }

        wtex::Header header
        {
            .containedFormat = wtex::ContainedFormat::RGBAF32Cube,
            .width = resolution,
            .height = resolution,
            .numMipLevels = 6,
            .isSrgb = false,
            .dataSize = totalDataSize,
            .dataOffset = sizeof(wtex::Header)
        };

        PHYSFS_File* outFile = PHYSFS_openWrite(tcti->outputPath.c_str());
        PHYSFS_writeBytes(outFile, &header, sizeof(header));
        PHYSFS_writeBytes(outFile, finalData, totalDataSize);
        PHYSFS_close(outFile);

        delete finalData;
        tcti->compileOp->completeAsSuccess();

        for (int i = 0; i < 36; i++)
        {
            free(faceMips[i].data);
        }
    }

    void TextureCompiler::compileInternal(TexCompileThreadInfo* tcti)
    {
        switch (tcti->assetSettings.type)
        {
        case TextureAssetType::Crunch:
            if (tcti->astcOverride)
            {
                compileASTC(tcti);
            }
            else
            {
                compileCrunch(tcti);
            }
            break;
        case TextureAssetType::RGBA:
            compileRGBA(tcti);
            break;
        case TextureAssetType::PBR:
            compilePBR(tcti);
            break;
        case TextureAssetType::Cubemap:
            if (tcti->astcOverride)
            {
                compileASTC(tcti);
            }
            else
            {
                compileRGBACube(tcti);
            }
            break;
        }
    }
}

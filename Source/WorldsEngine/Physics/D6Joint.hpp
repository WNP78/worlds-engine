#pragma once
#include <Core/Transform.hpp>
#include <entt/entity/fwd.hpp>
#include <glm/vec3.hpp>

namespace physx
{
    class PxD6Joint;
    class PxRigidActor;
}

namespace worlds
{
    enum class D6Motion
    {
        Locked,
        Limited,
        Free
    };

    enum class D6Axis
    {
        X,
        Y,
        Z,
        Twist,
        Swing1,
        Swing2,
        AngularX = Twist,
        AngularY = Swing1,
        AngularZ = Swing2
    };

    struct JointLimitParameters
    {
        float restitution;
        float bounceThreshold;
        float stiffness;
        float damping;
        float contactDistance_deprecated;
    };

    struct JointLinearLimitPair : public JointLimitParameters
    {
        float upper;
        float lower;
    };

    struct JointAngularLimitPair : public JointLimitParameters
    {
        float upper;
        float lower;
    };

    struct JointLimitPyramid : public JointLimitParameters
    {
        float yAngleMin;
        float yAngleMax;
        float zAngleMin;
        float zAngleMax;
    };

    struct JointLimitCone : public JointLimitParameters
    {
        float yAngle;
        float zAngle;
    };

    enum class D6JointDriveFlags : uint32_t
    {
        Acceleration = 1
    };

    struct D6JointDrive
    {
        float stiffness;
        float damping;
        float forceLimit;
        D6JointDriveFlags flags;
    };

    enum class D6JointDriveAxis
    {
        X,
        Y,
        Z,
        Swing,
        Twist,
        Slerp,
        AngularX = Twist,
        AngularYZ = Swing
    };

    struct D6Joint
    {
        D6Joint();
        D6Joint(const D6Joint&) = delete;
        D6Joint(D6Joint&& other) noexcept;
        void operator=(D6Joint&& other);
        physx::PxD6Joint* pxJoint = nullptr;
        bool reverseJoint = false;

        void setTarget(entt::entity newTargetEnt, entt::registry& reg);
        void setAllLinearMotion(D6Motion motion);
        void setAllAngularMotion(D6Motion motion);
        void setAxisMotion(D6Axis axis, D6Motion motion);

        Transform getLocalPose(int actorIndex);
        void setLocalPose(int actorIndex, Transform transform);

        void setLinearLimit(D6Axis axis, JointLinearLimitPair limit);
        void setTwistLimit(JointAngularLimitPair limit);
        void setPyramidSwingLimit(JointLimitPyramid limit);
        void setConeSwingLimit(JointLimitCone limit);

        void setBreakForce(float breakForce);
        float getBreakForce() const;
        void setBreakTorque(float breakTorque);
        float getBreakTorque() const;
        bool isBroken() const;

        void setDrive(D6JointDriveAxis axis, D6JointDrive drive);
        void setDriveTarget(Transform transform, bool autowake = true);
        void setDriveVelocity(glm::vec3 linear, glm::vec3 angular, bool autowake = true);
        void setDriveLinearVelocity(glm::vec3 velocity, bool autowake = true);
        void setDriveAngularVelocity(glm::vec3 velocity, bool autowake = true);

        Transform getDriveTarget() const;
        glm::vec3 getDriveLinearVelocity() const;
        glm::vec3 getDriveAngularVelocity() const;

        float getTwistAngle() const;
        float getSwingYAngle() const;
        float getSwingZAngle() const;

        glm::vec3 getConstraintLinearForce() const;
        glm::vec3 getConstraintAngularForce() const;

        entt::entity getTarget();

        entt::entity getAttached();
        void setAttached(entt::entity entity, entt::registry& reg);

        ~D6Joint();

      private:
        void updateJointActors();
        friend class PhysicsSystem;
        friend class D6JointEditor;
        physx::PxRigidActor* thisActor = nullptr;
        physx::PxRigidActor* originalThisActor = nullptr;
        physx::PxRigidActor* targetActor = nullptr;
        entt::entity targetEntity;
        entt::entity replaceThis;
    };
}

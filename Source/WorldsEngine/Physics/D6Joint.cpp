#include "D6Joint.hpp"
#include "../Core/Log.hpp"
#include "PhysicsActor.hpp"
#include <Physics/Physics.hpp>
#include <Util/MathsUtil.hpp>
#include <entt/entt.hpp>
#include <physx/PxPhysicsAPI.h>
#include <physx/extensions/PxD6Joint.h>
#include <physx/extensions/PxJoint.h>

namespace worlds
{
    D6Joint::D6Joint() : pxJoint{nullptr}, thisActor{nullptr}, targetEntity{entt::null}, replaceThis{entt::null}
    {
    }

    D6Joint::D6Joint(D6Joint&& other) noexcept
    {
        pxJoint = other.pxJoint;
        other.pxJoint = nullptr;
        thisActor = other.thisActor;
        other.thisActor = nullptr;
        targetEntity = other.targetEntity;
        other.targetEntity = entt::null;
        replaceThis = other.replaceThis;
        other.replaceThis = entt::null;
        originalThisActor = other.originalThisActor;
        other.originalThisActor = nullptr;
    }

    void D6Joint::operator=(D6Joint&& other)
    {
        pxJoint = other.pxJoint;
        other.pxJoint = nullptr;
        thisActor = other.thisActor;
        other.thisActor = nullptr;
        targetEntity = other.targetEntity;
        other.targetEntity = entt::null;
        replaceThis = other.replaceThis;
        other.replaceThis = entt::null;
        originalThisActor = other.originalThisActor;
        other.originalThisActor = nullptr;
    }

    physx::PxRigidActor* getSuitablePhysicsActor(entt::entity ent, entt::registry& reg)
    {
        auto* pa = reg.try_get<PhysicsActor>(ent);
        auto* dpa = reg.try_get<RigidBody>(ent);

        if (!(pa || dpa))
        {
            return nullptr;
        }

        return pa ? pa->actor : dpa->actor;
    }

    void D6Joint::setTarget(entt::entity newTargetEnt, entt::registry& reg)
    {
        targetEntity = newTargetEnt;
        auto* pa = reg.try_get<PhysicsActor>(newTargetEnt);
        auto* dpa = reg.try_get<RigidBody>(newTargetEnt);

        if (!(pa || dpa))
        {
            logErr(
                "Tried to set a D6 joint's target to an entity with neither a physics actor or dynamic physics actor");
            return;
        }

        targetActor = getSuitablePhysicsActor(newTargetEnt, reg);
        updateJointActors();
    }

    physx::PxD6Motion::Enum conv(D6Motion motion)
    {
        return (physx::PxD6Motion::Enum)motion;
    }

    void D6Joint::setAllLinearMotion(D6Motion wmotion)
    {
        auto motion = conv(wmotion);
        pxJoint->setMotion(physx::PxD6Axis::eX, motion);
        pxJoint->setMotion(physx::PxD6Axis::eY, motion);
        pxJoint->setMotion(physx::PxD6Axis::eZ, motion);
    }

    void D6Joint::setAllAngularMotion(D6Motion wmotion)
    {
        auto motion = conv(wmotion);
        pxJoint->setMotion(physx::PxD6Axis::eSWING1, motion);
        pxJoint->setMotion(physx::PxD6Axis::eSWING2, motion);
        pxJoint->setMotion(physx::PxD6Axis::eTWIST, motion);
    }

    void D6Joint::setAxisMotion(D6Axis axis, D6Motion motion)
    {
        auto pxMotion = conv(motion);
        auto pxAxis = (physx::PxD6Axis::Enum)axis;

        pxJoint->setMotion(pxAxis, pxMotion);
    }

    Transform D6Joint::getLocalPose(int actorIndex)
    {
        return px2glm(pxJoint->getLocalPose((physx::PxJointActorIndex::Enum)actorIndex));
    }

    void D6Joint::setLocalPose(int actorIndex, Transform transform)
    {
        pxJoint->setLocalPose((physx::PxJointActorIndex::Enum)actorIndex, glm2px(transform));
    }

    void D6Joint::setLinearLimit(D6Axis axis, JointLinearLimitPair limit)
    {
        static_assert(sizeof(JointLinearLimitPair) == sizeof(physx::PxJointLinearLimitPair));
        auto pxAxis = (physx::PxD6Axis::Enum)axis;
        pxJoint->setLinearLimit(pxAxis, *(physx::PxJointLinearLimitPair*)&limit);
    }

    void D6Joint::setTwistLimit(JointAngularLimitPair limit)
    {
        static_assert(sizeof(JointAngularLimitPair) == sizeof(physx::PxJointAngularLimitPair));
        pxJoint->setTwistLimit(*(physx::PxJointAngularLimitPair*)&limit);
    }

    void D6Joint::setPyramidSwingLimit(JointLimitPyramid limit)
    {
        static_assert(sizeof(JointLimitPyramid) == sizeof(physx::PxJointLimitPyramid));
        pxJoint->setPyramidSwingLimit(*(physx::PxJointLimitPyramid*)&limit);
    }

    void D6Joint::setConeSwingLimit(JointLimitCone limit)
    {
        static_assert(sizeof(JointLimitCone) == sizeof(physx::PxJointLimitCone));
        pxJoint->setSwingLimit(*(physx::PxJointLimitCone*)&limit);
    }

    void D6Joint::setBreakForce(float breakForce)
    {
        pxJoint->setBreakForce(breakForce, getBreakTorque());
    }

    float D6Joint::getBreakForce() const
    {
        float breakForce, breakTorque;
        pxJoint->getBreakForce(breakForce, breakTorque);
        return breakForce;
    }

    void D6Joint::setBreakTorque(float breakTorque)
    {
        pxJoint->setBreakForce(getBreakForce(), breakTorque);
    }

    float D6Joint::getBreakTorque() const
    {
        float breakForce, breakTorque;
        pxJoint->getBreakForce(breakForce, breakTorque);
        return breakTorque;
    }

    bool D6Joint::isBroken() const
    {
        return (pxJoint->getConstraintFlags() & physx::PxConstraintFlag::eBROKEN);
    }

    void D6Joint::setDrive(D6JointDriveAxis axis, D6JointDrive drive)
    {
        auto pxAxis = (physx::PxD6Drive::Enum)axis;
        pxJoint->setDrive(pxAxis, *(physx::PxD6JointDrive*)&drive);
    }

    void D6Joint::setDriveTarget(Transform transform, bool autowake)
    {
        pxJoint->setDrivePosition(glm2px(transform), autowake);
    }

    void D6Joint::setDriveVelocity(glm::vec3 linear, glm::vec3 angular, bool autowake)
    {
        pxJoint->setDriveVelocity(glm2px(linear), glm2px(angular), autowake);
    }

    void D6Joint::setDriveLinearVelocity(glm::vec3 velocity, bool autowake)
    {
        pxJoint->setDriveVelocity(glm2px(velocity), glm2px(getDriveAngularVelocity()), autowake);
    }

    void D6Joint::setDriveAngularVelocity(glm::vec3 velocity, bool autowake)
    {
        pxJoint->setDriveVelocity(glm2px(velocity), glm2px(getDriveAngularVelocity()), autowake);
    }

    Transform D6Joint::getDriveTarget() const
    {
        return px2glm(pxJoint->getDrivePosition());
    }

    glm::vec3 D6Joint::getDriveLinearVelocity() const
    {
        physx::PxVec3 linear, angular;
        pxJoint->getDriveVelocity(linear, angular);
        return px2glm(linear);
    }

    glm::vec3 D6Joint::getDriveAngularVelocity() const
    {
        physx::PxVec3 linear, angular;
        pxJoint->getDriveVelocity(linear, angular);
        return px2glm(angular);
    }

    float D6Joint::getTwistAngle() const
    {
        return pxJoint->getTwistAngle();
    }

    float D6Joint::getSwingYAngle() const
    {
        return pxJoint->getSwingYAngle();
    }

    float D6Joint::getSwingZAngle() const
    {
        return pxJoint->getSwingZAngle();
    }

    glm::vec3 D6Joint::getConstraintLinearForce() const
    {
        physx::PxVec3 linear, angular;
        pxJoint->getConstraint()->getForce(linear, angular);
        return px2glm(linear);
    }

    glm::vec3 D6Joint::getConstraintAngularForce() const
    {
        physx::PxVec3 linear, angular;
        pxJoint->getConstraint()->getForce(linear, angular);
        return px2glm(angular);
    }

    entt::entity D6Joint::getTarget()
    {
        return targetEntity;
    }

    entt::entity D6Joint::getAttached()
    {
        return replaceThis;
    }

    void D6Joint::setAttached(entt::entity ent, entt::registry& reg)
    {
        replaceThis = ent;

        if (ent != entt::null)
        {
            thisActor = getSuitablePhysicsActor(ent, reg);
        }
        else
        {
            thisActor = originalThisActor;
        }

        updateJointActors();
    }

    D6Joint::~D6Joint()
    {
    }

    void D6Joint::updateJointActors()
    {
        if (reverseJoint)
            pxJoint->setActors(targetActor, thisActor);
        else
            pxJoint->setActors(thisActor, targetActor);
    }
}

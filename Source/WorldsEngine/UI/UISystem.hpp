#pragma once
#include <vector>
#include <string>

namespace worlds
{
    class UIPanel;

    class UISystem
    {
    public:
        UISystem();
        ~UISystem();
        UIPanel* createPanel(std::string name, int width, int height);
        void destroyPanel(UIPanel* panel);
        void updatePanels();
        void renderPanels();
    private:
        std::vector<UIPanel*> panels;
    };
}
﻿#pragma once
#include <string>
#include <glm/vec3.hpp>


struct Transform;
namespace worlds
{
    class UIPanel;
    class UISystem;
    
    struct WorldSpaceUIPanel
    {
        // settings
        int width = 800;
        int height = 600;
        float pixelSize = 0.0005f;
        std::string name = "UI Panel";
        std::string path = "test.rml";
        float brightness = 1.0f;

        void createPanel(UISystem* system);
        bool raycastCursor(const Transform& panelTransform, glm::vec3 rayOrigin, glm::vec3 rayDirection, float maxDistance);
        void reload();
        
        UIPanel* panel = nullptr;
        UISystem* system = nullptr;
        bool cursorShowing = false;

        WorldSpaceUIPanel() {}
        WorldSpaceUIPanel(const WorldSpaceUIPanel&) = delete;
        WorldSpaceUIPanel(WorldSpaceUIPanel&&) noexcept;
        WorldSpaceUIPanel& operator=(WorldSpaceUIPanel&&) noexcept;
        ~WorldSpaceUIPanel();
    };
}

#include <UI/UISystem.hpp>
#include <RmlUi/Core/Context.h>
#include <RmlUi/Core/Core.h>
#include <RmlUi/Core/FileInterface.h>
#include <RmlUi/Debugger.h>
#include <UI/Private/PhysFSFileInterface.hpp>
#include <UI/Private/WorldsSystemInterface.hpp>
#include <UI/Private/R2RenderInterface.hpp>

namespace worlds
{
    PhysFSFileInterface* fileInterface = nullptr;
    WorldsSystemInterface* systemInterface = nullptr;
    R2RenderInterface* renderInterface = nullptr;

    UISystem::UISystem()
    {
        fileInterface = new PhysFSFileInterface;
        Rml::SetFileInterface(fileInterface);

        systemInterface = new WorldsSystemInterface();
        Rml::SetSystemInterface(systemInterface);

        renderInterface = new R2RenderInterface();
        Rml::SetRenderInterface(renderInterface);

        releaseAssert(Rml::Initialise());
        releaseAssert(Rml::LoadFontFace("Fonts/Nunito/EditorFont.ttf"));
        releaseAssert(Rml::LoadFontFace("Fonts/Audiowide-Regular.ttf"));
        releaseAssert(Rml::LoadFontFace("Fonts/ChakraPetch-Regular.ttf"));
    }

    UISystem::~UISystem()
    {
        for (UIPanel* panel : panels)
        {
            delete panel;
        }

        Rml::Shutdown();

        delete fileInterface;
        delete systemInterface;
        delete renderInterface;
    }

    UIPanel* UISystem::createPanel(std::string name, int width, int height)
    {
        auto* panel = new UIPanel(name, width, height);
        panels.push_back(panel);
        renderInterface->setupPanelRenderState(panel);

        return panel;
    }

    void UISystem::destroyPanel(UIPanel* panel)
    {
        std::erase(panels, panel);
        delete panel;
    }

    void UISystem::updatePanels()
    {
        for (UIPanel* panel : panels)
        {
            panel->context->Update();
        }
    }

    void UISystem::renderPanels()
    {
        for (UIPanel* panel : panels)
        {
            renderInterface->beginPanelRendering(panel);
            panel->context->Render();
            renderInterface->endPanelRendering(panel);
        }
    }
}
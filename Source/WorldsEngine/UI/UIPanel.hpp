#pragma once
#include <Core/Transform.hpp>
#include <string>
#include <vector>

namespace Rml
{
    class Element;
    class ElementDocument;
    class Context;
}

namespace worlds
{
    struct PanelRenderState;

    class UIPanel
    {
    public:
        int getWidth() const { return width; }
        int getHeight() const { return height; }
        void resize(int newWidth, int newHeight);
        uint32_t getBindlessTextureId();
        void setRenderState(PanelRenderState* renderState);
        PanelRenderState* getRenderState();
        void loadDocument(const char* path);
        void click();
        void setCursorPos(int x, int y);
        void addEventListener(const char* elementId, const char* eventId, void (*fPtr)(void*), void* dataArg);
        void setLoadListener(void (*fPtr)(void*), void* dataArg);
        Rml::Element* getElementById(const char* id);
        void mouseUp(int button);
        void mouseDown(int button);
        void mouseScroll(float delta);

    private:
        class FunctionPointerEventListener;
        UIPanel(std::string name, int width, int height);
        ~UIPanel();
        PanelRenderState* renderState = nullptr;
        Rml::Context* context;
        Rml::ElementDocument* currentDocument = nullptr;
        bool isWorldSpace;
        int width;
        int height;
        std::string name;
        std::vector<FunctionPointerEventListener*> listeners;
        void (*loadListener)(void*);
        void* loadListenerData;

        friend class UISystem;
    };
}
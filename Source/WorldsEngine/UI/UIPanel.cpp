#include <UI/UIPanel.hpp>
#include <Core/Fatal.hpp>
#include <RmlUi/Core/Context.h>
#include <RmlUi/Core/Core.h>
#include <RmlUi/Core/ContextInstancer.h>
#include <RmlUi/Core/ElementDocument.h>
#include <RmlUi/Core/EventListener.h>
#include <RmlUi/Debugger.h>
#include <UI/Private/R2RenderInterface.hpp>

#include "Core/Log.hpp"
#include "Private/R2RenderInterface.hpp"

namespace worlds
{
    class UIPanel::FunctionPointerEventListener : public Rml::EventListener
    {
    public:
        void (*fPtr)(void*);
        void* dataArg;
        
        void ProcessEvent(Rml::Event& event) override
        {
            logVrb("Processing event %u on %s", (uint32_t)event.GetId(), event.GetCurrentElement()->GetId().c_str());
            fPtr(dataArg);
        }
    };
    
    UIPanel::UIPanel(std::string name, int width, int height)
        : width(width)
        , height(height)
        , name(name)
        , loadListener(nullptr)
    {
        releaseAssert(Rml::GetContext(name) == nullptr);
        context = Rml::CreateContext(name, Rml::Vector2i(width, height));
        Rml::Debugger::Initialise(context);
    }

    UIPanel::~UIPanel()
    {
        releaseAssert(Rml::RemoveContext(name));
        delete renderState;
        for (auto* listener : listeners)
        {
            delete listener;
        }
    }

    void UIPanel::resize(int newWidth, int newHeight)
    {
        releaseAssert(newWidth > 0 && newHeight > 0);
        width = newWidth;
        height = newHeight;
        context->SetDimensions({ width, height });
    }

    uint32_t UIPanel::getBindlessTextureId()
    {
        releaseAssert(renderState);
        return renderState->panelBindlessId;
    }

    void UIPanel::setRenderState(PanelRenderState* renderState)
    {
        releaseAssert(this->renderState == nullptr);
        this->renderState = renderState;
    }

    PanelRenderState* UIPanel::getRenderState()
    {
        return renderState;
    }

    void UIPanel::loadDocument(const char* path)
    {
        if (currentDocument != nullptr)
        {
            context->UnloadDocument(currentDocument);
        }
        
        if (loadListener != nullptr)
        {
            FunctionPointerEventListener* listener = new FunctionPointerEventListener();
            listener->fPtr = loadListener;
            listener->dataArg = loadListenerData;
            listeners.push_back(listener);
            context->AddEventListener("load", listener, true);
        }
        
        currentDocument = context->LoadDocument(path);
        currentDocument->Show();
        context->Update();
    }

    void UIPanel::click()
    {
        context->ProcessMouseButtonDown(0, 0);
        context->ProcessMouseButtonUp(0, 0);
    }

    void UIPanel::setCursorPos(int x, int y)
    {
        context->ProcessMouseMove(x, y, 0);
    }

    void UIPanel::addEventListener(const char* elementId, const char* eventId, void (*fPtr)(void*), void* dataArg)
    {
        if (currentDocument == nullptr)
        {
            logErr(WELogCategoryUI, "addEventListener called on panel without loaded document");
            return;
        }
        
        auto* element = currentDocument->GetElementById(elementId);
        
        if (element == nullptr)
        {
            logErr(WELogCategoryUI, "Couldn't find element %s to add event listener to", elementId);
            return;
        }

        FunctionPointerEventListener* listener = new FunctionPointerEventListener();
        listener->fPtr = fPtr;
        listener->dataArg = dataArg;
        listeners.push_back(listener);
        element->AddEventListener(eventId, listener);
    }

    void UIPanel::setLoadListener(void(*fPtr)(void*), void* dataArg)
    {
        loadListener = fPtr;
        loadListenerData = dataArg;
    }

    Rml::Element* UIPanel::getElementById(const char* id)
    {
        if (currentDocument == nullptr) return nullptr;
        return currentDocument->GetElementById(id);
    }

    void UIPanel::mouseUp(int button)
    {
        context->ProcessMouseButtonUp(button, 0);
    }

    void UIPanel::mouseDown(int button)
    {
        context->ProcessMouseButtonDown(button, 0);
    }

    void UIPanel::mouseScroll(float delta)
    {
        context->ProcessMouseWheel(delta, 0);
    }
}

﻿#include <UI/WorldSpaceUIPanel.hpp>
#include <Core/Log.hpp>
#include <Core/Transform.hpp>
#include <physfs.h>
#include <glm/gtx/intersect.hpp>
#include <RmlUi/Core/Element.h>

#include "UIPanel.hpp"
#include "UISystem.hpp"

namespace worlds
{
    void WorldSpaceUIPanel::createPanel(UISystem* system)
    {
        this->system = system;
        logMsg("creating panel %s", name.c_str());
        panel = system->createPanel(name, width, height);
        reload();
    }

    bool WorldSpaceUIPanel::raycastCursor(const Transform& panelTransform, glm::vec3 rayOrigin, glm::vec3 rayDirection,
        float maxDistance)
    {
        auto* cursorEl = panel->getElementById("cursor");
        glm::vec3 normal = panelTransform.rotation * glm::vec3(0.0f, 0.0f, 1.0f);
        float intersectDist;
        if (!glm::intersectRayPlane(rayOrigin, rayDirection, panelTransform.position, normal, intersectDist)
            || intersectDist > maxDistance)
        {
            if (cursorShowing && cursorEl != nullptr)
            {
                cursorShowing = false;
                cursorEl->Animate("opacity", Rml::Property(0.0f, Rml::Property::Unit::NUMBER), 0.25f);
            }
            return false;
        }
        
        float worldWidth = (float)width * pixelSize;
        float worldHeight = (float)height * pixelSize;

        glm::vec3 intersectionPoint = rayOrigin + rayDirection * intersectDist;
        intersectionPoint = panelTransform.inverseTransformPoint(intersectionPoint);

        int x = (int)(intersectionPoint.x / pixelSize);
        int y = height - (int)(intersectionPoint.y / pixelSize);

        panel->setCursorPos(x, y);

        if (cursorEl != nullptr)
        {
            if (!cursorShowing)
            {
                cursorEl->Animate("opacity", Rml::Property(1.0f, Rml::Property::Unit::NUMBER), 0.25f);
                cursorShowing = true;
            }
            
            cursorEl->SetProperty(Rml::PropertyId::Top, Rml::Property(y, Rml::Property::Unit::PX));
            cursorEl->SetProperty(Rml::PropertyId::Left, Rml::Property(x, Rml::Property::Unit::PX));
        }

        return true;
    }

    void WorldSpaceUIPanel::reload()
    {
        if (PHYSFS_exists(path.c_str()))
        {
            panel->loadDocument(path.c_str());
        }
        else
        {
            logErr(WELogCategoryUI, "Failed to load panel %s!", path.c_str());
        }
    }

    WorldSpaceUIPanel::WorldSpaceUIPanel(WorldSpaceUIPanel&& old) noexcept
    {
        system = old.system;
        panel = old.panel;
        old.panel = nullptr;
        
        width = old.width;
        height = old.height;
        pixelSize = old.pixelSize;
        name = old.name;
        path = old.path;
        brightness = old.brightness;
    }

    WorldSpaceUIPanel& WorldSpaceUIPanel::operator=(WorldSpaceUIPanel&& old) noexcept
    {
        if (panel)
        {
            logMsg("destroying panel %s", name.c_str());
            system->destroyPanel(panel);
            panel = nullptr;
        }

        system = old.system;
        panel = old.panel;
        old.panel = nullptr;
        
        width = old.width;
        height = old.height;
        pixelSize = old.pixelSize;
        name = old.name;
        path = old.path;
        brightness = old.brightness;

        return *this;
    }

    WorldSpaceUIPanel::~WorldSpaceUIPanel()
    {
        if (system && panel)
        {
            logMsg("destroying panel %s", name.c_str());
            system->destroyPanel(panel);
            panel = nullptr;
        }
    }
}

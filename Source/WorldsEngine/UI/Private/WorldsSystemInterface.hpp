#include <RmlUi/Core/SystemInterface.h>
#include <Core/Engine.hpp>
#include <Core/Log.hpp>

namespace worlds
{
    class WorldsSystemInterface : public Rml::SystemInterface
    {
    public:
        WorldsSystemInterface()
        {
        }

        double GetElapsedTime() override
        {
            return g_engine->getGameTime();
        }

        bool LogMessage(Rml::Log::Type type, const Rml::String& message)
        {
            switch (type)
            {
            case Rml::Log::Type::LT_ERROR:
            case Rml::Log::Type::LT_ASSERT:
                logErr(WELogCategoryUI, "%s", message.c_str());
                break;
            case Rml::Log::Type::LT_WARNING:
                logWarn(WELogCategoryUI, "%s", message.c_str());
                break;
            case Rml::Log::Type::LT_INFO:
            case Rml::Log::Type::LT_DEBUG:
                logVrb(WELogCategoryUI, "%s", message.c_str());
                break;
            }
            return true;
        }
    };
}
#include <RmlUi/Core/FileInterface.h>
#include <physfs.h>
#include <Core/Log.hpp>
#include <Core/Fatal.hpp>

namespace worlds
{
    class PhysFSFileInterface : public Rml::FileInterface
    {
    public:
        PhysFSFileInterface()
        {
        }
        
        Rml::FileHandle Open(const Rml::String& path) override
        {
            PHYSFS_File* file = PHYSFS_openRead(path.c_str());

            if (file == nullptr)
            {
                PHYSFS_ErrorCode errCode = PHYSFS_getLastErrorCode();
                const char* errStr = PHYSFS_getErrorByCode(errCode);
                logErr(WELogCategoryUI, "Failed to open file %s: %s", path.c_str(), errStr);
            }

            return (Rml::FileHandle)file;
        }

        void Close(Rml::FileHandle file) override
        {
            releaseAssert(PHYSFS_close((PHYSFS_File*)file) != 0);
        }

        size_t Read(void* buffer, size_t size, Rml::FileHandle rFile) override
        {
            auto* file = (PHYSFS_File*)rFile;
            int64_t readBytes = PHYSFS_readBytes(file, buffer, size);

            if (readBytes < 0)
            {
                logErr(WELogCategoryUI, "Error reading from file");
                return 0;
            }

            return (size_t)readBytes;
        }

        bool Seek(Rml::FileHandle rFile, long rOffset, int origin) override
        {
            auto* file = (PHYSFS_File*)rFile;

            int64_t cursor = PHYSFS_tell(file);
            int64_t fileLength = PHYSFS_fileLength(file);

            if (fileLength < 0 || cursor < 0)
            {
                PHYSFS_ErrorCode errCode = PHYSFS_getLastErrorCode();
                const char* errStr = PHYSFS_getErrorByCode(errCode);
                logErr(WELogCategoryUI, "Error seeking in file: %s", errStr);
                return false;
            }

            auto offset = (int64_t)rOffset;

            switch (origin)
            {
            case SEEK_SET:
                break;
            case SEEK_CUR:
                offset += cursor;
                break;
            case SEEK_END:
                offset += fileLength;
                break;
            }

            return PHYSFS_seek(file, (uint64_t)offset) == 1;
        }

        size_t Tell(Rml::FileHandle rFile) override
        {
            auto* file = (PHYSFS_File*)rFile;

            return (size_t)PHYSFS_tell(file);
        }
    };
}
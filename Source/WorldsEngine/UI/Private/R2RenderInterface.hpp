#pragma once
#include <Core/Engine.hpp>
#include <Core/Fatal.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <R2/BindlessTextureManager.hpp>
#include <R2/VKBuffer.hpp>
#include <R2/VKCommandBuffer.hpp>
#include <R2/VKCore.hpp>
#include <R2/VKPipeline.hpp>
#include <R2/VKRenderPass.hpp>
#include <R2/VKTexture.hpp>
#include <Render/Render.hpp>
#include <Render/RenderInternal.hpp>
#include <Render/ShaderCache.hpp>
#include <RmlUi/Core/Context.h>
#include <RmlUi/Core/RenderInterface.h>
#include <robin_hood.h>
#include <UI/UIPanel.hpp>
#include <Util/UniquePtr.hpp>

#include "R2/VKFrameSeparatedBuffer.hpp"

using namespace R2;

namespace worlds
{
    struct PanelRenderState
    {
        uint32_t panelBindlessId;
        VK::Texture* texture;
        VK::FrameSeparatedBuffer* vertexBuffer;
        VK::FrameSeparatedBuffer* indexBuffer;
        Rml::Vertex* vertsMapped;
        uint32_t* indicesMapped;
        uint32_t currentIndexPosition;
        uint32_t currentVertexPosition;
    };

    struct PanelPushConstants
    {
        glm::vec2 translate;
        uint32_t textureId;
        uint32_t pad;
        glm::mat4 transform;
    };

    const int MAX_UI_VERTS = 100'000;
    const int MAX_UI_INDICES = MAX_UI_VERTS * 3;

    class RmlTextureManager
    {
    public:
        RmlTextureManager(VKRenderer* renderer)
            : renderer(renderer)
        {}

        bool LoadTexture(Rml::TextureHandle& textureHandle, Rml::Vector2i& textureDimensions, const Rml::String& source)
        {
            /* FIXME: In theory, if no other textures have been loaded this could return a handle of 0 and break things!
             * However, since the missing texture is always loaded first, this shouldn't be an issue.
             */

            AssetID assetId = AssetDB::pathToId(source.c_str());
            uint32_t bindlessHandle = renderer->getTextureManager()->loadSynchronous(assetId);
            R2::VK::Texture* texture = renderer->getBindlessTextureManager()->GetTextureAt(bindlessHandle);

            textureDimensions.x = texture->GetWidth();
            textureDimensions.y = texture->GetHeight();

            textures.insert({ bindlessHandle, RmlTexture { false, bindlessHandle, assetId, nullptr }});
            textureHandle = (Rml::TextureHandle)bindlessHandle;

            return true;
        }

        bool GenerateTexture(Rml::TextureHandle& textureHandle, const Rml::byte* source, const Rml::Vector2i& sourceDimensions)
        {
            auto tci = VK::TextureCreateInfo::Texture2D(VK::TextureFormat::R8G8B8A8_SRGB, sourceDimensions.x, sourceDimensions.y);
            VK::Texture* texture = renderer->getCore()->CreateTexture(tci);
            uint32_t bindlessHandle = renderer->getBindlessTextureManager()->AllocateTextureHandle(texture);

            uint64_t dataSize = 4 * sourceDimensions.x * sourceDimensions.y;
            renderer->getCore()->QueueTextureUpload(texture, (void*)source, dataSize);

            textures.insert({ bindlessHandle, RmlTexture { true, bindlessHandle, ~0u, texture }});
            textureHandle = (Rml::TextureHandle)bindlessHandle;

            return true;
        }

        void release(uint32_t texId)
        {
            RmlTexture& texture = textures[texId];
            if (texture.isGenerated)
            {
                renderer->getCore()->DestroyTexture(texture.genTexture);
            }
            else
            {
                renderer->getTextureManager()->release(texture.asset);
            }
            renderer->getBindlessTextureManager()->FreeTextureHandle(texture.bindlessId);
            textures.erase(texId);
        }
    private:
        struct RmlTexture
        {
            bool isGenerated;
            uint32_t bindlessId;
            AssetID asset;
            VK::Texture* genTexture;
        };
        robin_hood::unordered_map<uint32_t, RmlTexture> textures;
        VKRenderer* renderer;
    };

    class R2RenderInterface : public Rml::RenderInterface
    {
    public:
        R2RenderInterface()
        {
            renderer = (VKRenderer*)g_renderer;
            currentTransform = Rml::Matrix4f::Identity();
            rmlTexManager = new RmlTextureManager(renderer);

            // Create pipeline
            R2::VK::ShaderModule& vs = ShaderCache::getModule("Shaders/ui_panel.vert.spv"_asset);
            R2::VK::ShaderModule& fs = ShaderCache::getModule("Shaders/ui_panel.frag.spv"_asset);

            VK::PipelineLayoutBuilder plb{renderer->getCore()};
            plb.DescriptorSet(&renderer->getBindlessTextureManager()->GetTextureDescriptorSetLayout())
               .PushConstants(VK::ShaderStage::AllRaster, 0, sizeof(PanelPushConstants));

            pipelineLayout = plb.Build();

            VK::VertexBinding vb{};
            vb.Size = sizeof(Rml::Vertex);
            vb.Attributes.emplace_back(0, VK::TextureFormat::R32G32_SFLOAT, (uint32_t)offsetof(Rml::Vertex, position));
            vb.Attributes.emplace_back(1, VK::TextureFormat::R8G8B8A8_UNORM, (uint32_t)offsetof(Rml::Vertex, colour));
            vb.Attributes.emplace_back(2, VK::TextureFormat::R32G32_SFLOAT, (uint32_t)offsetof(Rml::Vertex, tex_coord));

            R2::VK::PipelineBuilder pb{renderer->getCore()};
            pb.PrimitiveTopology(VK::Topology::TriangleList)
              .ColorAttachmentFormat(VK::TextureFormat::R8G8B8A8_SRGB)
              .AddShader(VK::ShaderStage::Vertex, vs)
              .AddShader(VK::ShaderStage::Fragment, fs)
              .CullMode(VK::CullMode::None)
              .Layout(pipelineLayout.Get())
              .AddVertexBinding(vb)
              .AlphaBlend(true)
              .DepthTest(false)
              .DepthWrite(false)
              .DepthCompareOp(VK::CompareOp::Always);
            pipeline = pb.Build();
        }

        void beginPanelRendering(UIPanel* uiPanel)
        {
            releaseAssert(renderingPanel == nullptr);
            renderingPanel = uiPanel;

            resizePanelTextureIfNecessary(uiPanel);

            VK::CommandBuffer cb = renderer->getCore()->GetFrameCommandBuffer();

            cb.BeginDebugLabel("UI Panel", 0.0f, 0.1f, 0.7f);

            PanelRenderState* renderState = uiPanel->getRenderState();
            VK::RenderPass renderPass{};
            renderPass.RenderArea(uiPanel->getWidth(), uiPanel->getHeight())
                      .ColorAttachment(renderState->texture, VK::LoadOp::Clear, VK::StoreOp::Store)
                      .ColorAttachmentClearValue(VK::ClearValue::FloatColorClear(0.0f, 0.0f, 0.0f, 0.0f))
                      .Begin(cb);

            // set up graphics state
            VK::DescriptorSet* textureDescriptorSet = &renderer->getBindlessTextureManager()->GetTextureDescriptorSet();
            cb.BindGraphicsDescriptorSet(pipelineLayout.Get(), textureDescriptorSet, 0);
            cb.BindIndexBuffer(renderState->indexBuffer->GetCurrentBuffer(), 0, VK::IndexType::Uint32);
            cb.BindVertexBuffer(0, renderState->vertexBuffer->GetCurrentBuffer(), 0);
            cb.BindPipeline(pipeline.Get());
            cb.SetViewport(VK::Viewport::Simple(uiPanel->getWidth(), uiPanel->getHeight()));

            // reset buffer write heads and map
            renderState->currentIndexPosition = 0;
            renderState->currentVertexPosition = 0;
            renderState->vertsMapped = (Rml::Vertex*)renderState->vertexBuffer->MapCurrent();
            renderState->indicesMapped = (uint32_t*)renderState->indexBuffer->MapCurrent();
        }

        void endPanelRendering(UIPanel* uiPanel)
        {
            releaseAssert(renderingPanel == uiPanel);
            renderingPanel = nullptr;
            VK::CommandBuffer cb = renderer->getCore()->GetFrameCommandBuffer();

            cb.EndRendering();
            cb.EndDebugLabel();

            PanelRenderState* renderState = uiPanel->getRenderState();
            renderState->vertexBuffer->UnmapCurrent();
            renderState->indexBuffer->UnmapCurrent();
        }

        void RenderGeometry(Rml::Vertex* vertices,
                            int numVertices, 
                            int* indices,
                            int numIndices,
                            Rml::TextureHandle texture,
                            const Rml::Vector2f& vec) override
        {
            releaseAssert(renderingPanel != nullptr);
            PanelRenderState* renderState = renderingPanel->getRenderState();

            // error checking to make sure we don't stomp over everything if we run out of buffer space
            if (numVertices + renderState->currentVertexPosition >= MAX_UI_VERTS)
                fatalErr("Out of vertices in UI panel buffer");

            if (numIndices + renderState->currentIndexPosition >= MAX_UI_INDICES)
                fatalErr("Out of indices in UI panel buffer");

            // remember where we put this geometry in the buffer
            uint32_t vertexStart = renderState->currentVertexPosition;
            uint32_t indexStart = renderState->currentIndexPosition;

            // copy into buffers
            memcpy(renderState->vertsMapped + renderState->currentVertexPosition, vertices, sizeof(Rml::Vertex) * numVertices);
            renderState->currentVertexPosition += numVertices;

            memcpy(renderState->indicesMapped + renderState->currentIndexPosition, indices, sizeof(uint32_t) * numIndices);            
            renderState->currentIndexPosition += numIndices;

            VK::CommandBuffer cb = renderer->getCore()->GetFrameCommandBuffer();

            uint32_t textureId = texture == 0 ? ~0u : (uint32_t)texture;

            // set up push constants for shader
            PanelPushConstants ppcs
            {
                .translate = { vec.x, vec.y },
                .textureId = textureId,
                .transform = glm::orthoZO(0.0f, (float)renderingPanel->getWidth(), (float)renderingPanel->getHeight(), 0.0f, -1000.0f, 1000.0f) * glm::make_mat4(currentTransform.data())
            };

            // push 'em and draw!
            cb.PushConstants(ppcs, VK::ShaderStage::AllRaster, pipelineLayout.Get());
            cb.DrawIndexed(numIndices, 1, indexStart, vertexStart, 0);
        }

        void EnableScissorRegion(bool enable) override
        {
            releaseAssert(renderingPanel != nullptr);
            VK::CommandBuffer cb = renderer->getCore()->GetFrameCommandBuffer();
            cb.SetScissor(VK::ScissorRect::Simple(renderingPanel->getWidth(), renderingPanel->getHeight()));
        }

        void SetScissorRegion(int x, int y, int width, int height) override
        {
            VK::CommandBuffer cb = renderer->getCore()->GetFrameCommandBuffer();

            // Rml sometimes gives us scissor regions that don't make sense, so just clamp them
            if (x < 0) x = 0;
            if (y < 0) y = 0;
            cb.SetScissor(VK::ScissorRect { x, y, (uint32_t)width, (uint32_t)height });
        }

        bool LoadTexture(Rml::TextureHandle& textureHandle, Rml::Vector2i& textureDimensions, const Rml::String& source) override
        {
            return rmlTexManager->LoadTexture(textureHandle, textureDimensions, source);
        }

        bool GenerateTexture(Rml::TextureHandle& textureHandle, const Rml::byte* source, const Rml::Vector2i& sourceDimensions) override
        {
            return rmlTexManager->GenerateTexture(textureHandle, source, sourceDimensions);
        }

        void ReleaseTexture(Rml::TextureHandle texture) override
        {
            return rmlTexManager->release((uint32_t)texture);
        }

        void SetTransform(const Rml::Matrix4f* transform) override
        {
            // RML submits a nullptr as a stand-in for the identity matrix for some reason
            if (transform == nullptr)
                currentTransform = Rml::Matrix4f::Identity();
            else
                currentTransform = *transform;
        }

        void setupPanelRenderState(UIPanel* panel)
        {
            PanelRenderState* renderState = new PanelRenderState;
            auto tci = VK::TextureCreateInfo::RenderTarget2D(VK::TextureFormat::R8G8B8A8_SRGB, panel->getWidth(), panel->getHeight());
            tci.CanSample = true;
            renderState->texture = renderer->getCore()->CreateTexture(tci);
            renderState->panelBindlessId = renderer->getBindlessTextureManager()->AllocateTextureHandle(renderState->texture);

            renderState->vertexBuffer = new VK::FrameSeparatedBuffer(renderer->getCore(), {
                .Usage = VK::BufferUsage::Vertex,
                .Size = MAX_UI_VERTS * sizeof(Rml::Vertex),
                .Mappable = true
            });

            renderState->indexBuffer = new VK::FrameSeparatedBuffer(renderer->getCore(), {
                .Usage = VK::BufferUsage::Index,
                .Size = MAX_UI_INDICES * sizeof(uint32_t),
                .Mappable = true
            });

            panel->setRenderState(renderState);
        }
    private:
        void resizePanelTextureIfNecessary(UIPanel* panel)
        {
            PanelRenderState* renderState = panel->getRenderState();
            auto* texture = renderState->texture;
            
            if (texture->GetWidth() != panel->getWidth() || texture->GetHeight() != panel->getHeight())
            {
                auto tci = VK::TextureCreateInfo::RenderTarget2D(VK::TextureFormat::R8G8B8A8_SRGB, panel->getWidth(), panel->getHeight());
                tci.CanSample = true;
                renderState->texture = renderer->getCore()->CreateTexture(tci);
                renderer->getBindlessTextureManager()->SetTextureAt(renderState->panelBindlessId, renderState->texture);
            }
        }

        VKRenderer* renderer;
        UniquePtr<RmlTextureManager> rmlTexManager;
        UIPanel* renderingPanel = nullptr;
        UniquePtr<VK::PipelineLayout> pipelineLayout;
        UniquePtr<VK::Pipeline> pipeline;
        Rml::Matrix4f currentTransform;
    };
}
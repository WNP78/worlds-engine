#pragma once
#include <stddef.h>
#include <string.h>

namespace worlds
{
	template <size_t maxLen>
	class StackStringBuilder
	{
	public:
		void append(const char* str)
		{
			size_t numChars = strlen(str);
			if (numChars + idx >= maxLen)
			{
				return;
			}

			memcpy(buffer + idx, str, numChars);
			idx += numChars;
		}

		void appendFmt(const char* format, ...)
		{
			va_list args;
			va_start(args, format);
			vsnprintf(buffer, maxLen, format, args);
			va_end(args);
		}

		const char* get() const
		{
			return buffer;
		}

		void operator+=(const char* lhs)
		{
			append(lhs);
		}

		bool empty()
		{
			return idx == 0;
		}

		size_t numBytes()
		{
			return idx + 1;
		}

		void clear()
		{
			memset(buffer, '\0', maxLen);
			idx = 0;
		}
	private:
		char buffer[maxLen + 1] = { 0 };
		size_t idx = 0;
	};
}

#pragma once
#include <cstdint>
#include <entt/entt.hpp>
#include <physfs.h>
#include <vector>

namespace worlds
{
    class IScriptEngine;
    typedef uint32_t AssetID;
    typedef void (*SceneLoadCallback)(void* ctx, entt::registry& reg);

    // This class acts as a dispatcher for the two supported scene serialization formats.
    // Given a file, it will determine which format that file is and call the appropriate
    // serializer.
    class SceneLoader
    {
    public:
        // Loads a scene from the specified file.
        // If additive is true, the registry will be cleared before loading the scene.
        // The file handle will be closed after loading.
        static void loadScene(PHYSFS_File* file, entt::registry& reg, bool additive = false, const char* rootName = nullptr);
        static entt::entity loadEntity(PHYSFS_File* file, entt::registry& reg);
        static entt::entity loadEntity(AssetID id, entt::registry& reg);
        static entt::entity createPrefab(AssetID id, entt::registry& reg);
        static void registerLoadCallback(void* ctx, SceneLoadCallback callback);
        SceneLoader() = delete;
    };

    class JsonSceneSerializer
    {
    public:
        [[deprecated("Please use the AssetID or path variants to avoid data loss.")]] static void saveScene(
            PHYSFS_File* file, entt::registry& reg);
        static void saveScene(AssetID id, entt::registry& reg);
        static void saveScene(std::string path, entt::registry& reg);
        static void loadScene(PHYSFS_File* file, entt::registry& reg, const char* rootName = nullptr);

        static void saveEntity(PHYSFS_File* file, entt::registry& reg, entt::entity ent);
        // Returns entt::null if the entity JSON is invalid.
        static entt::entity loadEntity(PHYSFS_File* file, entt::registry& reg);
        static entt::entity loadEntity(AssetID id, entt::registry& reg);

        // Converts the specified entity to JSON.
        static std::string entityToJson(entt::registry& reg, entt::entity ent);
        // Returns entt::null if the entity JSON is invalid.
        static entt::entity jsonToEntity(entt::registry& reg, std::string json);

        static std::string entitiesToJson(entt::registry& reg, entt::entity* entities, size_t entityCount, entt::entity relativeRoot = entt::null);
        static std::vector<entt::entity> jsonToEntities(entt::registry& reg, std::string json);

        static void setScriptEngine(IScriptEngine* scriptEngine);

        JsonSceneSerializer() = delete;
    };
}

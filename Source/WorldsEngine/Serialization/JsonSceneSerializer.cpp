#include "Core/Engine.hpp"
#include <Core/WorldComponents.hpp>
#include "SceneSerialization.hpp"
#include <string>
#include <nlohmann/json.hpp>
#include <ComponentMeta/ComponentMetadata.hpp>
#include <Core/Transform.hpp>
#include <Util/TimingUtil.hpp>
#include "Core/AssetDB.hpp"
#include "robin_hood.h"
#include "slib/StaticAllocList.hpp"
#include "Scripting/ScriptEngine.hpp"
#include <Editor/Editor.hpp>
#include <tracy/Tracy.hpp>
#include <Core/Fatal.hpp>
#include <Core/HierarchyUtil.hpp>
#include <Core/NameComponent.hpp>
#include <Libs/simdjson.h>

namespace worlds
{
    struct CachedPrefab
    {
        simdjson::dom::parser* parser;
        simdjson::dom::element rootElement;
    };

    robin_hood::unordered_flat_map<entt::entity, entt::entity> idRemap;
    robin_hood::unordered_flat_map<AssetID, CachedPrefab> prefabCache;
    IScriptEngine* scriptEngine;

    simdjson::dom::element getPrefabJson(AssetID id)
    {
        auto cacheIt = prefabCache.find(id);

        if (cacheIt != prefabCache.end())
        {
            return cacheIt->second.rootElement;
        }
        else
        {
            // not in cache, load from disk
            PHYSFS_File* file = AssetDB::openAssetFileRead(id);
            PHYSFS_sint64 fileLength = PHYSFS_fileLength(file);
            simdjson::padded_string str{(size_t)fileLength};
            PHYSFS_readBytes(file, str.data(), fileLength);
            PHYSFS_close(file);
            auto* parser = new simdjson::dom::parser;
            auto result = parser->parse(str);

            CachedPrefab cachedPrefab
            {
                .parser = parser,
                .rootElement = result.value()
            };

            prefabCache.insert({id, cachedPrefab});
            return result.value();
        }
    }

    struct ComponentDeserializationInfo
    {
        std::string id;
        bool isNative;
    };

    // @param   j The entity JSON element (an object containing the entity's components as keys)
    void deserializeEntityComponents(simdjson::dom::element j, entt::registry& reg, entt::entity ent)
    {
        ZoneScoped;

        for (ComponentMetadata* md : ComponentMetadataManager::sorted)
        {
            auto val = j[md->getName()];

            if (val.error() != simdjson::SUCCESS) continue;
            auto componentObject = val.value();
            ComponentDataWrapper cdw{componentObject};
            md->fromJson(ent, reg, idRemap, cdw);
        }

        for (const auto& compPair : j.get_object())
        {
            char buffer[128] = {0};
            releaseAssert(compPair.key.size() < 128);
            memcpy(buffer, compPair.key.data(), compPair.key.size());
            bool isNative = ComponentMetadataManager::byName.contains(buffer);
            if (!isNative)
            {
                auto& componentObject = compPair.value;
                ComponentDataWrapper cdw{componentObject};
                scriptEngine->deserializeManagedComponent(buffer, cdw, ent);
            }
        }
    }

    entt::entity createJsonEntity(simdjson::dom::element j, entt::registry& reg, entt::entity id)
    {
        entt::entity ent = reg.create(id);
        deserializeEntityComponents(j, reg, ent);

        if (!reg.has<Transform>(ent))
        {
            logErr("Not deserializing entity because it lacks a transform");
            reg.destroy(ent);
            return entt::null;
        }

        return ent;
    }

    entt::entity createJsonEntity(simdjson::dom::element j, entt::registry& reg)
    {
        entt::entity ent = reg.create();
        deserializeEntityComponents(j, reg, ent);

        if (!reg.has<Transform>(ent))
        {
            logErr("Not deserializing entity because it lacks a transform");
            reg.destroy(ent);
            return entt::null;
        }

        return ent;
    }

    // Loads entities into the specified registry.
    // j is the array of entities to load.
    void loadSceneEntities(entt::registry& reg, simdjson::dom::object j, entt::entity parentTo = entt::null)
    {
        ZoneScoped;
        idRemap.clear();

        // 1. Create all the scene's entities and deserialize transforms
        for (auto p : j)
        {
            char buffer[16] = {0};
            releaseAssert(p.key.size() < 16);
            memcpy(buffer, p.key.data(), p.key.size());
            entt::entity id = (entt::entity)std::atol(buffer);
            entt::entity newEnt = reg.create(id);

            idRemap.insert({id, newEnt});
            // Even though the ID map isn't complete, it's fine to pass it in here since transforms don't need it
            ComponentDataWrapper cdw{};
            cdw.instanceEl = p.value["Transform"].value();
            cdw.isPrefab = false;
            ComponentMetadataManager::byName["Transform"]->fromJson(newEnt, reg, idRemap, cdw);
        }

        // 2. Create patched KVs for entities
        std::vector<std::pair<entt::entity, ComponentDataWrapper>> entityComponentData;
        for (auto p : j)
        {
            char buffer[16] = {0};
            releaseAssert(p.key.size() < 16);
            memcpy(buffer, p.key.data(), p.key.size());
            entt::entity newEnt = idRemap[(entt::entity)std::atol(buffer)];
            if (p.value["prefabPath"].error() == simdjson::SUCCESS)
            {
                if (p.value["diff"].error() == simdjson::SUCCESS)
                {
                    logWarn("Old style prefab in scene! Will be reset to default");
                    std::string prefabPath;
                    prefabPath.assign(p.value["prefabPath"].get_string().value());
                    AssetID prefabId = AssetDB::pathToId(prefabPath);

                    auto prefabJson = getPrefabJson(prefabId);
                    ComponentDataWrapper cdw{prefabJson};

                    entityComponentData.push_back({newEnt, cdw});
                    reg.emplace<PrefabInstanceComponent>(newEnt).prefab = prefabId;
                }
                else
                {
                    std::string prefabPath;
                    prefabPath.assign(p.value["prefabPath"].get_string().value());
                    AssetID prefabId = AssetDB::pathToId(prefabPath);
                    auto prefabJson = getPrefabJson(prefabId);

                    // find root entity
                    std::string_view rootKey;
                    for (const auto& prefabEntPair : prefabJson.get_object().value())
                    {
                        auto res = prefabEntPair.value.get_object()["ChildComponent"];

                        // if there's no childcomponent, we've found the root
                        if (res.error() == simdjson::NO_SUCH_FIELD)
                        {
                            rootKey = prefabEntPair.key;
                        }
                    }

                    ComponentDataWrapper cdw{p.value["changed"], prefabJson[rootKey]};

                    entityComponentData.push_back({newEnt, cdw});
                    reg.emplace<PrefabInstanceComponent>(newEnt).prefab = prefabId;
                }
            }
            else
            {
                entityComponentData.push_back({newEnt, ComponentDataWrapper{p.value}});
            }
        }

        // 3. Deserialize in component order
        for (ComponentMetadata* cm : ComponentMetadataManager::sorted)
        {
            for (auto pair : entityComponentData)
            {
                if (cm->getComponentID() == entt::type_id<Transform>().hash())
                {
                    continue;
                }

                if (pair.second.contains(cm->getName()))
                {
                    cm->fromJson(pair.first, reg, idRemap, pair.second.getObject(cm->getName()));
                }
            }
        }

        // 4. Deserialize managed components
        if (scriptEngine)
        {
            for (auto &pair: entityComponentData)
            {
                auto entityCdw = pair.second;
                auto obj = entityCdw.isPrefab ? entityCdw.prefabEl.get_object()
                                              : entityCdw.instanceEl.get_object();
                auto objV = obj.value();
                for (const auto &componentPair: objV)
                {
                    char buf[128] = {0};
                    releaseAssert(componentPair.key.size() < 128);
                    memcpy(buf, componentPair.key.data(), componentPair.key.size());

                    if (scriptEngine->isManagedComponent(buf))
                    {
                        ComponentDataWrapper componentCdw = entityCdw.getObject(buf);
                        scriptEngine->deserializeManagedComponent(buf, componentCdw, pair.first);
                    }
                }

                if (entityCdw.isPrefab)
                {
                    for (const auto& componentPair : entityCdw.instanceEl.get_object().value())
                    {
                        // only looking for components added in the instance, skip over ones in
                        // the prefab since they're already handled
                        if (entityCdw.prefabEl[componentPair.key].error() == simdjson::SUCCESS)
                            continue;

						char buf[128] = {0};
						releaseAssert(componentPair.key.size() < 128);
						memcpy(buf, componentPair.key.data(), componentPair.key.size());

						if (scriptEngine->isManagedComponent(buf))
						{
							ComponentDataWrapper componentCdw = entityCdw.getObject(buf);
							scriptEngine->deserializeManagedComponent(buf, componentCdw, pair.first);
						}
                    }
                }
            }
        }

        if (parentTo == entt::null)
        {
            return;
        }

        for (auto& pair : entityComponentData)
        {
            if (reg.has<ChildComponent>(pair.first))
            {
                continue;
            }

            HierarchyUtil::setEntityParent(reg, pair.first, parentTo);
        }
    }

    void deserializeJsonScene(simdjson::dom::element j, entt::registry& reg, entt::entity parentTo = entt::null)
    {
        loadSceneEntities(reg, j["entities"].value(), parentTo);
        SkySettings settings{};
        settings.skybox = AssetDB::pathToId(j["settings"]["skyboxPath"].get_string().value());
        settings.skyboxBoost = j["settings"]["skyboxBoost"].get_double().value();
        reg.set<SkySettings>(settings);
    }

    simdjson::dom::parser sceneParser;

    void JsonSceneSerializer::loadScene(PHYSFS_File* file, entt::registry& reg, const char* rootName)
    {
        PerfTimer timer;
        entt::entity sceneRoot = reg.create();
        reg.emplace<Transform>(sceneRoot);
        reg.emplace<NameComponent>(sceneRoot, rootName ? rootName : "Scene Root");
        reg.emplace<SceneRootComponent>(sceneRoot);

#ifndef _DEBUG
        try
#endif
        {
            PHYSFS_sint64 fileLen = PHYSFS_fileLength(file);
            prefabCache.clear();
            idRemap.clear();
            simdjson::padded_string str{(size_t)fileLen};
            PHYSFS_readBytes(file, str.data(), fileLen);

            auto v = sceneParser.parse(str);
            deserializeJsonScene(v.value(), reg, sceneRoot);

            logMsg("loaded json scene in %.3fms", timer.stopGetMs());
        }
#ifndef _DEBUG
        catch (simdjson::simdjson_error& ex)
        {
            logErr("Failed to load scene: %s", ex.what());
        }
#endif
    }

    entt::entity JsonSceneSerializer::loadEntity(PHYSFS_File* file, entt::registry& reg)
    {
        std::string str;
        str.resize(PHYSFS_fileLength(file));
        PHYSFS_readBytes(file, str.data(), str.size());
        return jsonToEntity(reg, str);
    }

    entt::entity JsonSceneSerializer::loadEntity(AssetID id, entt::registry& reg)
    {
        auto json = getPrefabJson(id);
        
        if (!json.is_object())
        {
            logErr("Invalid multiple entity json");
            return entt::null;
        }

        loadSceneEntities(reg, json);

        std::vector<entt::entity> loadedEntities;

        entt::entity root = entt::null;
        
        for (auto p : json.get_object().value())
        {
            char buf[16] = {0};
            releaseAssert(p.key.size() < 16);
            memcpy(buf, p.key.data(), p.key.size());
            entt::entity ent = idRemap[(entt::entity)std::stoul(buf)];
            if (!reg.has<ChildComponent>(ent))
                root = ent;
        }
        
        return root;
    }
    
    std::vector<entt::entity> JsonSceneSerializer::jsonToEntities(entt::registry& reg, std::string json)
    {
        simdjson::dom::parser p;
        auto j = p.parse(json);

        if (!j.is_object())
        {
            logErr("Invalid multiple entity json");
            return std::vector<entt::entity>{};
        }

        loadSceneEntities(reg, j);

        std::vector<entt::entity> loadedEntities;

        for (auto p : j.get_object().value())
        {
            char buf[128] = {0};
            releaseAssert(p.key.size() < 128);
            memcpy(buf, p.key.data(), p.key.size());
            loadedEntities.push_back(idRemap[(entt::entity)std::stoul(buf)]);
        }

        return loadedEntities;
    }
    
    entt::entity JsonSceneSerializer::jsonToEntity(entt::registry& reg, std::string jText)
    {
        simdjson::dom::parser parser;
        auto v = parser.parse(jText);
        return createJsonEntity(v.value(), reg);
    }

    // Serialization
    // =============

    // Serialization Utils
    
    nlohmann::json getEntityJson(entt::entity ent, entt::registry& reg)
    {
        nlohmann::json j;

        for (auto& mdata : ComponentMetadataManager::sorted)
        {
            std::array<ENTT_ID_TYPE, 1> arr = {mdata->getComponentID()};
            auto rView = reg.runtime_view(arr.begin(), arr.end());

            if (!rView.contains(ent))
                continue;

            nlohmann::json compJ;
            mdata->toJson(ent, reg, compJ);
            if (compJ.is_null())
                continue;
            
            j[mdata->getName()] = compJ;
        }

        scriptEngine->serializeManagedComponents(j, ent);

        return j;
    }

    nlohmann::json getPrefabJsonNMJson(AssetID id)
    {
        PHYSFS_File* file = AssetDB::openAssetFileRead(id);
        PHYSFS_sint64 fileLength = PHYSFS_fileLength(file);
        std::string str{};
        str.resize(fileLength);
        PHYSFS_readBytes(file, str.data(), fileLength);
        PHYSFS_close(file);

        return nlohmann::json::parse(str);
    }

    nlohmann::json entityVectorToJson(const std::vector<entt::entity>& v)
    {
        nlohmann::json arr = nlohmann::json::array();

        for (const entt::entity e : v)
        {
            arr.push_back((uint32_t)e);
        }

        return arr;
    }

    nlohmann::json getChanged(nlohmann::json from, nlohmann::json to)
    {
        nlohmann::json result = nlohmann::json::object();
        for (auto pair : to.items())
        {
            if (!from.contains(pair.key()))
            {
                result[pair.key()] = pair.value();
                continue;
            }

            if (pair.value() == from[pair.key()]) continue;

            if (pair.value().type() == nlohmann::json::value_t::object)
            {
                result[pair.key()] = getChanged(from[pair.key()], pair.value());
            }
            else
            {
                result[pair.key()] = pair.value();
            }
        }

        return result;
    }

    void serializeEntityInScene(nlohmann::json& entities, entt::entity ent, entt::registry& reg, entt::entity sceneRoot)
    {
        if (reg.has<SceneRootComponent>(ent)) return;
        nlohmann::json entity;

        if (reg.has<PrefabInstanceComponent>(ent))
        {
            PrefabInstanceComponent& pic = reg.get<PrefabInstanceComponent>(ent);
            nlohmann::json instanceJson = getEntityJson(ent, reg);
            nlohmann::json prefabJson = getPrefabJsonNMJson(pic.prefab);

            // find the root
            bool found = false;
            nlohmann::json prefab;
            for (auto& kv : prefabJson.items())
            {
                if (!kv.value().contains("ChildComponent"))
                {
                    prefab = kv.value();
                    found = true;
                }
            }

            assert(found);

            // HACK: To avoid generating patch data for the transform, set the serialized instances's transform
            // to the prefab's.
            instanceJson["Transform"] = prefab["Transform"];
            if (instanceJson.contains("ChildComponent") && instanceJson["ChildComponent"]["parent"].get<uint32_t>() == (uint32_t)sceneRoot)
            {
                instanceJson.erase("ChildComponent");
            }
            nlohmann::json transform;
            ComponentMetadataManager::byName["Transform"]->toJson(ent, reg, transform);

            std::string path = AssetDB::idToPath(pic.prefab);

            if (path.find("SourceData/") != std::string::npos)
                path = path.substr(11);

            auto changed = getChanged(prefab, instanceJson);

            entity = {
                {"changed", changed},
                {"prefabPath", path},
                {"Transform", transform}
            };
        }
        else
        {
            entity = getEntityJson(ent, reg);
            if (entity.contains("ChildComponent") && entity["ChildComponent"]["parent"].get<uint32_t>() == (uint32_t)sceneRoot)
            {
                entity.erase("ChildComponent");
            }
        }

        entities[std::to_string((uint32_t)ent)] = entity;
    }

    nlohmann::json sceneToJsonObject(entt::registry& reg)
    {
        nlohmann::json entities;
        entt::entity sceneRoot = reg.view<SceneRootComponent>().front();

        reg.view<Transform>(entt::exclude_t<DontSerialize>{}).each([&](entt::entity ent, Transform&)
		{
			if (HierarchyUtil::getParentScene(reg, ent) != sceneRoot) return;
            serializeEntityInScene(entities, ent, reg, sceneRoot);
        });

        nlohmann::json scene{
            {"entities", entities},
            {
                "settings",
                {
                    {"skyboxPath", AssetDB::idToPath(reg.ctx<SkySettings>().skybox)},
                    {"skyboxBoost", reg.ctx<SkySettings>().skyboxBoost}
                }
            }
        };

        return scene;
    }

    std::string sceneToJson(entt::registry& reg)
    {
        return sceneToJsonObject(reg).dump(4);
    }

    // Serialization Public Interface
    
    void JsonSceneSerializer::saveScene(PHYSFS_File* file, entt::registry& reg)
    {
        std::string jsonStr = sceneToJson(reg);
        PHYSFS_writeBytes(file, jsonStr.data(), jsonStr.size());
        PHYSFS_close(file);
    }

    void JsonSceneSerializer::saveScene(AssetID id, entt::registry& reg)
    {
        std::string jsonStr = sceneToJson(reg);
        PHYSFS_File* file = AssetDB::openAssetFileWrite(id);
        PHYSFS_writeBytes(file, jsonStr.data(), jsonStr.size());
        PHYSFS_close(file);
    }

    void JsonSceneSerializer::saveScene(std::string path, entt::registry& reg)
    {
        std::string jsonStr = sceneToJson(reg);
        PHYSFS_File* file = PHYSFS_openWrite(path.c_str());
        PHYSFS_writeBytes(file, jsonStr.data(), jsonStr.size());
        PHYSFS_close(file);
    }
    
    void JsonSceneSerializer::saveEntity(PHYSFS_File* file, entt::registry& reg, entt::entity ent)
    {
        std::string jsonStr = entityToJson(reg, ent);
        PHYSFS_writeBytes(file, jsonStr.data(), jsonStr.size());
    }

    std::string JsonSceneSerializer::entityToJson(entt::registry& reg, entt::entity ent)
    {
        nlohmann::json j = getEntityJson(ent, reg);
        return j.dump(4);
    }

    std::string JsonSceneSerializer::entitiesToJson(entt::registry& reg, entt::entity* entities, size_t entityCount, entt::entity relativeRoot)
    {
        nlohmann::json entitiesJ;

        for (size_t i = 0; i < entityCount; i++)
        {
            entt::entity root = relativeRoot;
            if (root == entt::null)
            {
				root = HierarchyUtil::getParentScene(reg, entities[i]);
            }
            serializeEntityInScene(entitiesJ, entities[i], reg, root);
        }

        return entitiesJ.dump(4);
    }

    void JsonSceneSerializer::setScriptEngine(IScriptEngine* scriptEngine)
    {
        worlds::scriptEngine = scriptEngine;
    }
}

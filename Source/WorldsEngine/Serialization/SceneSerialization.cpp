#include "SceneSerialization.hpp"
#include "../Core/AssetDB.hpp"
#include "../Core/Log.hpp"
#include "Core/Engine.hpp"
#include "Core/WorldComponents.hpp"
#include <Audio/Audio.hpp>
#include <Navigation/Navigation.hpp>
#include <physfs.h>
#include <tracy/Tracy.hpp>

#include "Core/HierarchyUtil.hpp"

namespace worlds
{
    struct LoadCallbackWithContext
    {
        void* ctx;
        SceneLoadCallback callback;
    };

    std::vector<LoadCallbackWithContext> loadCallbacks;

    void clearEntities(entt::registry& reg)
    {
        std::vector<entt::entity> entitiesToClear;
        // Exclude child entities here because they'll be dealt with when the parent is destroyed
        reg.view<Transform>(entt::exclude_t<KeepOnSceneLoad, ChildComponent>{}).each([&](entt::entity e, Transform&) {
            entitiesToClear.push_back(e);
        });

        for (entt::entity e : entitiesToClear)
        {
            reg.destroy(e, 0);
        }
    }

    // Do basic checks on the first byte to determine
    // the most appropriate scene serializer to call.
    void SceneLoader::loadScene(PHYSFS_File* file, entt::registry& reg, bool additive, const char* rootName)
    {
        ZoneScoped;
        if (PHYSFS_fileLength(file) <= 4)
        {
            logErr(WELogCategoryEngine, "Scene file was too short.");
            return;
        }

        unsigned char firstByte;
        PHYSFS_readBytes(file, &firstByte, 1);
        PHYSFS_seek(file, 0);

        // check first character of magic to determine WSCN or ESCN
        if (firstByte == '{')
        {
            if (!additive)
            {
                clearEntities(reg);
            }

            JsonSceneSerializer::loadScene(file, reg, rootName);
        }
        else
        {
            logErr(WELogCategoryEngine, "Scene has unrecognized file format");
            PHYSFS_close(file);
            return;
        }

        PHYSFS_close(file);
        AudioSystem::getInstance()->updateAudioScene(reg);
        NavigationSystem::updateNavMesh(reg);

        for (LoadCallbackWithContext loadCallback : loadCallbacks)
        {
            loadCallback.callback(loadCallback.ctx, reg);
        }
    }

    entt::entity SceneLoader::loadEntity(PHYSFS_File* file, entt::registry& reg)
    {
        return JsonSceneSerializer::loadEntity(file, reg);
    }

    entt::entity SceneLoader::createPrefab(AssetID id, entt::registry& reg)
    {
        if (id == INVALID_ASSET || !AssetDB::exists(id))
        {
            logWarn(WELogCategoryScripting, "Invalid asset ID!");
            return entt::null;
        }

        entt::entity ent = JsonSceneSerializer::loadEntity(id, reg);
        if (reg.valid(ent))
        {
            PrefabInstanceComponent& pic = reg.emplace<PrefabInstanceComponent>(ent);
            pic.prefab = id;
        }

        HierarchyUtil::setEntityParent(reg, ent, reg.view<SceneRootComponent>().front());

        if (reg.has<AudioSource>(ent))
        {
            auto& as = reg.get<AudioSource>(ent);
            if (as.playOnSceneStart)
            {
                as.eventInstance->start();
            }
        }
        
        return ent;
    }

    void SceneLoader::registerLoadCallback(void* ctx, SceneLoadCallback callback)
    {
        loadCallbacks.push_back({ ctx, callback });
    }
}

﻿#pragma once
#include <Libs/simdjson.h>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>
#include <glm/vec4.hpp>
#include <glm/gtc/quaternion.hpp>

namespace simdjson::dom
{
    template <>
    inline simdjson_result<float> element::get<float>() const noexcept
    {
        auto v = get_double();
        if (v.error())
        {
            return simdjson_result<float>{v.error()};
        }

        return simdjson_result((float)v.value_unsafe());
    }

    template <>
    inline simdjson_result<int32_t> element::get<int32_t>() const noexcept
    {
        auto v = get_int64();
        if (v.error())
        {
            return simdjson_result<int32_t>{v.error()};
        }

        return simdjson_result((int32_t)v.value_unsafe());
    }

    template <>
    inline simdjson_result<uint32_t> element::get<uint32_t>() const noexcept
    {
        auto v = get_uint64();
        if (v.error())
        {
            return simdjson_result<uint32_t>{v.error()};
        }

        return simdjson_result((uint32_t)v.value_unsafe());
    }
}

namespace worlds
{
    struct ComponentDataWrapper
    {
        ComponentDataWrapper() = default;

        ComponentDataWrapper(simdjson::dom::element instanceEl)
            : instanceEl(instanceEl)
              , isPrefab(false)
        {
        }

        ComponentDataWrapper(simdjson::dom::element instanceEl, simdjson::dom::element prefabEl)
            : instanceEl(instanceEl)
              , prefabEl(prefabEl)
              , isPrefab(true)
        {
        }

        simdjson::dom::element instanceEl;
        simdjson::dom::element prefabEl;
        bool isPrefab;

        auto getElementFallback(const char* key)
        {
            auto el1 = instanceEl[key];
            if (el1.error() == simdjson::SUCCESS)
            {
                return el1;
            }

            if (!isPrefab)
                return el1;

            return prefabEl[key];
        }

        bool contains(const char* key)
        {
            auto el1 = instanceEl[key];
            if (el1.error() == simdjson::SUCCESS) return true;

            if (!isPrefab) return false;

            return prefabEl[key].error() == simdjson::SUCCESS;
        }

        template <typename T>
        T get(const char* key)
        {
            return getElementFallback(key).get<T>().value();
        }

        template <typename T>
        T get(const char* key, T defaultVal)
        {
            auto el = getElementFallback(key).get<T>();

            if (el.error() == simdjson::SUCCESS)
            {
                return el.value();
            }

            return defaultVal;
        }

        template <typename V>
        V getVec(const char* key)
        {
            auto el = getElementFallback(key).get_array().value();

            V v{};
            for (int i = 0; i < V::length(); i++)
            {
                v[i] = el.at(i).get_double().value();
            }

            return v;
        }

        glm::quat getQuat(const char* key)
        {
            auto e = getElementFallback(key);
            auto arr = e.get_array().value();
            assert(arr.size() == 4);
            return glm::quat
            {
                (float)arr.at(3).get_double().value(),
                (float)arr.at(0).get_double().value(),
                (float)arr.at(1).get_double().value(),
                (float)arr.at(2).get_double().value()
            };
        }

        std::string getString(const char* key)
        {
            return std::string(getElementFallback(key).get_string().value());
        }

        std::string_view getStringView(const char* key)
        {
            return getElementFallback(key).get_string().value();
        }

        simdjson::dom::array getArray(const char* key)
        {
            return getElementFallback(key).get_array().value();
        }

        ComponentDataWrapper getObject(const char* key)
        {
            ComponentDataWrapper newCdw;
            newCdw.isPrefab = false;
            auto el1 = instanceEl[key];

            if (!isPrefab)
            {
                newCdw.instanceEl = el1.value();
                return newCdw;
            }

            auto el2 = prefabEl[key];

            if (el1.error())
            {
                newCdw.instanceEl = el2.value();
                return newCdw;
            }

            if (el2.error())
            {
                newCdw.instanceEl = el1.value();
                return newCdw;
            }

            newCdw.instanceEl = el1.value();
            newCdw.prefabEl = el2.value();
            newCdw.isPrefab = true;
            return newCdw;
        }
    };
}

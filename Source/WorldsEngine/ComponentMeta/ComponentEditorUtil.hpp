#pragma once
#include "ComponentFuncs.hpp"
#include <entt/core/type_info.hpp>
#include <entt/entt.hpp>

namespace worlds
{
    template <typename T> class BasicComponentUtil : public ComponentMetadata
    {
    private:
        template <typename = typename std::is_default_constructible<T>::type>
        void createInternal(entt::entity ent, entt::registry& reg)
        {
            reg.emplace<T>(ent);
        }

    public:
        void create(entt::entity ent, entt::registry& reg) override
        {
            if constexpr (std::is_default_constructible<T>::value)
            {
                createInternal(ent, reg);
            }
            else
            {
                assert(false);
            }
        }

        void destroy(entt::entity ent, entt::registry& reg) override
        {
            reg.remove_if_exists<T>(ent);
        }

        bool allowInspectorAdd() override
        {
            return true;
        }

        ENTT_ID_TYPE getComponentID() override
        {
            return entt::type_id<T>().hash();
        }

        uint32_t getSerializedID() override
        {
            return entt::hashed_string{getName()};
        }
    };
}

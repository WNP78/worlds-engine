#include "ComponentFuncs.hpp"
#include "ComponentEditorUtil.hpp"
#include <Audio/Audio.hpp>
#include <Core/Engine.hpp>
#include <Core/HierarchyUtil.hpp>
#include <Core/NameComponent.hpp>
#include <Core/Transform.hpp>
#include <Editor/Editor.hpp>
#include <Editor/GuiUtil.hpp>
#include <ImGui/imgui.h>
#include <Libs/IconsFontAwesome5.h>
#include <Libs/IconsFontaudio.h>
#include <Physics/D6Joint.hpp>
#include <Physics/Physics.hpp>
#include <Render/DebugLines.hpp>
#include <Render/Render.hpp>
#include <Scripting/ScriptComponent.hpp>
#include <Serialization/SceneSerialization.hpp>
#include <UI/WorldSpaceUIPanel.hpp>
#include <Util/CreateModelObject.hpp>
#include <Util/EnumUtil.hpp>
#include <Util/JsonUtil.hpp>
#include <entt/entt.hpp>
#include <foundation/PxTransform.h>
#include <glm/gtc/constants.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <nlohmann/json.hpp>
#include <robin_hood.h>
#include <tracy/Tracy.hpp>
#include <UI/UIPanel.hpp>

using json = nlohmann::json;

glm::vec3 getVec3(simdjson::dom::element e)
{
    assert(e.is_array());
    auto arr = e.get_array().value();
    return glm::vec3
    {
        arr.at(0).get_double().value(),
        arr.at(1).get_double().value(),
        arr.at(2).get_double().value()
    };
}

glm::vec4 getVec4(simdjson::dom::element e)
{
    assert(e.is_array());
    auto arr = e.get_array().value();
    return glm::vec4
    {
        arr.at(0).get_double().value(),
        arr.at(1).get_double().value(),
        arr.at(2).get_double().value(),
        arr.at(3).get_double().value()
    };
}

glm::quat getQuat(simdjson::dom::element e)
{
    assert(e.is_array());
    auto arr = e.get_array().value();
    assert(arr.size() == 4);
    return glm::quat
    {
        (float)arr.at(3).get_double().value(),
        (float)arr.at(0).get_double().value(),
        (float)arr.at(1).get_double().value(),
        (float)arr.at(2).get_double().value()
    };
}

float getDefaultFloat(simdjson::dom::element e, const char* key, float defaultVal)
{
    if (e[key].error()) return defaultVal;
    return e[key].get_double().value();
}

bool getDefaultBool(simdjson::dom::element e, const char* key, bool defaultVal)
{
    if (e[key].error()) return defaultVal;
    return e[key].get_bool().value();
}

int getDefaultInt(simdjson::dom::element e, const char* key, int defaultVal)
{
    if (e[key].error()) return defaultVal;
    return e[key].get_int64().value();
}

bool jsonContains(simdjson::dom::element e, const char* key)
{
    return e[key].error() == simdjson::SUCCESS;
}

// Janky workaround to fix static constructors not being called
// (static constructors are only required to be called before the first function in the translation unit)
// (yay for typical c++ specification bullshittery)
#include "D6JointEditor.hpp"

namespace worlds
{
    ComponentEditorLink* ComponentMetadata::first = nullptr;

    ComponentMetadata::ComponentMetadata()
    {
        if (!first)
        {
            first = new ComponentEditorLink;
            first->next = nullptr;
        }
        else
        {
            ComponentEditorLink* next = first;
            first = new ComponentEditorLink;
            first->next = next;
        }

        first->editor = this;
    }

    class TransformEditor : public virtual BasicComponentUtil<Transform>
    {
    private:
        glm::vec3 getEulerAngles(glm::quat rotation)
        {
            float roll = atan2f(
                2.0f * rotation.y * rotation.w - 2.0f * rotation.x * rotation.z,
                1.0f - 2.0f * rotation.y * rotation.y - 2.0f * rotation.z * rotation.z
            );
            float pitch = atan2f(
                2.0f * rotation.x * rotation.w - 2.0f * rotation.y * rotation.z,
                1.0f - 2.0f * rotation.x * rotation.x - 2.0f * rotation.z * rotation.z
            );
            float yaw = glm::asin(2.0f * rotation.x * rotation.y + 2.0f * rotation.z * rotation.w);

            return glm::vec3(pitch, roll, yaw);
        }

        glm::quat eulerQuat(glm::vec3 eulerAngles)
        {
            eulerAngles *= 0.5f;
            glm::vec3 c(cosf(eulerAngles.x), cosf(eulerAngles.y), cosf(eulerAngles.z));
            glm::vec3 s(sinf(eulerAngles.x), sinf(eulerAngles.y), sinf(eulerAngles.z));

            float w = c.x * c.y * c.z - s.x * s.y * s.z;
            float x = s.x * c.y * c.z + c.x * s.y * s.z;
            float y = c.x * s.y * c.z + s.x * c.y * s.z;
            float z = c.x * c.y * s.z - s.x * s.y * c.z;

            return glm::quat{w, x, y, z};
        }

        bool showTransformControls(entt::registry& reg, Transform& selectedTransform, Editor* ed)
        {
            bool modified = false;
            glm::vec3 pos = selectedTransform.position;
            if (ImGui::DragFloat3("Position", &pos.x))
            {
                ed->undo.pushState(reg);
                selectedTransform.position = pos;
                modified = true;
            }

            glm::vec3 eulerRot = glm::degrees(getEulerAngles(selectedTransform.rotation));
            if (ImGui::DragFloat3("Rotation", glm::value_ptr(eulerRot)))
            {
                ed->undo.pushState(reg);
                selectedTransform.rotation = eulerQuat(glm::radians(eulerRot));
                modified = true;
            }

            glm::vec3 scale = selectedTransform.scale;
            if (ImGui::DragFloat3("Scale", &scale.x) && !glm::any(glm::equal(scale, glm::vec3{0.0f})))
            {
                selectedTransform.scale = scale;
                modified = true;
            }

            if (ImGui::Button("Snap to world grid"))
            {
                ed->undo.pushState(reg);
                selectedTransform.position = glm::round(selectedTransform.position);
                selectedTransform.scale = glm::round(selectedTransform.scale);
                eulerRot = glm::round(eulerRot / 15.0f) * 15.0f;
                selectedTransform.rotation = glm::radians(eulerRot);
                modified = true;
            }

            ImGui::SameLine();

            if (ImGui::Button("Snap Rotation"))
            {
                ed->undo.pushState(reg);
                // eulerRot = glm::round(eulerRot / 15.0f) * 15.0f;
                glm::vec3 radEuler = glm::eulerAngles(selectedTransform.rotation);
                const float ROUND_TO = glm::half_pi<float>() * 0.25f;
                radEuler = glm::round(radEuler / ROUND_TO) * ROUND_TO;
                selectedTransform.rotation = radEuler;
                modified = true;
            }

            return modified;
        }

    public:
        int getSortID() override
        {
            return -1;
        }

        const char* getName() override
        {
            return "Transform";
        }

        bool allowInspectorAdd() override
        {
            return false;
        }

#ifdef BUILD_EDITOR
        void edit(entt::entity ent, entt::registry& reg, Editor* ed) override
        {
            if (ImGui::CollapsingHeader(ICON_FA_ARROWS_ALT u8" Transform"))
            {
                auto& selectedTransform = reg.get<Transform>(ent);

                if (!reg.has<ChildComponent>(ent))
                {
                    showTransformControls(reg, selectedTransform, ed);
                }
                else
                {
                    auto& cc = reg.get<ChildComponent>(ent);
                    auto& parentTransform = reg.get<Transform>(cc.parent);
                    if (showTransformControls(reg, cc.offset, ed))
                    {
                        reg.emplace_or_replace<LocalTransformDirty>(ent);
                        selectedTransform = cc.offset.transformBy(parentTransform);
                    }

                    if (ImGui::TreeNode("World Space Transform"))
                    {
                        if (showTransformControls(reg, selectedTransform, ed))
                        {
                            auto& parentTransform = reg.get<Transform>(cc.parent);
                            cc.offset = selectedTransform.transformByInverse(parentTransform);
                            reg.emplace_or_replace<LocalTransformDirty>(ent);
                        }
                        ImGui::TreePop();
                    }
                }

                ImGui::Separator();
            }
        }
#endif

        void toJson(entt::entity ent, entt::registry& reg, nlohmann::json& j) override
        {
            const auto& t = reg.get<Transform>(ent);
            j = {{"position", t.position}, {"rotation", t.rotation}, {"scale", t.scale}};
        }

        void fromJson(entt::entity ent, entt::registry& reg, EntityIDMap&, ComponentDataWrapper j) override
        {
            ZoneScoped;
            auto& t = reg.emplace_or_replace<Transform>(ent);
            t.position = j.getVec<glm::vec3>("position");
            t.rotation = j.getQuat("rotation");
            t.scale = j.getVec<glm::vec3>("scale");
        }
    };

    const robin_hood::unordered_flat_map<StaticFlags, const char*> flagNames = {
        {StaticFlags::Audio, "Audio"}, {StaticFlags::Rendering, "Rendering"}, {StaticFlags::Navigation, "Navigation"}
    };

    class WorldObjectEditor : public BasicComponentUtil<WorldObject>
    {
    public:
        const char* getName() override
        {
            return "World Object";
        }

        void create(entt::entity ent, entt::registry& reg) override
        {
            auto cubeId = AssetDB::pathToId("model.obj");
            auto matId = AssetDB::pathToId("Materials/DevTextures/dev_blue.wmatj");
            reg.emplace<WorldObject>(ent, matId, cubeId);
        }

#ifdef BUILD_EDITOR
        void edit(entt::entity ent, entt::registry& reg, Editor* ed) override
        {
            if (ImGui::CollapsingHeader(ICON_FA_PENCIL_ALT u8" WorldObject"))
            {
                if (ImGui::Button("Remove##WO"))
                {
                    reg.remove<WorldObject>(ent);
                }
                else
                {
                    auto& worldObject = reg.get<WorldObject>(ent);
                    if (ImGui::TreeNode("Static Flags"))
                    {
                        for (int i = 1; i < 8; i <<= 1)
                        {
                            bool hasFlag = enumHasFlag(worldObject.staticFlags, (StaticFlags)i);
                            if (ImGui::Checkbox(flagNames.at((StaticFlags)i), &hasFlag))
                            {
                                int withoutI = (int)worldObject.staticFlags & (~i);
                                withoutI |= i * hasFlag;
                                worldObject.staticFlags = (StaticFlags)withoutI;
                            }
                        }
                        ImGui::TreePop();
                    }

                    ImGui::DragFloat2("Texture Scale", &worldObject.texScaleOffset.x);
                    ImGui::DragFloat2("Texture Offset", &worldObject.texScaleOffset.z);

                    ImGui::Text("Mesh: %s", AssetDB::idToPath(worldObject.mesh).c_str());
                    ImGui::SameLine();

                    selectAssetPopup("Mesh", worldObject.mesh, ImGui::Button("Change##Mesh"));

                    bool hitNotSet = false;
                    int notSetCount = 0;
                    if (ImGui::TreeNode("Materials"))
                    {
                        if (ImGui::Button("Reset"))
                        {
                            worldObject.materials[0] = AssetDB::pathToId("Materials/DevTextures/dev_blue.wmatj");
                            worldObject.presentMaterials[0] = true;
                            for (int i = 1; i < NUM_SUBMESH_MATS; i++)
                            {
                                worldObject.materials[i] = INVALID_ASSET;
                                worldObject.presentMaterials[i] = false;
                            }
                        }
                        for (int i = 0; i < NUM_SUBMESH_MATS; i++)
                        {
                            bool showButton = false;

                            if (worldObject.presentMaterials[i])
                            {
                                if (hitNotSet)
                                {
                                    hitNotSet = false;
                                    ImGui::Text("%i unset materials", notSetCount);
                                    notSetCount = 0;
                                }

                                showButton = true;
                                ImGui::Text("Material %i: %s", i, AssetDB::idToPath(worldObject.materials[i]).c_str());
                            }
                            else
                            {
                                notSetCount++;
                                worldObject.materials[i] = INVALID_ASSET;

                                if (!hitNotSet)
                                {
                                    hitNotSet = true;
                                    ImGui::Text("Material %i: not set", i);
                                    showButton = true;
                                }
                            }

                            if (showButton)
                            {
                                ImGui::SameLine();

                                std::string idStr = "##" + std::to_string(i);

                                bool open = ImGui::Button(("Change" + idStr).c_str());
                                if (selectAssetPopup(("Material" + idStr).c_str(), worldObject.materials[i], open))
                                {
                                    worldObject.presentMaterials[i] = true;
                                }
                            }
                        }

                        if (hitNotSet)
                        {
                            hitNotSet = false;
                            ImGui::Text("%i unset materials", notSetCount);
                            notSetCount = 0;
                        }

                        ImGui::TreePop();
                    }

                    if (ImGui::TreeNode("Drawn Submeshes"))
                    {
                        const LoadedMesh& lm = MeshManager::loadOrGet(worldObject.mesh);
                        for (int i = 0; i < lm.numSubmeshes; i++)
                        {
                            bool drawn = worldObject.drawSubmeshes[i];
                            if (ImGui::Checkbox(std::to_string(i).c_str(), &drawn))
                                worldObject.drawSubmeshes[i] = drawn;
                        }

                        ImGui::TreePop();
                    }
                }

                ImGui::Separator();
            }
        }
#endif

        void toJson(entt::entity ent, entt::registry& reg, json& j) override
        {
            auto& wo = reg.get<WorldObject>(ent);

            uint32_t materialCount = wo.presentMaterials.count();
            nlohmann::json matArray;

            for (uint32_t i = 0; i < materialCount; i++)
            {
                matArray.push_back(AssetDB::idToPath(wo.materials[i]));
            }

            nlohmann::json drawnSubmeshArray;

            const LoadedMesh& lm = MeshManager::loadOrGet(wo.mesh);

            for (int i = 0; i < lm.numSubmeshes; i++)
            {
                drawnSubmeshArray.push_back((bool)wo.drawSubmeshes[i]);
            }

            j = {
                {"mesh", AssetDB::idToPath(wo.mesh)},
                {"texScaleOffset", wo.texScaleOffset},
                {"materials", matArray},
                {"drawnSubmeshes", drawnSubmeshArray},
                {"staticFlags", wo.staticFlags}
            };
        }

        void fromJson(entt::entity ent, entt::registry& reg, EntityIDMap&, ComponentDataWrapper j) override
        {
            ZoneScoped;
            auto& wo = reg.emplace<WorldObject>(ent, 0, 0);
            wo.mesh = AssetDB::pathToId(j.getStringView("mesh"));
            wo.texScaleOffset = j.getVec<glm::vec4>("texScaleOffset");
            wo.staticFlags = (StaticFlags)j.get<int>("staticFlags");

            uint32_t matIdx = 0;

            for (auto v : j.getArray("materials"))
            {
                wo.presentMaterials[matIdx] = true;
                if (v.is_number())
                    wo.materials[matIdx] = (AssetID)v.get_uint64().value();
                else
                {
                    wo.materials[matIdx] = AssetDB::pathToId(v.get_string().value());
                }
                matIdx++;
            }

            if (j.contains("drawnSubmeshes"))
            {
                auto dsArray = j.getArray("drawnSubmeshes");
                for (size_t i = 0; i < dsArray.size(); i++)
                {
                    wo.drawSubmeshes[i] = dsArray.at(i).get<bool>().value();
                }
            }
        }
    };

    class SkinnedWorldObjectEditor : public BasicComponentUtil<SkinnedWorldObject>
    {
    public:
        const char* getName() override
        {
            return "Skinned World Object";
        }

        void create(entt::entity ent, entt::registry& reg) override
        {
            auto cubeId = AssetDB::pathToId("model.obj");
            auto matId = AssetDB::pathToId("Materials/DevTextures/dev_blue.json");
            reg.emplace<SkinnedWorldObject>(ent, matId, cubeId);
        }

#ifdef BUILD_EDITOR
        void edit(entt::entity ent, entt::registry& reg, Editor* ed) override
        {
            if (ImGui::CollapsingHeader(ICON_FA_PENCIL_ALT u8" SkinnedWorldObject"))
            {
                if (ImGui::Button("Remove##WO"))
                {
                    reg.remove<SkinnedWorldObject>(ent);
                }
                else
                {
                    auto& worldObject = reg.get<SkinnedWorldObject>(ent);
                    if (ImGui::TreeNode("Static Flags"))
                    {
                        for (int i = 1; i < 8; i <<= 1)
                        {
                            bool hasFlag = enumHasFlag(worldObject.staticFlags, (StaticFlags)i);
                            if (ImGui::Checkbox(flagNames.at((StaticFlags)i), &hasFlag))
                            {
                                int withoutI = (int)worldObject.staticFlags & (~i);
                                withoutI |= i * hasFlag;
                                worldObject.staticFlags = (StaticFlags)withoutI;
                            }
                        }
                        ImGui::TreePop();
                    }

                    ImGui::DragFloat2("Texture Scale", &worldObject.texScaleOffset.x);
                    ImGui::DragFloat2("Texture Offset", &worldObject.texScaleOffset.z);

                    ImGui::Text("Mesh: %s", AssetDB::idToPath(worldObject.mesh).c_str());
                    ImGui::SameLine();

                    if (selectAssetPopup("Mesh", worldObject.mesh, ImGui::Button("Change##Mesh")))
                    {
                        worldObject.resetPose();
                    }

                    if (ImGui::TreeNode("Materials"))
                    {
                        for (int i = 0; i < NUM_SUBMESH_MATS; i++)
                        {
                            if (worldObject.presentMaterials[i])
                            {
                                ImGui::Text("Material %i: %s", i, AssetDB::idToPath(worldObject.materials[i]).c_str());
                            }
                            else
                            {
                                ImGui::Text("Material %i: not set", i);
                                worldObject.materials[i] = INVALID_ASSET;
                            }

                            ImGui::SameLine();

                            std::string idStr = "##" + std::to_string(i);

                            bool open = ImGui::Button(("Change" + idStr).c_str());
                            if (selectAssetPopup(("Material" + idStr).c_str(), worldObject.materials[i], open))
                            {
                                worldObject.presentMaterials[i] = true;
                            }
                        }
                        ImGui::TreePop();
                    }
                }

                ImGui::Separator();
            }
        }
#endif

        void toJson(entt::entity ent, entt::registry& reg, json& j) override
        {
            auto& wo = reg.get<SkinnedWorldObject>(ent);

            uint32_t materialCount = wo.presentMaterials.count();
            nlohmann::json matArray;

            for (uint32_t i = 0; i < materialCount; i++)
            {
                matArray.push_back(AssetDB::idToPath(wo.materials[i]));
            }

            j = {
                {"mesh", AssetDB::idToPath(wo.mesh)},
                {"texScaleOffset", wo.texScaleOffset},
                {"materials", matArray},
                {"staticFlags", wo.staticFlags}
            };
        }

        void fromJson(entt::entity ent, entt::registry& reg, EntityIDMap&, ComponentDataWrapper j) override
        {
            ZoneScoped;
            AssetID mesh = AssetDB::pathToId(j.getStringView("mesh"));
            auto& wo = reg.emplace<SkinnedWorldObject>(ent, INVALID_ASSET, mesh);

            wo.texScaleOffset = j.getVec<glm::vec4>("texScaleOffset");
            wo.staticFlags = (StaticFlags)j.get<int>("staticFlags");

            uint32_t matIdx = 0;

            auto matArray = j.getArray("materials");
            for (auto v : matArray)
            {
                wo.presentMaterials[matIdx] = true;
                if (v.is_number())
                    wo.materials[matIdx] = (AssetID)v.get_uint64().value();
                else
                {
                    wo.materials[matIdx] = AssetDB::pathToId(v.get_string().value());
                }
                matIdx++;
            }
        }
    };

    const std::unordered_map<LightType, const char*> lightTypeNames{
        {LightType::Directional, "Directional"},
        {LightType::Point, "Point"},
        {LightType::Spot, "Spot"},
        {LightType::Sphere, "Sphere"},
        {LightType::Tube, "Tube"}
    };

    class WorldLightEditor : public BasicComponentUtil<WorldLight>
    {
    private:
        void showTypeSpecificControls(WorldLight& worldLight)
        {
            // Show options specific to certain types of light
            switch (worldLight.type)
            {
            case LightType::Spot:
                {
                    float cutoff = glm::degrees(worldLight.spotCutoff);
                    ImGui::DragFloat("Spot Cutoff", &cutoff, 1.0f, 0.0f, 90.0f);
                    worldLight.spotCutoff = glm::radians(cutoff);

                    float outerCutoff = glm::degrees(worldLight.spotOuterCutoff);
                    ImGui::DragFloat("Spot Outer Cutoff", &outerCutoff, 1.0f, cutoff, 90.0f);
                    worldLight.spotOuterCutoff = glm::radians(outerCutoff);

                    ImGui::Checkbox("Enable Shadows", &worldLight.enableShadows);

                    if (worldLight.enableShadows)
                    {
                        ImGui::DragFloat("Near Plane", &worldLight.shadowNear, 0.1f, 0.001f, FLT_MAX);
                        ImGui::DragFloat("Far Plane", &worldLight.shadowFar, 1.0f, worldLight.shadowNear, FLT_MAX);
                        ImGui::InputFloat("Shadow Bias", &worldLight.shadowBias, 0.0001f, 0.001f, "%.6f");
                        ImGui::DragFloat("Shadow Radius", &worldLight.shadowRadius, 0.1f);
                    }
                }
                break;
            case LightType::Sphere:
                {
                    ImGui::DragFloat("Sphere Radius", &worldLight.spotCutoff, 1.0f, 0.0f, FLT_MAX);
                }
                break;
            case LightType::Tube:
                {
                    ImGui::DragFloat("Tube Length", &worldLight.tubeLength, 0.1f, 0.0f, FLT_MAX);
                    ImGui::DragFloat("Tube Radius", &worldLight.tubeRadius, 0.1f, 0.0f, FLT_MAX);
                }
                break;
            case LightType::Directional:
                {
                    ImGui::Checkbox("Enable Shadows", &worldLight.enableShadows);
                    if (worldLight.enableShadows)
                    {
                        ImGui::InputFloat("Shadow Bias", &worldLight.shadowBias, 0.0001f, 0.001f, "%.6f");
                        ImGui::DragFloat("Shadow Radius", &worldLight.shadowRadius, 0.1f);
                    }
                }
                break;
            default:
                break;
            }
        }

    public:
        const char* getName() override
        {
            return "World Light";
        }

#ifdef BUILD_EDITOR
        void edit(entt::entity ent, entt::registry& reg, Editor* ed) override
        {
            if (ImGui::CollapsingHeader(ICON_FA_LIGHTBULB u8" Light"))
            {
                if (ImGui::Button("Remove##WL"))
                {
                    reg.remove<WorldLight>(ent);
                }
                else
                {
                    WorldLight& worldLight = reg.get<WorldLight>(ent);
                    ImGui::Text("Current res: %i", worldLight.currentResolution);
                    Transform& transform = reg.get<Transform>(ent);

                    ImGui::Checkbox("Enabled", &worldLight.enabled);
                    ImGui::ColorEdit3("Color", &worldLight.color.x, ImGuiColorEditFlags_Float);
                    ImGui::DragFloat(
                        "Intensity",
                        &worldLight.intensity,
                        0.1f,
                        0.000001f,
                        FLT_MAX,
                        "%.3f",
                        ImGuiSliderFlags_AlwaysClamp
                    );
                    tooltipHover("Controls brightness of the light in the scene.");

                    if (ImGui::BeginCombo("Light Type", lightTypeNames.at(worldLight.type)))
                    {
                        for (auto& p : lightTypeNames)
                        {
                            bool isSelected = worldLight.type == p.first;
                            if (ImGui::Selectable(p.second, &isSelected))
                            {
                                worldLight.type = p.first;
                                if (p.first != LightType::Spot && p.first != LightType::Directional)
                                {
                                    worldLight.enableShadows = false;
                                }
                            }

                            if (isSelected)
                                ImGui::SetItemDefaultFocus();
                        }
                        ImGui::EndCombo();
                    }

                    // Show a rough guide for a radius - the point at which the light's
                    // intensity falls to less than 0.05
                    ImGui::Text("Recommended radius: %.3f", glm::sqrt(worldLight.intensity / 0.05f));
                    tooltipHover("This is the distance at which a light's influence falls below 5%.");

                    ImGui::DragFloat("Radius", &worldLight.maxDistance);
                    tooltipHover("Controls the maximum distance at which a light still affects objects.");

                    showTypeSpecificControls(worldLight);
                }
            }
        }

        void drawGizmos(entt::entity ent, entt::registry& reg, Editor* ed) override
        {
            WorldLight& worldLight = reg.get<WorldLight>(ent);
            Transform& transform = reg.get<Transform>(ent);

            if (worldLight.type == LightType::Spot)
            {
                glm::vec3 lightForward = transform.rotation * glm::vec3(0.0f, 0.0f, 1.0f);
                float radiusAtCutoff = glm::tan(worldLight.spotCutoff) * worldLight.maxDistance * 0.5f;
                drawCircle(
                    transform.position + lightForward * worldLight.maxDistance * 0.5f,
                    radiusAtCutoff,
                    transform.rotation * glm::angleAxis(glm::half_pi<float>(), glm::vec3(1.f, 0.f, 0.f)),
                    glm::vec4(1.0f)
                );
                drawLine(
                    transform.position,
                    transform.position + transform.rotation * glm::vec3(radiusAtCutoff, 0.0f, 0.0f) +
                    lightForward * worldLight.maxDistance * 0.5f,
                    glm::vec4(1.0f)
                );
                drawLine(
                    transform.position,
                    transform.position + transform.rotation * glm::vec3(-radiusAtCutoff, 0.0f, 0.0f) +
                    lightForward * worldLight.maxDistance * 0.5f,
                    glm::vec4(1.0f)
                );
                drawLine(
                    transform.position,
                    transform.position + transform.rotation * glm::vec3(0.0f, 0.0f, radiusAtCutoff) +
                    lightForward * worldLight.maxDistance * 0.5f,
                    glm::vec4(1.0f)
                );
                drawLine(
                    transform.position,
                    transform.position + transform.rotation * glm::vec3(0.0f, 0.0f, -radiusAtCutoff) +
                    lightForward * worldLight.maxDistance * 0.5f,
                    glm::vec4(1.0f)
                );
            }
            else
            {
                drawSphere(transform.position, transform.rotation, worldLight.maxDistance);
            }
        }
#endif

        void toJson(entt::entity ent, entt::registry& reg, json& j) override
        {
            auto& wl = reg.get<WorldLight>(ent);

            j = {
                {"type", wl.type},
                {"color", wl.color},
                {"spotCutoff", wl.spotCutoff},
                {"spotOuterCutoff", wl.spotOuterCutoff},
                {"intensity", wl.intensity},
                {"tubeLength", wl.tubeLength},
                {"tubeRadius", wl.tubeRadius},
                {"enableShadows", wl.enableShadows},
                {"enabled", wl.enabled},
                {"maxDistance", wl.maxDistance},
                {"shadowNear", wl.shadowNear},
                {"shadowFar", wl.shadowFar},
                {"shadowBias", wl.shadowBias},
                {"shadowRadius", wl.shadowRadius}
            };
        }

        void fromJson(entt::entity ent, entt::registry& reg, EntityIDMap&, ComponentDataWrapper j) override
        {
            ZoneScoped;
            auto& wl = reg.emplace<WorldLight>(ent);

            wl.type = (LightType)j.get<int>("type");
            wl.color = j.getVec<glm::vec3>("color");
            wl.spotCutoff = j.get<float>("spotCutoff");
            wl.spotOuterCutoff = j.get<float>("spotOuterCutoff", glm::pi<float>() * 0.6f);
            wl.intensity = j.get<float>("intensity");
            wl.tubeLength = j.get<float>("tubeLength");
            wl.tubeRadius = j.get<float>("tubeRadius");
            wl.enableShadows = j.get<bool>("enableShadows", false);
            wl.enabled = j.get<bool>("enabled", true);

            if (!j.contains("maxDistance") && j.contains("distanceCutoff"))
            {
                wl.maxDistance = 1.0f / sqrtf(j.get<float>("distanceCutoff"));
            }
            else
            {
                wl.maxDistance = j.get<float>("maxDistance", wl.maxDistance);
            }

            wl.shadowNear = j.get<float>("shadowNear", wl.shadowNear);
            wl.shadowFar = j.get<float>("shadowFar", wl.shadowFar);
            wl.shadowBias = j.get<float>("shadowBias", wl.shadowBias);
            wl.shadowRadius = j.get<float>("shadowRadius", wl.shadowRadius);
        }
    };

    const char* shapeTypeNames[(int)PhysicsShapeType::Count] = {"Sphere", "Box", "Capsule", "Mesh", "Convex Mesh"};

    PhysicsShapeType valToShapeType(std::string str)
    {
        if (str == "CUBE")
            return PhysicsShapeType::Box;
        else
            return PhysicsShapeType::Sphere;
    }

    std::vector<PhysicsShape> loadColliderJson(const char* path)
    {
        auto* file = PHYSFS_openRead(path);
        auto len = PHYSFS_fileLength(file);

        std::string str;
        str.resize(len);

        PHYSFS_readBytes(file, str.data(), len);
        PHYSFS_close(file);

        json j = json::parse(str);

        std::vector<PhysicsShape> shapes;
        shapes.reserve(j.size());

        for (const auto& el : j)
        {
            PhysicsShape shape;
            // const auto& typeval = el.get_value_of_key(sajson::string{ "type", 4 });
            shape.type = valToShapeType(el["type"]);

            shape.pos = el["position"];
            glm::vec3 eulerAngles = el["rotation"];
            shape.pos = glm::vec3{shape.pos.x, shape.pos.z, -shape.pos.y};
            eulerAngles = glm::vec3{eulerAngles.x, eulerAngles.z, -eulerAngles.y};
            shape.rot = glm::quat{eulerAngles};

            if (shape.type == PhysicsShapeType::Box)
            {
                shape.box.halfExtents = el["scale"];
                shape.box.halfExtents =
                    glm::abs(glm::vec3{shape.box.halfExtents.x, shape.box.halfExtents.z, shape.box.halfExtents.y});
            }

            shapes.push_back(shape);
        }

        return shapes;
    }

    glm::vec4 physShapeColor{0.f, 1.f, 0.f, 1.f};
    // Draws a box shape using lines.
    void drawPhysicsBox(const Transform& actorTransform, const PhysicsShape& ps)
    {
        Transform shapeTransform{ps.pos, ps.rot};
        shapeTransform = shapeTransform.transformBy(actorTransform);
        drawBox(
            shapeTransform.position, shapeTransform.rotation, ps.box.halfExtents * actorTransform.scale, physShapeColor
        );
    }

    void drawPhysicsSphere(const Transform& actorTransform, const PhysicsShape& ps)
    {
        Transform shapeTransform{ps.pos, ps.rot};
        shapeTransform = shapeTransform.transformBy(actorTransform);
        drawSphere(shapeTransform.position, shapeTransform.rotation, ps.sphere.radius, physShapeColor);
    }

    void drawPhysicsCapsule(const Transform& actorTransform, const PhysicsShape& ps)
    {
        Transform shapeTransform{ps.pos, ps.rot};
        shapeTransform = shapeTransform.transformBy(actorTransform);
        drawCapsule(
            shapeTransform.position,
            shapeTransform.rotation * glm::quat{glm::vec3{0.0f, 0.0f, glm::half_pi<float>()}},
            ps.capsule.height * 0.5f,
            ps.capsule.radius,
            physShapeColor
        );
    }

    void drawPhysicsMesh(const Transform& actorTransform, const PhysicsShape& ps)
    {
        AssetID meshId = ps.type == PhysicsShapeType::ConvexMesh ? ps.convexMesh.mesh : ps.mesh.mesh;
        const LoadedMesh& lm = MeshManager::loadOrGet(meshId);

        for (size_t i = 0; i < lm.indices.size(); i += 3)
        {
            glm::vec3 p0 =
                actorTransform.transformPoint(lm.vertices[lm.indices[i + 0]].position);
            glm::vec3 p1 =
                actorTransform.transformPoint(lm.vertices[lm.indices[i + 1]].position);
            glm::vec3 p2 =
                actorTransform.transformPoint(lm.vertices[lm.indices[i + 2]].position);

            drawLine(p0, p1, physShapeColor);
            drawLine(p1, p2, physShapeColor);
            drawLine(p0, p2, physShapeColor);
        }
    }

    // Draws the given shape using lines.
    void drawPhysicsShape(const Transform& actorTransform, const PhysicsShape& ps)
    {
        switch (ps.type)
        {
        case PhysicsShapeType::Box:
            drawPhysicsBox(actorTransform, ps);
            break;
        case PhysicsShapeType::Sphere:
            drawPhysicsSphere(actorTransform, ps);
            break;
        case PhysicsShapeType::ConvexMesh:
        case PhysicsShapeType::Mesh:
            drawPhysicsMesh(actorTransform, ps);
            break;
        case PhysicsShapeType::Capsule:
            drawPhysicsCapsule(actorTransform, ps);
            break;
        default:
            break;
        }
    }

    template <typename T>
    void editPhysicsShapes(T& actor, Transform& actorTransform, worlds::Editor* ed)
    {
        static size_t currentShapeIdx = 0;
        static bool transformingShape = false;
        ImGui::Checkbox("Scale Shapes", &actor.scaleShapes);
        if (ImGui::Button("Load Collider JSON"))
        {
            ImGui::OpenPopup("Collider JSON");
        }

        const char* extension = ".json";
        openFileModalOffset(
            "Collider JSON",
            [&](const char* p) { actor.physicsShapes = loadColliderJson(p); },
            "SourceData/",
            &extension,
            1,
            "ColliderJsons"
        );

        ImGui::Text("Shapes: %zu", actor.physicsShapes.size());

        ImGui::SameLine();

        if (ImGui::Button("Add"))
        {
            actor.physicsShapes.push_back(PhysicsShape::boxShape(glm::vec3(0.5f)));
        }

        std::vector<PhysicsShape>::iterator eraseIter;
        bool erase = false;

        size_t i = 0;
        for (auto it = actor.physicsShapes.begin(); it != actor.physicsShapes.end(); it++)
        {
            ImGui::PushID(i);
            if (ImGui::BeginCombo("Collider Type", shapeTypeNames[(int)it->type]))
            {
                for (int iType = 0; iType < (int)PhysicsShapeType::Count; iType++)
                {
                    auto type = (PhysicsShapeType)iType;
                    bool isSelected = it->type == type;
                    if (ImGui::Selectable(shapeTypeNames[iType], &isSelected))
                    {
                        it->type = type;

                        if (type == PhysicsShapeType::Mesh)
                        {
                            it->mesh.mesh = ~0u;
                        }

                        if (type == PhysicsShapeType::ConvexMesh)
                        {
                            it->convexMesh.mesh = ~0u;
                        }
                    }

                    if (isSelected)
                        ImGui::SetItemDefaultFocus();
                }
                ImGui::EndCombo();
            }


            if (ImGui::Button("Remove Shape"))
            {
                eraseIter = it;
                erase = true;
            }

            static Transform t;

            if (transformingShape && i == currentShapeIdx)
            {
                ed->overrideHandle(&t);
                Transform shapeTransform = t.transformByInverse(actorTransform);
                it->pos = shapeTransform.position;
                it->rot = shapeTransform.rotation;
                if (ImGui::Button("Done"))
                {
                    transformingShape = false;
                }
            }
            else
            {
                if (ImGui::Button("Transform"))
                {
                    transformingShape = true;
                    currentShapeIdx = i;
                    t = Transform{it->pos, it->rot}.transformBy(actorTransform);
                }
            }

            ImGui::DragFloat3("Position", &it->pos.x);

            switch (it->type)
            {
            case PhysicsShapeType::Sphere:
                ImGui::DragFloat("Radius", &it->sphere.radius);
                break;
            case PhysicsShapeType::Box:
                ImGui::DragFloat3("Half extents", &it->box.halfExtents.x);
                break;
            case PhysicsShapeType::Capsule:
                ImGui::DragFloat("Height", &it->capsule.height);
                ImGui::DragFloat("Radius", &it->capsule.radius);
                break;
            case PhysicsShapeType::Mesh:
                if (it->mesh.mesh == ~0u)
                    ImGui::Text("No mesh set");
                else
                    ImGui::Text("%s", AssetDB::idToPath(it->mesh.mesh).c_str());
                selectAssetPopup("Mesh", it->mesh.mesh, ImGui::Button("Change"));
                break;
            case PhysicsShapeType::ConvexMesh:
                if (it->convexMesh.mesh == ~0u)
                    ImGui::Text("No mesh set");
                else
                    ImGui::Text("%s", AssetDB::idToPath(it->convexMesh.mesh).c_str());
                selectAssetPopup("Mesh", it->convexMesh.mesh, ImGui::Button("Change"));
                break;
            default:
                break;
            }
            ImGui::PopID();
            i++;
        }

        if (erase)
            actor.physicsShapes.erase(eraseIter);
    }

    class PhysicsActorEditor : public BasicComponentUtil<PhysicsActor>
    {
    public:
        const char* getName() override
        {
            return "Physics Actor";
        }

        void create(entt::entity ent, entt::registry& reg) override
        {
            auto& t = reg.get<Transform>(ent);

            physx::PxTransform pTf(glm2px(t.position), glm2px(t.rotation));
            auto* actor = g_physics->physics()->createRigidStatic(pTf);
            reg.emplace<PhysicsActor>(ent, actor);
            g_physics->scene()->addActor(*actor);
        }

#ifdef BUILD_EDITOR
        void edit(entt::entity ent, entt::registry& reg, Editor* ed) override
        {
            auto& pa = reg.get<PhysicsActor>(ent);
            if (ImGui::CollapsingHeader(ICON_FA_SHAPES u8" Physics Actor"))
            {
                if (ImGui::Button("Remove##PA"))
                {
                    reg.remove<PhysicsActor>(ent);
                }
                else
                {
                    if (ImGui::Button("Update Collisions"))
                    {
                        g_physics->updatePhysicsShapes(pa, reg.get<Transform>(ent).scale);
                    }

                    auto& t = reg.get<Transform>(ent);
                    editPhysicsShapes(pa, t, ed);
                }

                ImGui::Separator();
            }
        }

        void drawGizmos(entt::entity ent, entt::registry& reg, Editor* ed) override
        {
            Transform& t = reg.get<Transform>(ent);
            PhysicsActor& pa = reg.get<PhysicsActor>(ent);

            for (const PhysicsShape& ps : pa.physicsShapes)
            {
                drawPhysicsShape(t, ps);
            }
        }
#endif

        void toJson(entt::entity ent, entt::registry& reg, json& j) override
        {
            auto& pa = reg.get<PhysicsActor>(ent);
            json shapeArray;

            for (auto& shape : pa.physicsShapes)
            {
                json jShape = {{"type", shape.type}, {"position", shape.pos}, {"rotation", shape.rot}};

                switch (shape.type)
                {
                case PhysicsShapeType::Sphere:
                    jShape["radius"] = shape.sphere.radius;
                    break;
                case PhysicsShapeType::Box:
                    jShape["halfExtents"] = shape.box.halfExtents;
                    break;
                case PhysicsShapeType::Capsule:
                    jShape["height"] = shape.capsule.height;
                    jShape["radius"] = shape.capsule.radius;
                    break;
                case PhysicsShapeType::Mesh:
                    jShape["mesh"] = AssetDB::idToPath(shape.mesh.mesh);
                    break;
                default:
                    assert(false && "invalid physics shape type");
                    break;
                }

                shapeArray.push_back(jShape);
            }

            j["shapes"] = shapeArray;
            j["layer"] = pa.layer;
        }

        void fromJson(entt::entity ent, entt::registry& reg, EntityIDMap&, ComponentDataWrapper j) override
        {
            ZoneScoped;
            auto* pActor = g_physics->physics()->createRigidStatic(glm2px(reg.get<Transform>(ent)));
            g_physics->scene()->addActor(*pActor);

            PhysicsActor& pa = reg.emplace<PhysicsActor>(ent, pActor);

            for (auto shape : j.getArray("shapes"))
            {
                PhysicsShape ps;

                ps.type = (PhysicsShapeType)shape["type"].get_int64().value();

                switch (ps.type)
                {
                case PhysicsShapeType::Sphere:
                    ps.sphere.radius = shape["radius"].get_double().value();
                    break;
                case PhysicsShapeType::Box:
                    ps.box.halfExtents = getVec3(shape["halfExtents"].value());
                    break;
                case PhysicsShapeType::Capsule:
                    ps.capsule.height = shape["height"].get_double().value();
                    ps.capsule.radius = shape["radius"].get_double().value();
                    break;
                case PhysicsShapeType::Mesh:
                    ps.mesh.mesh = AssetDB::pathToId(shape["mesh"].get_string().value());
                    break;
                default:
                    assert(false && "invalid physics shape type");
                    break;
                }

                ps.pos = getVec3(shape["position"].value());
                ps.rot = getQuat(shape["rotation"].value());

                pa.physicsShapes.push_back(ps);
            }

            pa.layer = j.get<int>("layer", 1);

            if (pa.layer == 0)
                pa.layer = 1;

            auto& t = reg.get<Transform>(ent);

            g_physics->updatePhysicsShapes(pa, t.scale);
        }
    };

    class RigidBodyEditor : public BasicComponentUtil<RigidBody>
    {
    public:
        int getSortID() override
        {
            return 1;
        }

        const char* getName() override
        {
            return "Dynamic Physics Actor";
        }

        void create(entt::entity ent, entt::registry& reg) override
        {
            auto& t = reg.get<Transform>(ent);

            physx::PxTransform pTf(glm2px(t.position), glm2px(t.rotation));
            auto* actor = g_physics->physics()->createRigidDynamic(pTf);
            actor->setSolverIterationCounts(24, 4);
            actor->setMaxAngularVelocity(1000.0f);
            reg.emplace<RigidBody>(ent, actor);
            g_physics->scene()->addActor(*actor);
        }

#ifdef BUILD_EDITOR
        void edit(entt::entity ent, entt::registry& reg, Editor* ed) override
        {
            auto& pa = reg.get<RigidBody>(ent);
            if (ImGui::CollapsingHeader(ICON_FA_SHAPES u8" RigidBody"))
            {
                if (ImGui::Button("Remove##RB"))
                {
                    reg.remove<RigidBody>(ent);
                }
                else
                {
                    if (ImGui::TreeNode("Lock Flags"))
                    {
                        uint32_t flags = (uint32_t)pa.lockFlags();

                        ImGui::CheckboxFlags("Lock X", &flags, (uint32_t)DPALockFlags::LinearX);
                        ImGui::CheckboxFlags("Lock Y", &flags, (uint32_t)DPALockFlags::LinearY);
                        ImGui::CheckboxFlags("Lock Z", &flags, (uint32_t)DPALockFlags::LinearZ);

                        ImGui::CheckboxFlags("Lock Angular X", &flags, (uint32_t)DPALockFlags::AngularX);
                        ImGui::CheckboxFlags("Lock Angular Y", &flags, (uint32_t)DPALockFlags::AngularY);
                        ImGui::CheckboxFlags("Lock Angular Z", &flags, (uint32_t)DPALockFlags::AngularZ);

                        pa.setLockFlags((DPALockFlags)flags);
                        ImGui::TreePop();
                    }

                    ImGui::DragScalar("Layer", ImGuiDataType_U32, &pa.layer);
                    ImGui::DragFloat("Mass", &pa.mass);
                    bool enabled = pa.enabled();

                    if (ImGui::Checkbox("Enabled", &enabled))
                    {
                        pa.setEnabled(enabled);
                    }

                    ImGui::Checkbox("Enable Gravity", &pa.enableGravity);
                    ImGui::Checkbox("Enable CCD", &pa.enableCCD);
                    if (ImGui::Button("Update Collisions##DPA"))
                    {
                        auto& t = reg.get<Transform>(ent);
                        g_physics->updatePhysicsShapes(pa, t.scale);
                        updateMass(pa);
                    }

                    auto& t = reg.get<Transform>(ent);
                    editPhysicsShapes(pa, t, ed);
                }

                ImGui::Separator();
            }
        }

        void drawGizmos(entt::entity ent, entt::registry& reg, Editor* ed) override
        {
            Transform& t = reg.get<Transform>(ent);
            RigidBody& pa = reg.get<RigidBody>(ent);

            for (const PhysicsShape& ps : pa.physicsShapes)
            {
                drawPhysicsShape(t, ps);
            }
        }
#endif

        void toJson(entt::entity ent, entt::registry& reg, json& j) override
        {
            auto& pa = reg.get<RigidBody>(ent);
            json shapeArray;

            for (auto& shape : pa.physicsShapes)
            {
                json jShape = {{"type", shape.type}, {"position", shape.pos}, {"rotation", shape.rot}};

                switch (shape.type)
                {
                case PhysicsShapeType::Sphere:
                    jShape["radius"] = shape.sphere.radius;
                    break;
                case PhysicsShapeType::Box:
                    jShape["halfExtents"] = shape.box.halfExtents;
                    break;
                case PhysicsShapeType::Capsule:
                    jShape["height"] = shape.capsule.height;
                    jShape["radius"] = shape.capsule.radius;
                    break;
                case PhysicsShapeType::ConvexMesh:
                    jShape["mesh"] = AssetDB::idToPath(shape.convexMesh.mesh);
                    break;
                default:
                    assert(false && "invalid physics shape type");
                    break;
                }

                shapeArray.push_back(jShape);
            }

            j["shapes"] = shapeArray;
            j["mass"] = pa.mass;
            j["enableCCD"] = pa.enableCCD;
            j["enableGravity"] = pa.enableGravity;
            j["lockFlags"] = (uint32_t)pa.lockFlags();
            j["layer"] = pa.layer;
            j["scaleShapes"] = pa.scaleShapes;
            if (!pa.enabled())
                j["enabled"] = false;
        }

        void fromJson(entt::entity ent, entt::registry& reg, EntityIDMap&, ComponentDataWrapper j) override
        {
            ZoneScoped;
            auto* pActor = g_physics->physics()->createRigidDynamic(glm2px(reg.get<Transform>(ent)));
            pActor->setSolverIterationCounts(24, 3);
            pActor->setMaxAngularVelocity(1000.0f);
            g_physics->scene()->addActor(*pActor);

            auto& pa = reg.emplace<RigidBody>(ent, pActor);
            pa.scaleShapes = j.get<bool>("scaleShapes", true);

            for (auto shape : j.getArray("shapes"))
            {
                PhysicsShape ps;

                ps.type = (PhysicsShapeType)shape["type"].get_int64().value();

                switch (ps.type)
                {
                case PhysicsShapeType::Sphere:
                    ps.sphere.radius = shape["radius"].get_double().value();
                    break;
                case PhysicsShapeType::Box:
                    ps.box.halfExtents = getVec3(shape["halfExtents"].value());
                    break;
                case PhysicsShapeType::Capsule:
                    ps.capsule.height = shape["height"].get_double().value();
                    ps.capsule.radius = shape["radius"].get_double().value();
                    break;
                case PhysicsShapeType::ConvexMesh:
                    ps.convexMesh.mesh = AssetDB::pathToId(shape["mesh"].get_string().value());
                    break;
                default:
                    assert(false && "invalid physics shape type");
                    break;
                }
                ps.pos = getVec3(shape["position"].value());
                ps.rot = getQuat(shape["rotation"].value());

                pa.physicsShapes.push_back(ps);
            }

            pa.layer = j.get<int>("layer", 1);

            if (pa.layer == 0)
                pa.layer = 1;

            auto& t = reg.get<Transform>(ent);

            g_physics->updatePhysicsShapes(pa, t.scale);
            pa.mass = j.get<float>("mass");
            pa.enableCCD = j.get<bool>("enableCCD", false);
            pa.enableGravity = j.get<bool>("enableGravity", true);
            updateMass(pa);

            if (j.contains("lockFlags"))
                pa.setLockFlags((DPALockFlags)j.get<int>("lockFlags"));

            if (!j.get<bool>("enabled", true))
                pa.setEnabled(false);
        }
    };

    class NameComponentEditor : public BasicComponentUtil<NameComponent>
    {
    public:
        int getSortID() override
        {
            return -2;
        }

        const char* getName() override
        {
            return "Name Component";
        }

#ifdef BUILD_EDITOR
        void edit(entt::entity ent, entt::registry& registry, Editor* ed) override
        {
            auto& nc = registry.get<NameComponent>(ent);

            ImGui::InputText("Name", &nc.name);
            ImGui::SameLine();
            if (ImGui::Button("Remove##Name"))
            {
                registry.remove<NameComponent>(ent);
            }
            ImGui::Separator();
        }
#endif

        void toJson(entt::entity ent, entt::registry& reg, json& j) override
        {
            auto& nc = reg.get<NameComponent>(ent);
            j = {{"name", nc.name}};
        }

        void fromJson(entt::entity ent, entt::registry& reg, EntityIDMap&, ComponentDataWrapper j) override
        {
            auto& nc = reg.emplace<NameComponent>(ent);
            nc.name = j.getString("name");
        }
    };

    class FMODAudioSourceEditor : public BasicComponentUtil<AudioSource>
    {
    public:
        const char* getName() override
        {
            return "FMOD Audio Source";
        }

        void create(entt::entity ent, entt::registry& reg) override
        {
            reg.emplace<AudioSource>(ent);
        }

#ifdef BUILD_EDITOR
        void edit(entt::entity ent, entt::registry& registry, Editor* ed) override
        {
            auto& as = registry.get<AudioSource>(ent);

            if (ImGui::CollapsingHeader(ICON_FA_VOLUME_UP u8" Audio Source"))
            {
                static bool editingEventPath = false;
                static std::string currentEventPath;

                if (!editingEventPath)
                {
                    ImGui::Text("Current event path: %s", as.eventPath().data());

                    if (ImGui::Button("Change"))
                    {
                        editingEventPath = true;
                        currentEventPath.assign(as.eventPath());
                    }
                }
                else
                {
                    if (ImGui::InputText("Event Path", &currentEventPath, ImGuiInputTextFlags_EnterReturnsTrue))
                    {
                        editingEventPath = false;
                        as.changeEventPath(currentEventPath);
                    }
                    if (!AudioSystem::getInstance()->validateEventPath(currentEventPath.c_str()))
                    {
                        ImGui::TextColored(ImVec4(1.f, 0.f, 0.f, 1.f), "Invalid event path");
                    }
                }

                ImGui::Checkbox("Play On Scene Start", &as.playOnSceneStart);

                if (as.eventInstance != nullptr)
                {
                    if (as.playbackState() != FMOD_STUDIO_PLAYBACK_PLAYING)
                    {
                        if (ImGui::Button(ICON_FA_PLAY u8" Preview"))
                            as.eventInstance->start();
                    }
                    else
                    {
                        if (ImGui::Button(ICON_FA_STOP u8" Stop"))
                            as.eventInstance->stop(FMOD_STUDIO_STOP_ALLOWFADEOUT);
                    }
                }

                ImGui::Separator();
            }
        }
#endif

        void toJson(entt::entity ent, entt::registry& reg, json& j) override
        {
            auto& as = reg.get<AudioSource>(ent);
            std::string evtPath;
            evtPath.assign(as.eventPath());

            j = {{"eventPath", evtPath}, {"playOnSceneStart", as.playOnSceneStart}};
        }

        void fromJson(entt::entity ent, entt::registry& reg, EntityIDMap&, ComponentDataWrapper j) override
        {
            ZoneScoped;
            auto& as = reg.emplace<AudioSource>(ent);
            as.playOnSceneStart = j.get<bool>("playOnSceneStart");
            as.changeEventPath(j.getString("eventPath"));
        }
    };

    class WorldCubemapEditor : public BasicComponentUtil<WorldCubemap>
    {
    public:
        const char* getName() override
        {
            return "World Cubemap";
        }

        void create(entt::entity ent, entt::registry& reg) override
        {
            auto& wc = reg.emplace<WorldCubemap>(ent);

            wc.cubemapId = AssetDB::pathToId(
                "LevelData/Cubemaps/" + reg.ctx<SceneInfo>().name + "/" + reg.get<NameComponent>(ent).name + ".wtex"
            );
            wc.extent = glm::vec3{1.0f};
        }

#ifdef BUILD_EDITOR
        void edit(entt::entity ent, entt::registry& reg, Editor* ed) override
        {
            auto& wc = reg.get<WorldCubemap>(ent);
            wc.cubemapId = AssetDB::pathToId(
                "LevelData/Cubemaps/" + reg.ctx<SceneInfo>().name + "/" + reg.get<NameComponent>(ent).name + ".wtex"
            );

            if (ImGui::CollapsingHeader(ICON_FA_CIRCLE u8" Cubemap"))
            {
                ImGui::DragFloat3("Extent", &wc.extent.x);
                ImGui::DragFloat3("Capture Offset", glm::value_ptr(wc.captureOffset));
                ImGui::InputInt("Resolution", &wc.resolution);
                tooltipHover("Powers of two are highly recommended for this setting.");
                ImGui::Checkbox("Parallax Correction", &wc.cubeParallax);
                ImGui::InputInt("Priority", &wc.priority);
                tooltipHover("Cubemaps with a higher priority value will be preferred over cubemaps with a lower "
                    "priority value.");
                ImGui::DragFloat("Blend Distance", &wc.blendDistance);

                Transform boundsTransform{};
                boundsTransform.position = reg.get<Transform>(ent).position;
                boundsTransform.scale = wc.extent;
                drawBox(boundsTransform.position, glm::quat{1.0f, 0.0f, 0.0f, 0.0f}, wc.extent);
                drawSphere(
                    boundsTransform.position + wc.captureOffset,
                    glm::quat{1.0f, 0.0f, 0.0f, 0.0f},
                    0.25f,
                    glm::vec4(1.0f)
                );

                ImGui::Separator();
            }
        }
#endif

        void toJson(entt::entity ent, entt::registry& reg, json& j) override
        {
            auto& wc = reg.get<WorldCubemap>(ent);

            j = {
                {"useCubeParallax", wc.cubeParallax},
                {"extent", wc.extent},
                {"priority", wc.priority},
                {"resolution", wc.resolution},
                {"captureOffset", wc.captureOffset},
                {"blendDistance", wc.blendDistance}
            };
        }

        void fromJson(entt::entity ent, entt::registry& reg, EntityIDMap&, ComponentDataWrapper j) override
        {
            ZoneScoped;
            auto& wc = reg.emplace<WorldCubemap>(ent);

            const char* ext = ".wtex";

            wc.cubemapId = AssetDB::pathToId(
                "LevelData/Cubemaps/" + reg.ctx<SceneInfo>().name + "/" + reg.get<NameComponent>(ent).name + ext
            );
            wc.extent = j.getVec<glm::vec3>("extent");
            wc.cubeParallax = j.get<bool>("useCubeParallax");
            wc.priority = j.get<int>("priority", 0);
            wc.resolution = j.get<int>("resolution", 128);
            wc.captureOffset = j.getVec<glm::vec3>("captureOffset");
            wc.blendDistance = j.get<float>("blendDistance", 1.0f);
        }
    };

    class ReverbProbeBoxEditor : public BasicComponentUtil<ReverbProbeBox>
    {
    public:
        const char* getName() override
        {
            return "Reverb Probe Box";
        }

#ifdef BUILD_EDITOR
        void edit(entt::entity ent, entt::registry& reg, Editor* ed) override
        {
            if (ImGui::CollapsingHeader("Reverb Probe Box"))
            {
                ImGui::Separator();
            }
        }
#endif

        void toJson(entt::entity ent, entt::registry& reg, json& j) override
        {
            j = {};
        }

        void fromJson(entt::entity ent, entt::registry& reg, EntityIDMap&, ComponentDataWrapper j) override
        {
            ZoneScoped;
            reg.emplace<ReverbProbeBox>(ent);
        }
    };

    class AudioTriggerEditor : public BasicComponentUtil<AudioTrigger>
    {
    public:
        const char* getName() override
        {
            return "Audio Trigger";
        }

#ifdef BUILD_EDITOR
        void edit(entt::entity ent, entt::registry& reg, Editor* ed) override
        {
            auto& trigger = reg.get<AudioTrigger>(ent);

            if (ImGui::CollapsingHeader("Audio Trigger"))
            {
                if (ImGui::Button("Remove##AudioTrigger"))
                {
                    reg.remove<AudioTrigger>(ent);
                    return;
                }

                ImGui::Checkbox("Play Once", &trigger.playOnce);
                ImGui::Separator();
            }
        }
#endif

        void toJson(entt::entity ent, entt::registry& reg, json& j) override
        {
            auto& at = reg.get<AudioTrigger>(ent);

            j = {{"playOnce", at.playOnce}};
        }

        void fromJson(entt::entity ent, entt::registry& reg, EntityIDMap&, ComponentDataWrapper j) override
        {
            ZoneScoped;
            auto& at = reg.emplace<AudioTrigger>(ent);

            at.playOnce = j.get<bool>("playOnce");
        }
    };

    class ProxyAOEditor : public BasicComponentUtil<ProxyAOComponent>
    {
    public:
        const char* getName() override
        {
            return "AO Proxy";
        }

#ifdef BUILD_EDITOR
        void edit(entt::entity ent, entt::registry& reg, Editor* ed) override
        {
            auto& pac = reg.get<ProxyAOComponent>(ent);

            if (ImGui::CollapsingHeader("AO Proxy"))
            {
                if (ImGui::Button("Remove##AOProxy"))
                {
                    reg.remove<ProxyAOComponent>(ent);
                    return;
                }
                ImGui::DragFloat3("bounds", &pac.bounds.x);
                ImGui::Separator();
            }
        }
#endif

        void toJson(entt::entity ent, entt::registry& reg, json& j) override
        {
            auto& pac = reg.get<ProxyAOComponent>(ent);

            j = {{"bounds", pac.bounds}};
        }

        void fromJson(entt::entity ent, entt::registry& reg, EntityIDMap&, ComponentDataWrapper j) override
        {
            ZoneScoped;
            auto& pac = reg.emplace<ProxyAOComponent>(ent);

            pac.bounds = j.getVec<glm::vec3>("bounds");
        }
    };

    class PrefabInstanceEditor : public BasicComponentUtil<PrefabInstanceComponent>
    {
    public:
        const char* getName() override
        {
            return "Prefab Instance";
        }

#ifdef BUILD_EDITOR
        void edit(entt::entity ent, entt::registry& reg, Editor* ed) override
        {
            auto& pic = reg.get<PrefabInstanceComponent>(ent);

            if (ImGui::CollapsingHeader("Prefab Instance"))
            {
                if (ImGui::Button("Remove##PrefabInstance"))
                {
                    reg.remove<PrefabInstanceComponent>(ent);
                    return;
                }

                ImGui::Text("Instance of %s", AssetDB::idToPath(pic.prefab).c_str());

                if (ImGui::Button("Apply Prefab"))
                {
                    AssetID prefabId = pic.prefab;
                    std::string path = AssetDB::idToPath(pic.prefab);
                    
                    if (path.find("SourceData") == std::string::npos)
                    {
                        path = "SourceData/" + path;
                    }

                    // Remove the prefab instance component temporarily so we serialize the whole entity
                    reg.remove<PrefabInstanceComponent>(ent);
                    slib::List<entt::entity> prefabEntities = HierarchyUtil::getAllChildEntities(reg, ent);
                    prefabEntities.add(ent);

                    entt::entity relativeRoot = entt::null;

                    if (reg.has<ChildComponent>(ent))
                    {
                        relativeRoot = reg.get<ChildComponent>(ent).parent;
                    }
                    
                    std::string json = JsonSceneSerializer::entitiesToJson(reg, prefabEntities.data(), prefabEntities.numElements(), relativeRoot);
                    
                    PHYSFS_File* file = PHYSFS_openWrite(path.c_str());
                    PHYSFS_writeBytes(file, json.data(), json.size());
                    PHYSFS_close(file);
                    reg.emplace<PrefabInstanceComponent>(ent, prefabId);
                }

                ImGui::Separator();
            }
        }
#endif

        void toJson(entt::entity ent, entt::registry& reg, json& j) override
        {
            j = nullptr;
        }

        void fromJson(entt::entity ent, entt::registry& reg, EntityIDMap&, ComponentDataWrapper j) override
        {
        }
    };

    class SphereAOProxyEditor : public BasicComponentUtil<SphereAOProxy>
    {
    public:
        const char* getName() override
        {
            return "Sphere AO Proxy";
        }

#ifdef BUILD_EDITOR
        void edit(entt::entity entity, entt::registry& reg, Editor* ed) override
        {
            auto& proxy = reg.get<SphereAOProxy>(entity);

            if (ImGui::CollapsingHeader("Sphere AO Proxy"))
            {
                if (ImGui::Button("Remove##SphereAO"))
                {
                    reg.remove<SphereAOProxy>(entity);
                    return;
                }

                ImGui::DragFloat("Radius", &proxy.radius);
            }
        }
#endif

        void toJson(entt::entity ent, entt::registry& reg, json& j) override
        {
            auto& proxy = reg.get<SphereAOProxy>(ent);

            j = {{"radius", proxy.radius}};
        }

        void fromJson(entt::entity ent, entt::registry& reg, EntityIDMap&, ComponentDataWrapper j) override
        {
            ZoneScoped;
            auto& proxy = reg.emplace<SphereAOProxy>(ent);

            proxy.radius = j.get<float>("radius");
        }
    };

    class EditorLabelEditor : public BasicComponentUtil<EditorLabel>
    {
    public:
        const char* getName() override
        {
            return "Editor Label";
        }

#ifdef BUILD_EDITOR
        void edit(entt::entity entity, entt::registry& reg, Editor* ed) override
        {
            auto& label = reg.get<EditorLabel>(entity);

            if (ImGui::CollapsingHeader("Editor Label"))
            {
                if (ImGui::Button("Remove##EditorLabel"))
                {
                    reg.remove<EditorLabel>(entity);
                    return;
                }

                ImGui::InputText("Label", &label.label);
            }
        }
#endif

        void toJson(entt::entity ent, entt::registry& reg, json& j) override
        {
            auto& label = reg.get<EditorLabel>(ent);

            j = {{"label", label.label}};
        }

        void fromJson(entt::entity ent, entt::registry& reg, EntityIDMap&, ComponentDataWrapper j) override
        {
            ZoneScoped;
            auto& label = reg.emplace<EditorLabel>(ent);

            label.label = j.getString("label").c_str();
        }
    };

    class AudioListenerOverrideEditor : public BasicComponentUtil<AudioListenerOverride>
    {
    public:
        const char* getName() override
        {
            return "AudioListenerOverride";
        }

#ifdef BUILD_EDITOR
        void edit(entt::entity entity, entt::registry& reg, Editor* ed) override
        {
            if (ImGui::CollapsingHeader("AudioListenerOverride"))
            {
                if (ImGui::Button("Remove##AudioListenerOverride"))
                {
                    reg.remove<AudioListenerOverride>(entity);
                    return;
                }
            }
        }
#endif

        void toJson(entt::entity ent, entt::registry& reg, json& j) override
        {
            j = {};
        }

        void fromJson(entt::entity ent, entt::registry& reg, EntityIDMap&, ComponentDataWrapper j) override
        {
            ZoneScoped;
            reg.emplace<AudioListenerOverride>(ent);
        }
    };

    class ChildComponentEditor : public BasicComponentUtil<ChildComponent>
    {
    public:
        const char* getName() override
        {
            return "ChildComponent";
        }

        void create(entt::entity ent, entt::registry& reg) override
        {
            reg.emplace<ChildComponent>(ent);
        }

        bool allowInspectorAdd() override
        {
            return false;
        }

#ifdef BUILD_EDITOR
        void edit(entt::entity entity, entt::registry& reg, Editor* ed) override
        {
            static bool changingTarget = false;

            if (ImGui::CollapsingHeader("ChildComponent"))
            {
                if (ImGui::Button("Remove##ChildComponent"))
                {
                    reg.remove<ChildComponent>(entity);
                    return;
                }

                ChildComponent& cc = reg.get<ChildComponent>(entity);

                if (!changingTarget)
                {
                    if (ImGui::Button("Change"))
                    {
                        changingTarget = true;
                    }
                }

                if (changingTarget)
                {
                    if (ed->entityEyedropper(cc.parent))
                    {
                        changingTarget = false;
                    }
                }
            }
            else
            {
                changingTarget = false;
            }
        }
#endif

        void toJson(entt::entity ent, entt::registry& reg, json& j) override
        {
            ChildComponent& c = reg.get<ChildComponent>(ent);

            j = {
                {"parent", (uint32_t)c.parent},
                {"position", c.offset.position},
                {"rotation", c.offset.rotation},
                {"scale", c.offset.scale}
            };
        }

        void fromJson(entt::entity ent, entt::registry& reg, EntityIDMap& idMap, ComponentDataWrapper j) override
        {
            ZoneScoped;

            entt::entity realParent = idMap[(entt::entity)j.get<int>("parent")];

            if (!reg.valid(realParent))
            {
                logWarn("Entity %u had invalid parent %u", ent, realParent);
                return;
            }

            HierarchyUtil::setEntityParent(reg, ent, realParent);

            auto& c = reg.get<ChildComponent>(ent);
            c.offset.position = j.getVec<glm::vec3>("position");
            c.offset.rotation = j.getQuat("rotation");
            c.offset.scale = j.getVec<glm::vec3>("scale");
        }
    };

    class ParticleSystemEditor : public BasicComponentUtil<ParticleSystem>
    {
    public:
        const char* getName() override
        {
            return "ParticleSystem";
        }

#ifdef BUILD_EDITOR
        void edit(entt::entity entity, entt::registry& reg, Editor* ed) override
        {
            if (ImGui::CollapsingHeader("Particle System"))
            {
                if (ImGui::Button("Remove##ParticleSystem"))
                {
                    reg.remove<ParticleSystem>(entity);
                    return;
                }

                auto& ps = reg.get<ParticleSystem>(entity);

                ImGui::InputInt("Emission Rate", &ps.emissionRate);
                ps.settingsDirty = ImGui::InputInt("Max Particles", &ps.maxParticles);
                if (ps.maxParticles == 0)
                    ps.settingsDirty = false;
            }
        }
#endif

        void toJson(entt::entity ent, entt::registry& reg, json& j) override
        {
            ParticleSystem& ps = reg.get<ParticleSystem>(ent);
            j = {
                {"emissionRate", ps.emissionRate},
                {"maxParticles", ps.maxParticles}
            };
        }

        void fromJson(entt::entity ent, entt::registry& reg, EntityIDMap&, ComponentDataWrapper j) override
        {
            ZoneScoped;

            auto& ps = reg.emplace<ParticleSystem>(ent);
            ps.emissionRate = j.get<int>("emissionRate", 1);
            ps.maxParticles = j.get<int>("maxParticles", 1000);
        }
    };

    class WorldSpaceUIPanelEditor : public BasicComponentUtil<WorldSpaceUIPanel>
    {
    public:
        const char* getName() override
        {
            return "WorldSpaceUIPanel";
        }
        
        void create(entt::entity ent, entt::registry& reg) override
        {
            reg.emplace<WorldSpaceUIPanel>(ent).createPanel(g_ui);
        }

#ifdef BUILD_EDITOR
        void edit(entt::entity entity, entt::registry& reg, Editor* ed) override
        {
            if (ImGui::CollapsingHeader("World Space UI Panel"))
            {
                if (ImGui::Button("Remove##WorldSpaceUIPanel"))
                {
                    reg.remove<WorldSpaceUIPanel>(entity);
                    return;
                }

                auto& wsup = reg.get<WorldSpaceUIPanel>(entity);

                ImGui::InputText("Panel Name", &wsup.name);
                ImGui::InputText("UI Path", &wsup.path);
                if (ImGui::Button("Refresh"))
                {
                    wsup.panel->loadDocument(wsup.path.c_str());
                }
                
                bool resized = false;
                resized |= ImGui::InputInt("Width", &wsup.width);
                resized |= ImGui::InputInt("Height", &wsup.height);

                if (resized)
                {
                    wsup.panel->resize(wsup.width, wsup.height);
                }
                
                wsup.pixelSize *= 100.0f;
                ImGui::InputFloat("Pixel Size (cm)", &wsup.pixelSize);
                wsup.pixelSize /= 100.0f;
                ImGui::Text("Dimensions: %.3fm x %.3fm", wsup.width * wsup.pixelSize, wsup.height * wsup.pixelSize);

                ImGui::DragFloat("Brightness", &wsup.brightness);
            }
        }
#endif
        
        void toJson(entt::entity ent, entt::registry& reg, json& j) override
        {
            auto& wsup = reg.get<WorldSpaceUIPanel>(ent);
            j = {
                { "name", wsup.name },
                { "path", wsup.path },
                { "width", wsup.width },
                { "height", wsup.height },
                { "pixelSize", wsup.pixelSize },
                { "brightness", wsup.brightness }
            };
        }

        void fromJson(entt::entity ent, entt::registry& reg, EntityIDMap&, ComponentDataWrapper j) override
        {
            ZoneScoped;

            auto& wsup = reg.emplace<WorldSpaceUIPanel>(ent);
            wsup.name = j.getString("name");
            wsup.path = j.getString("path");
            wsup.width = j.get<int64_t>("width");
            wsup.height = j.get<int64_t>("height");
            wsup.pixelSize = j.get<float>("pixelSize");
            wsup.brightness = j.get<float>("brightness", 1.0f);
            wsup.createPanel(g_ui);
        }
    };

    TransformEditor transformEd;
    WorldObjectEditor worldObjEd;
    SkinnedWorldObjectEditor skWorldObjEd;
    WorldLightEditor worldLightEd;
    PhysicsActorEditor paEd;
    RigidBodyEditor dpaEd;
    NameComponentEditor ncEd;
    WorldCubemapEditor wcEd;
    ReverbProbeBoxEditor rpbEd;
    AudioTriggerEditor atEd;
    ProxyAOEditor aoEd;
    PrefabInstanceEditor pie;
    SphereAOProxyEditor saope;
    EditorLabelEditor ele;
    FMODAudioSourceEditor fase;
    AudioListenerOverrideEditor alo;
    ChildComponentEditor ced;
    ParticleSystemEditor pse;
    WorldSpaceUIPanelEditor wsupe;
}

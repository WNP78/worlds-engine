#include "Editor.hpp"
#include "fts_fuzzy_match.h"
#include <AssetCompilation/AssetCompilerUtil.hpp>
#include <AssetCompilation/AssetCompilers.hpp>
#include <Core/AssetDB.hpp>
#include <Core/Log.hpp>
#include <filesystem>
#include <physfs.h>
#include <Core/ConVar.hpp>
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
#endif

namespace worlds
{
#ifdef _WIN32
    HANDLE watcherStopEvent = NULL;
#endif
    
    ConVar ed_assetCompilerDebug{ "ed_assetCompilerDebug", "0" };
    ProjectAssets::ProjectAssets(const GameProject& project, bool startWatcherThread)
        : project(project)
        , threadActive(startWatcherThread)
    {
#ifdef _WIN32
        watcherStopEvent = CreateEventA(NULL, FALSE, 0, NULL);
#endif
        if (startWatcherThread)
            this->startWatcherThread();
    }

    ProjectAssets::~ProjectAssets()
    {
        threadActive = false;
#ifdef _WIN32
        SetEvent(watcherStopEvent);
#endif
        if (watcherThread.joinable())
            watcherThread.join();
    }

    void ProjectAssets::startWatcherThread()
    {
        enumerateAssets();
        watcherThread = std::thread([&]{
#ifdef _WIN32
            std::string srcDataPath { project.sourceData() };
            std::string rawDataPath { project.rawData() };
            HANDLE sourceDirHandle = CreateFileA(srcDataPath.c_str(), FILE_LIST_DIRECTORY,
                FILE_SHARE_READ | FILE_SHARE_WRITE, nullptr,
                OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_OVERLAPPED, nullptr);
            HANDLE rawDirHandle = CreateFileA(rawDataPath.c_str(), FILE_LIST_DIRECTORY,
                FILE_SHARE_READ | FILE_SHARE_WRITE, nullptr,
                OPEN_EXISTING, FILE_FLAG_BACKUP_SEMANTICS | FILE_FLAG_OVERLAPPED, nullptr);
            
            OVERLAPPED srcDataOverlapped{ .hEvent = CreateEventA(nullptr, FALSE, 0, nullptr) };
            OVERLAPPED rawDataOverlapped{ .hEvent = CreateEventA(nullptr, FALSE, 0, nullptr) };
            
            uint8_t srcChangeBuffer[1024];
            uint8_t rawChangeBuffer[1024];
            ReadDirectoryChangesW(
                sourceDirHandle, srcChangeBuffer, 1024, TRUE,
                FILE_NOTIFY_CHANGE_FILE_NAME | FILE_NOTIFY_CHANGE_LAST_WRITE, nullptr, &srcDataOverlapped, nullptr);
            ReadDirectoryChangesW(
                rawDirHandle, rawChangeBuffer, 1024, TRUE,
                FILE_NOTIFY_CHANGE_FILE_NAME | FILE_NOTIFY_CHANGE_LAST_WRITE, nullptr, &rawDataOverlapped, nullptr);
#endif
            while (threadActive)
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(1000));
                size_t lastAssetsSize = assetFiles.size();

                if (!pauseWatcher)
                {
#ifdef _WIN32
                    HANDLE waitObjects[]
                    {
                        srcDataOverlapped.hEvent,
                        rawDataOverlapped.hEvent,
                        watcherStopEvent
                    };

                    DWORD triggered = WaitForMultipleObjects(3, waitObjects, FALSE, INFINITE);

                    if (triggered == WAIT_OBJECT_0)
                    {
                        ReadDirectoryChangesW(
                            sourceDirHandle, srcChangeBuffer, 1024, TRUE,
                            FILE_NOTIFY_CHANGE_FILE_NAME | FILE_NOTIFY_CHANGE_LAST_WRITE, nullptr, &srcDataOverlapped, nullptr);
                    }
                    else if (triggered == STATUS_WAIT_0 + 1)
                    {
                        ReadDirectoryChangesW(
                            rawDirHandle, rawChangeBuffer, 1024, TRUE,
                            FILE_NOTIFY_CHANGE_FILE_NAME | FILE_NOTIFY_CHANGE_LAST_WRITE, nullptr, &rawDataOverlapped, nullptr);
                    }
                    else
                    {
                        break;
                    }
#endif
                    for (AssetFile& af : assetFiles)
                    {
                        checkForAssetChange(af);
                        if (lastAssetsSize != assetFiles.size()) break;
                    }
                }
            }
#ifdef _WIN32
            CloseHandle(srcDataOverlapped.hEvent);
            CloseHandle(rawDataOverlapped.hEvent);
            CloseHandle(sourceDirHandle);
            CloseHandle(rawDirHandle);
#endif
        });
    }

    void ProjectAssets::checkForAssetChanges()
    {
        for (AssetFile& af : assetFiles)
        {
            checkForAssetChange(af);
        }
    }

    void ProjectAssets::checkForAssetChange(AssetFile& file)
    {
        if (!file.isCompiled)
            return;

        IAssetCompiler* compiler = AssetCompilers::getCompilerFor(file.sourceAssetId);
        std::vector<std::string> dependencies;
        compiler->getFileDependencies(file.sourceAssetId, dependencies);

        file.dependenciesExist = true;
        for (auto& dependency : dependencies)
        {
            if (!PHYSFS_exists(dependency.c_str()))
            {
                if (ed_assetCompilerDebug)
					logVrb("%s is missing dependency %s, marking for recompile", file.path.c_str(), dependency.c_str());

                file.dependenciesExist = false;
                return;
            }
        }

        if (!PHYSFS_exists(file.compiledPath.c_str()))
        {
            if (ed_assetCompilerDebug)
				logVrb("%s is missing compiled asset %s, marking for recompile", file.path.c_str(), file.compiledPath.c_str());

            file.needsCompile = true;
            recompileFlag = true;
            return;
        }

        PHYSFS_Stat compiledStat;
        if (PHYSFS_stat(file.compiledPath.c_str(), &compiledStat) == 0)
        {
            if (ed_assetCompilerDebug)
				logVrb("stat failed on compiled asset %s");

            file.needsCompile = true;
            recompileFlag = true;
            return;
        }

        if (compiledStat.modtime < file.lastModified)
        {
            if (ed_assetCompilerDebug)
                logVrb("marking %s as needing compile as source asset was modified after compiled asset", file.path.c_str());

            file.needsCompile = true;
            recompileFlag = true;
        }

        for (auto& dependency : dependencies)
        {
            PHYSFS_Stat dependencyStat;
            if (PHYSFS_stat(dependency.c_str(), &dependencyStat) == 0)
            {
                logVrb("stat failed on dependency %s of asset %s", dependency.c_str(), file.path.c_str());
                continue;
            }

            if (dependencyStat.modtime > compiledStat.modtime)
            {
                file.needsCompile = true;
                recompileFlag = true;
            }
        }
    }

    void ProjectAssets::enumerateAssets()
    {
        assetFiles.clear();
        enumerateForAssets("SourceData");
    }

    void ProjectAssets::enumerateForAssets(const char* path)
    {
        char** list = PHYSFS_enumerateFiles(path);
        auto unixTime =
            std::chrono::duration_cast<std::chrono::seconds>(
                std::chrono::system_clock::now().time_since_epoch()).count();

        for (char** p = list; *p != nullptr; p++)
        {
            char* subpath = *p;
            std::string fullPath = std::string(path) + '/' + subpath;

            PHYSFS_Stat stat;
            PHYSFS_stat(fullPath.c_str(), &stat);

            if (stat.filetype == PHYSFS_FILETYPE_DIRECTORY)
            {
                enumerateForAssets(fullPath.c_str());
            }
            else if (stat.filetype == PHYSFS_FILETYPE_REGULAR)
            {
                if (stat.modtime > unixTime)
                {
                    // file is from the future!!!
                    logWarn("Asset file %s has a modify time from the future! Rewriting...", fullPath.c_str());
                    // rewrite the file to fix it
                    PHYSFS_File* fileHandle = PHYSFS_openAppend(fullPath.c_str());
                    PHYSFS_close(fileHandle);
                    stat.modtime = unixTime;
                }

                std::filesystem::path sp = subpath;
                if (sp.has_extension())
                {
                    std::string extension = std::filesystem::path{subpath}.extension().string();

                    if (extension == ".wmdlj" || extension == ".wtexj")
                    {
                        assetFiles.push_back(AssetFile{.sourceAssetId = AssetDB::pathToId(fullPath),
                                                       .path = fullPath,
                                                       .lastModified = stat.modtime,
                                                       .compiledPath = getOutputPath(fullPath).substr(5),
                                                       .isCompiled = true});
                    }
                    else if (extension == ".wscn" || extension == ".wprefab")
                    {
                        assetFiles.push_back(AssetFile{.sourceAssetId = AssetDB::pathToId(fullPath),
                                                       .path = fullPath,
                                                       .lastModified = stat.modtime,
                                                       .isCompiled = false});
                    }
                }
            }
        }

        PHYSFS_freeList(list);
    }

    struct AssetSearchCandidate
    {
        AssetID assetId;
        int score;
    };

    slib::List<AssetID> ProjectAssets::searchForAssets(slib::String pattern)
    {
        std::vector<AssetSearchCandidate> candidates;

        for (AssetFile& af : assetFiles)
        {
            int score;
            if (fts::fuzzy_match(pattern.cStr(), af.path.c_str(), score))
            {
                candidates.push_back({af.sourceAssetId, score});
            }
        }

        std::sort(candidates.begin(), candidates.end(),
                  [](const AssetSearchCandidate& a, const AssetSearchCandidate& b) { return a.score > b.score; });

        slib::List<AssetID> orderedIds;
        orderedIds.reserve(candidates.size());

        for (AssetSearchCandidate& sc : candidates)
        {
            orderedIds.add(sc.assetId);
        }

        return orderedIds;
    }
}
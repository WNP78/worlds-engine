﻿#include <filesystem>
#include <Core/Log.hpp>
#include <Editor/Editor.hpp>
#include <slib/Path.hpp>
#include <Editor/AssetEditors.hpp>

namespace worlds
{
    AssetID importAsExtension(const char* pathInRaw, GameProject& project, const char* newExtension)
    {
        slib::Path p{pathInRaw};
        auto ext = p.fileExtension();
        std::string path = pathInRaw;
        
        // now something like Textures/whatever/something.png
        size_t pos = path.find(ext.cStr());
        path.erase(pos, ext.byteLength());
        path += newExtension;
        // now something like Textures/whatever/something.wtexj
        slib::String fullPath = slib::String(project.sourceData().data()) +
                                '/' + slib::Path{path.c_str()}.parentPath();

        std::filesystem::create_directories(fullPath.cStr());
        AssetEditors::getEditorFor(newExtension)->importAsset("Raw/" + std::string(pathInRaw), path);
        addNotification("Importing as " + path);
        return AssetDB::pathToId(path);
    }
    
    AssetID Editor::importAsset(const char* pathInRaw)
    {
        slib::Path p{pathInRaw};
        auto ext = p.fileExtension();

        if (ext == ".png" || ext == ".jpg" || ext == ".exr")
        {
            return importAsExtension(pathInRaw, currentProject(), ".wtexj");
        }

        if (ext == ".fbx" || ext == ".glb" || ext == ".blend")
        {
            return importAsExtension(pathInRaw, currentProject(), ".wmdlj");
        }

        return ~0u;
    }
}

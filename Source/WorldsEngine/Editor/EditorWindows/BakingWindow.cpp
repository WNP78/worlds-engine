#include "EditorWindows.hpp"
#include "Navigation/Navigation.hpp"
#include <Core/Engine.hpp>
#include <Core/Log.hpp>
#include <Core/NameComponent.hpp>
#include <ImGui/imgui.h>
#include <Libs/IconsFontAwesome5.h>
#include <Libs/IconsFontaudio.h>
#include <Render/RenderInternal.hpp>
#include <Util/EnumUtil.hpp>
#include <glm/gtc/quaternion.hpp>
#include <Audio/Audio.hpp>
#include <Core/MeshManager.hpp>
#include <Editor/GuiUtil.hpp>
#include <Recast.h>
#include <filesystem>
#include <deque>
#include <R2/VK.hpp>
#include <Render/CubemapConvoluter.hpp>
#include <nlohmann/json.hpp>
#include <tinyexr.h>
#define STB_IMAGE_WRITE_IMPLEMENTATION
#include <stb_image_write.h>

#include "R2/VKUtil.hpp"

using namespace R2;

namespace worlds
{
    void stbiWriteFunc(void* ctx, void* data, int bytes)
    {
        PHYSFS_writeBytes((PHYSFS_File*)ctx, data, bytes);
    }

    class CubemapBaker
    {
        struct CubemapBakeOp
        {
            std::string name;
            glm::vec3 pos;
            int faceIdx = 0;
            bool waitingForResult = false;
            entt::entity entity;

            std::vector<std::string> outputPaths;
        };

        std::deque<CubemapBakeOp> cubemapBakeOps;

        const std::string outputNames[6] = {
            "px",
            "nx",
            "py",
            "ny",
            "pz",
            "nz"
        };

        const glm::vec3 directions[6] = {
            glm::vec3(1.0f, 0.0f, 0.0f),
            glm::vec3(-1.0f, 0.0f, 0.0f),
            glm::vec3(0.0f, -1.0f, 0.0f),
            glm::vec3(0.0f, 1.0f, 0.0f),
            glm::vec3(0.0f, 0.0f, -1.0f),
            glm::vec3(0.0f, 0.0f, 1.0f)
        };

        const glm::vec3 upDirs[6] = {
            glm::vec3(0.0f, 1.0f, 0.0f),
            glm::vec3(0.0f, 1.0f, 0.0f),
            glm::vec3(0.0f, 0.0f, -1.0f),
            glm::vec3(0.0f, 0.0f, 1.0f),
            glm::vec3(0.0f, 1.0f, 0.0f),
            glm::vec3(0.0f, 1.0f, 0.0f)
        };

        void saveEXR(PHYSFS_File* file, int resolution, float* data)
        {
            // deinterleave channels
            float* rChannel = new float[resolution * resolution];
            float* gChannel = new float[resolution * resolution];
            float* bChannel = new float[resolution * resolution];

            for (int i = 0; i < resolution * resolution; i++)
            {
                rChannel[i] = data[(i * 4) + 0];
                gChannel[i] = data[(i * 4) + 1];
                bChannel[i] = data[(i * 4) + 2];
            }

            EXRHeader header;
            InitEXRHeader(&header);

            EXRImage image;
            InitEXRImage(&image);

            // EXR is generally BGR for some reason so we use that channel ordering
            float* channelPointers[3] = { bChannel, gChannel, rChannel};
            // slightly weird type aliasing
            image.images = (unsigned char**)channelPointers;
            image.width = resolution;
            image.height = resolution;
            image.num_channels = 3;

            header.num_channels = 3;
            header.channels = new EXRChannelInfo[header.num_channels];
            header.pixel_types = new int[header.num_channels];
            header.requested_pixel_types = new int[header.num_channels];

            const char* channelNames[3] = { "B", "G", "R" };

            for (int i = 0; i < 3; i++)
            {
                strncpy(header.channels[i].name, channelNames[i], 255);
                header.channels[i].name[strlen(channelNames[i])] = '\0';
                header.pixel_types[i] = TINYEXR_PIXELTYPE_FLOAT;
                header.requested_pixel_types[i] = TINYEXR_PIXELTYPE_HALF;
            }

            const char* err;
            uint8_t* outData;
            size_t nBytes = SaveEXRImageToMemory(&image, &header, &outData, &err);

            if (nBytes == 0)
            {
                logErr("Failed to save cubemap EXR file: %s", err);
            }
            else
            {
                PHYSFS_writeBytes(file, outData, nBytes);
            }

            delete[] header.channels;
            delete[] header.pixel_types;
            delete[] header.requested_pixel_types;

            delete[] rChannel;
            delete[] gChannel;
            delete[] bChannel;
        }
    public:
        RTTPass* rttPass;
        Camera cam;
        Renderer* renderer;
        entt::registry& world;
        VK::Texture* cubemapTarget;
        CubemapConvoluter* convoluter;

        CubemapBaker(Renderer* renderer, entt::registry& world)
            : renderer(renderer)
            , world(world)
        {
            // create RTT pass
            RTTPassSettings rtci;
            rtci.cam = &cam;
            rtci.enableShadows = true;
            rtci.width = 128;
            rtci.height = 128;
            rtci.useForPicking = false;
            rtci.msaaLevel = 1;
            rtci.renderDebugShapes = false;
            rtci.staticsOnly = true;
            cam.verticalFOV = glm::radians(90.0f);

            rttPass = renderer->createRTTPass(rtci);
            rttPass->active = false;

            VK::TextureCreateInfo tci = VK::TextureCreateInfo::RenderTarget2D(VK::TextureFormat::R32G32B32A32_SFLOAT, rtci.width, rtci.height);
            tci.NumMips = 6;
            tci.Dimension = VK::TextureDimension::Cube;
            tci.CanSample = true;
            tci.CanTransfer = true;

            // ew ew ew! bad cast
            VKRenderer* vkRenderer = (VKRenderer*)renderer;
            
            cubemapTarget = vkRenderer->getCore()->CreateTexture(tci);
            convoluter = new CubemapConvoluter(vkRenderer->getCore());
        }

        void addToQueue(std::string name, glm::vec3 pos, entt::entity entity)
        {
            cubemapBakeOps.emplace_back(name, pos, 0, false, entity);
        }

        void update()
        {
            if (cubemapBakeOps.empty())
            {
                rttPass->active = false;
                return;
            }

            VKRenderer* vkRenderer = (VKRenderer*)renderer;

            CubemapBakeOp& currentOp = cubemapBakeOps.front();
            rttPass->active = true;

            if (currentOp.faceIdx > 0)
            {
                // blitting time!
                VK::CommandBuffer cb = VK::Utils::AcquireImmediateCommandBuffer();
                VK::TextureBlit faceBlit
                {
                    .Source = VK::SubtextureRange
                    {
                        .MipLevel = 0,
                        .LayerStart = 0,
                        .LayerCount = 1
                    },
                    .SourceOffsets =
                    {
                        VK::Offset3D { 0, 0, 0 },
                        VK::Offset3D { 128, 128, 1 }
                    },
                    .Destination = VK::SubtextureRange
                    {
                        .MipLevel = 0,
                        .LayerStart = (uint32_t)currentOp.faceIdx - 1,
                        .LayerCount = 1
                    },
                    .DestinationOffsets =
                    {
                        VK::Offset3D { 0, 0, 0 },
                        VK::Offset3D { 128, 128, 1 }
                    }
                };
                cb.TextureBlit(((VKRTTPass*)rttPass)->getHDRTarget(), cubemapTarget, faceBlit);
                VK::Utils::ExecuteImmediateCommandBuffer();
            }
            
            if (currentOp.faceIdx >= 6)
            {
                VK::CommandBuffer cb = VK::Utils::AcquireImmediateCommandBuffer();
                convoluter->Convolute(cb, cubemapTarget);
                VK::Utils::ExecuteImmediateCommandBuffer();

                // download time!!
                for (uint32_t face = 0; face < 6; face++)
                for (uint32_t mip = 0; mip < 6; mip++)
                {
                    uint32_t size = std::max(128 >> mip, 1);
                    VK::BufferCreateInfo bci { VK::BufferUsage::Storage, sizeof(float) * 4 * size * size, true };
                    VK::Buffer* buf = vkRenderer->getCore()->CreateBuffer(bci);
                    
                    cb = VK::Utils::AcquireImmediateCommandBuffer();
                    VK::TextureToBufferCopy tbc
                    {
                        .textureRange = VK::SubtextureRange
                        {
                            .MipLevel = mip,
                            .LayerStart = face,
                            .LayerCount = 1
                        },
                        .textureExtent = { size, size, 1 }
                    };
                    cb.TextureCopyToBuffer(cubemapTarget, buf, tbc);
                    VK::Utils::ExecuteImmediateCommandBuffer();

                    float* texData = (float*)buf->Map();
                    auto outName = currentOp.name + outputNames[face] + "_mip" + std::to_string(mip) + ".exr";

                    std::string levelCubemapPath = "Raw/LevelData/Cubemaps/" + world.ctx<SceneInfo>().name + "/";
                    auto outPath = levelCubemapPath + outName;
                    PHYSFS_File* outFile = PHYSFS_openWrite(outPath.c_str());
                    //stbi_write_hdr_to_func(stbiWriteFunc, outFile, size, size, 4, texData);
                    saveEXR(outFile, size, texData);
                    PHYSFS_close(outFile);
                    buf->Unmap();

                    delete buf;
                }

                // now set up the source file for the texture build
                nlohmann::json j
                {
                    { "type", "cubemap" },
                    { "faces", {} }
                };

                for (uint32_t face = 0; face < 6; face++)
                for (uint32_t mip = 0; mip < 6; mip++)
                {
                    auto outName = currentOp.name + outputNames[face] + "_mip" + std::to_string(mip) + ".exr";

                    std::string levelCubemapPath = "Raw/LevelData/Cubemaps/" + world.ctx<SceneInfo>().name + "/";
                    auto outPath = levelCubemapPath + outName;
                    j["faces"].push_back(outPath);
                }

                std::string sourcePath = "SourceData/LevelData/Cubemaps/" + world.ctx<SceneInfo>().name + "/" + currentOp.name + ".wtexj";
                PHYSFS_File* outSourceFile = PHYSFS_openWrite(sourcePath.c_str());
                std::string sourceJson = j.dump();
                PHYSFS_writeBytes(outSourceFile, sourceJson.c_str(), sourceJson.size());
                PHYSFS_close(outSourceFile);
                
                cubemapBakeOps.pop_front();
                return;
            }
            
            cam.position = currentOp.pos;
            cam.rotation = glm::quatLookAt(directions[currentOp.faceIdx], upDirs[currentOp.faceIdx]);
            currentOp.faceIdx++;
        }

        ~CubemapBaker()
        {
            renderer->destroyRTTPass(rttPass);
        }
    };

    void BakingWindow::draw(entt::registry& reg)
    {
        if (ImGui::Begin(ICON_FA_COOKIE u8" Baking", &active))
        {
            if (ImGui::CollapsingHeader(ICON_FA_VOLUME_UP u8" Audio"))
            {
                uint32_t staticAudioGeomCount = 0;

                reg.view<WorldObject>().each([&](auto, WorldObject& wo) {
                    if (enumHasFlag(wo.staticFlags, StaticFlags::Audio))
                        staticAudioGeomCount++;
                });

                if (staticAudioGeomCount > 0)
                {
                    ImGui::Text("%u static geometry objects", staticAudioGeomCount);
                }
                else
                {
                    ImGui::TextColored(ImColor(1.0f, 0.0f, 0.0f),
                                       "There aren't any objects marked as audio static in the scene.");
                }

                // if (ImGui::Button("Bake")) {
                //    AudioSystem::getInstance()->bakeProbes(reg);
                //}
                if (ImGui::Button("Bake Geometry"))
                {
                    std::string rawPath = std::string(editor->currentProject().root()) + "/Data/LevelData/PhononScenes";
                    std::filesystem::create_directories(rawPath);
                    std::string savedPath = "Data/LevelData/PhononScenes/" + reg.ctx<SceneInfo>().name + ".bin";
                    AudioSystem::getInstance()->saveAudioScene(reg, savedPath.c_str());
                }
            }

            if (ImGui::CollapsingHeader(ICON_FA_CUBE u8" Cubemaps"))
            {
                static int numIterations = 1;
                ImGui::DragInt("Iterations", &numIterations);
                static CubemapBaker* cb = nullptr;

                if (cb == nullptr)
                {
                    cb = new CubemapBaker(g_renderer, reg);
                }
                cb->update();

                bool bakeAll = ImGui::Button("Bake All");

                reg.view<Transform, WorldCubemap, NameComponent>().each(
                    [&](entt::entity entity, Transform& t, WorldCubemap& wc, NameComponent& nc) {
                        ImGui::Text("%s (%.2f, %.2f, %.2f)", nc.name.c_str(), t.position.x, t.position.y, t.position.z);
                        ImGui::SameLine();
                        ImGui::PushID(nc.name.c_str());
                        if (ImGui::Button("Bake") || bakeAll)
                        {
                            std::string rawPath = std::string(editor->currentProject().root()) + "/Raw/LevelData/Cubemaps/" + reg.ctx<SceneInfo>().name + "/";
                            std::filesystem::create_directories(rawPath);
                            std::string sourcePath = std::string(editor->currentProject().root()) + "/SourceData/LevelData/Cubemaps/" + reg.ctx<SceneInfo>().name + "/";
                            std::filesystem::create_directories(sourcePath);
                            
                            cb->addToQueue(nc.name, t.position + wc.captureOffset, entity);
                        }
                        ImGui::PopID();
                    });
            }

            if (ImGui::CollapsingHeader(ICON_FA_MAP u8" Navigation"))
            {
                glm::vec3 bbMin{FLT_MAX};
                glm::vec3 bbMax{-FLT_MAX};
                reg.view<Transform, WorldObject>().each([&](Transform& t, WorldObject& o) {
                    if (!enumHasFlag(o.staticFlags, StaticFlags::Navigation))
                        return;

                    glm::mat4 tMat = t.getMatrix();
                    auto& mesh = MeshManager::loadOrGet(o.mesh);
                    for (const Vertex& v : mesh.vertices)
                    {
                        glm::vec3 transformedPosition = tMat * glm::vec4(v.position, 1.0f);
                        bbMin = glm::min(transformedPosition, bbMin);
                        bbMax = glm::max(transformedPosition, bbMax);
                    }
                });
                ImGui::Text("World Bounds: (%.3f, %.3f, %.3f) to (%.3f, %.3f, %.3f)", bbMin.x, bbMin.y, bbMin.z,
                            bbMax.x, bbMax.y, bbMax.z);
                NavigationSystem::drawNavMesh();

                if (ImGui::Button("Bake"))
                {
                    std::string rawPath = std::string(editor->currentProject().root()) + "/Data/LevelData/Navmeshes";
                    std::filesystem::create_directories(rawPath);
                    std::string savedPath = "Data/LevelData/Navmeshes/" + reg.ctx<SceneInfo>().name + ".bin";
                    NavigationSystem::buildAndSave(reg, savedPath.c_str());
                }
            }
        }
        ImGui::End();
    }
}

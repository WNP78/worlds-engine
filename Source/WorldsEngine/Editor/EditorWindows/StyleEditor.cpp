#include "../../ImGui/imgui.h"
#include "EditorWindows.hpp"

namespace worlds
{
    extern void editThemeColors();

    void StyleEditor::draw(entt::registry& reg)
    {
        if (ImGui::Begin("Style Editor", &active))
        {
            editThemeColors();
        }
        ImGui::End();
    }
}

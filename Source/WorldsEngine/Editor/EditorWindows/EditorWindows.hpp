#pragma once
#include "../Editor.hpp"
#include "AssetCompilation/AssetCompilers.hpp"
#include "Core/AssetDB.hpp"
#include "Editor/AssetEditors.hpp"

namespace worlds
{
    class EntityList : public EditorWindow
    {
    public:
        EntityList(Editor* editor) : EditorWindow(editor)
        {
        }
        void draw(entt::registry& reg) override;
        const char* getName() override
        {
            return "EntityList";
        }
        ~EntityList()
        {
        }
    };

    class Assets : public EditorWindow
    {
        std::string lastHoveredDir;
    public:
        Assets(Editor* editor) : EditorWindow(editor)
        {
        }
        void draw(entt::registry& reg) override;
        const char* getName() override
        {
            return "Assets";
        }
        
        bool acceptsFileDrops() override { return true; }
        void onFileDropped(const char* filePath) override;
        
        ~Assets()
        {
        }
    };

    class RawAssets : public EditorWindow
    {
    public:
        RawAssets(Editor* editor) : EditorWindow(editor)
        {
        }
        void draw(entt::registry& reg) override;
        const char* getName() override
        {
            return "Raw Assets";
        }
        ~RawAssets()
        {
        }
    };

    class GameControls : public EditorWindow
    {
    public:
        GameControls(Editor* editor) : EditorWindow(editor)
        {
            active = false;
        }
        void draw(entt::registry& reg) override;
        const char* getName() override
        {
            return "GameControls";
        }
        ~GameControls()
        {
        }
    };

    class StyleEditor : public EditorWindow
    {
    public:
        StyleEditor(Editor* editor) : EditorWindow(editor)
        {
            active = false;
        }
        void draw(entt::registry& reg) override;
        const char* getName() override
        {
            return "Style Editor";
        }
        ~StyleEditor()
        {
        }
    };

    class AboutWindow : public EditorWindow
    {
    public:
        AboutWindow(Editor* editor) : EditorWindow{editor}
        {
            active = false;
        }
        void draw(entt::registry&) override;
        EditorMenu menuSection() override
        {
            return EditorMenu::Help;
        }
        void setActive(bool active) override;
        const char* getName() override
        {
            return "About";
        }
        ~AboutWindow()
        {
        }
    };

    class BakingWindow : public EditorWindow
    {
    public:
        BakingWindow(Editor* editor) : EditorWindow{editor}
        {
            active = false;
        }
        void draw(entt::registry&) override;
        EditorMenu menuSection() override
        {
            return EditorMenu::Edit;
        }
        const char* getName() override
        {
            return "Baking";
        }
        ~BakingWindow()
        {
        }
    };

    class SceneSettingsWindow : public EditorWindow
    {
    public:
        SceneSettingsWindow(Editor* editor) : EditorWindow{editor}
        {
            active = false;
        }
        void draw(entt::registry&) override;
        EditorMenu menuSection() override
        {
            return EditorMenu::Edit;
        }
        const char* getName() override
        {
            return "Scene Settings";
        }
        ~SceneSettingsWindow()
        {
        }
    };

    class AssetEditorWindow : public EditorWindow
    {
    public:
        AssetEditorWindow(AssetID id, Editor* editor);
        void draw(entt::registry&) override;
        EditorMenu menuSection() override
        {
            return EditorMenu::Edit;
        }
        const char* getName() override
        {
            return "Asset Editor";
        }

        
        ~AssetEditorWindow();

    private:
        AssetID assetId;
        AssetCompileOperation* currCompileOp;
        IAssetEditor* assetEditor;
    };

    class AssetCompilationManager : public EditorWindow
    {
    public:
        AssetCompilationManager(Editor* editor) : EditorWindow{editor}
        {
            active = false;
        }
        void draw(entt::registry&) override;
        EditorMenu menuSection() override
        {
            return EditorMenu::Edit;
        }
        const char* getName() override
        {
            return "Asset Compilation Manager";
        }
        ~AssetCompilationManager()
        {
        }
    };

    class NodeEditorTest : public EditorWindow
    {
    public:
        NodeEditorTest(Editor* editor) : EditorWindow{editor}
        {
            active = false;
        }
        void draw(entt::registry&) override;
        EditorMenu menuSection() override
        {
            return EditorMenu::Help;
        }
        const char* getName() override
        {
            return "Node Editor Test";
        }
        ~NodeEditorTest()
        {
        }
    };

    class GameView : public EditorWindow
    {
    public:
        GameView(Editor* editor) : EditorWindow{editor}
        {
            active = true;
        }
        void draw(entt::registry&) override;

        EditorMenu menuSection() override
        {
            return EditorMenu::Window;
        }

        const char* getName() override
        {
            return "Game View";
        }

        ~GameView()
        {
        }
    };
}

#include <Core/ConVar.hpp>
#include <Core/Log.hpp>

#include "EditorWindows.hpp"
#include <Render/Render.hpp>
#include <Libs/IconsFontAwesome5.h>
#include <VR/OpenXRInterface.hpp>

namespace worlds
{
    RTTPass* gameRTTPass = nullptr;
    uint32_t currentWidth;
    uint32_t currentHeight;
    bool playingLast = false;
    bool wasVrLast = false;

    ConVar ed_gameViewOversample{ "ed_gameViewOversample", "1.0" };
    ConVar ed_gameViewShowDebugDraw { "ed_gameViewShowDebugDraw", "0" };

    void GameView::draw(entt::registry& reg)
    {
        ImGui::SetNextWindowSizeConstraints(ImVec2(256.0f, 256.0f), ImVec2(FLT_MAX, FLT_MAX));
        bool isVR = g_vrInterface != nullptr && g_vrInterface->isInitialized();

        if (editor->isPlaying() && !playingLast)
        {
            ImGui::SetNextWindowFocus();
        }
        playingLast = editor->isPlaying();

        g_input->setImGuiIgnore(true);
        if (ImGui::Begin(ICON_FA_GAMEPAD u8" Game View"))
        {
            g_input->getInputChannel(InputChannelKind::Game).active = ImGui::IsWindowHovered();
            g_input->getInputChannel(InputChannelKind::Editor).active = !ImGui::IsWindowHovered();
            ImVec2 contentRegion = ImGui::GetContentRegionAvail();

            if (gameRTTPass == nullptr || isVR != wasVrLast)
            {
                if (gameRTTPass != nullptr)
                    g_renderer->destroyRTTPass(gameRTTPass);

                currentWidth = contentRegion.x;
                currentHeight = contentRegion.y;

                uint32_t vrWidth, vrHeight;
                if (isVR)
					g_vrInterface->getRenderResolution(&vrWidth, &vrHeight);

                RTTPassSettings gameViewPassCI
                {
                    .cam = g_mainCamera,
                    .width = isVR ? vrWidth : currentWidth,
                    .height = isVR ? vrHeight : currentHeight,
                    .useForPicking = false,
                    .enableShadows = true,
                    .msaaLevel = 2,
                    .numViews = isVR ? 2 : 1,
                    .renderDebugShapes = ed_gameViewShowDebugDraw,
                    .outputToXR = isVR,
                    .setViewsFromXR = isVR,
                    .allowFFR = isVR
                };
                wasVrLast = isVR;

                gameRTTPass = g_renderer->createRTTPass(gameViewPassCI);
            }

            gameRTTPass->setDebugDrawEnabled(ed_gameViewShowDebugDraw);

            gameRTTPass->active = true;

            if ((contentRegion.x != currentWidth || contentRegion.y != currentHeight) && 
                contentRegion.x > 256 && contentRegion.y > 256)
            {
                currentWidth = contentRegion.x;
                currentHeight = contentRegion.y;
                if (!isVR)
                {
                    if (ed_gameViewOversample.getFloat() != 1.0f)
                    {
                        logMsg("Game view resizing to %ix%i", (int)(currentWidth * ed_gameViewOversample.getFloat()), (int)(currentHeight * ed_gameViewOversample.getFloat()));
                    }
                    gameRTTPass->active = false;
                    gameRTTPass->resize(currentWidth * ed_gameViewOversample.getFloat(), currentHeight * ed_gameViewOversample.getFloat());
                }
            }

            if (isVR)
            {
                uint32_t vrWidth, vrHeight;
                g_vrInterface->getRenderResolution(&vrWidth, &vrHeight);

                if (gameRTTPass->width != vrWidth || gameRTTPass->height != vrHeight)
                {
                    gameRTTPass->active = false;
                    gameRTTPass->resize((int)vrWidth, (int)vrHeight);
                }

                // Calculate the best crop for the current window size against the VR render target
                float aspect = (float)currentHeight / (float)currentWidth;
                float invAspect = (float)currentWidth / currentHeight;

                float zoom = 0.8f;

                glm::vec2 srcCorner0{ -1.0f, -1.0f };
                glm::vec2 srcCorner1{ 1.0f, 1.0f };

                if (aspect < 1.0f)
                {
                    // Crop vertically
                    srcCorner0.y = -aspect;
                    srcCorner1.y = aspect;
                }
                else
                {
                    // Crop horizontally
                    srcCorner0.x = -invAspect;
                    srcCorner1.x = invAspect;
                }

                // Scale
                srcCorner0 *= zoom;
                srcCorner1 *= zoom;

                // And convert back to 0 -> 1
                srcCorner0 = (srcCorner0 * 0.5f) + 0.5f;
                srcCorner1 = (srcCorner1 * 0.5f) + 0.5f;

                ImGui::Image(gameRTTPass->getUITextureID(), ImVec2(currentWidth, currentHeight), srcCorner0, srcCorner1);
            }
            else
            {
                ImGui::Image(gameRTTPass->getUITextureID(), ImVec2(currentWidth, currentHeight));
            }
        }
        else
        {
            gameRTTPass->active = false;
            g_input->getInputChannel(InputChannelKind::Game).active = false;
            g_input->getInputChannel(InputChannelKind::Editor).active = true;
        }

        if (isVR)
        {
            if (editor->isPlaying() && g_renderer->getVsync())
            {
                g_renderer->setVsync(false);
            }
            else if (!editor->isPlaying())
            {
                g_renderer->setVsync(true);
            }
        }
        ImGui::End();
    }
}
#include "EditorWindows.hpp"
#include <Core/HierarchyUtil.hpp>
#include <Core/Log.hpp>
#include <Core/NameComponent.hpp>
#include <Core/WorldComponents.hpp>
#include <ImGui/imgui.h>
#include <ImGui/imgui_stdlib.h>
#include <Libs/IconsFontAwesome5.h>
#include <algorithm>
#include <Physics/PhysicsActor.hpp>
#include <Util/StringBuilder.hpp>

namespace worlds
{
    void EntityList::draw(entt::registry& reg)
    {
        static std::string searchText;
        static std::vector<entt::entity> filteredEntities;
        static size_t numNamedEntities;
        static entt::entity popupOpenFor = entt::null;

        if (ImGui::Begin(ICON_FA_LIST u8" Entity List", &active))
        {
            size_t currNamedEntCount = reg.view<NameComponent>().size();
            bool searchNeedsUpdate = !searchText.empty() && numNamedEntities != currNamedEntCount;

            ImVec2 searchTextSize = ImGui::CalcTextSize("Search");
            ImGui::PushItemWidth(-(searchTextSize.x + ImGui::GetStyle().FramePadding.x));
            if (ImGui::InputText("Search", &searchText) || searchNeedsUpdate)
            {
                ImGui::PopItemWidth();
                std::string lSearchTxt = searchText;
                std::transform(lSearchTxt.begin(), lSearchTxt.end(), lSearchTxt.begin(),
                               [](unsigned char c) { return std::tolower(c); });

                filteredEntities.clear();
                reg.view<NameComponent>().each([&](auto ent, NameComponent& nc)
                {
                    std::string name = nc.name;

                    std::transform(name.begin(), name.end(), name.begin(),
                                   [](unsigned char c) { return std::tolower(c); });

                    size_t pos = name.find(lSearchTxt);

                    if (pos != std::string::npos)
                    {
                        filteredEntities.push_back(ent);
                    }
                });
            }

            numNamedEntities = currNamedEntCount;

            if (ImGui::IsWindowHovered() && ImGui::GetIO().MouseClicked[1])
            {
                ImGui::OpenPopup("AddEntity");
            }

            bool openEntityContextMenu = false;
            bool navDown = false;
            bool navUp = false;

            if (ImGui::IsWindowHovered(ImGuiHoveredFlags_RootAndChildWindows) && ImGui::IsWindowFocused(
                ImGuiFocusedFlags_RootAndChildWindows))
            {
                navDown = ImGui::IsKeyPressed(SDL_SCANCODE_DOWN);
                navUp = ImGui::IsKeyPressed(SDL_SCANCODE_UP);
            }
            else
            {
                navDown = false;
                navUp = false;
            }

            bool selectNext = false;
            entt::entity lastEntity = entt::null;
            std::function<void(entt::entity)> forEachEnt = [&](entt::entity ent)
            {
                ImGui::PushID((uint32_t)ent);
                auto nc = reg.try_get<NameComponent>(ent);
                const char* entityName = nc ? nc->name.c_str() : "Unnamed Entity";
                float lineHeight = ImGui::CalcTextSize("w").y;

                ImVec2 cursorPos = ImGui::GetCursorPos();
                ImDrawList* drawList = ImGui::GetWindowDrawList();
                float windowWidth = ImGui::GetWindowWidth();
                ImVec2 windowPos = ImGui::GetWindowPos();

                if (selectNext)
                {
                    editor->select(ent);
                    selectNext = false;
                }

                if (editor->isEntitySelected(ent))
                {
                    if (navDown)
                    {
                        selectNext = true;
                        navDown = false;
                    }

                    if (navUp)
                    {
                        if (lastEntity != entt::null)
                            editor->select(lastEntity);
                        navUp = false;
                    }
                }

                lastEntity = ent;


                // Parent drag/drop
                ImGuiDragDropFlags entityDropFlags = 0 | ImGuiDragDropFlags_SourceNoDisableHover |
                    ImGuiDragDropFlags_SourceNoHoldToOpenOthers |
                    ImGuiDragDropFlags_SourceAllowNullID;

                bool isOpen = false;
                
                ImGuiTreeNodeFlags treeNodeFlags = ImGuiTreeNodeFlags_OpenOnArrow | ImGuiTreeNodeFlags_SpanAvailWidth | ImGuiTreeNodeFlags_FramePadding;
                if (editor->isEntitySelected(ent)) treeNodeFlags |= ImGuiTreeNodeFlags_Selected;
                if (!reg.has<ParentComponent>(ent)) treeNodeFlags |= ImGuiTreeNodeFlags_Leaf;

				const char* icon = (const char*)ICON_FA_VECTOR_SQUARE;

				if (reg.has<ParentComponent>(ent))
				{
					icon = (const char*)ICON_FA_OBJECT_GROUP;
				}

				if (reg.has<WorldObject>(ent))
				{
					icon = (const char*)ICON_FA_PENCIL_ALT;
				}

				if (reg.has<RigidBody>(ent))
				{
					icon = (const char*)ICON_FA_SHAPES;
				}

				if (reg.has<WorldLight>(ent))
				{
					icon = (const char*)ICON_FA_LIGHTBULB;
				}

				if (reg.has<WorldCubemap>(ent))
				{
					icon = (const char*)ICON_FA_CUBE;
				}

				if (reg.has<SceneRootComponent>(ent))
				{
					treeNodeFlags |= ImGuiTreeNodeFlags_DefaultOpen;
					icon = (const char*)ICON_FA_MAP;
				}

				if (reg.has<LocalTransformDirty>(ent))
					ImGui::PushStyleColor(ImGuiCol_Text, 0xFFFF0000);

				isOpen = ImGui::TreeNodeEx(entityName, treeNodeFlags, "%s  %s", icon, entityName);

				if (reg.has<LocalTransformDirty>(ent))
					ImGui::PopStyleColor();

                if (ImGui::BeginDragDropSource(entityDropFlags))
                {
                    int numSelected = editor->getSelectedEntities().numElements();
                    if (editor->isEntitySelected(ent) && numSelected > 0)
                    {
                        ImGui::Text("%s (and %i other selected entities)", entityName, numSelected);
                    }
                    else
                    {
                        ImGui::TextUnformatted(entityName);
                    }
                    ImGui::SetDragDropPayload("HIERARCHY_ENTITY", &ent, sizeof(entt::entity));
                    ImGui::EndDragDropSource();
                }

                if (ImGui::BeginDragDropTarget())
                {
                    if (const ImGuiPayload* payload = ImGui::AcceptDragDropPayload("HIERARCHY_ENTITY"))
                    {
                        assert(payload->DataSize == sizeof(entt::entity));
                        entt::entity droppedEntity = *reinterpret_cast<entt::entity*>(payload->Data);
                
                        if (!HierarchyUtil::isEntityDirectChildOf(reg, droppedEntity, ent))
                            HierarchyUtil::setEntityParent(reg, droppedEntity, ent);

                        if (editor->isEntitySelected(droppedEntity))
                        {
                            for (entt::entity object : editor->getSelectedEntities())
                            {
                                if (!HierarchyUtil::isEntityDirectChildOf(reg, object, ent))
                                    HierarchyUtil::setEntityParent(reg, object, ent);
                            }
                        }
                    }
                    ImGui::EndDragDropTarget();
                }

                if (ImGui::IsItemHovered() && ImGui::IsMouseClicked(ImGuiMouseButton_Right))
                {
                    popupOpenFor = ent;
                    openEntityContextMenu = true;
                    ImGui::OpenPopup("Entity Context Menu");
                }

                if (ImGui::IsItemHovered() && ImGui::IsMouseReleased(ImGuiMouseButton_Left) && !ImGui::IsMouseDragging(ImGuiMouseButton_Left))
                {
                    if (!g_input->keyHeld(SDL_SCANCODE_LSHIFT))
                    {
                        editor->select(ent);
                    }
                    else
                    {
                        editor->multiSelect(ent);
                    }
                }

                if (isOpen)
                {
                    if (reg.has<ParentComponent>(ent))
                    {
                        auto& pc = reg.get<ParentComponent>(ent);

                        entt::entity currentChild = pc.firstChild;

                        ImVec2 start = ImGui::GetCursorScreenPos();
                        while (reg.valid(currentChild))
                        {
                            auto& childComponent = reg.get<ChildComponent>(currentChild);
                            ImVec2 childPos = ImGui::GetCursorScreenPos();

                            float yOffset = (ImGui::GetFontSize() + ImGui::GetStyle().FramePadding.y * 2.0f) * 0.5f;
                            childPos.y += yOffset;

                            ImVec2 childLineEnd = childPos;
                            childLineEnd.x += 5.0f;
                            childPos.x -= 8.0f;

							ImGui::GetWindowDrawList()->AddLine(childPos, childLineEnd, ImGui::GetColorU32(ImGuiCol_Border));

                            forEachEnt(currentChild);

                            currentChild = childComponent.nextChild;
                        }
                        ImVec2 end = ImGui::GetCursorScreenPos();

                        start.x -= 8.0f;
                        end.x -= 8.0f;
                        end.y -= ImGui::GetFontSize() * 0.5f + ImGui::GetStyle().FramePadding.y + ImGui::GetStyle().ItemSpacing.y;

                        ImGui::GetWindowDrawList()->AddLine(start, end, ImGui::GetColorU32(ImGuiCol_Border));
                    }
                    ImGui::TreePop();
                }
                ImGui::PopID();
            };

            static uint32_t renamingFolder = 0u;

            if (ImGui::BeginChild("Entities"))
            {
                if (ImGui::BeginPopupContextWindow("AddEntity"))
                {
                    entt::entity newEnt;
                    if (ImGui::Button("Empty"))
                    {
                        newEnt = reg.create();
                        Transform& emptyT = reg.emplace<Transform>(newEnt);
                        reg.emplace<NameComponent>(newEnt).name = "Empty";
                        editor->select(newEnt);
                        Camera& cam = editor->getFirstSceneView()->getCamera();
                        emptyT.position = cam.position + cam.rotation * glm::vec3(0.0f, 0.0f, 1.0f);
                        ImGui::CloseCurrentPopup();
                    }

                    if (ImGui::Button("Point Light"))
                    {
                        newEnt = reg.create();
                        Transform& emptyT = reg.emplace<Transform>(newEnt);
                        reg.emplace<NameComponent>(newEnt).name = "Point Light";
                        editor->select(newEnt);
                        Camera& cam = editor->getFirstSceneView()->getCamera();
                        emptyT.position = cam.position + cam.rotation * glm::vec3(0.0f, 0.0f, 1.0f);
                        auto& light = reg.emplace<WorldLight>(newEnt);
                        light.type = LightType::Point;
                        ImGui::CloseCurrentPopup();
                    }

                    if (ImGui::Button("Spot Light"))
                    {
                        newEnt = reg.create();
                        Transform& emptyT = reg.emplace<Transform>(newEnt);
                        reg.emplace<NameComponent>(newEnt).name = "Spot Light";
                        editor->select(newEnt);
                        Camera& cam = editor->getFirstSceneView()->getCamera();
                        emptyT.position = cam.position + cam.rotation * glm::vec3(0.0f, 0.0f, 1.0f);
                        emptyT.rotation = cam.rotation;
                        auto& light = reg.emplace<WorldLight>(newEnt);
                        light.type = LightType::Spot;
                        light.spotOuterCutoff = glm::radians(50.0f);
                        light.spotCutoff = glm::radians(20.0f);
                        light.maxDistance = 5.0f;
                        light.intensity = 3.0f;
                        ImGui::CloseCurrentPopup();
                    }

                    if (reg.valid(newEnt))
                    {
                        HierarchyUtil::setEntityParent(reg, newEnt, reg.view<SceneRootComponent>().front());
                    }
                    ImGui::EndPopup();
                }

                if (searchText.empty())
                {
					reg.view<Transform>(entt::exclude_t<ChildComponent>{}).each(
						[&](entt::entity ent, const Transform&) { forEachEnt(ent); });
                }
                else
                {
                    for (auto& ent : filteredEntities)
                        forEachEnt(ent);
                }
            }
            ImGui::EndChild();

            bool openFolderPopup = false;

            // Using a lambda here for early exit
            auto entityPopup = [&](entt::entity e)
            {
                if (!reg.valid(e))
                {
                    ImGui::CloseCurrentPopup();
                    return;
                }

                if (ImGui::Button("Delete"))
                {
                    reg.destroy(e);
                    ImGui::CloseCurrentPopup();
                    return;
                }
            };

            if (openEntityContextMenu)
            {
                ImGui::OpenPopup("Entity Context Menu");
            }

            if (ImGui::BeginPopup("Entity Context Menu"))
            {
                entityPopup(popupOpenFor);
                ImGui::EndPopup();
            }
        }
        ImGui::End();
    }
}

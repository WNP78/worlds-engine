#include "EditorWindows.hpp"
#include <AssetCompilation/AssetCompilers.hpp>
#include <Core/Console.hpp>
#include <Core/Log.hpp>
#include <Editor/AssetEditors.hpp>
#include <Editor/GuiUtil.hpp>
#include <ImGui/imgui.h>
#include <Libs/IconsFontAwesome5.h>
#include <Serialization/SceneSerialization.hpp>
#include <Util/CreateModelObject.hpp>
#include <filesystem>
#include <slib/Path.hpp>
#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <shellapi.h>
#elif defined(__linux__)
#endif
#include <AssetCompilation/AssetCompilerUtil.hpp>

namespace worlds
{
    struct EnumerateCallbackData
    {
        Editor* editor;
        entt::registry* registry;
        std::string popupToOpen;
        std::string hoveredDir;
    };

    PHYSFS_EnumerateCallbackResult enumerateCallbackTree(void* data, const char* origDir, const char* fName)
    {
        EnumerateCallbackData* callbackData = (EnumerateCallbackData*)data;
        PHYSFS_Stat stat;
        std::string filePath = std::string(origDir) + "/" + fName;
        PHYSFS_stat(filePath.c_str(), &stat);

        if (stat.filetype == PHYSFS_FILETYPE_DIRECTORY)
        {
            if (ImGui::TreeNodeEx(fName, 0))
            {
                if (ImGui::IsItemHovered())
                {
                    callbackData->hoveredDir = filePath;
                }
                
                if (ImGui::BeginPopupContextItem())
                {
                    if (ImGui::Button("Create Folder"))
                    {
                        callbackData->popupToOpen = "New Folder";
                        ImGui::CloseCurrentPopup();
                    }

                    if (ImGui::Button("Create Material"))
                    {
                        callbackData->popupToOpen = "New Material";
                        ImGui::CloseCurrentPopup();
                    }

                    ImGui::EndPopup();
                }
                
                std::string enumDir = std::string(origDir) + "/" + fName;
                PHYSFS_enumerate(enumDir.c_str(), enumerateCallbackTree, data);
                ImGui::TreePop(); 
            }
            else if (ImGui::IsItemHovered())
            {
                callbackData->hoveredDir = filePath;
            }
        }
        else
        {
            if (ImGui::TreeNodeEx(fName, ImGuiTreeNodeFlags_Leaf))
            {
               ImGui::TreePop(); 
            }

            if (ImGui::BeginPopupContextItem())
            {
                if (std::filesystem::path(filePath).extension() == ".wmdlj")
                {
                    if (ImGui::Button("Create in scene"))
                    {
                        AssetID compiledAsset = getOutputAsset(filePath);
                        Camera& editorCam = callbackData->editor->getFirstSceneView()->getCamera();
                        glm::vec3 pos = editorCam.position + editorCam.rotation * glm::vec3(0.0f, 0.0f, 1.0f);
                        createModelObject(*callbackData->registry, pos, glm::quat{1.0f, 0.0f, 0.0f, 0.0f}, compiledAsset,
                                          AssetDB::pathToId("Materials/DevTextures/dev_blue.wmatj"));
                        ImGui::CloseCurrentPopup();
                    } 
                }
                ImGui::EndPopup();
            }

            if (ImGui::IsItemHovered())
            {
                callbackData->hoveredDir = origDir;
            }
            
            if (ImGui::IsItemHovered() && ImGui::IsMouseDoubleClicked(ImGuiMouseButton_Left))
            {
                callbackData->editor->openAsset(AssetDB::pathToId(filePath.c_str()));
            }
        }
        
        return PHYSFS_ENUM_OK;
    }
    
    void Assets::draw(entt::registry& reg)
    {
        if (ImGui::Begin(ICON_FA_FOLDER u8" Assets", &active))
        {
            EnumerateCallbackData callbackData
            {
                .editor = editor,
                .registry = &reg
            };
            
            PHYSFS_enumerate("SourceData", enumerateCallbackTree, &callbackData);

            if (ImGui::BeginPopup("New Folder"))
            {
                static std::string newFolderName;
                if (ImGui::InputText("Folder Name", &newFolderName, ImGuiInputTextFlags_EnterReturnsTrue))
                {
                    std::string newFolderPath = callbackData.hoveredDir + "/" + newFolderName;
                    std::filesystem::create_directories(newFolderPath);
                    ImGui::CloseCurrentPopup();
                }
                ImGui::EndPopup();
            }

            if (ImGui::BeginPopup("New Material"))
            {
                static std::string newMaterialName;
                if (ImGui::InputText("Name (without extension)", &newMaterialName, ImGuiInputTextFlags_EnterReturnsTrue))
                {
                    std::string p = callbackData.hoveredDir + "/" + newMaterialName + ".wmatj";
                    PHYSFS_File* f = PHYSFS_openWrite(p.c_str());
                    const char emptyJson[] = "{}";
                    PHYSFS_writeBytes(f, emptyJson, sizeof(emptyJson));
                    PHYSFS_close(f);
                    ImGui::CloseCurrentPopup();
                }
                ImGui::EndPopup();
            }

            if (!callbackData.popupToOpen.empty())
            {
                ImGui::OpenPopup(callbackData.popupToOpen.c_str());
            }
            
            callbackData.popupToOpen.clear();
            lastHoveredDir = callbackData.hoveredDir;
        }

        ImGui::End();
    }

    void Assets::onFileDropped(const char* filePath)
    {
        if (ImGui::Begin(ICON_FA_FOLDER u8" Assets", &active))
        {
            // ImGui's mouse position doesn't get updated when dragging something in from another window,
            // so we have to manually check if the mouse is over the assets panel
            int x, y;
            SDL_GetMouseState(&x, &y);
            ImVec2 wPos = ImGui::GetWindowPos();
            ImVec2 wSize = ImGui::GetWindowSize();

            if (x > wPos.x && x < wPos.x + wSize.x && y > wPos.y && y < wPos.y + wSize.y)
            {
                std::string trimmedHoverDir = lastHoveredDir.empty() ? "" : lastHoveredDir.substr(11);
                // alright! we've got a file being dropped on the asset panel.
                // now we need to figure out where to put it.
                std::string rawDir = std::string(editor->currentProject().root()) + "/Raw/" + trimmedHoverDir;
                std::filesystem::create_directories(rawDir);
                
                std::filesystem::path srcPath = filePath;
                std::string dest = rawDir + "/" + srcPath.filename().string();
                std::filesystem::copy_file(filePath, dest);
                std::string pathInRaw = trimmedHoverDir + "/" + srcPath.filename().string();
                editor->importAsset(pathInRaw.c_str());
            }
        }

        ImGui::End();
    }
}

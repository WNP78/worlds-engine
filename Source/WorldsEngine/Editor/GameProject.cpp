#include "Editor.hpp"
#include <Core/Log.hpp>
#include <Editor/ProjectAssetCompiler.hpp>
#include <filesystem>
#include <fstream>
#include <nlohmann/json.hpp>
#include <physfs.h>
#include <slib/Path.hpp>
#include <Util/JsonUtil.hpp>

namespace worlds
{
    GameProject::GameProject(std::string path)
    {
        {
            // Parse the project file from the specified path
            std::ifstream i(path);
            nlohmann::json j;
            i >> j;

            _name = j["projectName"];

            slib::Path parsedPath{ path.c_str() };
            slib::Path rootPath = parsedPath.parentPath();

            _root = rootPath.cStr();

            _srcDataPath = ((slib::String)rootPath + "/SourceData").cStr();
            _compiledDataPath = ((slib::String)rootPath + "/Data").cStr();
            _rawPath = ((slib::String)rootPath + "/Raw").cStr();

            for (auto& dir : j["copyDirectories"])
            {
                _copyDirs.push_back(dir);
            }
        }

        {
            std::ifstream i(_root + "/UserSettings.json");
            if (i.good()) i >> _userSettings;
            else _userSettings = {};

            if (_userSettings.contains("lastScenePath"))
            {
                _lastScenePath = _userSettings["lastScenePath"];
            }

            if (!_userSettings.contains("sceneCameraPose"))
            {
                _userSettings["sceneCameraPose"] = nlohmann::json::object();
            }
        }

        slib::String tempDir = (_root + "/Temp").c_str();
        bool tempDirExists = std::filesystem::is_directory(tempDir.cStr());

        if (!tempDirExists)
        {
            std::filesystem::create_directory(tempDir.cStr());
            std::filesystem::create_directory((tempDir + "/Undo").cStr());
            logVrb("Creating project temp directory %s", tempDir.cStr());
        }

        // Copy in 
        const char* basePath = SDL_GetBasePath();
        std::string managedAssemblyPath = basePath;
        managedAssemblyPath += "NetAssemblies/WorldsEngineManaged.dll";
        std::string targetPath = _root + std::string("/Temp/WorldsEngineManaged.dll");
        std::filesystem::copy_file(managedAssemblyPath, targetPath,
            std::filesystem::copy_options::overwrite_existing);

        _projectAssets = std::make_unique<ProjectAssets>(*this, true);
        _assetCompiler = std::make_unique<ProjectAssetCompiler>(*this);
    }

    void GameProject::saveProjectSettingsJson()
    {
        nlohmann::json j;

        j["projectName"] = _name;

        auto copy = nlohmann::json::array();
        for (auto& dir : _copyDirs)
        {
            copy.push_back(dir);
        }

        j["copyDirectories"] = copy;

        std::ofstream o(_root + "/WorldsProject.json");
        o << std::setw(4) << j;
    }

    void GameProject::saveUserSettingsJson()
    {
        _userSettings["lastScenePath"] = _lastScenePath;
        
        std::ofstream o(_root + "/UserSettings.json");
        o << std::setw(4) << _userSettings;
    }

    void GameProject::setCameraPoseForScene(AssetID scene, glm::vec3 position, glm::quat rotation)
    {
        _userSettings["sceneCameraPose"][std::to_string(scene)] = {
            {"pos", position },
            {"rot", rotation },
        };
    }

    bool GameProject::getCameraPoseForScene(AssetID scene, glm::vec3& position, glm::quat& rotation)
    {
        auto &poses = _userSettings["sceneCameraPose"];
        std::string key = std::to_string(scene);
        if (poses.contains(key))
        {
            nlohmann::json& pose = poses[key];
            position = pose["pos"];
            rotation = pose["rot"];

            return true;
        }

        return false;
    }

    std::string_view GameProject::name() const
    {
        return _name;
    }

    std::string_view GameProject::lastScenePath() const
    {
        return _lastScenePath;
    }

    void GameProject::setLastScenePath(std::string_view const newPath)
    {
        _lastScenePath = newPath;
    }

    std::string_view GameProject::root() const
    {
        return _root;
    }

    std::string_view GameProject::builtData() const
    {
        return _compiledDataPath;
    }

    std::string_view GameProject::rawData() const
    {
        return _rawPath;
    }

    std::string_view GameProject::sourceData() const
    {
        return _srcDataPath;
    }

    ProjectAssets& GameProject::assets()
    {
        return *_projectAssets;
    }

    ProjectAssetCompiler& GameProject::assetCompiler()
    {
        return *_assetCompiler;
    }

    void GameProject::mountPaths()
    {
        logMsg("Mounting project %s", _name.c_str());
        logVrb("Mounting %s as compiled data path", _compiledDataPath.c_str());
        logVrb("Mounting %s as source data path", _srcDataPath.c_str());
        PHYSFS_mount(_compiledDataPath.c_str(), "/", 0);
        PHYSFS_mount(_srcDataPath.c_str(), "/SourceData", 0);
        PHYSFS_mount(_rawPath.c_str(), "/Raw", 0);
        PHYSFS_mount((_root + "/Temp").c_str(), "/Temp", 0);
        PHYSFS_setWriteDir(_root.c_str());

        for (const std::string& dir : _copyDirs)
        {
            std::string dirPath = _srcDataPath + "/" + dir;
            logVrb("Mounting %s as %s", dirPath.c_str(), dir.c_str());
            if (PHYSFS_mount(dirPath.c_str(), dir.c_str(), 1) == 0)
            {
                PHYSFS_ErrorCode errCode = PHYSFS_getLastErrorCode();
                logErr("Error mounting %s: %s", dirPath.c_str(), PHYSFS_getErrorByCode(errCode));
            }
        }

        _projectAssets->enumerateAssets();
    }

    void GameProject::unmountPaths()
    {
        _projectAssets->pauseWatcher = true;
        PHYSFS_unmount(_compiledDataPath.c_str());
        PHYSFS_unmount(_srcDataPath.c_str());
        PHYSFS_unmount(_rawPath.c_str());
        PHYSFS_unmount((_root + "/Temp").c_str());

        for (const std::string& dir : _copyDirs)
        {
            std::string dirPath = _srcDataPath + "/" + dir;
            if (PHYSFS_unmount(dirPath.c_str()) == 0)
            {
                PHYSFS_ErrorCode errCode = PHYSFS_getLastErrorCode();
                logErr("Error unmounting %s: %s", dirPath.c_str(), PHYSFS_getErrorByCode(errCode));
            }
        }
    }
}

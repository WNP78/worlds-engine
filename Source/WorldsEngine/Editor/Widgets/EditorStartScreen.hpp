#pragma once
#include <Editor/Editor.hpp>
#include <Editor/Widgets/LogoWidget.hpp>
#include <IO/IOUtil.hpp>
#include <filesystem>
#include <fstream>
#include <nlohmann/json.hpp>
#include <slib/Path.hpp>
#include <nfd.hpp>
#include <sstream>
#include <Util/StringBuilder.hpp>

namespace worlds
{
    class EditorStartScreen
    {
    public:
        void draw(Editor* ed)
        {
            struct RecentProject
            {
                std::string name;
                std::string path;
                ImTextureID badge = nullptr;
            };

            static std::vector<RecentProject> recentProjects;
            static bool loadedRecentProjects = false;
            static LogoWidget logo{};

            if (!loadedRecentProjects)
            {
                loadedRecentProjects = true;
                char* prefPath = SDL_GetPrefPath("Someone Somewhere", "Worlds Engine");
                std::ifstream recentProjectsStream(prefPath + std::string{"recentProjects.txt"});

                if (recentProjectsStream.good())
                {
                    std::string currLine;

                    while (std::getline(recentProjectsStream, currLine))
                    {
                        try
                        {
                            nlohmann::json pj = nlohmann::json::parse(std::ifstream(currLine));
                            RecentProject rp{pj["projectName"], currLine};

                            std::filesystem::path badgePath =
                                std::filesystem::path(currLine).parent_path() / "Editor" / "badge.png";
                            if (std::filesystem::exists(badgePath))
                            {
                            }

                            recentProjects.push_back({pj["projectName"], currLine});
                        }
                        catch (const nlohmann::detail::exception& except)
                        {
                            logWarn("Failed to parse line of recent project file.");
                        }
                    }
                }

                SDL_free(prefPath);
            }

            ImVec2 menuBarSize;
            if (ImGui::BeginMainMenuBar())
            {
                menuBarSize = ImGui::GetWindowSize();
                ImGui::EndMainMenuBar();
            }

            ImGuiViewport* viewport = ImGui::GetMainViewport();
            glm::vec2 projectWinSize = (glm::vec2)viewport->Size - glm::vec2(0.0f, menuBarSize.y);
            ImGui::SetNextWindowPos((glm::vec2)viewport->Pos + glm::vec2(0.0f, menuBarSize.y));
            ImGui::SetNextWindowSize(projectWinSize);
            ImGui::SetNextWindowViewport(viewport->ID);
            ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
            ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
            ImGui::Begin("ProjectWindow", 0,
                         ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize |
                             ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_NoScrollbar |
                             ImGuiWindowFlags_NoBringToFrontOnFocus | ImGuiWindowFlags_NoScrollWithMouse);
            ImGui::PopStyleVar(2);

            glm::vec2 initialCpos = ImGui::GetCursorScreenPos();
            ImGui::GetWindowDrawList()->AddLine(
                initialCpos + glm::vec2(490.0f, 0.0f),
                initialCpos + glm::vec2(490.0f, projectWinSize.y - ImGui::GetStyle().WindowPadding.y * 2.0f),
                ImGui::GetColorU32(ImGuiCol_Separator), 2.0f);

            ImGui::Columns(2, nullptr, false);
            ImGui::SetColumnWidth(0, 500.0f);

            logo.draw();
            ImGui::Text("Select a project.");

            bool open = true;
            ImVec2 popupSize(windowSize.x - 50.0f, windowSize.y - 50.0f);
            ImGui::SetNextWindowPos(ImVec2(25.0f, 25.0f));
            ImGui::SetNextWindowSize(popupSize);
            if (ImGui::BeginPopupModal("Create Project", &open, ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove))
            {
                static std::string projectName;
                ImGui::InputText("Name", &projectName);

                static bool showSpaceWarning = false;

                if (showSpaceWarning)
                {
                    ImGui::TextColored(ImVec4(1.f, 0.f, 0.f, 1.f), "The project name cannot contain spaces!");
                }

                static slib::String projectPath;
                ImGui::InputText("Path", &projectPath);
                ImGui::SameLine();

                if (ImGui::Button("Choose Folder"))
                {
                    NFD::Guard nfdGuard;
                    NFD::UniquePath pickedPath;
                    nfdresult_t res = NFD::PickFolder(pickedPath);

                    if (res == NFD_OKAY)
                    {
                        projectPath = pickedPath.get();
                    }
                }

                if (ImGui::Button("Create"))
                {
                    if (projectName.find(' ') != std::string::npos)
                    {
                        showSpaceWarning = true;
                    }
                    else
                    {
                        // Create the thing!
                        std::string basePath = SDL_GetBasePath();
                        std::string projectDir = (projectPath).cStr() + ("/" + projectName);
                        std::filesystem::copy(basePath + "ProjectTemplate", projectDir,
                                              std::filesystem::copy_options::recursive);
                        templateFile(projectDir + "/WorldsProject.json", projectName);
                        templateFile(projectDir + "/Code/FreecamSystem.cs", projectName);
                        templateFile(projectDir + "/Code/RollyBall.cs", projectName);
                        templateFile(projectDir + "/Code/Game.csproj", projectName);
                        templateFile(projectDir + "/build.py", projectName);
                        std::filesystem::create_directory(projectDir + "/Data");
#ifdef _WIN32
                        char sep = '\\';
#else
                        char sep = '/ ';
#endif
                        ed->openProject(projectDir + sep + "WorldsProject.json");
                        ImGui::CloseCurrentPopup();
                    }
                }
                ImGui::EndPopup();
            }

            if (ImGui::Button("Open"))
            {
                NFD::Guard nfdGuard;
                NFD::UniquePath path;
                nfdfilteritem_t filter[1] = {{"Worlds Project file",".json"}};
                nfdresult_t result = NFD::OpenDialog(path, filter, 1);

                if (result == NFD_OKAY)
                {
                    ed->openProject(path.get());
                }
            }

            if (ImGui::Button("Create New"))
            {
                ImGui::OpenPopup("Create Project");
            }

            ImGui::NextColumn();

            ImGui::BeginChild("projects");
            for (RecentProject& project : recentProjects)
            {
                const glm::vec2 widgetSize{600.0f, 200.0f};
                ImGui::PushID(project.path.c_str());

                ImDrawList* dl = ImGui::GetWindowDrawList();
                dl->AddRect(ImGui::GetCursorScreenPos(), (glm::vec2)ImGui::GetCursorScreenPos() + widgetSize,
                            ImColor(ImGui::GetStyleColorVec4(ImGuiCol_FrameBg)), 5.0f, 0, 3.0f);

                ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(5.0f, 5.0f));
                if (ImGui::BeginChild(ImGui::GetID("project"), widgetSize, true,
                                      ImGuiWindowFlags_AlwaysUseWindowPadding))
                {
                    pushBoldFont();
                    ImGui::TextUnformatted(project.name.c_str());
                    ImGui::PopFont();
                    ImGui::SameLine();
                    ImGui::TextUnformatted(project.path.c_str());

                    if (ImGui::Button("Open Project"))
                    {
                        ed->openProject(project.path);
                    }
                }
                ImGui::EndChild();

                ImGui::PopStyleVar();
                ImGui::PopID();
                ImGui::Dummy(ImVec2(0.0f, ImGui::GetTextLineHeightWithSpacing()));
            }
            ImGui::EndChild();

            ImGui::Dummy(ImVec2(
                0.0f, glm::max(projectWinSize.y - ImGui::GetCursorPosY() - ImGui::GetStyle().WindowPadding.y, 0.0f)));

            ImGui::End();
        }

    private:
        void templateFile(std::string filePath, std::string projectName)
        {
            std::ifstream is{filePath};
            std::stringstream ss;
            ss << is.rdbuf();

            std::string str = ss.str();

            char* basePath = SDL_GetBasePath();
            StackStringBuilder<8192> pathBuilder;

            replaceAll(str, "{{PROJECT_NAME}}", projectName);

            pathBuilder.append(basePath);
            pathBuilder.append("NetAssemblies");
            replaceAll(str, "{{NETASSEMBLIES_PATH}}", pathBuilder.get());

            pathBuilder.clear();
            pathBuilder.append(basePath);
            pathBuilder.append("GameBuild");
            replaceAll(str, "{{GAME_BUILD_PATH}}", pathBuilder.get());

            std::ofstream s{filePath, std::ios::trunc};
            s << str;
            s.close();
        }

        void replaceAll(std::string& str, const std::string& from, const std::string& to)
        {
            size_t start_pos = 0;
            while ((start_pos = str.find(from, start_pos)) != std::string::npos)
            {
                str.replace(start_pos, from.length(), to);
                start_pos += to.length(); // Handles case where 'to' is a substring of 'from'
            }
        }
    };
}

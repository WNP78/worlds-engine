#pragma once
#include <Core/IGameEventHandler.hpp>

namespace worlds
{
    class IntegratedMenubar
    {
    public:
        IntegratedMenubar();
        void draw();
    };
}
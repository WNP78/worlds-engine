#include "Editor.hpp"
#include "Editor/EditorAssetSearchPopup.hpp"
#include "Editor/Widgets/IntegratedMenubar.hpp"
#include "GuiUtil.hpp"
#include "SDL_scancode.h"
#include <ComponentMeta/ComponentMetadata.hpp>
#include <Core/Engine.hpp>
#include <Core/Transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/norm.hpp>
#include <memory>
#define IMGUI_DEFINE_MATH_OPERATORS
#include "AssetEditors.hpp"
#include "EditorWindows/EditorWindows.hpp"
#include "ImGui/imgui_internal.h"
#include <tracy/Tracy.hpp>
#include <AssetCompilation/AssetCompilers.hpp>
#include <Audio/Audio.hpp>
#include <ComponentMeta/ComponentFuncs.hpp>
#include <Core/Log.hpp>
#include <Core/NameComponent.hpp>
#include <Core/Window.hpp>
#include <Editor/EditorActions.hpp>
#include <Editor/ProjectAssetCompiler.hpp>
#include <Editor/Widgets/EditorStartScreen.hpp>
#include <ImGui/imgui.h>
#include <ImGui/imgui_internal.h>
#include <Libs/IconsFontAwesome5.h>
#include <Libs/IconsFontaudio.h>
#include <Libs/pcg_basic.h>
#include <Physics/Physics.hpp>
#include <Physics/PhysicsActor.hpp>
#include <Scripting/ScriptEngine.hpp>
#include <Serialization/SceneSerialization.hpp>
#include <Util/CreateModelObject.hpp>
#include <filesystem>
#include <fstream>
#include <glm/gtx/matrix_decompose.hpp>
#include <nlohmann/json.hpp>
#include <slib/Subprocess.hpp>
#ifdef _WIN32
#include <slib/Win32Util.hpp>
#endif
#include <VR/OpenXRInterface.hpp>

#include "Core/HierarchyUtil.hpp"
#include <Util/StringBuilder.hpp>

namespace worlds
{
    slib::Subprocess* dotnetWatchProcess = nullptr;

    const char* toolStr(Tool tool)
    {
        switch (tool)
        {
        case Tool::None:
            return "None";
        case Tool::Rotate:
            return "Rotate";
        case Tool::Scale:
            return "Scale";
        case Tool::Translate:
            return "Translate";
        case Tool::Bounds:
            return "Bounds";
        default:
            return "???";
        }
    }

    std::string Editor::generateWindowTitle()
    {
        if (!project)
#ifndef NDEBUG
            return "Worlds Engine (debug)";
#else
            return "Worlds Engine";
#endif

        std::string base =
#ifndef NDEBUG
            "Worlds Engine Editor (debug) - "
#else
            "Worlds Engine Editor - "
#endif
            + std::string(project->name());

        if (lastSaveModificationCount != undo.modificationCount())
        {
            base += "*";
        }

        return base;
    }

    void Editor::updateWindowTitle()
    {
        static std::string lastTitle;
        std::string newTitle = generateWindowTitle();

        if (lastTitle != newTitle)
        {
            g_engine->getMainWindow().setTitle(newTitle.c_str());
        }
    }

    static int menuButtonsExtent = 0;

    Editor::Editor(World& world)
        : actionSearch(this, world), assetSearch(this), currentTool(Tool::Translate), world(world),
          currentSelectedEntity(entt::null), cameraSpeed(5.0f), imguiMetricsOpen(false),
          settings()
    {
        g_engine->pauseSim = true;

#define ADD_EDITOR_WINDOW(type) editorWindows.add(std::make_unique<type>(this))

        ADD_EDITOR_WINDOW(EntityList);
        ADD_EDITOR_WINDOW(Assets);
        ADD_EDITOR_WINDOW(GameControls);
        ADD_EDITOR_WINDOW(StyleEditor);
        ADD_EDITOR_WINDOW(AboutWindow);
        ADD_EDITOR_WINDOW(BakingWindow);
        ADD_EDITOR_WINDOW(SceneSettingsWindow);
        ADD_EDITOR_WINDOW(RawAssets);
        ADD_EDITOR_WINDOW(AssetCompilationManager);
        ADD_EDITOR_WINDOW(NodeEditorTest);
        ADD_EDITOR_WINDOW(GameView);

#undef ADD_EDITOR_WINDOW
        AssetCompilers::initialise();
        AssetEditors::initialise();
        sceneViews.add(new EditorSceneView{this});

        titleBarIcon = g_renderer->getUITextureManager()->loadOrGet(
            AssetDB::pathToId("UI/Editor/Images/logo_no_background_small.png"));

        loadOpenWindows();

        g_input->addKeydownHandler(
            [&](SDL_Scancode scancode)
            {
                ModifierFlags flags = ModifierFlags::None;

                if (g_input->ctrlHeld())
                    flags = flags | ModifierFlags::Control;

                if (g_input->shiftHeld())
                    flags = flags | ModifierFlags::Shift;

                queuedKeydowns.add({scancode, flags});
            }
        );

        g_console->registerCommand(
            [&](const char*)
            {
                if (currentState != GameState::Editing)
                    return;
                AssetID sceneId = world.registry.ctx<SceneInfo>().id;
                auto startPlay = [sceneId, this]()
                {
                    g_engine->pauseSim = false;

                    if (sceneId != ~0u)
                        g_engine->loadScene(sceneId);
                    currentState = GameState::Playing;
                };
                
                if (lastSaveModificationCount == undo.modificationCount())
                {
                    startPlay();
                }
                else
                {
                    messageBoxModal(
                        "Unsaved Changes",
                        "You have unsaved changes that will be lost if you continue to play mode. Save the scene first?",
                        [&, startPlay](bool response)
                        {
                            if (response)
                            {
                                EditorActions::findAction("scene.save").function(this, world);
                                startPlay();
                            }
                        }
                    );
                }
            },
            "play", "play."
        );

        g_console->registerCommand(
            [&](const char*)
            {
                if (currentState != GameState::Playing)
                    return;

                g_engine->pauseSim = true;
                g_input->lockMouse(false);
                addNotification("Scene paused");
                currentState = GameState::Paused;
            },
            "pauseAndEdit", "pause and edit."
        );

        g_console->registerCommand(
            [&](const char*)
            {
                if (openedScene != ~0u)
                {
                    g_engine->loadScene(openedScene);
                }

                g_engine->pauseSim = true;
                g_input->lockMouse(false);
                currentState = GameState::Editing;
            },
            "reloadAndEdit", "reload and edit."
        );

        g_console->registerCommand(
            [&](const char*)
            {
                g_engine->pauseSim = false;
                currentState = GameState::Playing;
            },
            "unpause", "unpause and go back to play mode."
        );

        EditorActions::addAction({
            "scene.save",
            [&](Editor* ed, World& world)
            {
                if (isPlaying())
                {
                    addNotification("No saving in play mode!!", NotificationType::Error);
                    return;
                }
                if (world.registry.ctx<SceneInfo>().id != ~0u && !g_input->shiftHeld())
                {
                    AssetID sceneId = world.registry.ctx<SceneInfo>().id;

                    // update scene camera position
                    worlds::EditorSceneView* sceneView = ed->getFirstSceneView();
                    if (sceneView != nullptr)
                    {
                        worlds::Camera& cam = sceneView->getCamera();
                        ed->currentProject().setCameraPoseForScene(sceneId, cam.position, cam.rotation);
                        ed->currentProject().saveUserSettingsJson();
                    }

                    JsonSceneSerializer::saveScene(sceneId, world.registry);

                    lastSaveModificationCount = undo.modificationCount();
                }
                else
                {
                    ImGui::OpenPopup("Save Scene");
                }
            },
            "Save Scene"
        });

        EditorActions::addAction({
            "scene.open",
            [](Editor* ed, World&) { ImGui::OpenPopup("Open Scene"); },
            "Open Scene"
        });

        EditorActions::addAction({
            "scene.new",
            [](Editor* ed, World& world)
            {
                entt::registry* regPtr = &world.registry;
                messageBoxModal(
                "New Scene", 
                "Are you sure you want to clear the current scene and create a new one?",
                [ed, &world, regPtr](bool result)
                {
                    if (result)
                    {
                        world.clear();
                        regPtr->set<SceneInfo>("Untitled", INVALID_ASSET);
                        ed->updateWindowTitle();
                    }
                });
            },
            "New Scene"
        });

        EditorActions::addAction({
            "editor.undo",
            [](Editor* ed, World& world) { ed->undo.undo(world.registry); },
            "Undo"
        });

        EditorActions::addAction({
            "editor.redo",
            [](Editor* ed, World& world) { ed->undo.redo(world.registry); },
            "Redo"
        });

        EditorActions::addAction({
            "editor.togglePlay",
            [](Editor* ed, World& world)
            {
                if (ed->isPlaying())
                    g_console->executeCommandStr("reloadAndEdit");
                else
                    g_console->executeCommandStr("play");
            },
            "Play"
        });

        EditorActions::addAction({
            "editor.togglePause",
            [](Editor* ed, World& world)
            {
                GameState state = ed->getCurrentState();
                if (state == GameState::Paused)
                    g_console->executeCommandStr("unpause");
                else if (state == GameState::Playing)
                    g_console->executeCommandStr("pauseAndEdit");
            },
            "Toggle Pause"
        });

        EditorActions::addAction({
            "editor.toggleImGuiMetrics",
            [](Editor* ed, World&){ ed->imguiMetricsOpen = !ed->imguiMetricsOpen; },
            "Toggle ImGUI Metrics"
        });

        EditorActions::addAction({
            "editor.openActionSearch",
            [](Editor* ed, World&) { ed->actionSearch.show(); }
        });

        EditorActions::addAction({
            "editor.openAssetSearch",
            [](Editor* ed, World&) { ed->assetSearch.show(); }
        });

        EditorActions::addAction({
            "editor.addStaticPhysics",
            [](Editor* ed, World& world)
            {
                if (!world.registry.valid(ed->currentSelectedEntity))
                {
                    addNotification("Nothing selected to add physics to!", NotificationType::Error);
                    return;
                }

                if (!world.registry.has<WorldObject>(ed->currentSelectedEntity))
                {
                    addNotification("Selected object doesn't have a WorldObject!", NotificationType::Error);
                    return;
                }

                WorldObject& wo = world.registry.get<WorldObject>(ed->currentSelectedEntity);
                ComponentMetadataManager::byName["Physics Actor"]->create(ed->currentSelectedEntity, world.registry);
                PhysicsActor& pa = world.registry.get<PhysicsActor>(ed->currentSelectedEntity);
                PhysicsShape ps;
                ps.type = PhysicsShapeType::Mesh;
                ps.mesh.mesh = wo.mesh;
                pa.physicsShapes.push_back(std::move(ps));
            },
            "Add static physics"
        });

        EditorActions::addAction({
            "editor.createPrefab",
            [](Editor*, World& world) { ImGui::OpenPopup("Save Prefab"); },
            "Create prefab"
        });

        EditorActions::addAction({
            "editor.roundScale",
            [](Editor* ed, World& world)
            {
                if (!world.registry.valid(ed->currentSelectedEntity))
                    return;
                Transform& t = world.registry.get<Transform>(ed->currentSelectedEntity);
                t.scale = glm::round(t.scale);

                for (entt::entity e : ed->selectedEntities)
                {
                    Transform& t = world.registry.get<Transform>(e);
                    t.scale = glm::round(t.scale);
                }
            },
            "Round selection scale"
        });

        EditorActions::addAction({
            "editor.clearScale",
            [](Editor* ed, World& world)
            {
                if (!world.registry.valid(ed->currentSelectedEntity))
                    return;
                Transform& t = world.registry.get<Transform>(ed->currentSelectedEntity);
                t.scale = glm::vec3(1.0f);

                for (entt::entity e : ed->selectedEntities)
                {
                    Transform& t = world.registry.get<Transform>(e);
                    t.scale = glm::vec3(1.0f);
                }
            },
            "Clear selection scale"
        });

        EditorActions::addAction({
            "editor.setStatic",
            [](Editor* ed, World& world)
            {
                if (!world.registry.valid(ed->currentSelectedEntity))
                    return;

                StaticFlags allFlags =
                    StaticFlags::Audio |
                    StaticFlags::Rendering |
                    StaticFlags::Navigation;

                world.registry.get<WorldObject>(ed->currentSelectedEntity).staticFlags = allFlags;
                for (entt::entity e : ed->getSelectedEntities())
                {
                    world.registry.get<WorldObject>(e).staticFlags = allFlags;
                }
            },
            "Set selected object as static"
        });

        EditorActions::addAction({
            "editor.copy",
            [](Editor* ed, World& world)
            {
                if (!world.registry.valid(ed->currentSelectedEntity)) return;
                std::vector<entt::entity> allSelectedEntities;
                allSelectedEntities.push_back(ed->currentSelectedEntity);
                for (entt::entity e : ed->selectedEntities)
                {
                    allSelectedEntities.push_back(e);
                }

                std::string entityJson =
                    JsonSceneSerializer::entitiesToJson(world.registry, allSelectedEntities.data(), allSelectedEntities.size());
                SDL_SetClipboardText(entityJson.c_str());
            },
            "Copy selected entities"
        });

        EditorActions::addAction({
            "editor.paste",
            [](Editor* ed, World& world)
            {
                if (!SDL_HasClipboardText()) return;
                const char* txt = SDL_GetClipboardText();
                try
                {
                    auto pastedEnts = JsonSceneSerializer::jsonToEntities(world.registry, txt);
                    if (!pastedEnts.empty())
                    {
                        ed->select(pastedEnts[0]);

                        for (size_t i = 1; i < pastedEnts.size(); i++)
                        {
                            ed->multiSelect(pastedEnts[i]);
                        }
                    }
                    addNotification("Entity pasted! :)");
                }
                catch (nlohmann::detail::exception& e)
                {
                    logErr("Failed to deserialize clipboard entity: %s", e.what());
                    addNotification("Sorry, we couldn't paste that into the scene.", NotificationType::Error);
                }
            }
        });

        EditorActions::addAction({
            "assets.refresh",
            [](Editor* ed, World& world)
            {
                ed->currentProject().assets().enumerateAssets();
                ed->currentProject().assets().checkForAssetChanges();
                ed->currentProject().assetCompiler().startCompiling();
            },
            "Refresh assets"
        });

        struct ActionBindingPair
        {
            const char* actionName;
            SDL_Scancode scancode;
            ModifierFlags flags;
        };

        ActionBindingPair bindingPairs[] = {
            {"scene.save", SDL_SCANCODE_S, ModifierFlags::Control},
            {"scene.open", SDL_SCANCODE_O, ModifierFlags::Control},
            {"scene.new", SDL_SCANCODE_N, ModifierFlags::Control},
            {"editor.undo", SDL_SCANCODE_Z, ModifierFlags::Control},
            {"editor.redo", SDL_SCANCODE_Z, ModifierFlags::Control | ModifierFlags::Shift},
            {"editor.togglePlay", SDL_SCANCODE_P, ModifierFlags::Control},
            {"editor.togglePause", SDL_SCANCODE_P, ModifierFlags::Control | ModifierFlags::Shift},
            {"editor.openActionSearch", SDL_SCANCODE_SPACE, ModifierFlags::Control},
            {"editor.openAssetSearch", SDL_SCANCODE_SPACE, ModifierFlags::Control | ModifierFlags::Shift},
            {"editor.copy", SDL_SCANCODE_C, ModifierFlags::Control},
            {"editor.paste", SDL_SCANCODE_V, ModifierFlags::Control}
        };

        for (auto& bindPair : bindingPairs)
        {
            ActionKeybind kb{bindPair.scancode, bindPair.flags};
            EditorActions::bindAction(bindPair.actionName, kb);
        }

        SceneLoader::registerLoadCallback(this, sceneLoadCallback);
        g_engine->getMainWindow().addFileDropHandler([&](const char* filePath)
        {
            for (auto& editorWindow : editorWindows)
            {
                if (!editorWindow->acceptsFileDrops()) continue;
                editorWindow->onFileDropped(filePath);
            }
        });
    }

    Editor::~Editor()
    {
        saveOpenWindows();

        if (dotnetWatchProcess)
        {
            dotnetWatchProcess->kill();
            delete dotnetWatchProcess;
        }
    }

    void Editor::select(entt::entity entity)
    {
        // Remove selection from existing entity
        if (world.valid(currentSelectedEntity) && world.registry.has<EditorGlow>(currentSelectedEntity))
        {
            world.registry.remove<EditorGlow>(currentSelectedEntity);
        }

        currentSelectedEntity = entity;
        // A null entity means we should deselect the current entity
        for (auto ent : selectedEntities)
        {
            if (world.valid(ent))
                world.registry.remove_if_exists<EditorGlow>(ent);
        }
        selectedEntities.clear();

        if (!world.registry.valid(entity))
        {
            return;
        }

        world.registry.emplace<EditorGlow>(currentSelectedEntity);
    }

    void Editor::multiSelect(entt::entity entity)
    {
        if (!world.registry.valid(entity))
            return;

        if (!world.registry.valid(currentSelectedEntity))
        {
            select(entity);
            return;
        }

        if (entity == currentSelectedEntity)
        {
            if (selectedEntities.numElements() == 0)
            {
                select(entt::null);
            }
            else
            {
                select(selectedEntities[0]);
                selectedEntities.removeAt(0);
            }
            return;
        }

        if (selectedEntities.contains(entity))
        {
            selectedEntities.removeValue(entity);
            world.registry.remove_if_exists<EditorGlow>(entity);
        }
        else
        {
            world.registry.emplace<EditorGlow>(entity);
            selectedEntities.add(entity);
        }
    }

    void Editor::activateTool(Tool newTool)
    {
        assert(world.registry.valid(currentSelectedEntity));
        currentTool = newTool;
        originalObjectTransform = world.registry.get<Transform>(currentSelectedEntity);
    }

    bool Editor::isEntitySelected(entt::entity ent) const
    {
        if (currentSelectedEntity == ent)
            return true;

        for (entt::entity selectedEnt : selectedEntities)
        {
            if (selectedEnt == ent)
                return true;
        }

        return false;
    }

    void Editor::overrideHandle(Transform* t)
    {
        handleOverriden = true;
        overrideTransform = t;
    }

    void Editor::overrideHandle(entt::entity e)
    {
        handleOverrideEntity = e;
    }

    bool Editor::entityEyedropper(entt::entity& picked)
    {
        entityEyedropperActive = true;

        if (world.registry.valid(eyedroppedEntity))
        {
            picked = eyedroppedEntity;
            eyedroppedEntity = entt::null;
            entityEyedropperActive = false;
            return true;
        }

        return false;
    }

    void Editor::saveOpenWindows()
    {
        char* prefPath = SDL_GetPrefPath("Someone Somewhere", "Worlds Engine");
        std::string openWindowPath = prefPath + std::string("openWindows.json");

        nlohmann::json j;

        for (auto& window : editorWindows)
        {
            j[window->getName()] = {{"open", window->isActive()}};
        }

        std::ofstream o{openWindowPath};
        o << std::setw(2) << j;
        o.close();
    }

    void Editor::loadOpenWindows()
    {
        char* prefPath = SDL_GetPrefPath("Someone Somewhere", "Worlds Engine");
        std::string openWindowPath = prefPath + std::string("openWindows.json");

        nlohmann::json j;
        std::ifstream i{openWindowPath};

        if (!i.good())
            return;

        i >> j;
        i.close();

        for (auto& p : j.items())
        {
            std::string name = p.key();
            for (auto& win : editorWindows)
            {
                if (win->getName() == name)
                {
                    win->setActive(p.value().value("open", win->isActive()));
                }
            }
        }
    }

    EditorSceneView* Editor::getFirstSceneView()
    {
        if (sceneViews.numElements() == 0) return nullptr;
        return sceneViews[0];
    }

    void Editor::openAsset(AssetID id)
    {
        if (AssetDB::getAssetExtension(id) == ".wscn")
        {
            openedScene = id;
            g_engine->loadScene(id);
            updateWindowTitle();
            undo.clear();
            return;
        }
        else if (AssetDB::getAssetExtension(id) == ".wprefab")
        {
            entt::entity ent = SceneLoader::createPrefab(id, world.registry);
            if (getFirstSceneView())
            {
                float dist = 1.0f;
                if (world.registry.has<WorldObject>(ent))
                {
                    WorldObject& wo = world.registry.get<WorldObject>(ent);
                    const LoadedMesh& lm = MeshManager::loadOrGet(wo.mesh);
                    dist = lm.sphereBoundRadius + 1.0f;
                }

                Transform& t = world.registry.get<Transform>(ent);
                Camera& cam = getFirstSceneView()->getCamera();
                t.position = cam.position + cam.rotation * glm::vec3(0.0f, 0.0f, dist);
            }
            return;
        }

        IAssetEditorMeta* assetEditorMeta = AssetEditors::getEditorFor(id);
        if (assetEditorMeta == nullptr)
        {
            addNotification("Couldn't open asset", NotificationType::Error);
            return;
        }
        AssetEditorWindow* editor = new AssetEditorWindow(id, this);
        editor->setActive(true);
        assetEditors.add(editor);
    }



    void Editor::eyedropperSelect(entt::entity picked)
    {
        eyedroppedEntity = picked;
    }

    ConVar ed_runDotNetWatch{"ed_runDotNetWatch", "1"};

    void Editor::openProject(std::string_view path)
    {
        ZoneScoped;

        project = std::make_unique<GameProject>(std::string(path));
        project->mountPaths();
        // interfaces.renderer->reloadContent(ReloadFlags::All);

        // Update recent projects list
        std::vector<std::string> recentProjects;

        char* prefPath = SDL_GetPrefPath("Someone Somewhere", "Worlds Engine");
        std::ifstream recentProjectsStream(prefPath + std::string{"recentProjects.txt"});

        if (recentProjectsStream.good())
        {
            std::string currLine;

            while (std::getline(recentProjectsStream, currLine))
            {
                recentProjects.push_back(currLine);
            }
        }

        recentProjects.erase(std::remove(recentProjects.begin(), recentProjects.end(), path), recentProjects.end());
        recentProjects.insert(recentProjects.begin(), std::string(path));
        // Explicitly close streams so the file stays consistent
        recentProjectsStream.close();

        std::ofstream recentProjectsOutStream(prefPath + std::string{"recentProjects.txt"});

        if (recentProjectsOutStream.good())
        {
            for (std::string& path : recentProjects)
            {
                recentProjectsOutStream << path << "\n";
            }
        }

        recentProjectsOutStream.close();

        SDL_free(prefPath);

        AudioSystem::getInstance()->loadMasterBanks();
        if (g_scriptEngine)
        {
            if (gameProjectSelectedCallback == nullptr)
                g_scriptEngine->createManagedDelegate("WorldsEngine.Editor.Editor", "OnGameProjectSelected",
                                                               (void**)&gameProjectSelectedCallback);
            gameProjectSelectedCallback(project.get());
        }

        if (g_vrInterface && PHYSFS_exists("SourceData/VRInput/actions.json"))
        {
            g_vrInterface->loadActionJson("SourceData/VRInput/actions.json");
        }

        if (ed_runDotNetWatch)
        {
            dotnetWatchProcess =
                new slib::Subprocess("dotnet watch build",
                                     (std::string(project->root()) + "/Code").c_str());
        }

        if (!project->lastScenePath().empty())
        {
            AssetID scene = AssetDB::pathToId(project->lastScenePath());
            g_engine->loadScene(scene);
            openedScene = scene;
        }
    }

    void Editor::update(float deltaTime)
    {
        ZoneScoped;
        static IntegratedMenubar menubar{};

        auto sceneViewRemove = [](EditorSceneView* esv)
        {
            if (!esv->open)
            {
                delete esv;
                return true;
            }
            return false;
        };

        sceneViews.erase(std::remove_if(sceneViews.begin(), sceneViews.end(), sceneViewRemove), sceneViews.end());

        auto aeRemove = [](AssetEditorWindow* ae)
        {
            if (!ae->isActive())
            {
                delete ae;
                return true;
            }

            return false;
        };

        assetEditors.erase(std::remove_if(assetEditors.begin(), assetEditors.end(), aeRemove), assetEditors.end());

        bool anyFocused = false;

        if (ImGui::GetIO().ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
        {
            for (ImGuiViewport* v : ImGui::GetPlatformIO().Viewports)
            {
                bool focus = ImGui::GetPlatformIO().Platform_GetWindowFocus(v);
                if (focus)
                    anyFocused = true;
            }
        }
        else
        {
            anyFocused = g_engine->getMainWindow().isFocused();
        }

        static ConVar ed_gameViewOnly{ "ed_gameViewOnly", "0" };
        bool gameViewOnly = ed_gameViewOnly;

        for (EditorSceneView* esv : sceneViews)
        {
            esv->setViewportActive(anyFocused && project && !gameViewOnly);
        }

        if (!isPlaying())
            AudioSystem::getInstance()->stopEverything(world.registry);

        if (!project)
        {
            updateWindowTitle();

            ImVec2 menuBarSize;
            if (ImGui::BeginMainMenuBar())
            {
                if (g_console->getConVar("ed_integratedmenubar")->getInt())
                {
                    float menuBarHeight = ImGui::GetWindowHeight();
                    float spacing = (menuBarHeight - 24) / 2.0f;
                    float prevCursorY = ImGui::GetCursorPosY();
                    ImGui::SetCursorPosY(prevCursorY + spacing);
                    ImGui::Image(titleBarIcon, ImVec2(24, 24));
                    ImGui::SetCursorPosY(prevCursorY);
                }

                menuButtonsExtent = 24;

                menuBarSize = ImGui::GetWindowSize();
                menubar.draw();
                ImGui::EndMainMenuBar();
            }

            static EditorStartScreen startScreen{};
            startScreen.draw(this);

            if (EngineArguments::hasArgument("load-project"))
            {
                openProject(EngineArguments::argumentValue("load-project"));
            }
            return;
        }

        handleOverriden = false;

        updateWindowTitle();

        // Create global dock space
        ImGuiViewport* viewport = ImGui::GetMainViewport();
        ImGui::SetNextWindowPos(viewport->Pos);
        ImGui::SetNextWindowSize(viewport->Size);
        ImGui::SetNextWindowViewport(viewport->ID);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowRounding, 0.0f);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowBorderSize, 0.0f);
        ImGui::PushStyleVar(ImGuiStyleVar_WindowPadding, ImVec2(0.0f, 0.0f));
        ImGui::Begin("EditorDockspaceWindow", 0,
                     ImGuiWindowFlags_NoTitleBar | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize |
                     ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoBringToFrontOnFocus |
                     ImGuiWindowFlags_NoNavFocus | ImGuiWindowFlags_NoDocking | ImGuiWindowFlags_MenuBar);
        ImGui::PopStyleVar(3);
        ImGuiID dockspaceId = ImGui::GetID("EditorDockspace");
        ImGui::DockSpace(dockspaceId, ImVec2(0, 0));
        ImGui::End();

        // Draw black background
        ImGui::GetBackgroundDrawList()->AddRectFilled(viewport->Pos, viewport->Size, ImColor(0.0f, 0.0f, 0.0f, 1.0f));

        if (world.registry.valid(currentSelectedEntity))
        {
            // Right mouse button means that the view's being moved, so we'll ignore any tools
            // and assume the user's trying to move the camera
            if (!g_input->mouseButtonHeld(MouseButton::Right, true))
            {
                if (g_input->keyPressed(SDL_SCANCODE_G))
                {
                    activateTool(Tool::Translate);
                }
                else if (g_input->keyPressed(SDL_SCANCODE_R))
                {
                    activateTool(Tool::Rotate);
                }
                else if (g_input->keyPressed(SDL_SCANCODE_S))
                {
                    activateTool(Tool::Scale);
                }
                else if (g_input->keyPressed(SDL_SCANCODE_B))
                {
                    activateTool(Tool::Bounds);
                }
            }
        }

        if (!gameViewOnly)
        {
            if (ImGui::Begin(ICON_FA_EDIT u8" Editor"))
            {
                ImGui::Text("Modification Count: %u", undo.modificationCount());
                if (ImGui::Button(ICON_FA_UNDO u8" Undo"))
                {
                    undo.undo(world.registry);
                }

                ImGui::SameLine();

                if (ImGui::Button(ICON_FA_REDO u8" Redo"))
                {
                    undo.redo(world.registry);
                }

                ImGui::Text("Current tool: %s", toolStr(currentTool));

                ImGui::Checkbox("Manipulate in local space", &toolLocalSpace);

                ImGui::Checkbox("Global object snap", &settings.objectSnapGlobal);
                tooltipHover("If this is checked, moving an object with Ctrl held will snap in increments relative to the "
                    "world rather than the object's original position.");

                if (ImGui::CollapsingHeader("Physics Simulation"))
                {
                    if (ImGui::Checkbox("Pause physics", &g_engine->pauseSim))
                    {
                        if (!g_engine->pauseSim)
                        {
                            world.registry.view<RigidBody>().each([&](entt::entity e, RigidBody& rb)
							{
								HierarchyUtil::forceRecomputeTransform(world.registry, e);
								rb.setPose(world.registry.get<Transform>(e));
							});
                        }
                    }

                    if (ImGui::Button("Disable rigidbodies"))
                    {
                        world.registry.view<RigidBody>().each([](RigidBody& rb)
                            {
                                rb.actor->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, true);
                            });
                    }

                    ImGui::SameLine();

                    if (ImGui::Button("Enable rigidbodies"))
                    {
                        world.registry.view<RigidBody>().each([&](entt::entity e, RigidBody& rb)
                            {
								HierarchyUtil::forceRecomputeTransform(world.registry, e);
								rb.setPose(world.registry.get<Transform>(e));
                                rb.actor->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, true);
                            });
                    }

                    if (ImGui::Button("Enable Selected"))
                    {
                        auto enableRigidbody = [&](entt::entity ent)
                        {
                            if (!world.registry.valid(ent)) return;

                            RigidBody* rb = world.registry.try_get<RigidBody>(ent);
                            HierarchyUtil::forceRecomputeTransform(world.registry, ent);
                            if (rb)
                            {
                                rb->setPose(world.registry.get<Transform>(ent));
                                rb->actor->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, false);
                                rb->actor->wakeUp();
                            }
                        };

                        enableRigidbody(getSelectedEntity());
                        for (entt::entity ent : getSelectedEntities())
                        {
                            enableRigidbody(ent);
                        }
                    }

                    ImGui::SameLine();

                    if (ImGui::Button("Disable Selected"))
                    {
                        auto disableRigidbody = [&](entt::entity ent)
                        {
                            if (!world.registry.valid(ent)) return;

                            RigidBody* rb = world.registry.try_get<RigidBody>(ent);
                            if (rb)
                            {
                                rb->actor->setRigidBodyFlag(physx::PxRigidBodyFlag::eKINEMATIC, true);
                            }
                        };

                        disableRigidbody(getSelectedEntity());
                        for (entt::entity ent : getSelectedEntities())
                        {
                            disableRigidbody(ent);
                        }
                    }
                }

                float offsetWidth = ImGui::CalcTextSize("Angular snap increment").x + ImGui::GetStyle().WindowPadding.x;
                ImGui::PushItemWidth(ImGui::GetWindowContentRegionWidth() - offsetWidth);
                ImGui::InputFloat("Snap increment", &settings.snapIncrement, 0.1f, 0.5f);
                ImGui::InputFloat("Angular snap increment", &settings.angularSnapIncrement, 0.5f, 1.0f);
                ImGui::InputFloat("Camera speed", &cameraSpeed, 0.1f);
                ImGui::InputFloat("Camera vertical FOV", &settings.cameraVerticalFov, 0.5f, 1.0f);

                ImGui::InputFloat("Scene icon distance", &settings.sceneIconDrawDistance);
                ImGui::PopItemWidth();

                ConVar* useQuestVar = g_console->getConVar("r_usequestpipeline");
                if (useQuestVar)
                {
                    bool questEnabled = useQuestVar->getInt();
                    if (ImGui::Checkbox("Use Quest pipeline", &questEnabled))
                    {
                        useQuestVar->setValue(questEnabled ? "1" : "0");

                        for (EditorSceneView* esv : sceneViews)
                        {
                            esv->recreateRTT();
                        }
                    }
                }

                bool muteMusic = AudioSystem::getInstance()->getChannelVolume(VolumeChannel::Music) == 0.0f;

                if (ImGui::Checkbox("Mute Music", &muteMusic))
                {
                    float vol = muteMusic ? 0.0f : 1.0f;
                    AudioSystem::getInstance()->setChannelVolume(VolumeChannel::Music, vol);
                }

                if (g_vrInterface != nullptr && ImGui::Button("Toggle VR"))
                {
                    if (g_vrInterface->isInitialized())
                    {
                        g_vrInterface->deinit();
                    }
                    else
                    {
                        if (!g_vrInterface->init())
                            addNotification("VR initialization failed", NotificationType::Error);
                    }
                }
            }
            ImGui::End();
        }


        for (auto& edWindow : editorWindows)
        {
            if (gameViewOnly && strcmp(edWindow->getName(), "Game View") != 0) continue;
            if (edWindow->isActive())
            {
                edWindow->draw(world.registry);
            }
        }

        if (!gameViewOnly)
        {
            for (AssetEditorWindow* ae : assetEditors)
            {
                ae->draw(world.registry);
            }
        }

        saveFileModal("Save Scene", [this](const char* path)
        {
            AssetID sceneId = AssetDB::createAsset(path);
            JsonSceneSerializer::saveScene(sceneId, world.registry);
            lastSaveModificationCount = undo.modificationCount();
            g_engine->loadScene(sceneId);
        });

        const char* sceneFileExts[] = {".wscn"};

        openFileModalOffset(
            "Open Scene",
            [this](const char* path)
            {
                AssetID sceneId = AssetDB::pathToId(path);
                g_engine->loadScene(sceneId);
                openedScene = sceneId;
                updateWindowTitle();
                undo.clear();
            },
            "SourceData/", sceneFileExts, 1);

        std::string popupToOpen;

        if (ImGui::BeginMainMenuBar())
        {
            if (g_console->getConVar("ed_integratedmenubar")->getInt())
            {
                float menuBarHeight = ImGui::GetWindowHeight();
                float spacing = (menuBarHeight - 24) / 2.0f;
                float prevCursorY = ImGui::GetCursorPosY();
                ImGui::SetCursorPosY(prevCursorY + spacing);
                ImGui::Image(titleBarIcon, ImVec2(24, 24));
                ImGui::SetCursorPosY(prevCursorY);
            }

            if (ImGui::BeginMenu("File"))
            {
                for (auto& window : editorWindows)
                {
                    if (window->menuSection() == EditorMenu::File)
                    {
                        if (ImGui::MenuItem(window->getName()))
                        {
                            window->setActive(!window->isActive());
                        }
                    }
                }

                if (ImGui::MenuItem("New", "Ctrl+N"))
                {
                    EditorActions::findAction("scene.new").function(this, world);
                }

                if (ImGui::MenuItem("Open", "Ctrl+O"))
                {
                    EditorActions::findAction("scene.open").function(this, world);
                }

                if (ImGui::MenuItem("Save", "Ctrl+S"))
                {
                    EditorActions::findAction("scene.save").function(this, world);
                }

                ImGui::Separator();

                if (ImGui::MenuItem("Close Project"))
                {
                    project->unmountPaths();
                    project.reset();

                    world.clear();
                    world.registry.set<SceneInfo>("Untitled", INVALID_ASSET);
                    openedScene = ~0u;
                    // interfaces.renderer->reloadContent(worlds::ReloadFlags::All);

                    if (dotnetWatchProcess)
                    {
                        dotnetWatchProcess->kill();
                        delete dotnetWatchProcess;
                    }

                    if (gameProjectClosedCallback == nullptr)
                    {
                        g_scriptEngine->createManagedDelegate(
                            "WorldsEngine.Editor.Editor", "OnGameProjectClosed", (void**)&gameProjectClosedCallback);
                    }

                    gameProjectClosedCallback();
                }

                if (ImGui::MenuItem("Quit"))
                {
                    g_engine->quit();
                }
                ImGui::EndMenu();
            }

            if (ImGui::BeginMenu("Edit"))
            {
                if (ImGui::MenuItem("Undo", "Ctrl+Z"))
                {
                    undo.undo(world.registry);
                }

                if (ImGui::MenuItem("Redo", "Ctrl+Shift+Z"))
                {
                    undo.redo(world.registry);
                }

                ImGui::Separator();

                for (auto& window : editorWindows)
                {
                    if (window->menuSection() == EditorMenu::Edit)
                    {
                        if (ImGui::MenuItem(window->getName()))
                        {
                            window->setActive(!window->isActive());
                        }
                    }
                }

                ImGui::Separator();

                if (ImGui::MenuItem("New Scene View"))
                {
                    sceneViews.add(new EditorSceneView{this});
                }

                if (ImGui::MenuItem("Create Prefab", nullptr, false, world.registry.valid(currentSelectedEntity)))
                {
                    popupToOpen = "Save Prefab";
                }

#ifdef _WIN32
                if (ImGui::MenuItem("Open C# Project"))
                {
                    std::string projectPath;
                    projectPath.assign(project->root());
                    projectPath += "/Code/Game.csproj";
                    slib::Win32Util::shellOpen(projectPath.c_str());
                }
#endif

                if (ImGui::MenuItem("Pause Asset Watcher", nullptr, &project->assets().pauseWatcher, true))
                {
                }

                ImGui::EndMenu();
            }

            if (ImGui::BeginMenu("Window"))
            {
                for (auto& window : editorWindows)
                {
                    if (window->menuSection() == EditorMenu::Window)
                    {
                        if (ImGui::MenuItem(window->getName()))
                        {
                            window->setActive(!window->isActive());
                        }
                    }
                }

                ImGui::EndMenu();
            }

            if (ImGui::BeginMenu("Help"))
            {
                for (auto& window : editorWindows)
                {
                    if (window->menuSection() == EditorMenu::Help)
                    {
                        if (ImGui::MenuItem(window->getName()))
                        {
                            window->setActive(!window->isActive());
                        }
                    }
                }

                ImGui::EndMenu();
            }

            if (project && project->assetCompiler().isCompiling())
            {
                AssetCompileOperation* currentOp = project->assetCompiler().currentOperation();
                if (currentOp)
                {
                    std::filesystem::path filename = std::filesystem::path(AssetDB::idToPath(currentOp->outputId)).
                        filename();
                    StackStringBuilder<512> sb;
                    sb.append("Compiling ");
                    sb.append(filename.string().c_str());
                    widgets::spinner(sb.get());
                }
                else
                {
                    widgets::spinner("Compiling");
                }
            }

            menuButtonsExtent = (int)ImGui::GetCursorPosX();

            menubar.draw();

            ImGui::EndMainMenuBar();
        }

        saveFileModal("Save Prefab", [&](const char* path)
        {
            slib::List<entt::entity> prefabEntities = HierarchyUtil::getAllChildEntities(world.registry, currentSelectedEntity);
            prefabEntities.add(currentSelectedEntity);
            
            if (selectedEntities.numElements() > 0)
            {
                addNotification("Prefabs can only contain one entity and its children!", NotificationType::Warning);
            }
            
            std::string json = JsonSceneSerializer::entitiesToJson(world.registry, prefabEntities.data(), prefabEntities.numElements());
            
            PHYSFS_File* file = PHYSFS_openWrite(path);
            PHYSFS_writeBytes(file, json.data(), json.size());
            PHYSFS_close(file);
            
            world.registry.emplace<PrefabInstanceComponent>(currentSelectedEntity, AssetDB::pathToId(path));
        });

        g_scriptEngine->onEditorUpdate(deltaTime, gameViewOnly);

        if (!gameViewOnly)
        {
            int sceneViewIndex = 0;
            for (EditorSceneView* sceneView : sceneViews)
            {
                sceneView->drawWindow(sceneViewIndex++);
            }
        }

        actionSearch.draw();
        assetSearch.draw();

        if (!popupToOpen.empty())
            ImGui::OpenPopup(popupToOpen.c_str());

        if (imguiMetricsOpen)
            ImGui::ShowMetricsWindow(&imguiMetricsOpen);

        drawModals();
        drawPopupNotifications();
        updateWindowTitle();

        for (QueuedKeydown& qd : queuedKeydowns)
        {
            EditorActions::triggerBoundActions(this, world, qd.scancode, qd.modifiers);
        }
        queuedKeydowns.clear();
        EditorActions::reenable();

        if (project)
        {
            if (project->assets().recompileFlag)
            {
                project->assetCompiler().startCompiling();
                project->assets().recompileFlag = false;
            }
            project->assetCompiler().updateCompilation();
            
            if (ed_runDotNetWatch)
            {
                if (dotnetWatchProcess == nullptr)
                    dotnetWatchProcess =
                        new slib::Subprocess("dotnet watch build --no-restore",
                                             (std::string(project->root()) + "/Code").c_str());
            }
            else if (dotnetWatchProcess != nullptr)
            {
                dotnetWatchProcess->kill();
                delete dotnetWatchProcess;
                dotnetWatchProcess = nullptr;
            }
        }

        entityEyedropperActive = false;
        handleOverrideEntity = entt::null;
    }

    void Editor::sceneLoadCallback(void* ctx, entt::registry& reg)
    {
        Editor* _this = (Editor*)ctx;
        _this->select(entt::null);

        AssetID scene = reg.ctx<SceneInfo>().id;
        _this->project->setLastScenePath(AssetDB::idToPath(scene));
        _this->project->saveUserSettingsJson();

        if (_this->sceneViews.numElements() > 0)
        {
            worlds::Camera& camera = _this->sceneViews[0]->getCamera();
            _this->project->getCameraPoseForScene(scene, camera.position, camera.rotation);
            _this->sceneViews[0]->updateCameraRotation();
        }
    }
}

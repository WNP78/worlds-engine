﻿#include <Editor/Editor.hpp>
#define IMGUI_DEFINE_MATH_OPERATORS
#include <Core/Log.hpp>
#include <ImGui/imgui_internal.h>
#include <Render/DebugLines.hpp>

#include "Core/WorldComponents.hpp"

namespace worlds
{
    struct ToolHandleContext
    {
        Transform& transform;
        const Camera& cam;
        ImVec2 screenSize;
        ImVec2 screenOffset;
        bool isLocal;
        Editor* editor;
        entt::registry& reg;
        float snap;
        bool snapGlobal;

        // find distance to point along camera's Z axis
        // useful for scaling something to be constantish size in screen space
        float getCamZDistance(glm::vec3 pos) const
        {
            glm::vec3 cameraZ = cam.rotation * glm::vec3(0.0f, 0.0f, 1.0f);
            glm::vec3 cameraDir = pos - cam.position;
            return glm::dot(cameraDir, cameraZ);
        }

        glm::vec3 getCameraRay(glm::vec2 mousePos) const
        {
            glm::vec2 halfWindowSize = screenSize * 0.5f;
            
            glm::vec2 ndcMousePos = mousePos;
            ndcMousePos -= halfWindowSize;
            ndcMousePos.x /= halfWindowSize.x;
            ndcMousePos.y /= halfWindowSize.y;
            ndcMousePos *= -1.0f;

            float aspect = screenSize.x / screenSize.y;
            float tanHalfFov = glm::tan(cam.verticalFOV * 0.5f);
            glm::vec3 dir(ndcMousePos.x * aspect * tanHalfFov, ndcMousePos.y * tanHalfFov, 1.0f);
            dir = glm::normalize(dir);
            return cam.rotation * dir;
        }
        
        glm::vec2 projectToScreen(glm::vec3 position, bool* isBehind = nullptr) const
        {
            // Convert position from world space to screen space
            glm::mat4 vp = cam.getProjectionMatrix(screenSize.x / screenSize.y) * cam.getViewMatrix();
            glm::vec4 ndcObjPosPreDivide = vp * glm::vec4(position, 1.0f);
     
            // NDC -> screen space
            glm::vec2 ndcObjectPosition(ndcObjPosPreDivide);
            ndcObjectPosition /= ndcObjPosPreDivide.w;
            ndcObjectPosition *= 0.5f;
            ndcObjectPosition += 0.5f;
            ndcObjectPosition *= glm::vec2(screenSize);
            // Not sure why flipping Y is necessary?
            ndcObjectPosition.y = screenSize.y - ndcObjectPosition.y;

            if (isBehind)
            {
                *isBehind = (ndcObjPosPreDivide.z / ndcObjPosPreDivide.w) < 0.0f;
            }
     
            return ndcObjectPosition;
        }
    };

    glm::vec3 filterAxes(glm::vec3 vec, AxisFlagBits axisFlags)
    {
        return vec * glm::vec3(
            (axisFlags & AxisFlagBits::X) == AxisFlagBits::X,
            (axisFlags & AxisFlagBits::Y) == AxisFlagBits::Y,
            (axisFlags & AxisFlagBits::Z) == AxisFlagBits::Z);
    }

    uint32_t setColorAlpha(uint32_t color, uint8_t newAlpha)
    {
        uint32_t colorNoAlpha = color & 0x00FFFFFF;
        uint32_t alphaPart = ((uint32_t)newAlpha) << 24;

        return colorNoAlpha | alphaPart;
    }

    struct Plane
    {
        glm::vec3 normal;
        float d;

        Plane() = default;
        Plane(glm::vec3 normal, glm::vec3 point)
            : normal(normal)
        {
            d = -glm::dot(point, normal);
        }

        glm::vec3 intersectRay(glm::vec3 origin, glm::vec3 dir)
        {
            float t = -(glm::dot(origin, normal) + d) / glm::dot(dir, normal);

            return origin + dir * t;
        }
    };

    const uint32_t axisColors[] = { 0xFF4242FF, 0xFF4BFF28, 0xFFFF6600 };
    const glm::vec3 axes[] = {
        glm::vec3(1.0f, 0.0f, 0.0f),
        glm::vec3(0.0f, 1.0f, 0.0f),
        glm::vec3(0.0f, 0.0f, 1.0f)
    };
    const AxisFlagBits axisFlags[] = {
        AxisFlagBits::X,
        AxisFlagBits::Y,
        AxisFlagBits::Z
    };
    const uint32_t hoveredAxisColor = 0xFFFF00BB;

    class TranslationGizmo
    {
        const ToolHandleContext* thc = nullptr;
        bool isDragging = false;
        int dragAxis = -1;
        bool draggingPlane = false;
        glm::vec3 initialTransformPos{};
        bool hasSetProjectedPos = false;
        glm::vec3 initialProjectedPos{};

        glm::vec3 snappedTranslate(glm::vec3 origPos, glm::vec3 translation) const
        {
            if (thc->snap > 0.0f && !thc->snapGlobal)
            {
                if (thc->isLocal)
                {
                    translation = thc->transform.rotation * translation;
                }
                
                translation /= thc->snap;
                translation = glm::round(translation);
                translation *= thc->snap;

                if (thc->isLocal)
                {
                    translation = glm::inverse(thc->transform.rotation) * translation;
                }
            }

            glm::vec3 newPos = origPos + translation;
            
            if (thc->snap > 0.0f && thc->snapGlobal)
            {
                newPos /= thc->snap;
                newPos = glm::round(translation);
                newPos *= thc->snap;
            }

            return newPos;
        }

        glm::vec3 getWorldSpaceAxis(int axis) const
        {
            return thc->isLocal ? thc->transform.rotation * axes[axis] : axes[axis];
        }
        
        bool calculateAxisLinePoints(int axis, glm::vec3 pos, glm::vec2& p0, glm::vec2& p1, float axisLen = 0.15f, bool scale = true) const
        {
            assert(0 <= axis && axis < 3);
            bool p0Behind;
            p0 = thc->projectToScreen( pos, &p0Behind);
            
            glm::vec3 axisVec = axes[axis] * (scale ? thc->getCamZDistance(pos) : 1.0f) * axisLen;
            
            if (thc->isLocal)
            {
                axisVec = thc->transform.rotation * axisVec;
            }

            bool p1Behind;
            p1 = thc->projectToScreen(pos + axisVec, &p1Behind);

            return !p0Behind && !p1Behind;
        }

        bool calculateAxisLinePoints(int axis, glm::vec2& p0, glm::vec2& p1) const
        {
            return calculateAxisLinePoints(axis, thc->transform.position, p0, p1);
        }

        void drawTranslationAxis(glm::vec2 p0, glm::vec2 p1, uint32_t color) const
        {
            glm::vec2 screenDir = glm::normalize(p1 - p0);
            glm::vec2 perpendicularDir(screenDir.y, -screenDir.x);

            p0 += glm::vec2(thc->screenOffset);
            p1 += glm::vec2(thc->screenOffset);

            ImGui::GetWindowDrawList()->AddLine(p0, p1, color, 3.0f);

            const float arrowHeight = 15.0f;
            const float arrowWidth = 7.0f;
            ImGui::GetWindowDrawList()->AddTriangleFilled(
                p1 + perpendicularDir * arrowWidth,
                p1 - perpendicularDir * arrowWidth,
                p1 + screenDir * arrowHeight,
                color);
        }

        bool isMouseOverAxis(glm::vec2 p0, glm::vec2 p1, glm::vec2 mousePos) const
        {
            glm::vec2 dir = p1 - p0;
            glm::vec2 relativeMousePos = mousePos - p0;
            float t = glm::clamp(glm::dot(relativeMousePos, dir) / glm::length2(dir), 0.0f, 1.0f);

            glm::vec2 projected = p0 + dir * t;

            return glm::distance(projected, mousePos) < 7.0f;
        }

        [[nodiscard]]
        bool isMouseOverAxis(int axisIndex, glm::vec2 mousePos) const
        {
            glm::vec2 p0, p1;
            if (!calculateAxisLinePoints(axisIndex, p0, p1)) return false;
            return isMouseOverAxis(p0, p1, mousePos);
        }

        void handleAxisDragging(glm::vec2 mousePos, glm::vec3 initialPos)
        {
            glm::vec3 dir = thc->getCameraRay(mousePos);

            // find the normal of a plane we can drag on
            glm::vec3 n = dragAxis == 1 ? glm::vec3(1.0f, 0.0f, 0.0f) : glm::vec3(0.0f, 1.0f, 0.0f);
            if (thc->isLocal)
                n = thc->transform.rotation * n;

            // position the plane at the object's initial position and intersect camera ray against it
            Plane p{n, initialPos};
            glm::vec3 projectedOnPlane = p.intersectRay(thc->cam.position, dir);

            // now project onto the axis
            glm::vec3 relPos = projectedOnPlane - initialPos;
            glm::vec3 axis = getWorldSpaceAxis(dragAxis);
            float axisT = glm::dot(relPos, axis) / glm::length2(axis);
            glm::vec3 translation = axis * axisT;
            
            if (!hasSetProjectedPos)
            {
                initialProjectedPos = translation;
                hasSetProjectedPos = true;
            }
            translation -= initialProjectedPos;

            thc->transform.position = snappedTranslate(initialPos, translation);
        }

        // https://wrfranklin.org/Research/Short_Notes/pnpoly.html
        bool hoversPoly(glm::vec2 mousePos, const ImVec2* polyPoints, int numPolyPoints) const
        {
            bool oddCrossingCount = false;
            
            int j = numPolyPoints - 1;
            for (int i = 0; i < numPolyPoints; i++)
            {
                ImVec2 iP = polyPoints[i];
                ImVec2 jP = polyPoints[j];

                if ((iP.y > mousePos.y) != (jP.y > mousePos.y))
                {
                    float xIntersection = (jP.x - iP.x) * (mousePos.y - iP.y ) / (jP.y - iP.y) + iP.x;

                    if (mousePos.x < xIntersection)
                        oddCrossingCount = !oddCrossingCount;
                }

                j = i;
            }

            return oddCrossingCount;
        }

        void calculateTranslationPlaneScreenVerts(ImVec2 screenSpaceVerts[4]) const
        {
            // draw XZ plane
            const float planeScale = 0.08f * thc->getCamZDistance(thc->transform.position);
            const glm::vec3 planeVerts[]
            {
                thc->transform.position + glm::vec3(0.0f, 0.0f, planeScale),
                thc->transform.position + glm::vec3(planeScale, 0.0f, planeScale),
                thc->transform.position + glm::vec3(planeScale, 0.0f, 0.0f),
                thc->transform.position
            };

            for (int i = 0; i < 4; i++)
            {
                screenSpaceVerts[i] = thc->projectToScreen(planeVerts[i]);
            }
        }

        void drawTranslationPlane(ImVec2 verts[4], uint32_t color) const
        {
            ImVec2 screenSpaceVerts[4];

            for (int i = 0; i < 4; i++)
                screenSpaceVerts[i] = verts[i] + thc->screenOffset;

            ImGui::GetWindowDrawList()->AddPolyline(screenSpaceVerts, 4, color, ImDrawFlags_None, 2.0f);
            uint32_t transparentColor = 0x33000000 | (color & 0x00FFFFFF);
            ImGui::GetWindowDrawList()->AddConvexPolyFilled(screenSpaceVerts, 4, transparentColor);
        }
    public:
        void setContext(const ToolHandleContext* thc)
        {
            this->thc = thc;
        }

        [[nodiscard]]
        bool isHovered() const
        {
            if (isDragging) return true;
            
            glm::vec2 mousePos = ImGui::GetMousePos() - thc->screenOffset;
            for (int axis = 0; axis < 3; axis++)
            {
                bool hovered = isMouseOverAxis(axis, mousePos);

                if (hovered) return true;
            }

            return false;
        }
        
        void update()
        {
            glm::vec2 mousePos = ImGui::GetMousePos() - thc->screenOffset;
            if (!isDragging)
            {
                int hoveredAxis = -1;
            
                for (int axis = 0; axis < 3; axis++)
                {
                    glm::vec2 p0, p1;
                    if (!calculateAxisLinePoints(axis, p0, p1)) continue;
                
                    bool isHovered = isMouseOverAxis(p0, p1, mousePos);
                    uint32_t color = isHovered ? hoveredAxisColor : axisColors[axis];
                
                    drawTranslationAxis(p0, p1, color);

                    if (isHovered)
                    {
                        hoveredAxis = axis;
                    }
                }

                ImVec2 planeVerts[4];
                calculateTranslationPlaneScreenVerts(planeVerts);
                bool hoversPlane = hoversPoly(mousePos, planeVerts, 4);
                drawTranslationPlane(planeVerts, hoversPlane ? 0xFFFFFFFF : 0xFF0000FF);

                if (hoversPlane && ImGui::IsMouseClicked(ImGuiMouseButton_Left))
                {
                    isDragging = true;
                    draggingPlane = true;
                    initialTransformPos = thc->transform.position;
                    thc->editor->undo.pushState(thc->reg);
                }
                
                if (hoveredAxis != -1 && ImGui::IsMouseClicked(ImGuiMouseButton_Left))
                {
                    isDragging = true;
                    dragAxis = hoveredAxis;
                    initialTransformPos = thc->transform.position;
                    thc->editor->undo.pushState(thc->reg);
                    hasSetProjectedPos = false;
                }
            }
            else if (ImGui::IsMouseDown(ImGuiMouseButton_Left))
            {
                if (draggingPlane)
                {
                    glm::vec3 dir = thc->getCameraRay(mousePos);

                    // find the normal of a plane we can drag on
                    glm::vec3 n = glm::vec3(0.0f, 1.0f, 0.0f);
                    if (thc->isLocal)
                        n = thc->transform.rotation * n;

                    // position the plane at the object's initial position and intersect camera ray against it
                    Plane p{n, initialTransformPos};
                    glm::vec3 projectedOnPlane = p.intersectRay(thc->cam.position, dir);

                    // now project onto the axis
                    glm::vec3 relPos = projectedOnPlane - initialTransformPos;
                    thc->transform.position = snappedTranslate(initialTransformPos, relPos);
                }
                else
                {
                    handleAxisDragging(mousePos, initialTransformPos);
                    // draw the dragged axis
                    glm::vec2 p0, p1;
                    calculateAxisLinePoints(dragAxis, p0, p1);
                
                    drawTranslationAxis(p0, p1, axisColors[dragAxis]);
                }
            }
            else
            {
                isDragging = false;
                draggingPlane = false;
            }
        }
    };

    class RotationGizmo
    {
        const ToolHandleContext* thc = nullptr;
        bool isDragging = false;
        int dragAxis = -1;
        glm::quat initialRotation = glm::identity<glm::quat>();
        float initialAngle = 0.0f;

        void getAxisTangents(int axisIndex, glm::vec3& tangent, glm::vec3& bitangent) const
        {
            assert(0 <= axisIndex && axisIndex < 3);
            switch (axisIndex)
            {
            case 0: // X axis
                tangent = glm::vec3(0.0f, 1.0f, 0.0f);
                bitangent = glm::vec3(0.0f, 0.0f, 1.0f);
                break;
            case 1: // Y axis
                tangent = glm::vec3(0.0f, 0.0f, 1.0f);
                bitangent = glm::vec3(1.0f, 0.0f, 0.0f);
                break;
            case 2: // Z axis
                tangent = glm::vec3(1.0f,  0.0f, 0.0f);
                bitangent = glm::vec3(0.0f, 1.0f, 0.0f);
                break;
            default: assert(false);
            }

            if (thc->isLocal)
            {
                tangent = thc->transform.rotation * tangent;
                bitangent = thc->transform.rotation * bitangent;
            }
        }

        void drawAxisCircle(int axisIndex, ImU32 color) const
        {
            assert(0 <= axisIndex && axisIndex < 3);
            
            glm::vec3 tangent, bitangent;
            getAxisTangents(axisIndex, tangent, bitangent);

            float zDist = thc->getCamZDistance(thc->transform.position);
            tangent *= zDist * 0.15f;
            bitangent *= zDist * 0.15f;

            const int numSegments = 32;
            auto* drawList = ImGui::GetWindowDrawList();
            for (int i = 0; i <= numSegments; i++)
            {
                float angle = i * (glm::two_pi<float>() / (float)numSegments);
                glm::vec3 point = thc->transform.position + tangent * glm::sin(angle) + bitangent * glm::cos(angle);
                glm::vec2 screenPoint = thc->screenOffset + thc->projectToScreen(point);
                
                drawList->PathLineTo(screenPoint);
            }

            drawList->PathStroke(color, 0, 3.f);
        }
        
        void drawAxisLines(int axisIndex, ImU32 color, float angleOffset) const
        {
            assert(0 <= axisIndex && axisIndex < 3);
            
            glm::vec3 tangent, bitangent;
            getAxisTangents(axisIndex, tangent, bitangent);

            float zDist = thc->getCamZDistance(thc->transform.position);
            tangent *= zDist * 0.15f;
            bitangent *= zDist * 0.15f;

            // 15 degree lines
            const int numSegments = 24;
            auto* drawList = ImGui::GetWindowDrawList();
            for (int i = 0; i <= numSegments; i++)
            {
                float angle = i * (glm::two_pi<float>() / (float)numSegments) + angleOffset;
                
                glm::vec3 offset = tangent * glm::sin(angle) + bitangent * glm::cos(angle);
                glm::vec3 point = thc->transform.position + offset;
                glm::vec3 pointInner = thc->transform.position + offset * 0.75f;
                glm::vec2 screenPoint = thc->screenOffset + thc->projectToScreen(point);
                glm::vec2 screenPointInner = thc->screenOffset + thc->projectToScreen(pointInner);

                uint32_t lineColor;

                // make 90 degree lines more opaque
                float thickness = 2.0f;
                if (i == 0)
                {
                    thickness = 3.0f;
                    lineColor = 0xFFFFFFFF;
                }
                else if (i % 4 == 0)
                {
                    lineColor = setColorAlpha(color, 150);
                    thickness = 3.0f;
                }
                else
                {
                    lineColor = setColorAlpha(color, 60);
                }

                drawList->AddLine(screenPoint, screenPointInner, lineColor, thickness);
            }
        }

        [[nodiscard]]
        glm::vec3 projectMouseToAxisCircle(int axisIndex, glm::vec2 mousePos) const
        {
            // calculate world space size of circle to keep constant view space size
            float circleSize = 0.15f * thc->getCamZDistance(thc->transform.position);

            glm::vec3 axis = thc->isLocal ? thc->transform.rotation * axes[axisIndex] : axes[axisIndex];

            // project onto plane defined by axis, then project that onto circle around transform
            Plane p{axis, thc->transform.position};
            glm::vec3 onPlane = p.intersectRay(thc->cam.position, thc->getCameraRay(mousePos));
            glm::vec3 dirToPlane = glm::normalize(onPlane - thc->transform.position);
            return thc->transform.position + (dirToPlane * circleSize);
        }

        [[nodiscard]]
        bool isMouseOverAxis(int axisIndex, glm::vec2 mousePos) const
        {
            glm::vec2 screenPos = thc->projectToScreen( projectMouseToAxisCircle(axisIndex, mousePos));

            // debug visualisation
            // ImGui::GetWindowDrawList()->AddCircleFilled(
            //     screenPos + thc.screenOffset, 15.0f, axisColors[axisIndex]);

            return glm::distance(screenPos, mousePos) < 6.0f;
        }

        [[nodiscard]]
        float getMouseAngleAroundAxis(int axisIndex, glm::vec2 mousePos) const
        {
            glm::vec3 circlePos = projectMouseToAxisCircle(axisIndex, mousePos) - thc->transform.position;

            // now figure out x+y coordinates on the circle
            glm::vec2 circleCoords;
            glm::vec3 tangent, bitangent;
            getAxisTangents(axisIndex, tangent, bitangent);

            circleCoords.x = glm::dot(circlePos, tangent);
            circleCoords.y = glm::dot(circlePos, bitangent);

            return atan2f(circleCoords.y, circleCoords.x);
        }
    public:
        void setContext(const ToolHandleContext* thc)
        {
            this->thc = thc;
        }

        [[nodiscard]]
        bool isHovered() const
        {
            if (isDragging) return true;
            
            glm::vec2 mousePos = ImGui::GetMousePos() - thc->screenOffset;
            for (int axis = 0; axis < 3; axis++)
            {
                if (isMouseOverAxis(axis, mousePos)) return true;
            }

            return false;
        }

        void update()
        {
            glm::vec2 mousePos = ImGui::GetMousePos() - thc->screenOffset;
            if (!isDragging)
            {
                int hoveredAxis = -1;
                for (int axis = 0; axis < 3; axis++)
                {
                    bool hovered = isMouseOverAxis(axis, mousePos);
                    drawAxisCircle(axis, hovered ? hoveredAxisColor : axisColors[axis]);

                    if (hovered)
                    {
                        hoveredAxis = axis;
                    }
                }
                
                if (hoveredAxis != -1 && ImGui::IsMouseClicked(ImGuiMouseButton_Left))
                {
                    isDragging = true;
                    dragAxis = hoveredAxis;
                    initialRotation = thc->transform.rotation;
                    thc->editor->undo.pushState(thc->reg);
                    initialAngle = getMouseAngleAroundAxis(hoveredAxis, mousePos);
                }
            }
            else if (ImGui::IsMouseDown(ImGuiMouseButton_Left))
            {
                thc->transform.rotation = initialRotation;
                drawAxisCircle(dragAxis, axisColors[dragAxis]);
                drawAxisLines(dragAxis, axisColors[dragAxis], initialAngle);
                
                float mouseAngle = getMouseAngleAroundAxis(dragAxis, mousePos);
                float angle = mouseAngle - initialAngle;

                if (thc->snap != 0.0f)
                {
                    float angleDeg = glm::degrees(angle);
                    angle = glm::radians(glm::round(angleDeg / thc->snap) * thc->snap);
                }
                
                glm::vec3 axis = thc->isLocal ? initialRotation * axes[dragAxis] : axes[dragAxis];
                thc->transform.rotation = glm::angleAxis(angle, axis) * initialRotation;

                char textBuffer[32];
                snprintf(textBuffer, 32, "%.1fdeg", glm::degrees(angle));
                auto* drawList = ImGui::GetWindowDrawList();
                drawList->AddText(thc->screenOffset + mousePos + glm::vec2(15.0f, 2.0f), 0xFF000000, textBuffer);
                drawList->AddText(thc->screenOffset + mousePos + glm::vec2(13.0f, 0.0f), 0xFFFFFFFF, textBuffer);
            }
            else
            {
                isDragging = false;
            }
        }
    };
 
    void EditorSceneView::handleTools(Transform& t, ImVec2 wPos, ImVec2 wSize, Camera& camera)
    {
        ToolHandleContext thc
        {
            .transform = t,
            .cam = camera,
            .screenSize = wSize,
            .screenOffset = wPos,
            .isLocal = ed->toolLocalSpace,
            .editor = ed,
            .reg = ed->world.registry,
            .snap = 0.0f,
            .snapGlobal = ed->settings.objectSnapGlobal
        };

        // initialize gizmos if necessary
        if (translationGizmo == nullptr)
        {
            translationGizmo = new TranslationGizmo;
        }
        
        if (rotationGizmo == nullptr)
        {
            rotationGizmo = new RotationGizmo;
        }
        
        auto drawList = ImGui::GetWindowDrawList();
 
        drawList->PushClipRect(wPos, wPos + wSize);
 
        if (g_input->keyHeld(SDL_SCANCODE_LCTRL, true))
        {
            switch (ed->currentTool)
            {
            case Tool::Rotate:
                thc.snap = ed->settings.angularSnapIncrement;
                break;
            default:
                thc.snap = ed->settings.snapIncrement;
                break;
            }
        }

        Transform original = t;
        if (ed->currentTool == Tool::Translate)
        {
            translationGizmo->setContext(&thc);
            translationGizmo->update();
            gizmosHovered = translationGizmo->isHovered();
        }
        else if (ed->currentTool == Tool::Rotate)
        {
            rotationGizmo->setContext(&thc);
            rotationGizmo->update();

            gizmosHovered = rotationGizmo->isHovered();
        }
        else
        {
            gizmosHovered = false;
        }
        
        bool isBehind;
        glm::vec2 screenPos = thc.projectToScreen(thc.transform.position, &isBehind);
        if (!isBehind)
        {
            drawList->AddCircleFilled(thc.projectToScreen(t.position) + wPos, 9.0f, hoveredAxisColor);
            drawList->AddCircleFilled(thc.projectToScreen(t.position) + wPos, 5.0f, 0xFFFFFFFF);
        }

        drawList->PopClipRect();

        // Transform the other selected entities along with the active selection
        for (entt::entity ent : ed->selectedEntities)
        {
            Transform& entTransform = ed->world.registry.get<Transform>(ent);
            Transform newTransform = entTransform.transformByInverse(original).transformBy(t);
            
            ChildComponent* childComponent = ed->world.registry.try_get<ChildComponent>(ed->currentSelectedEntity);
            if (childComponent)
            {
                if (ed->world.registry.valid(childComponent->parent))
                {
                    Transform& parentTransform = ed->world.registry.get<Transform>(childComponent->parent);

                    // preserve scale!!!
                    glm::vec3 scale = childComponent->offset.scale;
                    childComponent->offset = newTransform.transformByInverse(parentTransform);
                    childComponent->offset.scale = scale;
                    ed->world.registry.replace<Transform>(ent, newTransform);
                    ed->world.registry.emplace_or_replace<LocalTransformDirty>(ed->currentSelectedEntity);
                }
            }
            
            entTransform = newTransform;
        }
    }
}

﻿#include "Export.hpp"
#include <UI/WorldSpaceUIPanel.hpp>
#include <Core/Engine.hpp>
#include <RmlUi/Core/Element.h>
#include <UI/UIPanel.hpp>

using namespace worlds;

extern "C"
{
    EXPORT bool worldSpaceUiPanel_raycastCursor(entt::registry& reg, entt::entity entity, const Transform& panelTransform, glm::vec3 rayOrigin, glm::vec3 rayDirection, float maxDistance)
    {
        releaseAssert(reg.has<WorldSpaceUIPanel>(entity));
        return reg.get<WorldSpaceUIPanel>(entity).raycastCursor(panelTransform, rayOrigin, rayDirection, maxDistance);
    }

    EXPORT void worldSpaceUiPanel_click(entt::registry& reg, entt::entity entity)
    {
        releaseAssert(reg.has<WorldSpaceUIPanel>(entity));
        reg.get<WorldSpaceUIPanel>(entity).panel->click();
    }
    
    EXPORT void worldSpaceUiPanel_mouseDown(entt::registry& reg, entt::entity entity, int button)
    {
        releaseAssert(reg.has<WorldSpaceUIPanel>(entity));
        reg.get<WorldSpaceUIPanel>(entity).panel->mouseDown(button);
    }
    
    EXPORT void worldSpaceUiPanel_mouseUp(entt::registry& reg, entt::entity entity, int button)
    {
        releaseAssert(reg.has<WorldSpaceUIPanel>(entity));
        reg.get<WorldSpaceUIPanel>(entity).panel->mouseUp(button);
    }

    EXPORT void worldSpaceUiPanel_mouseScroll(entt::registry& reg, entt::entity entity, float delta)
    {
        releaseAssert(reg.has<WorldSpaceUIPanel>(entity));
        reg.get<WorldSpaceUIPanel>(entity).panel->mouseScroll(delta);
    }
    
    EXPORT void worldSpaceUiPanel_addEventListener(entt::registry& reg, entt::entity entity, const char* elementId, const char* eventId, void (*funPtr)(void*), void* dataArg)
    {
        releaseAssert(reg.has<WorldSpaceUIPanel>(entity));
        reg.get<WorldSpaceUIPanel>(entity).panel->addEventListener(elementId, eventId, funPtr, dataArg);
    }
    
    EXPORT void worldSpaceUiPanel_setLoadListener(entt::registry& reg, entt::entity entity, void (*funPtr)(void*), void* dataArg)
    {
        releaseAssert(reg.has<WorldSpaceUIPanel>(entity));
        reg.get<WorldSpaceUIPanel>(entity).panel->setLoadListener(funPtr, dataArg);
    }

    EXPORT WorldSpaceUIPanel* worldSpaceUiPanel_getNativePtr(entt::registry& reg, entt::entity entity)
    {
        releaseAssert(reg.has<WorldSpaceUIPanel>(entity));
        return &reg.get<WorldSpaceUIPanel>(entity);
    }
    
    EXPORT void worldSpaceUiPanel_reload(entt::registry& reg, entt::entity entity)
    {
        releaseAssert(reg.has<WorldSpaceUIPanel>(entity));
        reg.get<WorldSpaceUIPanel>(entity).reload();
    }

    EXPORT Rml::Element* worldSpaceUiPanel_getElementById(entt::registry& reg, entt::entity entity, const char* id)
    {
        releaseAssert(reg.has<WorldSpaceUIPanel>(entity));
        auto* panel = reg.get<WorldSpaceUIPanel>(entity).panel;
        if (panel)
            return panel->getElementById(id);
        else
            return nullptr;
    }

    EXPORT void uiElement_setClass(Rml::Element* el, const char* elClass, bool active)
    {
        el->SetClass(elClass, active);
    }

    EXPORT bool uiElement_isClassSet(Rml::Element* el, const char* elClass)
    {
        return el->IsClassSet(elClass);
    }

    EXPORT void uiElement_setStyleProperty(Rml::Element* el, const char* attr, const char* val)
    {
        el->SetProperty(attr, val);
    }

    EXPORT void uiElement_setAttribute(Rml::Element* el, const char* attr, const char* val)
    {
        el->SetAttribute(attr, val);
    }
    
    EXPORT char* uiElement_getAttribute(Rml::Element* el, const char* attr)
    {
        return _strdup(el->GetAttribute(attr)->Get<Rml::String>().c_str());
    }
    
    EXPORT bool uiElement_hasAttribute(Rml::Element* el, const char* attr)
    {
        return el->HasAttribute(attr);
    }

    EXPORT void uiElement_setInnerRml(Rml::Element* el, const char* rml)
    {
        el->SetInnerRML(rml);
    }
}

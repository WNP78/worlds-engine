#include "Export.hpp"
#include <Serialization/ComponentDataWrapper.hpp>

extern "C"
{
#define GETAS(TYPE, NAME) \
    EXPORT TYPE NAME(simdjson::dom::element el) \
    { \
        return el.get<TYPE>().value(); \
    }
    
    EXPORT bool sj_contains(simdjson::dom::element el, const char* key)
    {
        return el[key].error() == simdjson::SUCCESS;
    }

    EXPORT simdjson::dom::element sj_get(simdjson::dom::element el, const char* key)
    {
        return el[key].value();
    }

    GETAS(int, sj_getAsInt);
    GETAS(uint32_t, sj_getAsUint);
    GETAS(float, sj_getAsFloat);
    GETAS(double, sj_getAsDouble);
    GETAS(bool, sj_getAsBool);

    EXPORT const char* sj_getAsString(simdjson::dom::element el)
    {
        return el.get_c_str().value();
    }

    EXPORT simdjson::dom::element_type sj_getType(simdjson::dom::element el)
    {
        return el.type();
    }

    EXPORT int sj_getArrayCount(simdjson::dom::element el)
    {
        return el.get_array().value().size();
    }

    EXPORT simdjson::dom::element sj_getArrayElement(simdjson::dom::element el, int idx)
    {
        return el.at(idx);
    }

    EXPORT simdjson::dom::element cdw_getElementFallback(ComponentDataWrapper cdw, const char* key)
    {
        return cdw.getElementFallback(key);
    }

    EXPORT bool cdw_contains(ComponentDataWrapper cdw, const char* key)
    {
        return cdw.contains(key);
    }

    EXPORT ComponentDataWrapper cdw_getObject(ComponentDataWrapper cdw, const char* key)
    {
        return cdw.getObject(key);
    }
}

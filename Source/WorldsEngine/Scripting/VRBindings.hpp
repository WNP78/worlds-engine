#include "Export.hpp"
#include "VR/OpenXRInterface.hpp"

using namespace worlds;

extern "C"
{
    EXPORT bool vr_enabled()
    {
        return g_vrInterface != nullptr && g_vrInterface->isInitialized();
    }

    EXPORT void vr_getHeadTransform(Transform* transform)
    {
        const UnscaledTransform& ut = g_vrInterface->getHmdTransform();
        transform->position = ut.position;
        transform->rotation = ut.rotation;
        transform->scale = glm::vec3{ 1.0f };
    }

    EXPORT uint64_t vr_getActionHandle(const char* actionSet, const char* action)
    {
        if (g_vrInterface == nullptr) return 0;
        return g_vrInterface->getActionHandle(actionSet, action);
    }

    EXPORT uint64_t vr_getSubactionHandle(const char* subaction)
    {
        if (g_vrInterface == nullptr) return 0;
        return g_vrInterface->getSubactionHandle(subaction);
    }

    EXPORT BooleanActionState vr_getBooleanActionState(uint64_t actionHandle, uint64_t subactionHandle)
    {
        return g_vrInterface->getBooleanActionState(actionHandle, subactionHandle);
    }

    EXPORT FloatActionState vr_getFloatActionState(uint64_t actionHandle, uint64_t subactionHandle)
    {
        return g_vrInterface->getFloatActionState(actionHandle, subactionHandle);
    }

    EXPORT Vector2fActionState vr_getVector2fActionState(uint64_t actionHandle, uint64_t subactionHandle)
    {
        return g_vrInterface->getVector2fActionState(actionHandle, subactionHandle);
    }

    EXPORT void vr_getPoseActionState(uint64_t actionHandle, uint64_t subactionHandle, Transform* t)
    {
        auto us = g_vrInterface->getPoseActionState(actionHandle, subactionHandle);
        t->position = us.position;
        t->rotation = us.rotation;
        t->scale = glm::vec3{ 1.0f };
    }

    EXPORT void vr_triggerHaptics(float duration, float frequency, float amplitude, uint64_t actionHandle, uint64_t subactionHandle)
    {
        g_vrInterface->applyHapticFeedback(duration, frequency, amplitude, actionHandle, subactionHandle);
    }

    EXPORT bool vr_hasInputFocus()
    {
        return g_vrInterface->hasFocus();
    }
}

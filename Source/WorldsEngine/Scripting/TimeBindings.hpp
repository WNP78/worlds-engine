﻿#include "Export.hpp"
#include <Core/Engine.hpp>
using namespace worlds;

extern "C"
{
    EXPORT double time_getScale()
    {
        return g_engine->timeScale;
    }

    EXPORT void time_setScale(double scale)
    {
        g_engine->timeScale = scale;
    }
}

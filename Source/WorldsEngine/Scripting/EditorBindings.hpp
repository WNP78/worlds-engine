#include "Editor/Editor.hpp"
#include "Editor/GuiUtil.hpp"
#include "Export.hpp"

using namespace worlds;

extern "C"
{
    EXPORT uint32_t editor_getCurrentlySelected()
    {
        return (uint32_t)g_editor->getSelectedEntity();
    }

    EXPORT void editor_select(uint32_t ent)
    {
        g_editor->select((entt::entity)ent);
    }

    EXPORT void editor_addNotification(const char* notification, NotificationType type)
    {
        addNotification(notification, type);
    }

    EXPORT void editor_overrideHandle(entt::entity entity)
    {
        g_editor->overrideHandle(entity);
    }

    EXPORT GameState editor_getCurrentState()
    {
        return g_editor->getCurrentState();
    }

    EXPORT const slib::List<entt::entity>* editor_getSelectionList()
    {
        return &g_editor->getSelectedEntities();
    }
}

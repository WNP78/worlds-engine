﻿#include <Scripting/ScriptEngine.hpp>

namespace slib
{
    class DynamicLibrary;
}

namespace worlds
{
    struct DotNetFunctionPtrs
    {
#ifndef __CORECLR_HOST_H__
        void* init;
        void* createDelegate;
        void* shutdown;
#else
        coreclr_initialize_ptr init;
        coreclr_create_delegate_ptr createDelegate;
        coreclr_shutdown_ptr shutdown;
#endif
    };
    
    class DotNetScriptEngine final : public IScriptEngine
    {
    public:
        DotNetScriptEngine(World& world);
        bool initialise(Editor* editor) override;
        void shutdown() override;
        void onSceneStart() override;
        void onUpdate(float deltaTime, float interpAlpha) override;
        void onEditorUpdate(float deltaTime, bool gameViewOnly) override;
        void onSimulate(float deltaTime) override;
        void onMidSimulate(float deltaTime) override;
        void handleCollision(entt::entity entity, PhysicsContactInfo* contactInfo) override;
        void serializeManagedComponents(nlohmann::json& entityJson, entt::entity entity) override;
        bool isManagedComponent(const char* id) override;
        void deserializeManagedComponent(const char* id, ComponentDataWrapper componentJson, entt::entity entity) override;
        void createManagedDelegate(const char* typeName, const char* methodName, void** func) override;

    private:
        World& world;
        void onTransformDestroy(entt::registry& reg, entt::entity ent);
        void* hostHandle;
        unsigned int domainId;
        DotNetFunctionPtrs netFuncs;
        void (*updateFunc)(float deltaTime, float interpAlpha);
        void (*simulateFunc)(float deltaTime);
        void (*midSimulateFunc)(float deltaTime);
        void (*editorUpdateFunc)(bool gameViewOnly);
        void (*nativeEntityDestroyFunc)(uint32_t id);
        void (*serializeComponentsFunc)(void* serializationContext, uint32_t entity);
        void (*deserializeComponentFunc)(const char* id, ComponentDataWrapper* componentJson, uint32_t entity);
        bool (*isManagedComponentFunc)(const char* id);
        void (*physicsContactFunc)(uint32_t id, PhysicsContactInfo* contactInfo);
        void (*sceneStartFunc)();
        slib::DynamicLibrary* coreclrLib;
    };
}
#pragma once
#include "Core/IGameEventHandler.hpp"
#include <entt/entity/fwd.hpp>
#include <nlohmann/json_fwd.hpp>

namespace worlds
{
    class Editor;

    struct PhysicsContactInfo;
    struct ComponentDataWrapper;

    class IScriptEngine
    {
    public:
        virtual bool initialise(Editor* editor) = 0;
        virtual void shutdown() = 0;
        virtual void onSceneStart() = 0;
        virtual void onUpdate(float deltaTime, float interpAlpha) = 0;
        virtual void onEditorUpdate(float deltaTime, bool gameViewOnly) = 0;
        virtual void onSimulate(float deltaTime) = 0;
        virtual void onMidSimulate(float deltaTime) = 0;
        virtual void handleCollision(entt::entity entity, PhysicsContactInfo* contactInfo) = 0;
        virtual void serializeManagedComponents(nlohmann::json& entityJson, entt::entity entity) = 0;
        virtual bool isManagedComponent(const char* id) = 0;
        virtual void deserializeManagedComponent(const char* id, ComponentDataWrapper componentJson, entt::entity entity) = 0;
        virtual void createManagedDelegate(const char* typeName, const char* methodName, void** func) = 0;
        virtual ~IScriptEngine() = default;
    };

}

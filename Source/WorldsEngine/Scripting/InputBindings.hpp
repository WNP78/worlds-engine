#include "Export.hpp"
#include "Input/Input.hpp"

using namespace worlds;

extern "C"
{
    EXPORT int input_getKeyHeld(int keyCode)
    {
        return g_input->getInputChannel(InputChannelKind::Game).keyHeld((SDL_Scancode)keyCode);
    }

    EXPORT int input_getKeyPressed(int keyCode)
    {
        return g_input->getInputChannel(InputChannelKind::Game).keyPressed((SDL_Scancode)keyCode);
    }

    EXPORT int input_getKeyReleased(int keyCode)
    {
        return g_input->getInputChannel(InputChannelKind::Game).keyReleased((SDL_Scancode)keyCode);
    }

    EXPORT void input_getMousePosition(glm::vec2* posOut)
    {
        *posOut = g_input->getMousePosition();
    }

    EXPORT void input_setMousePosition(glm::vec2* pos)
    {
        g_input->warpMouse(*pos);
    }

    EXPORT void input_getMouseDelta(glm::vec2* deltaOut)
    {
        *deltaOut = g_input->getInputChannel(InputChannelKind::Game).getMouseDelta();
    }

    EXPORT int input_getMouseButtonPressed(MouseButton button)
    {
        return g_input->getInputChannel(InputChannelKind::Game).mouseButtonPressed(button);
    }

    EXPORT int input_getMouseButtonReleased(MouseButton button)
    {
        return g_input->getInputChannel(InputChannelKind::Game).mouseButtonReleased(button);
    }

    EXPORT int input_getMouseButtonHeld(MouseButton button)
    {
        return g_input->getInputChannel(InputChannelKind::Game).mouseButtonHeld(button);
    }

    EXPORT void input_triggerControllerHaptics(uint16_t leftIntensity, uint16_t rightIntensity, uint32_t duration)
    {
        return g_input->triggerControllerHaptics(leftIntensity, rightIntensity, duration);
    }
}

#include "Engine.hpp"
#include <Core/Console.hpp>
#include <Physics/Physics.hpp>
#include <Scripting/ScriptEngine.hpp>
#include <robin_hood.h>
#include <tracy/Tracy.hpp>
#include <Util/TimingUtil.hpp>
#include <entt/entity/registry.hpp>

#include "HierarchyUtil.hpp"
#include "NameComponent.hpp"

namespace worlds
{
    robin_hood::unordered_map<entt::entity, physx::PxTransform> currentState;
    robin_hood::unordered_map<entt::entity, physx::PxTransform> previousState;

    ConVar pauseSimulation  { "sim_pause", "0",
                              "Pauses the simulation (physics and Simulate() methods)." };
    ConVar lockSimToRefresh { "sim_lockToRefresh", "0",
                              "Instead of using a simulation timestep, run the simulation in "
                              "lockstep with the rendering." };
    ConVar disableSimInterp { "sim_disableInterp", "0",
                              "Disables interpolation and uses the results of the last run "
                              "simulation step." };
    ConVar simStepTime      { "sim_stepTime", "0.01",
                              "Time between each simulation step in seconds (as long as "
                              "sim_lockToRefresh is 0)." };

    SimulationLoop::SimulationLoop(IGameEventHandler* evtHandler,
                                   World& world)
        : world(world)
        , evtHandler(evtHandler)
        , simAccumulator(0.0)
    {
    }

    void SimulationLoop::doSimStep(float deltaTime, bool physicsOnly)
    {
        ZoneScoped;
        
        g_physics->collide(deltaTime);
        if (g_scriptEngine)
            g_scriptEngine->onMidSimulate(deltaTime);
        g_physics->advance();
        
        if (!physicsOnly)
        {
            evtHandler->simulate(world.registry, deltaTime);
            if (g_scriptEngine)
                g_scriptEngine->onSimulate(deltaTime);
            
            world.registry.view<LocalTransformDirty, ParentComponent>().each(
                [&](entt::entity e, const ParentComponent& pc)
                {
                    HierarchyUtil::propagateTransformDirty(world.registry, e);
                });

            world.registry.view<LocalTransformDirty, RigidBody, ChildComponent, Transform>().each(
                [&](entt::entity e, RigidBody& rb, ChildComponent& c, Transform& t)
                {
                    if (!world.registry.valid(c.parent))
                        return;
                        
                    HierarchyUtil::forceRecomputeTransform(world.registry, e);
                    rb.actor->setGlobalPose(glm2px(t));
                });
        }
    }


    bool SimulationLoop::updateSimulation(float& interpAlpha, double timeScale,
                                          double deltaTime, bool physicsOnly)
    {
        ZoneScoped;
        bool ran = false;

        if (pauseSimulation) return false;

        // if (lockSimToRefresh.getInt() || disableSimInterp.getInt())
        // {
        //     registry.view<RigidBody, Transform>().each(
        //             [](RigidBody& dpa, Transform& transform)
        //             {
        //                 auto curr = dpa.actor->getGlobalPose();
        //
        //                 if (curr.p != glm2px(transform.position) || curr.q != glm2px(transform.rotation))
        //                 {
        //                     physx::PxTransform pt(glm2px(transform.position), glm2px(transform.rotation));
        //                     dpa.actor->setGlobalPose(pt);
        //                 }
        //             }
        //     );
        // }

        world.registry.view<PhysicsActor, Transform>().each(
                [](PhysicsActor& pa, Transform& transform)
                {
                    auto curr = pa.actor->getGlobalPose();
                    if (curr.p != glm2px(transform.position) || curr.q != glm2px(transform.rotation))
                    {
                        physx::PxTransform pt(glm2px(transform.position), glm2px(transform.rotation));
                        pa.actor->setGlobalPose(pt);
                    }
                }
        );

        if (!lockSimToRefresh.getInt())
        {
            simAccumulator += deltaTime;

            if (world.registry.view<RigidBody>().size() != currentState.size())
            {
                currentState.clear();
                previousState.clear();

                currentState.reserve(world.registry.view<RigidBody>().size());
                previousState.reserve(world.registry.view<RigidBody>().size());

                world.registry.view<RigidBody>().each(
                        [&](auto ent, RigidBody& dpa)
                        {
                            auto startTf = dpa.actor->getGlobalPose();
                            currentState.insert({ent, startTf});
                            previousState.insert({ent, startTf});
                        }
                );
            }

            while (simAccumulator >= simStepTime.getFloat())
            {
                ran = true;
                ZoneScopedN("Simulation step");
                previousState = currentState;
                simAccumulator -= simStepTime.getFloat();

                PerfTimer timer;

                doSimStep(simStepTime.getFloat() * timeScale, physicsOnly);

                double realTime = timer.stopGetMs() / 1000.0;

                // avoid spiral of death if simulation is taking too long
                if (realTime > simStepTime.getFloat())
                    simAccumulator = 0.0;
                
                world.registry.view<RigidBody>().each([&](auto ent, RigidBody& dpa)
                                        { currentState[ent] = dpa.actor->getGlobalPose(); });
            }


            float alpha = simAccumulator / simStepTime.getFloat();

            if (disableSimInterp.getInt())
                alpha = 1.0f;

            world.registry.view<RigidBody, Transform>().each(
                    [&](entt::entity ent, RigidBody& dpa, Transform& transform)
                    {
                        if (dpa.actor->isSleeping() && !(dpa.actor->getRigidBodyFlags() & physx::PxRigidBodyFlag::eKINEMATIC)) return;
                        world.registry.emplace_or_replace<LocalTransformDirty>(ent);
                        if (!previousState.contains(ent))
                        {
                            transform.position = px2glm(currentState[ent].p);
                            transform.rotation = px2glm(currentState[ent].q);
                        }
                        else
                        {
                            transform.position =
                                    glm::mix(px2glm(previousState[ent].p), px2glm(currentState[ent].p), (float)alpha);
                            transform.rotation =
                                    glm::slerp(px2glm(previousState[ent].q), px2glm(currentState[ent].q), (float)alpha);
                        }
                    }
            );

            world.registry.view<RigidBody, ChildComponent, Transform>().each(
                    [&](entt::entity ent, RigidBody& dpa, ChildComponent& cc, Transform& t)
                    {
                        if (dpa.actor->isSleeping() && !(dpa.actor->getRigidBodyFlags() & physx::PxRigidBodyFlag::eKINEMATIC)) return;
                        cc.offset = t.transformByInverse(world.registry.get<Transform>(cc.parent));
                    }
            );
            interpAlpha = alpha;
        }
        else
        {
            int numSubsteps = std::max((int)(deltaTime / simStepTime.getFloat()), 1);
            for (int i = 0; i < std::min(numSubsteps, 5); i++)
            {
                doSimStep(deltaTime / numSubsteps, physicsOnly);
            }
            ran = true;

            world.registry.view<RigidBody, Transform>().each(
                    [&](entt::entity, RigidBody& dpa, Transform& transform)
                    {
                        if (dpa.actor->isSleeping()) return;
                        physx::PxTransform pt = dpa.actor->getGlobalPose();
                        transform.position = px2glm(pt.p);
                        transform.rotation = px2glm(pt.q);
                    }
            );
            world.registry.view<RigidBody, ChildComponent, Transform>().each(
                    [&](entt::entity ent, RigidBody& dpa, ChildComponent& cc, Transform& t)
                    {
                        if (dpa.actor->isSleeping()) return;
                        cc.offset = t.transformByInverse(world.registry.get<Transform>(cc.parent));
                        world.registry.emplace_or_replace<LocalTransformDirty>(ent);
                    }
            );
        }

        return ran;
    }
}

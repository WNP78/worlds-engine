#include "EngineInternal.hpp"
#include "Window.hpp"
#include <Core/Log.hpp>
#include <physfs.h>
#include <Libs/IconsFontAwesome5.h>
#include <Libs/IconsFontaudio.h>
#define IMGUI_DEFINE_MATH_OPERATORS
#include <ImGui/imgui_internal.h>
#include <ImGui/imgui_impl_sdl.h>
#include <ImGui/imgui_freetype.h>
#include <fstream>
#include <nlohmann/json.hpp>
#include <SDL_filesystem.h>
#include <Util/StringBuilder.hpp>
#include <tracy/Tracy.hpp>

namespace worlds
{
    extern ImFont* boldFont;
    const char* fontNames[] =
    {
        "Nunito",
        "Golos",
        "Inter",
        "Hack"
    };
    LoadedFonts fonts[sizeof(fontNames) / sizeof(fontNames[0])] = { nullptr };
    int currentFont = 0;

    ImFont* addImGuiFont(std::string fontPath, float size, ImFontConfig* config, const ImWchar* ranges)
    {
        PHYSFS_File* ttfFile = PHYSFS_openRead(fontPath.c_str());
        if (ttfFile == nullptr)
        {
            logWarn("Couldn't open font file");
            return nullptr;
        }

        auto fileLength = PHYSFS_fileLength(ttfFile);

        if (fileLength == -1)
        {
            PHYSFS_close(ttfFile);
            logWarn("Couldn't determine size of editor font file");
            return nullptr;
        }

        void* buf = malloc(fileLength);
        auto readBytes = PHYSFS_readBytes(ttfFile, buf, fileLength);

        if (readBytes != fileLength)
        {
            PHYSFS_close(ttfFile);
            logWarn("Failed to read full TTF file");
            return nullptr;
        }

        PHYSFS_close(ttfFile);

        ImFontConfig defaultConfig{};
        defaultConfig.FontBuilderFlags = ImGuiFreeTypeBuilderFlags_NoAutoHint;

        if (config)
        {
            memcpy(config->Name, fontPath.c_str(), fontPath.size());
        }
        else
        {
            for (size_t i = 0; i < fontPath.size(); i++)
            {
                defaultConfig.Name[i] = fontPath[i];
            }
            config = &defaultConfig;
        }

        ImFont* font = ImGui::GetIO().Fonts->AddFontFromMemoryTTF(buf, (int)readBytes, size, config, ranges);
        logVrb("Loaded font %s", fontPath.c_str());
        return font;
    }

    static float dpiScale = 1.0f;

    LoadedFonts setupUIFonts(float dpiScale, const char* fontFolder)
    {
        std::string regular = "Fonts/" + std::string(fontFolder) + "/EditorFont.ttf";
        std::string bold = "Fonts/" + std::string(fontFolder) + "/EditorFont-bold.ttf";

        if (!PHYSFS_exists(regular.c_str()))
        {
            logErr("Couldn't find regular editor font for font folder %s", fontFolder);
            return LoadedFonts{ nullptr, nullptr };
        }

        if (!PHYSFS_exists(bold.c_str()))
        {
            logErr("Couldn't find bold editor font for font folder %s", fontFolder);
            return LoadedFonts{ nullptr, nullptr };
        }

        float fontSize = roundf(17.0f * dpiScale);
        ImFont* boldFont = addImGuiFont(bold, fontSize);
        ImFont* regularFont = addImGuiFont(regular, fontSize);

        float iconFontSize = fontSize * 2.0f / 3.0f;
        static const ImWchar iconRanges[] = {ICON_MIN_FA, ICON_MAX_FA, 0};
        ImFontConfig iconConfig{};
        iconConfig.MergeMode = true;
        iconConfig.GlyphMinAdvanceX = iconFontSize;
        iconConfig.PixelSnapH = true;
        iconConfig.FontBuilderFlags = ImGuiFreeTypeBuilderFlags_NoHinting;

        addImGuiFont("Fonts/" FONT_ICON_FILE_NAME_FAS, iconFontSize, &iconConfig, iconRanges);

        return LoadedFonts{ regularFont, boldFont };
    }

    LoadedFonts setupUIFonts(const char* fontFolder)
    {
        return setupUIFonts(dpiScale, fontFolder);
    }

    void loadDefaultUITheme();

    void initializeImGui(Window* mainWindow, bool isEditor, bool headless)
    {
        ZoneScoped;
        IMGUI_CHECKVERSION();
        ImGui::CreateContext();
        ImGuiIO& io = ImGui::GetIO();
        io.ConfigFlags |= ImGuiConfigFlags_DockingEnable | ImGuiConfigFlags_ViewportsEnable;
        io.IniFilename = nullptr;

        char* basePath = SDL_GetBasePath();
        if (basePath)
        {
            StackStringBuilder<8192> sb;
            sb.append(basePath);
            if (isEditor)
            {
                sb.append("imgui_editor.ini");
            }
            else
            {
                sb.append("imgui.ini");
            }
            ImGui::LoadIniSettingsFromDisk(sb.get());
            SDL_free((void*)basePath);
        }


        char* prefPath = SDL_GetPrefPath("Someone Somewhere", "Worlds Engine");
		StackStringBuilder<8192> layoutFilename;
        layoutFilename.append(prefPath);
        layoutFilename.append("layout.ini");
        io.IniFilename = strdup(layoutFilename.get());
        ImGui::LoadIniSettingsFromDisk(io.IniFilename);
        SDL_free(prefPath);
        ImGui::SaveIniSettingsToDisk(layoutFilename.get());

        if (!headless)
        {
            ImGui_ImplSDL2_InitForVulkan(mainWindow->getWrappedHandle());

			dpiScale = 1.0f;
            int dI = SDL_GetWindowDisplayIndex(mainWindow->getWrappedHandle());
            float ddpi, hdpi, vdpi;
            if (SDL_GetDisplayDPI(dI, &ddpi, &hdpi, &vdpi) != -1)
            {
                dpiScale = ddpi / 96.0f;
            }


#ifdef __ANDROID__
            dpiScale *= 0.6f;
            setupUIFonts(dpiScale);
            ImGui::GetStyle().ScaleAllSizes(dpiScale);
#else
			ImGui::GetIO().Fonts->Clear();
            int i = 0;
            for (const char* name : fontNames)
            {
                fonts[i] = setupUIFonts(name);
				i++;
            }
            ImGui::GetIO().FontDefault = fonts[0].regular;
            boldFont = fonts[0].bold;
#endif
            loadDefaultUITheme();
        }
    }
    
    struct AutoThemeColors
    {
        ImVec4 background;
        ImVec4 foreground;
        ImVec4 accent;
    };

    ImVec4 modifyHsl(ImVec4 inColor, float saturationMul, float valueMul)
    {
        float hue, saturation, value;
        ImGui::ColorConvertRGBtoHSV(inColor.x, inColor.y, inColor.z, hue, saturation, value);

        saturation *= saturationMul;
        value *= valueMul;
        
        if (value > 1.0f) value = 1.0f;
        if (saturation > 1.0f) saturation = 1.0f;

        ImVec4 outCol = inColor;
        ImGui::ColorConvertHSVtoRGB(hue, saturation, value, outCol.x, outCol.y, outCol.z);

        return outCol;
    }

    ImVec4 blendBg(ImVec4 accent, ImVec4 bg, float t)
    {
        ImVec4 result = bg + ((accent - bg) * ImVec4(t, t, t, t));
        result.w = 1.0f;
        return result;
    }

    float getLuminance(ImVec4 color)
    {
        // TODO: Improve
        return color.y;
    }

    void setupUiTheme(AutoThemeColors themeColors)
    {
        ImVec4 accent1 = blendBg(themeColors.accent, themeColors.background, 0.9f);
        ImVec4 accent2 = blendBg(themeColors.accent, themeColors.background, 0.75f);
        ImVec4 accent3 = blendBg(themeColors.accent, themeColors.background, 0.4f);
        ImVec4 accentFg = blendBg(themeColors.accent, themeColors.foreground, 0.5f);

        ImVec4* colors = ImGui::GetStyle().Colors;
        colors[ImGuiCol_Text] = themeColors.foreground;
        colors[ImGuiCol_TextDisabled] = modifyHsl(themeColors.foreground, 1.0f, 0.5f);
        colors[ImGuiCol_WindowBg] = themeColors.background;
        colors[ImGuiCol_ChildBg] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
        colors[ImGuiCol_PopupBg] = themeColors.background;
        colors[ImGuiCol_Border] = themeColors.accent;
        colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.50f);
        colors[ImGuiCol_FrameBg] = accent1;
        colors[ImGuiCol_FrameBgHovered] = themeColors.accent;
        colors[ImGuiCol_FrameBgActive] = accent2;
        colors[ImGuiCol_TitleBg] = themeColors.background;
        colors[ImGuiCol_TitleBgActive] = themeColors.background;
        colors[ImGuiCol_TitleBgCollapsed] = themeColors.background;
        colors[ImGuiCol_MenuBarBg] = themeColors.background;

        colors[ImGuiCol_ScrollbarBg] = themeColors.background;
        colors[ImGuiCol_ScrollbarGrab] = accent1;
        colors[ImGuiCol_ScrollbarGrabHovered] = themeColors.accent;
        colors[ImGuiCol_ScrollbarGrabActive] = accent2;

        colors[ImGuiCol_CheckMark] = accentFg;

        colors[ImGuiCol_SliderGrab] = accent3;
        colors[ImGuiCol_SliderGrabActive] = accentFg;
        colors[ImGuiCol_Button] = accent1;
        colors[ImGuiCol_ButtonHovered] = themeColors.accent;
        colors[ImGuiCol_ButtonActive] = accent2;

        colors[ImGuiCol_Header] = accent2;
        colors[ImGuiCol_HeaderHovered] = themeColors.accent;
        colors[ImGuiCol_HeaderActive] = accent3;
        colors[ImGuiCol_Separator] = accent1;
        colors[ImGuiCol_SeparatorHovered] = themeColors.accent;
        colors[ImGuiCol_SeparatorActive] = accent2;
        colors[ImGuiCol_ResizeGrip] = accent1;
        colors[ImGuiCol_ResizeGripHovered] = themeColors.accent;
        colors[ImGuiCol_ResizeGripActive] = accent2;

        colors[ImGuiCol_Tab] = accent3;
        colors[ImGuiCol_TabHovered] = accent2;
        colors[ImGuiCol_TabActive] = accent2;
        colors[ImGuiCol_TabUnfocused] = accent3;
        colors[ImGuiCol_TabUnfocusedActive] = themeColors.accent;
        colors[ImGuiCol_TabUnfocusedBorder] = themeColors.accent;

        colors[ImGuiCol_DockingPreview] = ImVec4(0.58f, 0.54f, 0.80f, 0.78f);
        colors[ImGuiCol_DockingEmptyBg] = ImVec4(0.12f, 0.11f, 0.14f, 1.00f);

        colors[ImGuiCol_PlotLines] = ImVec4(0.61f, 0.61f, 0.61f, 1.00f);
        colors[ImGuiCol_PlotLinesHovered] = ImVec4(1.00f, 0.43f, 0.35f, 1.00f);
        colors[ImGuiCol_PlotHistogram] = accentFg;
        colors[ImGuiCol_PlotHistogramHovered] = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);

        colors[ImGuiCol_TextSelectedBg] = modifyHsl(themeColors.accent, 0.8f, 2.0f);
        colors[ImGuiCol_DragDropTarget] = ImVec4(1.00f, 1.00f, 0.00f, 0.90f);

        colors[ImGuiCol_NavHighlight] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
        colors[ImGuiCol_NavWindowingHighlight] = ImVec4(1.00f, 1.00f, 1.00f, 0.70f);
        colors[ImGuiCol_NavWindowingDimBg] = ImVec4(0.80f, 0.80f, 0.80f, 0.20f);
        colors[ImGuiCol_ModalWindowDimBg] = ImVec4(0.80f, 0.80f, 0.80f, 0.35f);

        ImGuiStyle& style = ImGui::GetStyle();
        style.WindowBorderSize = 1.0f;
        style.PopupBorderSize = 1.0f;
        style.FrameRounding = 6.0f;
        style.PopupRounding = 6.0f;
        style.WindowRounding = 6.0f;
        style.ScrollbarRounding = 3.0f;
        style.GrabRounding = 2.0f;
        style.ChildBorderSize = 0.0f;
        style.TabBorderSize = 1.0f;
        style.ItemInnerSpacing = ImVec2(8, 8);
    }

    AutoThemeColors themeColors
    {
        .background = ImVec4(0.06f, 0.03f, 0.11f, 1.00f),
        .foreground = ImVec4(1.00f, 1.00f, 1.00f, 1.00f),
        .accent = ImVec4(0.38f, 0.21f, 0.62f, 1.00f)
    };

    AutoThemeColors themes[] =
    {
        // Default
        {
            .background = ImVec4(0.06f, 0.03f, 0.11f, 1.00f),
            .foreground = ImVec4(1.00f, 1.00f, 1.00f, 1.00f),
            .accent = ImVec4(0.38f, 0.21f, 0.62f, 1.00f)
        },
        // Pastel
        {
            .background = ImVec4(1.00f, 0.96f, 0.99f, 1.00f),
            .foreground = ImVec4(0.00f, 0.00f, 0.00f, 1.00f),
            .accent = ImVec4(1.00f, 0.75f, 0.95f, 1.00f)
        },
        // Matrix
        {
            .background = ImVec4(0.00f, 0.00f, 0.00f, 1.00f),
            .foreground = ImVec4(0.00f, 1.00f, 0.16f, 1.00f),
            .accent = ImVec4(0.02f, 0.35f, 0.00f, 1.00f)
        },
        // Red
        {
            .background = ImVec4(0.03f, 0.03f, 0.11f, 1.00f),
            .foreground = ImVec4(1.00f, 1.00f, 1.00f, 1.00f),
            .accent = ImVec4(0.62f, 0.21f, 0.32f, 1.00f)
        },
        // Blue
		{
			.background = ImVec4(0.11f, 0.13f, 0.25f, 1.00f),
			.foreground = ImVec4(0.97f, 0.97f, 0.95f, 1.00f),
			.accent = ImVec4(0.18f, 0.27f, 0.86f, 1.00f)
		},
        // MonoDark
        {
            .background = ImVec4(0.0f, 0.0f, 0.0f, 1.0f),
            .foreground = ImVec4(1.0f, 1.0f, 1.0f, 1.0f),
            .accent = ImVec4(0.3f, 0.3f, 0.3f, 1.0f)
        },
        // MonoLight
        {
            .background = ImVec4(1.0f, 1.0f, 1.0f, 1.0f),
            .foreground = ImVec4(0.0f, 0.0f, 0.0f, 1.0f),
            .accent = ImVec4(0.7f, 0.7f, 0.7f, 1.0f)
		}
    };

    const char* themeNames[] =
    {
        "Default",
        "Pastel",
        "Matrix",
        "Red",
        "Blue",
        "MonoDark",
        "MonoLight"
    };

    int currentTheme = 0;
    bool usingCustomTheme = false;

    nlohmann::json jsonArrayFromColor(ImVec4 color)
    {
        return { color.x, color.y, color.z };
    }

    nlohmann::json serializeThemeSettings()
    {
        if (usingCustomTheme)
        {
            return
            {
                { 
                    "customTheme", 
					{
                        { "accent", jsonArrayFromColor(themeColors.accent) },
						{ "foreground", jsonArrayFromColor(themeColors.foreground) },
                        { "background", jsonArrayFromColor(themeColors.background) }
					}
                }
            };
        }
        else
        {
            return
            {
                { "themeIndex", currentTheme }
            };
        }
    }

    void editThemeColors()
    {
        ImVec4 accentFg = blendBg(themeColors.accent, themeColors.foreground, 0.5f);
        ImVec4 accent1 = blendBg(themeColors.accent, themeColors.background, 0.9f);
        ImVec4 accent2 = blendBg(themeColors.accent, themeColors.background, 0.75f);
        ImVec4 accent3 = blendBg(themeColors.accent, themeColors.background, 0.4f);

        float barWidth = ImGui::GetWindowContentRegionWidth() / 7.0f;
        auto drawList = ImGui::GetWindowDrawList();
        ImVec2 size(barWidth, 30.0f);

        ImVec2 startPos = ImGui::GetCursorPos();
        drawList->AddRectFilled(ImGui::GetCursorScreenPos(), ImGui::GetCursorScreenPos() + size, ImColor(themeColors.foreground));
        ImGui::SetCursorPos(ImGui::GetCursorPos() + ImVec2(barWidth, 0.0f));
        drawList->AddRectFilled(ImGui::GetCursorScreenPos(), ImGui::GetCursorScreenPos() + size, ImColor(accentFg));
        ImGui::SetCursorPos(ImGui::GetCursorPos() + ImVec2(barWidth, 0.0f));
        drawList->AddRectFilled(ImGui::GetCursorScreenPos(), ImGui::GetCursorScreenPos() + size, ImColor(themeColors.accent));
        ImGui::SetCursorPos(ImGui::GetCursorPos() + ImVec2(barWidth, 0.0f));
        drawList->AddRectFilled(ImGui::GetCursorScreenPos(), ImGui::GetCursorScreenPos() + size, ImColor(accent1));
        ImGui::SetCursorPos(ImGui::GetCursorPos() + ImVec2(barWidth, 0.0f));
        drawList->AddRectFilled(ImGui::GetCursorScreenPos(), ImGui::GetCursorScreenPos() + size, ImColor(accent2));
        ImGui::SetCursorPos(ImGui::GetCursorPos() + ImVec2(barWidth, 0.0f));
        drawList->AddRectFilled(ImGui::GetCursorScreenPos(), ImGui::GetCursorScreenPos() + size, ImColor(accent3));
        ImGui::SetCursorPos(ImGui::GetCursorPos() + ImVec2(barWidth, 0.0f));
        drawList->AddRectFilled(ImGui::GetCursorScreenPos(), ImGui::GetCursorScreenPos() + size, ImColor(themeColors.background));
        ImGui::SetCursorPos(startPos + ImVec2(0, 30.0f + ImGui::GetStyle().ItemSpacing.y));

		usingCustomTheme |= ImGui::ColorEdit3("Background", &themeColors.background.x);
		usingCustomTheme |= ImGui::ColorEdit3("Foreground", &themeColors.foreground.x);
		usingCustomTheme |= ImGui::ColorEdit3("Accent", &themeColors.accent.x);

        if (ImGui::Button("Copy"))
        {
			ImGui::LogToClipboard();
            ImGui::LogText("AutoThemeColors themeColors\n");
            ImGui::LogText("{\n");
			ImGui::LogText("    .background = ImVec4(%.2ff, %.2ff, %.2ff, %.2ff),\n", themeColors.background.x, themeColors.background.y, themeColors.background.z, themeColors.background.w);
			ImGui::LogText("    .foreground = ImVec4(%.2ff, %.2ff, %.2ff, %.2ff),\n", themeColors.foreground.x, themeColors.foreground.y, themeColors.foreground.z, themeColors.foreground.w);
			ImGui::LogText("    .accent = ImVec4(%.2ff, %.2ff, %.2ff, %.2ff)\n", themeColors.accent.x, themeColors.accent.y, themeColors.accent.z, themeColors.accent.w);
            ImGui::LogText("};\n");
			ImGui::LogFinish();
        }

        ImGui::SameLine();

        if (ImGui::Button("Save"))
        {
			char* prefPath = SDL_GetPrefPath("Someone Somewhere", "Worlds Engine");
			std::ofstream themePrefsStream(prefPath + std::string{"themePrefs.json"});

            themePrefsStream << serializeThemeSettings();
        }

        if (ImGui::BeginCombo("Theme", usingCustomTheme ? "Custom" : themeNames[currentTheme]))
        {
            int i = 0;
            for (const char* name : themeNames)
            {
                bool isSelected = currentTheme == i;
                if (ImGui::Selectable(name, &isSelected))
                {
                    currentTheme = i;
                    themeColors = themes[i];
                    usingCustomTheme = false;
                }

                if (isSelected)
                    ImGui::SetItemDefaultFocus();

                i++;
            }
            ImGui::EndCombo();
        }

        setupUiTheme(themeColors);

        if (ImGui::BeginCombo("Font", fontNames[currentFont]))
        {
            int i = 0;
            for (const char* name : fontNames)
            {
                bool isSelected = currentFont == i;
                if (ImGui::Selectable(name, &isSelected))
                {
                    currentFont = i;
                    ImGui::GetIO().FontDefault = fonts[i].regular;
                    boldFont = fonts[i].bold;
                }

                if (isSelected)
                    ImGui::SetItemDefaultFocus();

                i++;
            }
            ImGui::EndCombo();
        }

        static bool showStyleEditor = false;

        ImGui::Checkbox("Show Advanced Editor", &showStyleEditor);

        if (showStyleEditor)
			ImGui::ShowStyleEditor();
    }

    ImVec4 getColorFromJsonArray(nlohmann::json arr)
    {
        return ImVec4(arr[0], arr[1], arr[2], 1.0f);
    }


    void loadDefaultUITheme()
    {
		char* prefPath = SDL_GetPrefPath("Someone Somewhere", "Worlds Engine");
		std::ifstream themePrefsStream(prefPath + std::string{"themePrefs.json"});
        if (themePrefsStream.good())
        {
            nlohmann::json pj = nlohmann::json::parse(themePrefsStream);

            themeColors.accent.w = 1.0f;
            themeColors.background.w = 1.0f;
            themeColors.foreground.w = 1.0f;

            if (pj.contains("customTheme"))
            {
                usingCustomTheme = true;
                auto customTheme = pj["customTheme"];

                themeColors.accent = getColorFromJsonArray(customTheme["accent"]);
                themeColors.foreground = getColorFromJsonArray(customTheme["foreground"]);
                themeColors.background = getColorFromJsonArray(customTheme["background"]);
            }
            else
            {
                int themeIndex = pj["themeIndex"];

                if (themeIndex < 0 || themeIndex >= sizeof(themes) / sizeof(themes[0]))
                {
                    logWarn("Invalid theme index in settings");
                    themeIndex = 0;
                }

                currentTheme = pj["themeIndex"];
                themeColors = themes[pj["themeIndex"]];
            }
        }

        setupUiTheme(themeColors);
    }

}
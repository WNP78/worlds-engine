#include "HierarchyUtil.hpp"
#include <Core/Log.hpp>
#include <Core/WorldComponents.hpp>
#include <assert.h>
#include <entt/entity/registry.hpp>

namespace worlds
{
    bool HierarchyUtil::isEntityDescendantOf(entt::registry& reg, entt::entity object, entt::entity parent)
    {
        ParentComponent* pc = reg.try_get<ParentComponent>(parent);

        if (pc == nullptr)
            return false;

        entt::entity currentChild = pc->firstChild;
        while (currentChild != entt::null)
        {
            if (currentChild == object)
                return true;
            ChildComponent& cc = reg.get<ChildComponent>(currentChild);

            if (isEntityDescendantOf(reg, object, currentChild))
                return true;

            currentChild = cc.nextChild;
        }

        return false;
    }

    bool HierarchyUtil::isEntityDirectChildOf(entt::registry& reg, entt::entity object, entt::entity parent)
    {
        ParentComponent* pc = reg.try_get<ParentComponent>(parent);

        if (pc == nullptr)
            return false;

        entt::entity currentChild = pc->firstChild;
        while (currentChild != entt::null)
        {
            if (currentChild == object)
                return true;
            
            ChildComponent& cc = reg.get<ChildComponent>(currentChild);
            currentChild = cc.nextChild;
        }

        return false;
    }

    void HierarchyUtil::setEntityParent(entt::registry& reg, entt::entity object, entt::entity parent)
    {
        assert(object != parent);
        if (reg.has<ChildComponent>(object))
        {
            removeEntityParent(reg, object);
        }

        if (!reg.valid(parent))
        {
            if (parent != entt::null)
                logWarn("setEntityParent called with invalid parent");
            return;
        }

        Transform parentTransform = reg.get<Transform>(parent);

        auto& childComponent = reg.emplace<ChildComponent>(object);
        childComponent.offset = reg.get<Transform>(object).transformByInverse(parentTransform);
        childComponent.offset.scale = reg.get<Transform>(object).scale;
        childComponent.parent = parent;

        if (reg.has<ParentComponent>(parent))
        {
            auto& parentComponent = reg.get<ParentComponent>(parent);
            childComponent.nextChild = parentComponent.firstChild;

            auto& oldChildComponent = reg.get<ChildComponent>(parentComponent.firstChild);
            oldChildComponent.prevChild = object;

            parentComponent.firstChild = object;
        }
        else
        {
            reg.emplace<ParentComponent>(parent, object);
        }
    }

    void HierarchyUtil::removeEntityParent(entt::registry& reg, entt::entity object, bool removeChildComponent)
    {
        assert(reg.has<ChildComponent>(object));
        assert(reg.valid(object));

        auto& childComponent = reg.get<ChildComponent>(object);

        entt::entity parent = childComponent.parent;
        assert(reg.valid(parent));
        childComponent.parent = entt::null;

        if (!reg.has<ParentComponent>(parent))
            return;

        auto& parentComponent = reg.get<ParentComponent>(parent);

        if (childComponent.nextChild == entt::null && childComponent.prevChild == entt::null)
        {
            // the parent has no other children
            // it is no longer a parent.
            // set it to null before removing the parent component so it doesn't destroy the child
            parentComponent.firstChild = entt::null;
            reg.remove<ParentComponent>(parent);

            if (removeChildComponent)
                reg.remove<ChildComponent>(object);

            return;
        }

        if (parentComponent.firstChild == object)
        {
            parentComponent.firstChild = childComponent.nextChild;
        }

        if (childComponent.nextChild != entt::null)
        {
            auto& nextChildComponent = reg.get<ChildComponent>(childComponent.nextChild);
            nextChildComponent.prevChild = childComponent.prevChild;
        }

        if (childComponent.prevChild != entt::null)
        {
            auto& prevChildComponent = reg.get<ChildComponent>(childComponent.prevChild);
            prevChildComponent.nextChild = childComponent.nextChild;
        }

        if (removeChildComponent)
            reg.remove<ChildComponent>(object);
    }

    entt::entity HierarchyUtil::getParentScene(entt::registry& registry, entt::entity entity)
    {
        entt::entity currentEnt = entity;
        while (registry.valid(currentEnt))
        {
            if (registry.has<SceneRootComponent>(currentEnt)) return currentEnt;
            auto* cc = registry.try_get<ChildComponent>(currentEnt);
            if (cc == nullptr) return entt::null;
            currentEnt = cc->parent;
        }

        return currentEnt;
    }

    Transform computeTransform(entt::registry& reg, entt::entity ent)
    {
        auto* cc = reg.try_get<ChildComponent>(ent);

        if (cc == nullptr)
        {
            return reg.get<Transform>(ent);
        }

        return cc->offset.transformBy(computeTransform(reg, cc->parent));
    }

    void HierarchyUtil::forceRecomputeTransform(entt::registry& registry, entt::entity entity)
    {
        if (!registry.has<ChildComponent>(entity)) return;
        registry.emplace_or_replace<Transform>(entity, computeTransform(registry, entity));
    }
    
    void HierarchyUtil::addAllChildrenToList(slib::List<entt::entity>& list, entt::registry& reg, entt::entity obj)
    {
        if (!reg.has<ParentComponent>(obj)) return;
        auto& pc = reg.get<ParentComponent>(obj);
        entt::entity currentEntity = pc.firstChild;
        
        while (currentEntity != entt::null)
        {
            list.add(currentEntity);
            
            if (reg.has<ParentComponent>(currentEntity))
            {
                addAllChildrenToList(list, reg, currentEntity);
            }
            
            auto& cc = reg.get<ChildComponent>(currentEntity);
            currentEntity = cc.nextChild;
        }
    }
    
    slib::List<entt::entity> HierarchyUtil::getAllChildEntities(entt::registry& registry, entt::entity root)
    {
        slib::List<entt::entity> list;
        addAllChildrenToList(list, registry, root);
        return list;
    }
    
    void HierarchyUtil::propagateTransformDirty(entt::registry& registry, entt::entity e)
    {
        registry.emplace_or_replace<LocalTransformDirty>(e);
        auto* pc = registry.try_get<ParentComponent>(e);
        
        if (pc == nullptr) return;

        entt::entity currentChild = pc->firstChild;

        while (registry.valid(currentChild))
        {
            propagateTransformDirty(registry, currentChild);
            currentChild = registry.get<ChildComponent>(currentChild).nextChild;
        }
    }
}

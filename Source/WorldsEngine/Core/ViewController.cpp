#include "ViewController.hpp"
#include <Core/Engine.hpp>
#include <Core/Window.hpp>
#include <Render/Render.hpp>
#include <ImGui/imgui.h>
#include <VR/OpenXRInterface.hpp>

namespace worlds
{
    float resScale = 1.0f;
    ViewController::ViewController(MainViewSettings settings)
        : xrEnabled(settings.xrEnabled)
        , boundWindow(settings.boundWindow)
    {
        RTTPassSettings passSettings
        {
            .cam = g_mainCamera,
            .enableShadows = true,
            .msaaLevel = 4,
            .numViews = settings.xrEnabled ? 2 : 1,
            .renderDebugShapes = false,
            .outputToXR = settings.xrEnabled,
            .setViewsFromXR = settings.xrEnabled
        };
        getViewResolution(&passSettings.width, &passSettings.height);

        passSettings.width *= resScale;
        passSettings.height *= resScale;

        rttPass = g_renderer->createRTTPass(passSettings);
        rttPass->active = true;
    }

    ViewController::~ViewController()
    {
        g_renderer->destroyRTTPass(rttPass);
    }

    void ViewController::draw()
    {
        resizeIfNecessary();

        ImDrawList* drawList = ImGui::GetBackgroundDrawList();
        ImTextureID id = rttPass->getUITextureID();
        drawList->AddImage(id, ImVec2(0, 0), ImVec2(rttPass->width / resScale, rttPass->height / resScale));
    }

    void ViewController::getViewResolution(uint32_t* width, uint32_t* height)
    {
        if (!xrEnabled)
        {
            int iWidth, iHeight;
            boundWindow->getSize(&iWidth, &iHeight);
            *width = iWidth;
            *height = iHeight;
        }
        else
        {
#ifdef ENABLE_OPENXR
            g_vrInterface->getRenderResolution(width, height);
#endif
        }
    }

    void ViewController::resizeIfNecessary()
    {
        uint32_t w, h;
        getViewResolution(&w, &h);
        w *= resScale;
        h *= resScale;

        if (w != rttPass->width || h != rttPass->height)
        {
            rttPass->resize(w, h);
        }
    }
}
#include <Core/EngineInternal.hpp>
#include <Core/Console.hpp>
#include <Core/Engine.hpp>
#include <Util/CircularBuffer.hpp>
#include <Libs/IconsFontAwesome5.h>
#include <Render/Render.hpp>
#include <Physics/Physics.hpp>
#include <Editor/GuiUtil.hpp>
#include <entt/entity/registry.hpp>

namespace worlds
{
    ConVar showDebugInfo{"showDebugInfo", "0", "Shows the debug info window"};
    ConVar printDebugInfo{"printDebugInfo", "0"};

    float printTimer = 0.0f;

    void drawDebugInfoWindow(entt::registry& reg, DebugTimeInfo timeInfo)
    {
        Renderer* renderer = g_renderer;
        if (printDebugInfo)
        {
            printTimer += timeInfo.deltaTime;
            if (printTimer >= 1.0f)
            {
                printTimer = 0.0f;
                const auto& dbgStats = renderer->getDebugStats();
                float gpuTime = renderer->getLastGPUTime() / 1000.0f / 1000.0f;
                float skin = dbgStats.skinningTime / 1000.0f / 1000.0f;
                float depth = dbgStats.depthPassTime / 1000.0f / 1000.0f;
                float lcull = dbgStats.lightCullTime / 1000.0f / 1000.0f;
                float main = dbgStats.mainPassTime / 1000.0f / 1000.0f;
                float tonemap = dbgStats.tonemapTime / 1000.0f / 1000.0f;
                logMsg("CPU cmdbuf time: %.3fms, GPU time %.3fs", dbgStats.cmdBufWriteTime, gpuTime);
                logMsg("breakdown: %.3fms skin, %.3fms depth, %.3fms lcull, %.3fms main, %.3fms tonemap",
                       skin, depth, lcull, main, tonemap);
                float sum = skin + depth + lcull + main + tonemap;
                logMsg("unaccounted: %.3fms", gpuTime - sum);
            }
        }
        if (showDebugInfo.getInt())
        {
            bool open = true;
            if (ImGui::Begin("Info", &open))
            {
                static CircularBuffer<float, 128> historicalFrametimes;
                static CircularBuffer<float, 128> historicalUpdateTimes;
                static CircularBuffer<float, 128> historicalPhysicsTimes;

                static CircularBuffer<float, 128> historicalRenderTimes;
                static CircularBuffer<float, 128> historicalGpuTimes;

                historicalFrametimes.add(timeInfo.deltaTime * 1000.0f);
                historicalUpdateTimes.add(timeInfo.updateTime * 1000.0f - timeInfo.simTime);

                if (timeInfo.didSimRun)
                    historicalPhysicsTimes.add((float)timeInfo.simTime);

                const auto& dbgStats = renderer->getDebugStats();
                historicalRenderTimes.add((float)timeInfo.lastTickRendererTime);
                historicalGpuTimes.add(renderer->getLastGPUTime() / 1000.0f / 1000.0f);

                if (ImGui::CollapsingHeader(ICON_FA_CLOCK u8" Performance"))
                {
                    ImGui::PlotLines(
                            "Historical Frametimes",
                            historicalFrametimes.values,
                            historicalFrametimes.size(),
                            historicalFrametimes.idx,
                            nullptr,
                            0.0f,
                            FLT_MAX,
                            ImVec2(0.0f, 125.0f)
                    );

                    ImGui::PlotLines(
                            "Historical UpdateTimes",
                            historicalUpdateTimes.values,
                            historicalUpdateTimes.size(),
                            historicalUpdateTimes.idx,
                            nullptr,
                            0.0f,
                            FLT_MAX,
                            ImVec2(0.0f, 125.0f)
                    );

                    ImGui::PlotLines(
                            "Historical PhysicsTimes",
                            historicalPhysicsTimes.values,
                            historicalPhysicsTimes.size(),
                            historicalPhysicsTimes.idx,
                            nullptr,
                            0.0f,
                            FLT_MAX,
                            ImVec2(0.0f, 125.0f)
                    );

                    ImGui::Text("Frametime: %.3fms", timeInfo.deltaTime * 1000.0);
                    ImGui::Text("Update time: %.3fms", timeInfo.updateTime * 1000.0);

                    double highestUpdateTime = 0.0f;
                    for (int i = 0; i < 128; i++)
                    {
                        highestUpdateTime = glm::max((double)historicalUpdateTimes.values[i], highestUpdateTime);
                    }
                    ImGui::Text("Peak update time: %.3fms", highestUpdateTime);
                    ImGui::Text("Transforms updated: %i", timeInfo.updatedTransformCount);

                    ImGui::Text("Physics time: %.3fms", timeInfo.simTime);
                    ImGui::Text(
                            "Update time without physics: %.3fms", (timeInfo.updateTime * 1000.0) - timeInfo.simTime
                    );
                    ImGui::Text("Framerate: %.1ffps", 1.0 / timeInfo.deltaTime);
                }

                if (ImGui::CollapsingHeader(ICON_FA_BARS u8" Misc"))
                {
                    Camera& cam = *g_mainCamera;
                    ImGui::Text("Frame: %i", timeInfo.frameCounter);
                    ImGui::Text("Cam pos: %.3f, %.3f, %.3f", cam.position.x, cam.position.y, cam.position.z);
                    ImGui::Text("%zu live entities", reg.alive());
                }

                if (ImGui::CollapsingHeader(ICON_FA_PENCIL_ALT u8" Render Stats"))
                {
                    ImGui::PlotLines(
                            "Render times",
                            historicalRenderTimes.values,
                            historicalRenderTimes.size(),
                            historicalRenderTimes.idx,
                            nullptr,
                            0.0f,
                            FLT_MAX,
                            ImVec2(0.0f, 125.0f)
                    );

                    ImGui::PlotLines(
                            "GPU times",
                            historicalGpuTimes.values,
                            historicalGpuTimes.size(),
                            historicalGpuTimes.idx,
                            nullptr,
                            0.0f,
                            FLT_MAX,
                            ImVec2(0.0f, 125.0f)
                    );

                    ImGui::Text("Draw calls: %i", dbgStats.numDrawCalls);
                    ImGui::Text("%i pipeline switches", dbgStats.numPipelineSwitches);
                    ImGui::Text("Frustum culled objects: %i", dbgStats.numCulledObjs);
                    ImGui::Text("Active RTT passes: %i/%i", dbgStats.numActiveRTTPasses, dbgStats.numRTTPasses);
                    ImGui::Text("Active shadowmaps: %i", dbgStats.numActiveShadowmaps);
                    ImGui::Text(
                            "Time spent in renderer: %.3fms", timeInfo.lastTickRendererTime
                    );
                    ImGui::Text("- Writing command buffer: %.3fms", dbgStats.cmdBufWriteTime);
                    static float avgGpuTime = 0.0f;
                    float lastGpuTime = renderer->getLastGPUTime() / 1000.0f / 1000.0f;
                    float averageAlpha = 0.05f;
                    avgGpuTime = (averageAlpha * lastGpuTime) + (1.0 - averageAlpha) * avgGpuTime;
                    ImGui::Text("GPU render time: %.3fms", lastGpuTime);
                    ImGui::Text("Average GPU render time: %.3fms", avgGpuTime);
                    pushBoldFont();
                    ImGui::Text("The following statistics are for the last RTT pass only.");
                    ImGui::PopFont();
                    ImGui::Text("GPU skinning time: %.3fms", dbgStats.skinningTime / 1000.0f / 1000.0f);
                    ImGui::Text("GPU depth pass time: %.3fms", dbgStats.depthPassTime / 1000.0f / 1000.0f);
                    ImGui::Text("GPU light cull time: %.3fms", dbgStats.lightCullTime / 1000.0f / 1000.0f);
                    ImGui::Text("GPU main pass time: %.3fms", dbgStats.mainPassTime / 1000.0f / 1000.0f);
                    ImGui::Text("GPU bloom time: %.3fms", dbgStats.bloomTime / 1000.0f / 1000.0f);
                    ImGui::Text("GPU tonemap time: %.3fms", dbgStats.tonemapTime / 1000.0f / 1000.0f);
                    float sum = dbgStats.skinningTime + dbgStats.depthPassTime +
                            dbgStats.lightCullTime + dbgStats.mainPassTime + dbgStats.bloomTime +
                            dbgStats.tonemapTime;
                    ImGui::Text("Sum of above: %.3fms", sum / 1000.0f / 1000.0f);
                    ImGui::Text("Unaccounted (e.g. ImGui): %.3fms", lastGpuTime - (sum / 1000.0f / 1000.0f));
                    ImGui::Text("V-Sync status: %s", renderer->getVsync() ? "On" : "Off");
                    ImGui::Text("Triangles: %u", dbgStats.numTriangles);
                    ImGui::Text("Lights in view: %i", dbgStats.numLightsInView);
                    ImGui::Text("%i textures loaded", dbgStats.numTexturesLoaded);
                    ImGui::Text("%i materials loaded", dbgStats.numMaterialsLoaded);
                }

                if (ImGui::CollapsingHeader(ICON_FA_SHAPES u8" Physics Stats"))
                {
                    physx::PxScene* scene = g_physics->scene();
                    uint32_t nDynamic = scene->getNbActors(physx::PxActorTypeFlag::eRIGID_DYNAMIC);
                    uint32_t nStatic = scene->getNbActors(physx::PxActorTypeFlag::eRIGID_STATIC);
                    uint32_t nTotal = nDynamic + nStatic;

                    ImGui::Text("%u dynamic actors, %u static actors (%u total)", nDynamic, nStatic, nTotal);
                    uint32_t nConstraints = scene->getNbConstraints();
                    ImGui::Text("%u constraints", nConstraints);
                    uint32_t nShapes = g_physics->physics()->getNbShapes();
                    ImGui::Text("%u shapes", nShapes);
                    int numRigidbodies = 0;
                    int numActiveRigidbodies = 0;
                    reg.view<RigidBody>().each([&](RigidBody& rb)
                    {
                        numRigidbodies++;
                        if (!rb.actor->isSleeping()) numActiveRigidbodies++;
                    });
                    ImGui::Text("%i/%i rigidbodies active", numActiveRigidbodies, numRigidbodies);
                }
            }
            ImGui::End();

            if (!open)
            {
                showDebugInfo.setValue("0");
            }
        }
    }
}
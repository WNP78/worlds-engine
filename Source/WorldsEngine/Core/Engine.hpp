#pragma once
#include <stdint.h>
#include <string>
#include <vector>

#include <SDL_events.h>
#include <entt/entity/registry.hpp>
#include <glm/glm.hpp>

#include "AssetDB.hpp"
#include <Render/Camera.hpp>
#include <Util/UniquePtr.hpp>

namespace worlds
{
    extern glm::ivec2 windowSize;
    class Renderer;
    class AudioSystem;
    class InputManager;
    class Editor;
    class IScriptEngine;
    class OpenXRInterface;
    class RTTPass;
    class Console;
    class IGameEventHandler;
    class ISystem;
    class Window;
    class World;
    class PhysicsSystem;
    class ViewController;
    class UISystem;

    struct SceneInfo
    {
        std::string name;
        AssetID id;
    };

    struct EngineInitOptions
    {
        EngineInitOptions()
            : useEventThread(false), workerThreadOverride(-1), runAsEditor(false), enableVR(false),
              dedicatedServer(false), eventHandler(nullptr), gameName("Untitled")
        {
        }
        bool useEventThread;
        int workerThreadOverride;
        bool runAsEditor;
        bool enableVR;
        bool dedicatedServer;
        IGameEventHandler* eventHandler;
        const char* gameName;
    };

    struct SkySettings
    {
        AssetID skybox;
        float skyboxBoost;
    };

    struct PrefabInstanceComponent
    {
        AssetID prefab;
    };

    class EngineArguments
    {
    public:
        static void parseArguments(int argc, char** argv);
        static void addArgument(const char* arg, const char* value = nullptr);
        static bool hasArgument(const char* arg);
        static std::string_view argumentValue(const char* arg);
    };

    class WorldsEngine;
    extern WorldsEngine* g_engine;
    extern OpenXRInterface* g_vrInterface;
    extern Renderer* g_renderer;
    extern Camera* g_mainCamera;
    extern InputManager* g_input;
    extern IScriptEngine* g_scriptEngine;
    extern PhysicsSystem* g_physics;
#ifdef BUILD_EDITOR
    extern Editor* g_editor;
#endif
    extern UISystem* g_ui;
    
    class SimulationLoop
    {
    public:
        SimulationLoop(IGameEventHandler* evtHandler, World& world);
        // returns true if the simulation actually ran
        bool updateSimulation(float& interpAlpha, double timeScale, double deltaTime,
                              bool physicsOnly);
    private:
        void doSimStep(float deltaTime, bool physicsOnly);
        double simAccumulator;
        World& world;
        IGameEventHandler* evtHandler;
    };

    struct SceneEntityOwnership
    {
        AssetID owningScene;
    };

    class World
    {
    public:
        World();
        void loadScene(AssetID scene);
        void clear();
        bool valid(entt::entity ent);
        entt::registry registry;
        SkySettings skySettings;
    };

    class WorldsEngine
    {
    public:
        WorldsEngine(EngineInitOptions initOptions, char* argv0);
        ~WorldsEngine();

        static void initTaskSched(int threads = -1);

        void run();
        void loadScene(AssetID scene);
        
        Window& getMainWindow() const { return *window; }
        
        void quit()
        {
            running = false;
        }
        
        bool pauseSim;
        bool runAsEditor;
        
        void destroyNextFrame(entt::entity ent)
        {
            this->nextFrameKillList.push_back(ent);
        }
        
        [[nodiscard]]
        double getGameTime() const
        {
            return gameTime;
        }
        
        double timeScale = 1.0;
    private:
        struct InterFrameInfo
        {
            uint64_t lastPerfCounter;
            double deltaTime;
            double lastUpdateTime;
            int frameCounter;
            double lastTickRendererTime;
        };

        static int eventFilter(void* enginePtr, SDL_Event* evt);
        static int windowThread(void* data);
        void setupSDL();
        Window* createWindow();
        void setupPhysfs(char* argv0, bool mountGameData);
        void tickRenderer(float deltaTime, bool renderImgui = false);
        void runSingleFrame(bool processEvents);

        Window* window;
        int windowWidth, windowHeight;

        bool running;
        bool headless;
        int workerThreadOverride;
        bool allowVR;
        World world;

        IGameEventHandler* evtHandler;
        ViewController* viewController;
        Camera cam;

        bool sceneLoadQueued = false;
        AssetID queuedSceneID;

        double gameTime = 0.0;

        UniquePtr<AudioSystem> audioSystem;
        UniquePtr<Console> console;
        UniquePtr<SimulationLoop> simLoop;

        std::vector<entt::entity> nextFrameKillList;

        InterFrameInfo interFrameInfo;
    };
}

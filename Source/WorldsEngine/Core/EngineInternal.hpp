#pragma once
#include <string>
#include <ImGui/imgui.h>
#include <entt/entity/lw_fwd.hpp>

namespace worlds
{
    struct DebugTimeInfo
    {
        double deltaTime;
        double updateTime;
        double simTime;
        bool didSimRun;
        double lastUpdateTime;
        double lastTickRendererTime;
        int frameCounter;
        int updatedTransformCount;
    };

    class Window;
    ImFont* addImGuiFont(std::string fontPath, float size, ImFontConfig* config = nullptr,
                         const ImWchar* ranges = nullptr);

    struct LoadedFonts
    {
        ImFont* regular;
        ImFont* bold;
    };

    LoadedFonts setupUIFonts(float dpiScale, const char* fontFolder);
    LoadedFonts setupUIFonts(const char* fontFolder);
    void initializeImGui(Window* mainWindow, bool isEditor, bool isHeadless);

    void drawDebugInfoWindow(entt::registry& reg, DebugTimeInfo debugTimeInfo);
}
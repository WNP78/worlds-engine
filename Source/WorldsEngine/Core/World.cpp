#include <Core/Engine.hpp>
#include <Core/WorldComponents.hpp>

namespace worlds
{
    World::World()
    {

    }

    bool World::valid(entt::entity ent)
    {
        return registry.valid(ent);
    }
    
    void World::clear()
    {
        std::vector<entt::entity> entitiesToClear;
        // Exclude child entities here because they'll be dealt with when the parent is destroyed
        registry.view<Transform>(entt::exclude_t<ChildComponent>{}).each([&](entt::entity e, Transform&) {
            entitiesToClear.push_back(e);
        });

        for (entt::entity e : entitiesToClear)
        {
            registry.destroy(e, 0);
        }
    }
}

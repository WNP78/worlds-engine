#pragma once
#include <entt/entity/lw_fwd.hpp>
#include <slib/List.hpp>

namespace worlds
{
    class HierarchyUtil
    {
    public:
        static bool isEntityDescendantOf(entt::registry& registry, entt::entity object, entt::entity parent);
        static bool isEntityDirectChildOf(entt::registry& registry, entt::entity object, entt::entity parent);
        static void setEntityParent(entt::registry& registry, entt::entity object, entt::entity parent);
        static void removeEntityParent(entt::registry& registry, entt::entity object, bool removeChildComponent = true);
        static entt::entity getParentScene(entt::registry& registry, entt::entity entity);
        static void forceRecomputeTransform(entt::registry& registry, entt::entity entity);
        static void addAllChildrenToList(slib::List<entt::entity>& list, entt::registry& reg, entt::entity root);
        static slib::List<entt::entity> getAllChildEntities(entt::registry& registry, entt::entity root);
        static void propagateTransformDirty(entt::registry& registry, entt::entity e);
    };
}

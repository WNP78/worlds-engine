#define _CRT_SECURE_NO_WARNINGS
#include <Core/Engine.hpp>
#include <Core/Window.hpp>
#include <Core/IGameEventHandler.hpp>
#include <Input/Input.hpp>
#include <Util/CreateModelObject.hpp>
#include <Core/Log.hpp>

#ifdef __ANDROID__
#include <SDL_main.h>
#include <VR/OpenXRInterface.hpp>
#else
#undef main
#include <Editor/ProjectAssetCompiler.hpp>
#include <Core/Console.hpp>
#endif

using namespace worlds;

// 32 bit systems are not supported!
// Make sure that we're 64 bit
static_assert(sizeof(void*) == 8);

#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#include <dbghelp.h>
#include <winuser.h>

// Taken from https://gist.github.com/statico/6809850727c708f08458
// Use discrete GPU by default.
extern "C"
{
    // http://developer.download.nvidia.com/devzone/devcenter/gamegraphics/files/OptimusRenderingPolicies.pdf
    __declspec(dllexport) DWORD NvOptimusEnablement = 0x00000001;

    // http://developer.amd.com/community/blog/2015/10/02/amd-enduro-system-for-developers/
    // https://gpuopen.com/amdpowerxpressrequesthighperformance/
    __declspec(dllexport) int AmdPowerXpressRequestHighPerformance = 1;
}


LONG unhandledExceptionHandler(LPEXCEPTION_POINTERS exceptionPtrs)
{
    BOOL hasSyms = SymInitialize(GetCurrentProcess(), NULL, TRUE);
    FILE *f = fopen("crash.txt", "w");
    fprintf(f, "hey, we're still in alpha ok?\n");
    fprintf(f, "(disregard if we are no longer in alpha)\n");
    auto record = exceptionPtrs->ExceptionRecord;
    fprintf(f, "exception code: %lu\n", record->ExceptionCode);
    fprintf(f, "occurred at address: 0x%zX", (uintptr_t)record->ExceptionAddress);
    if (hasSyms)
    {
        char symbolName[sizeof(SYMBOL_INFO) + sizeof(CHAR) * 1024];
        PSYMBOL_INFO symbolInfo = (PSYMBOL_INFO)&symbolName;
        symbolInfo->SizeOfStruct = sizeof(SYMBOL_INFO);
        symbolInfo->MaxNameLen = 1024;

        if (SymFromAddr(GetCurrentProcess(), (uintptr_t)record->ExceptionAddress, 0, symbolInfo))
        {
            fprintf(f, " (symbol %s)\n", symbolInfo->Name);
        }
        else
        {
            fprintf(f, " - SymFromAddr failed :(\n");
        }
    }
    else
    {
        fprintf(f, "(symbols disabled)\n");
    }
    
    if (record->ExceptionCode == EXCEPTION_ACCESS_VIOLATION)
    {
        fprintf(f, "btw, the access violation was a ");
        if (record->ExceptionInformation[0] == 1)
            fprintf(f, "write");
        else if (record->ExceptionInformation[0] == 0)
            fprintf(f, "read");
        else
            fprintf(f, "screwup so bad we can't even tell if it's read or write");
        
        fprintf(f, "\n");
        
        fprintf(f, "tried to access address 0x%zX", record->ExceptionInformation[1]);
        if (record->ExceptionInformation[1] == NULL)
        {
            fprintf(f, " (...come on guys, you're dereferencing a null pointer! open your eyes!)");
        }
        fprintf(f, "\n");
    }
    fclose(f);

    MINIDUMP_EXCEPTION_INFORMATION exceptionInfo;
    exceptionInfo.ThreadId = GetCurrentThreadId();
    exceptionInfo.ExceptionPointers = exceptionPtrs;
    exceptionInfo.ClientPointers = false;

    HANDLE dumpFile =
        CreateFileA("latest_crash.dmp", GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
    MiniDumpWriteDump(GetCurrentProcess(), GetCurrentProcessId(), dumpFile, MiniDumpNormal, &exceptionInfo, 0, 0);
    CloseHandle(dumpFile);
    return EXCEPTION_CONTINUE_SEARCH;
}
#endif

WorldsEngine* createEngine(EngineInitOptions& initOptions, char* argv0)
{
    return new WorldsEngine(initOptions, argv0);
}

int main(int argc, char **argv)
{
#ifdef _WIN32
    SetUnhandledExceptionFilter(unhandledExceptionHandler);
#ifndef __MINGW32__
    SetProcessDpiAwarenessContext(DPI_AWARENESS_CONTEXT_PER_MONITOR_AWARE_V2);
#endif
#endif

    EngineInitOptions initOptions;
    initOptions.gameName = "Lightline";

    EngineArguments::parseArguments(argc, argv);
#ifdef BUILD_EDITOR
    initOptions.runAsEditor = !EngineArguments::hasArgument("no-editor");
#else
    initOptions.runAsEditor = false;
#endif
    initOptions.enableVR = !EngineArguments::hasArgument("novr");
    initOptions.dedicatedServer = EngineArguments::hasArgument("dedicated-server");

#ifdef BUILD_EDITOR
    if (EngineArguments::hasArgument("build-project-android-textures"))
    {
        PHYSFS_init(argv[0]);
        WorldsEngine::initTaskSched();
        Console* console = new Console(true, false);
        console->getConVar("tc_astc")->setValue("1");
        std::string projectPath;
        projectPath.assign(EngineArguments::argumentValue("build-project-android-textures"));
        logMsg("Building all Android textures for %s", projectPath.c_str());
        worlds::GameProject project{projectPath};
        project.mountPaths();

        AssetCompilers::initialise();
        project.assets().enumerateAssets();

        int numTextures = 0;
        for (AssetFile& asset : project.assets().assetFiles)
        {
            if (EngineArguments::hasArgument("cubemaps-only"))
            {
                if (AssetDB::idToPath(asset.sourceAssetId).find("Cubemaps") == std::string_view::npos)
                    continue;
            }
            auto ext = AssetDB::getAssetExtension(asset.sourceAssetId);
            
            if (ext == ".wtexj")
            {
                numTextures++;
            }
        }
        logMsg("%i textures to compile", numTextures);

        int i = 0;
        for (AssetFile& asset : project.assets().assetFiles)
        {
            if (EngineArguments::hasArgument("cubemaps-only"))
            {
                if (AssetDB::idToPath(asset.sourceAssetId).find("Cubemaps") == std::string_view::npos)
                    continue;
            }
            
            auto ext = AssetDB::getAssetExtension(asset.sourceAssetId);
            
            if (ext == ".wtexj")
            {
                AssetCompileOperation* compileOp = AssetCompilers::buildAsset(project.root(), asset.sourceAssetId);

                while (!compileOp->complete)
                {
#ifdef _WIN32
                    Sleep(100);
#else
                    usleep(100 * 1000);
#endif
                }
                logMsg("%i/%i compiled (%i%%)", i, numTextures, i * 100 / numTextures);
                i++;
            }
        }
        logMsg("%i/%i compiled (%i%%)", i, numTextures, i * 100 / numTextures);
        project.unmountPaths();
        delete console;
        PHYSFS_deinit();
        return 0;
    }
#endif

    if (initOptions.dedicatedServer && (initOptions.enableVR || initOptions.runAsEditor))
    {
        fprintf(stderr, "%s: invalid arguments.\n", argv[0]);
        return -1;
    }

    initOptions.useEventThread = false;
#ifdef __ANDROID__
    initOptions.eventHandler = new TestEvtHandler;
#endif

    worlds::WorldsEngine* engine = createEngine(initOptions, argv[0]);
    engine->run();
    delete engine;

    return 0;
}

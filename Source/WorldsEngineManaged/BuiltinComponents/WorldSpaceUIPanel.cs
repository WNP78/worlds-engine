﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using WorldsEngine.ComponentMeta;
using WorldsEngine.ECS;
using WorldsEngine.Math;
using WorldsEngine.UI;

namespace WorldsEngine;

[StructLayout(LayoutKind.Sequential)]
internal struct NativeWorldSpaceUIPanel
{
    public int Width;
    public int Height;
    public float PixelSize;
}

public class WorldSpaceUIPanel : BuiltinComponent
{
    internal static ComponentMetadata Metadata
    {
        get
        {
            if (cachedMetadata == null)
                cachedMetadata = MetadataManager.FindNativeMetadata("WorldSpaceUIPanel")!;

            return cachedMetadata;
        }
    }

    private static ComponentMetadata? cachedMetadata;

    [DllImport(Engine.NativeModule)]
    [return: MarshalAs(UnmanagedType.I1)]
    private static extern bool worldSpaceUiPanel_raycastCursor(IntPtr regPtr, uint entityId, ref Transform t,
        Vector3 rayOrigin, Vector3 rayDirection, float maxDistance);

    [DllImport(Engine.NativeModule)]
    private static extern void worldSpaceUiPanel_click(IntPtr regPtr, uint entityId);
    
    [DllImport(Engine.NativeModule)]
    private static extern void worldSpaceUiPanel_mouseDown(IntPtr regPtr, uint entityId, int button);
    
    [DllImport(Engine.NativeModule)]
    private static extern void worldSpaceUiPanel_mouseUp(IntPtr regPtr, uint entityId, int button);
    
    [DllImport(Engine.NativeModule)]
    private static extern void worldSpaceUiPanel_mouseScroll(IntPtr regPtr, uint entityId, float delta);
    
    [DllImport(Engine.NativeModule)]
    private static extern unsafe void worldSpaceUiPanel_addEventListener(IntPtr regPtr, uint entityId, string elementId, string eventId, delegate* unmanaged[Cdecl]<IntPtr, void> funPtr, IntPtr dataArg);
    
    [DllImport(Engine.NativeModule)]
    private static extern unsafe void worldSpaceUiPanel_setLoadListener(IntPtr regPtr, uint entityId, delegate* unmanaged[Cdecl]<IntPtr, void> funPtr, IntPtr dataArg);
    
    [DllImport(Engine.NativeModule)]
    private static extern unsafe NativeWorldSpaceUIPanel* worldSpaceUiPanel_getNativePtr(IntPtr regPtr, uint entityId);
    
    [DllImport(Engine.NativeModule)]
    private static extern void worldSpaceUiPanel_reload(IntPtr regPtr, uint entityId);

    [DllImport(Engine.NativeModule)]
    private static extern IntPtr worldSpaceUiPanel_getElementById(IntPtr reg, uint entity, string id);

    private static readonly Dictionary<int, Action> _events = new();
    private static int _eventIdCounter = 0;

    [UnmanagedCallersOnly(CallConvs = new[] { typeof(CallConvCdecl) })]
    private static void EventListenerCallback(IntPtr dataArg)
    {
        if (!_events.ContainsKey((int)dataArg))
        {
            Log.Error("EventListenerCallback called with invalid data");
            return;
        }
        
        _events[(int)dataArg].Invoke();
    }

    public unsafe int Width
    {
        get => worldSpaceUiPanel_getNativePtr(regPtr, entityId)->Width;
        set => worldSpaceUiPanel_getNativePtr(regPtr, entityId)->Width = value;
    }
    
    public unsafe int Height
    {
        get => worldSpaceUiPanel_getNativePtr(regPtr, entityId)->Height;
        set => worldSpaceUiPanel_getNativePtr(regPtr, entityId)->Height = value;
    }
    
    public unsafe float PixelSize
    {
        get => worldSpaceUiPanel_getNativePtr(regPtr, entityId)->PixelSize;
        set => worldSpaceUiPanel_getNativePtr(regPtr, entityId)->PixelSize = value;
    }

    public event Action? OnLoad;
    
    internal unsafe WorldSpaceUIPanel(IntPtr regPtr, uint entityId) : base(regPtr, entityId)
    {
        _events.Add(_eventIdCounter, () =>
        {
            Log.Msg("Invoking OnLoad");
            OnLoad?.Invoke();
        });
        worldSpaceUiPanel_setLoadListener(regPtr, entityId, &EventListenerCallback, (IntPtr)_eventIdCounter);
        _eventIdCounter++;
    }

    public bool RaycastCursor(Vector3 rayOrigin, Vector3 rayDirection, float maxDistance)
    {
        Transform t = Registry.GetTransform(new Entity(entityId));
        return worldSpaceUiPanel_raycastCursor(regPtr, entityId, ref t, rayOrigin, rayDirection, maxDistance);
    }

    public void Click() => worldSpaceUiPanel_click(regPtr, entityId);

    public void MouseDown(int button) => worldSpaceUiPanel_mouseDown(regPtr, entityId, button);
    
    public void MouseUp(int button) => worldSpaceUiPanel_mouseUp(regPtr, entityId, button);

    public unsafe void AddEventListener(string elementId, string eventId, Action action)
    {
        _events.Add(_eventIdCounter, action);
        worldSpaceUiPanel_addEventListener(regPtr, entityId, elementId, eventId, &EventListenerCallback, (IntPtr)_eventIdCounter);
        _eventIdCounter++;
    }

    public void Reload() => worldSpaceUiPanel_reload(regPtr, entityId);

    public Element? GetElementById(string id)
    {
        var elPtr = worldSpaceUiPanel_getElementById(regPtr, entityId, id);
        if (elPtr != IntPtr.Zero)
            return new Element(elPtr);
        else
            return null;
    }
}
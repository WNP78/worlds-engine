﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace WorldsEngine
{
    public class Time
    {
        [DllImport(Engine.NativeModule)]
        private static extern double time_getScale();
        
        [DllImport(Engine.NativeModule)]
        private static extern void time_setScale(double scale);
        
        /// <summary>
        /// Time between last and current frame or simulation tick inside Update and Simulate respectively.
        /// </summary>
        public static float DeltaTime { get; internal set; }

        /// <summary>
        /// Game time passed since game/editor start.
        /// </summary>
        public static double CurrentTime { get; internal set; }
        
        /// <summary>
        /// 0-1 value used for interpolating between the most and second most recent simulation tick.
        /// </summary>
        public static float InterpolationAlpha { get; internal set; }

        public static double Scale
        {
            get => time_getScale();
            set => time_setScale(value);
        }
    }

    public class EngineEventAwaitable : INotifyCompletion
    {
        // Use two queues and swap so we don't end up in an infinite loop
        internal Queue<Action> _queuedContinuationsA = new();
        internal Queue<Action> _queuedContinuationsB = new();
        internal bool _useQueueB = false;

        internal Queue<Action> currentQueue => _useQueueB ? _queuedContinuationsB : _queuedContinuationsA;

        public bool IsCompleted => false;

        public void OnCompleted(Action continuation)
        {
            currentQueue.Enqueue(continuation);
        }

        public void GetResult() { }

        internal void Run()
        {
            Queue<Action> cq = currentQueue;
            _useQueueB = !_useQueueB;
            while (cq.Count > 0)
            {
                Action a = cq.Dequeue();
                a.Invoke();
            }

            cq.Clear();
        }

        internal void Clear()
        {
            currentQueue.Clear();
        }
    }

    public class AwaitableWrapper<T>
    {
        internal T Wrapped => _wrapped;
        private readonly T _wrapped;

        public AwaitableWrapper(T toWrap)
        {
            _wrapped = toWrap;
        }

        public T GetAwaiter() => _wrapped;
    }

    public static class Awaitables
    {
        public readonly static AwaitableWrapper<EngineEventAwaitable> NextFrame = new(new EngineEventAwaitable());
        public readonly static AwaitableWrapper<EngineEventAwaitable> NextSimulationTick = new(new EngineEventAwaitable());
    }
}

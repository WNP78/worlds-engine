﻿namespace WorldsEngine;

public interface IMidSimulateableComponent
{
    void OnMidSimulate();
}
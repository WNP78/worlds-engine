﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.Tracing;
using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Runtime.Serialization;
using System.Text.Json.Serialization;

namespace WorldsEngine.NativeInterop;

enum JsonType : byte
{
    Array = 91,
    Object = 123,
    Int64 = 108,
    Uint64 = 117,
    Double = 100,
    String = 34,
    Bool = 116,
    Null = 110
}

public static class NmJson
{
    [DllImport(Engine.NativeModule, CharSet = CharSet.Ansi)]
    [SuppressGCTransition]
    private static extern bool sj_contains(SimdJsonElement jv, string key);
    
    [DllImport(Engine.NativeModule, CharSet = CharSet.Ansi)]
    [SuppressGCTransition]
    private static extern SimdJsonElement sj_get(SimdJsonElement jv, string key);
    
    [DllImport(Engine.NativeModule, CharSet = CharSet.Ansi)]
    [SuppressGCTransition]
    private static extern int sj_getAsInt(SimdJsonElement jv);
    
    [DllImport(Engine.NativeModule, CharSet = CharSet.Ansi)]
    [SuppressGCTransition]
    private static extern bool sj_getAsBool(SimdJsonElement jv);
    
    [DllImport(Engine.NativeModule, CharSet = CharSet.Ansi)]
    [SuppressGCTransition]
    private static extern uint sj_getAsUint(SimdJsonElement jv);
    
    [DllImport(Engine.NativeModule, CharSet = CharSet.Ansi)]
    [SuppressGCTransition]
    private static extern float sj_getAsFloat(SimdJsonElement jv);
    
    [DllImport(Engine.NativeModule, CharSet = CharSet.Ansi)]
    [SuppressGCTransition]
    private static extern double sj_getAsDouble(SimdJsonElement jv);
    
    [DllImport(Engine.NativeModule, CharSet = CharSet.Ansi)]
    [SuppressGCTransition]
    private static extern IntPtr sj_getAsString(SimdJsonElement jv);

    [DllImport(Engine.NativeModule)]
    [SuppressGCTransition]
    private static extern JsonType sj_getType(SimdJsonElement jv);

    [DllImport(Engine.NativeModule)]
    [SuppressGCTransition]
    private static extern int sj_getArrayCount(SimdJsonElement jv);
    
    [DllImport(Engine.NativeModule)]
    [SuppressGCTransition]
    private static extern SimdJsonElement sj_getArrayElement(SimdJsonElement jv, int idx);

    [DllImport(Engine.NativeModule, CharSet = CharSet.Ansi)]
    [SuppressGCTransition]
    private static extern SimdJsonElement cdw_getElementFallback(ComponentDataWrapper cdw, string key);
    
    [DllImport(Engine.NativeModule, CharSet = CharSet.Ansi)]
    [SuppressGCTransition]
    private static extern bool cdw_contains(ComponentDataWrapper cdw, string key);
    
    [DllImport(Engine.NativeModule, CharSet = CharSet.Ansi)]
    [SuppressGCTransition]
    private static extern ComponentDataWrapper cdw_getObject(ComponentDataWrapper cdw, string key);
    
    [DllImport(Engine.NativeModule)]
    private static extern void worlds__free(IntPtr nativePtr);

    [StructLayout(LayoutKind.Sequential)]
    internal struct SimdJsonElement
    {
        private readonly IntPtr docPtr;
        private readonly nint tapeOffset;
        
        public JsonType Type => sj_getType(this);
        public int IntValue => sj_getAsInt(this);
        public uint UintValue => sj_getAsUint(this);
        public float FloatValue => sj_getAsFloat(this);
        public double DoubleValue => sj_getAsDouble(this);
        public bool BoolValue => sj_getAsBool(this);

        public string StringValue
        {
            get
            {
                IntPtr ptr = sj_getAsString(this);
                string str = Marshal.PtrToStringUTF8(ptr)!;
                return str;
            }
        }

        public bool IsNumber => Type == JsonType.Double || Type == JsonType.Int64 ||
                                Type == JsonType.Uint64;

        public int Count => sj_getArrayCount(this);

        public bool Contains(string key)
        {
            return sj_contains(this, key);
        }

        public SimdJsonElement this[string key] => sj_get(this, key);
        public SimdJsonElement this[int idx] => sj_getArrayElement(this, idx);
    }

    [StructLayout(LayoutKind.Sequential)]
    internal struct ComponentDataWrapper
    {
        private readonly SimdJsonElement instanceEl;
        private readonly SimdJsonElement prefabEl;
        [MarshalAs(UnmanagedType.I1)]
        private readonly bool isPrefab;

        public bool Contains(string key) => cdw_contains(this, key);
        public SimdJsonElement GetElementFallback(string key) => cdw_getElementFallback(this, key);
        public ComponentDataWrapper GetObject(string key) => cdw_getObject(this, key);
    }

    public static T Deserialize<T>(IntPtr nativePtr)
    {
        return (T)Deserialize(typeof(T), Marshal.PtrToStructure<SimdJsonElement>(nativePtr));
    }

    public static object Deserialize(Type t, IntPtr nativePtr)
    {
        return Deserialize(t, Marshal.PtrToStructure<SimdJsonElement>(nativePtr));
    }

    private static bool IsTypeJsonObject(Type t)
    {
        if (t == typeof(int))
        {
            return false;
        }
        else if (t == typeof(float))
        {
            return false;
        }
        else if (t == typeof(double))
        {
            return false;
        }
        else if (t == typeof(bool))
        {
            return false;
        }
        else if (t == typeof(string))
        {
            return false;
        }
        else if (t.IsEnum)
        {
            return false;
        }
        else if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(System.Collections.Generic.List<>))
        {
            return false;
        }
        else if (t.IsArray)
        {
            return false;
        }

        return true;
    }

    public static object DeserializeCDW(Type t, IntPtr cdwPtr)
    {
        var cdw = Marshal.PtrToStructure<ComponentDataWrapper>(cdwPtr);
        return DeserializeCDW(t, cdw);
    }
    
    private static object DeserializeCDW(Type t, ComponentDataWrapper cdw)
    {
        object? o = Activator.CreateInstance(t);
        if (o == null)
        {
            Log.Warn($"Deserializing type {t} using uninitialized object");
            o = RuntimeHelpers.GetUninitializedObject(t);
        }

        var fields = t.GetFields(BindingFlags.Public | BindingFlags.Instance);

        foreach (var fieldInfo in fields)
        {
            if (fieldInfo.GetCustomAttribute<NonSerializedAttribute>() != null || fieldInfo.GetCustomAttribute<JsonIgnoreAttribute>() != null)
            {
                continue;
            }
        
            if (cdw.Contains(fieldInfo.Name))
            {
                if (IsTypeJsonObject(fieldInfo.FieldType))
                {
                    fieldInfo.SetValue(o, DeserializeCDW(fieldInfo.FieldType, cdw.GetObject(fieldInfo.Name)));
                }
                else
                {
                    fieldInfo.SetValue(o, Deserialize(fieldInfo.FieldType, cdw.GetElementFallback(fieldInfo.Name)));
                }
            }
        }

        return o;
    }

    private static object Deserialize(Type t, SimdJsonElement jval)
    {
        if (jval.Type == JsonType.Null) return null;
    
        if (t == typeof(int))
        {
            if (!jval.IsNumber)
                throw new SerializationException($"Expected number, got {jval.Type}");
            return jval.IntValue;
        }
        else if (t == typeof(float))
        {
            if (!jval.IsNumber)
                throw new SerializationException($"Expected number, got {jval.Type}");
            return jval.FloatValue;
        }
        else if (t == typeof(double))
        {
            if (!jval.IsNumber)
                throw new SerializationException($"Expected number, got {jval.Type}");
            return jval.DoubleValue;
        }
        else if (t == typeof(bool))
        {
            if (jval.Type != JsonType.Bool)
                throw new SerializationException($"Expected boolean, got {jval.Type}");
            return jval.BoolValue;
        }
        else if (t == typeof(string))
        {
            if (jval.Type != JsonType.String)
                throw new SerializationException($"Expected string, got {jval.Type}");
            return jval.StringValue;
        }
        else if (t.IsEnum)
        {
            if (!jval.IsNumber)
                throw new SerializationException($"Expected Number_unsigned for enum, got {jval.Type}");
            return jval.IntValue;
        }
        else if (t.IsGenericType && t.GetGenericTypeDefinition() == typeof(System.Collections.Generic.List<>))
        {
            if (jval.Type != JsonType.Array)
                throw new SerializationException($"Expected array, got {jval.Type}");

            Type elementType = t.GetGenericArguments()[0];
            int count = jval.Count;
            IList? list = (IList?)Activator.CreateInstance(t, count);

            if (list == null)
                throw new InvalidOperationException("Failed to create list");
        
            for (int i = 0; i < count; i++)
            {
                list.Add(Deserialize(elementType, jval[i]));
            }

            return list;
        }
        else if (t.IsArray)
        {
            if (jval.Type != JsonType.Array)
                throw new SerializationException($"Expected array, got {jval.Type}");

            Type elementType = t.GetElementType()!;
            int count = jval.Count;
            var array = Array.CreateInstance(elementType, count);

            for (int i = 0; i < count; i++)
            {
                array.SetValue(Deserialize(elementType, jval[i]), i);
            }

            return array;
        }
    
        if (jval.Type != JsonType.Object)
            throw new SerializationException($"Expected object, got {jval.Type}");

        object? o = Activator.CreateInstance(t);
        if (o == null)
        {
            Log.Warn($"Deserializing type {t} using uninitialized object");
            o = RuntimeHelpers.GetUninitializedObject(t);
        }

        var fields = t.GetFields(BindingFlags.Public | BindingFlags.Instance);

        foreach (var fieldInfo in fields)
        {
            if (fieldInfo.GetCustomAttribute<NonSerializedAttribute>() != null || fieldInfo.GetCustomAttribute<JsonIgnoreAttribute>() != null)
            {
                continue;
            }
        
            if (jval.Contains(fieldInfo.Name))
            {
                fieldInfo.SetValue(o, Deserialize(fieldInfo.FieldType, jval[fieldInfo.Name]));
            }
        }

        return o;
    }
}
﻿using System;
using System.Runtime.InteropServices;

namespace WorldsEngine.NativeInterop;

internal static class Memory
{
    [DllImport(Engine.NativeModule)]
    private static extern void worlds__free(IntPtr ptr);

    public static void Free(IntPtr ptr) => worlds__free(ptr);
}
﻿using System;
using System.Runtime.InteropServices;
using System.Text;
using WorldsEngine.NativeInterop;

namespace WorldsEngine.UI;

public struct Element
{
    [DllImport(Engine.NativeModule)]
    private static extern void uiElement_setClass(IntPtr ptr, string elClass, [MarshalAs(UnmanagedType.I1)] bool active);

    [DllImport(Engine.NativeModule)]
    [return: MarshalAs(UnmanagedType.I1)]
    private static extern bool uiElement_isClassSet(IntPtr ptr, string elClass);

    [DllImport(Engine.NativeModule)]
    private static extern void uiElement_setStyleProperty(IntPtr el, string attr, string val);
    
    [DllImport(Engine.NativeModule)]
    private static extern void uiElement_setAttribute(IntPtr el, string attr, string val);

    [DllImport(Engine.NativeModule)]
    private static extern IntPtr uiElement_getAttribute(IntPtr el, string attr);
    
    [DllImport(Engine.NativeModule)]
    [return: MarshalAs(UnmanagedType.I1)]
    private static extern bool uiElement_hasAttribute(IntPtr el, string attr);

    [DllImport(Engine.NativeModule)]
    private static extern void uiElement_setInnerRml(IntPtr el, string rml);
    
    private readonly IntPtr _nativePtr;
    
    internal Element(IntPtr nativePtr)
    {
        _nativePtr = nativePtr;
    }

    public void SetClass(string className, bool active) => uiElement_setClass(_nativePtr, className, active);

    public bool IsClassSet(string className) => uiElement_isClassSet(_nativePtr, className);
    
    public void SetStyleProperty(string property, string value) => uiElement_setStyleProperty(_nativePtr, property, value);
    
    public void SetAttribute(string attribute, string value) => uiElement_setAttribute(_nativePtr, attribute, value);

    public string? GetAttribute(string attribute)
    {
        IntPtr stringPtr = uiElement_getAttribute(_nativePtr, attribute);
        string? val = Marshal.PtrToStringUTF8(stringPtr);
        Memory.Free(stringPtr);
        return val;
    }

    public bool HasAttribute(string attribute) => uiElement_hasAttribute(_nativePtr, attribute);

    public void SetInnerRml(string rml) => uiElement_setInnerRml(_nativePtr, rml);
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;
using System.Collections.Concurrent;
using ImGuiNET;

namespace WorldsEngine
{
    internal class EngineSynchronizationContext : SynchronizationContext
    {
        struct CallbackInvocation
        {
            public SendOrPostCallback Callback;
            public object? State;

            public CallbackInvocation(SendOrPostCallback callback, object? state)
            {
                Callback = callback;
                State = state;
            }
        }

        public bool Enabled = true;

        readonly ConcurrentQueue<CallbackInvocation> callbacks = new ConcurrentQueue<CallbackInvocation>();
        private readonly string _name;

        public EngineSynchronizationContext(string name)
        {
            _name = name;
        }

        public override void Post(SendOrPostCallback d, object? state)
        {
            //Log.Msg($"Callback {d.Method.DeclaringType.Name}.{d.Method.Name} posted to {_name}");
            if (!Enabled) return;
            
            callbacks.Enqueue(new CallbackInvocation(d, state));
        }

        public void RunCallbacks()
        {
            while (!callbacks.IsEmpty)
            {
                if (!callbacks.TryDequeue(out CallbackInvocation callback)) continue;

                callback.Callback.Invoke(callback.State);
            }
        }

        public void ClearCallbacks()
        {
            callbacks.Clear();
        }

        public void DisplayDebugInfo()
        {
            if (ImGui.Begin(_name))
            {
                ImGui.Text($"{callbacks.Count} callbacks queued");
            }

            ImGui.End();
        }
    }
}